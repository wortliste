#!/usr/bin/env python3
# -*- coding: utf8 -*-
# :Copyright: © 2016 Günter Milde.
#             Released without warranty under the terms of the
#             GNU General Public License (v. 2 or later)

# umformatierung.py: Wandlung zwischen Lang- und Kurzformat der Wortliste(n)
# ==========================================================================
# ::

u"""
Wandlung zwischen Lang- und Kurzformat der Wortliste(n)

Die Eingabedateien werden in eine Liste gelesen, gewandelt, sortiert
und in die Standardausgabe, eine oder mehrere Dateien geschrieben.

Das spezielle Argument '-' steht für die Standardeingabe::

     umformatierung.py -k - < wortliste > wlst

     umformatierung.py neu.todo wlst-* > wortliste

Mit der Option ``--split`` kann die Aufteilung in Einzeldateien erzwungen
werden. Beispiel::

     umformatierung.py -k langeintraege --split -o kurz_

erzeugt die Dateien ``kurz_a`` bis ``kurz_z``.
"""

# Abhängigkeiten::

import argparse, sys, os, re

# path for local Python modules
sys.path.insert(0, os.path.join(os.path.dirname(__file__), 'lib'))

from py_wortliste.wortliste import (filelines, WordEntry, ShortEntry,
                                  long2short, short2long, sortkey_duden)
from py_wortliste.split_wortliste import split_a_z, write_a_z


# Default-Aktion
# ==============
#
# ::

if __name__ == '__main__':


# Optionen::

    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('INFILES', nargs='*', default=['-'],
                        help=u'Eingabedatei(en), Default: "-" (Standardeingabe)')
    parser.add_argument('-k', '--long2short',
                      help=u'vom Langformat in das Kurzformat wandeln.',
                      action="store_true", default=False)
    parser.add_argument('-o', '--outfile',
                      help=u'Ausgangsdatei, Vorgabe: - (Standardausgabe)',
                      default='-')
    parser.add_argument('-s', '--split',
                      help=u'Aufteilen in Dateien "OUTFILEa" bis "OUTFILEz".'
                           u'Vorgabe: False (nicht splitten)',
                      action="store_true", default=False)
    parser.add_argument('-e', '--explicit', action='store_true', default=False,
                      help=u'Einträge nicht zusammenfassen.')
    parser.add_argument('-u', '--unsorted', action='store_true', default=False,
                      help=u'Überspringe die Sortierung.')

    args = parser.parse_args()

# Iterator über Eingabezeilen::

    lines = filelines(args.INFILES)

# Wandeln::

    if args.long2short is True:
        entries = long2short(lines, prune=not(args.explicit))
    else:
        entries = short2long(lines, prune=not(args.explicit))

# Sortieren und Ausgeben::

    lines = (str(entry)+'\n' for entry in entries)

    if args.split is True:
        lists = split_a_z(lines)
        if args.unsorted is not True:
            for l in lists.values():
                l.sort(key=sortkey_duden)
        write_a_z(lists, args.outfile)
    else:
        if args.unsorted is not True:
            entries.sort(key=sortkey_duden)

    if args.outfile == '-': # default
        outfile = sys.stdout
    else:
        outfile = open(args.outfile, 'w', encoding='utf-8')

    outfile.writelines(lines)
