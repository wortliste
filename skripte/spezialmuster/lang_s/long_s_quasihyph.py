#!/usr/bin/env python3
# :Copyright: © 2011 Günter Milde.
#             Released without warranty under the terms of the
#             GNU General Public License (v. 2 or later)

u"""Filter zum Wandeln von Wörtern mit langem S in
Pseudo-Trennbeispiele (ausſagen -> auss-agen, eſſen -> es-s-en)."""

import sys

words = (word.replace(u'ſ', u's-').replace(u'S', u's-')
         for word in sys.stdin)

sys.stdout.writelines(words)
