#!/usr/bin/env python3

# ===================================================================
# Langes oder rundes S: Automatische Konversion nach Silbentrennung
# ===================================================================
#
# :Copyright: © 2012, 2014, 2023 Günter Milde.
# :Licence:   This work may be distributed and/or modified under
#             the conditions of the `LaTeX Project Public License`,
#             either version 1.3 of this license or (at your option)
#             any later  version.
# :Version:   0.4 (2023-03-25)
#
# ::

"""
Erstelle eine Wortliste mit (weitgehend) korrekter lang-S Schreibung.

Regelbasierte Bestimmung der S-Schreibung auf Basis der Silbentrennung
in der `Wortliste der deutschsprachigen Trennmustermannschaft`.

Für die Wandlung mit Hilfe der patgen-generierten Muster siehe
``skripte/lib/py_patuse/``.
"""

# .. contents::
#
# Vorspann
# ========
#
# Lade Funktionen und Klassen für reguläre Ausdrücke::

import os, argparse, re, sys

# path for local Python modules (../../lib/py_wortliste/)
skriptdirname = os.path.dirname(os.path.dirname(os.path.dirname(
                                           os.path.abspath(__file__))))
sys.path.insert(0, os.path.join(skriptdirname, 'lib'))

from py_wortliste.wortliste import filelines, join_word, WordEntry, WordFile


# Trennzeichen
# ------------
#
# Die Trennzeichen der Wortliste sind
#
# == =====================================================================
# =  Haupttrennstellen and Wortfugen (End=spiel)
# \- einfache Trennstellen (Ho-se)
# <  Trennstellen nach Vorsilben (Ab<gang)
# >  Trennstellen vor Suffixen (lieb>lich)
# \· Notentext-Trennstellen am Wortrand und in Abkürzungen (A·bend)
# .  Schwankungsfälle (Mil-li.on)
# .  (nach anderem Zeichen) ungünstige Trennstellen (Ur<in<.stinkt)
# == =====================================================================
#
#
# Funktionen
# ==========
#
# s_ersetzen()
# ------------
#
# Lang-ſ-Regeln
#
# Siehe [wikipedia]_ und [Duden71]_ (Regeln K 44,45)::

def s_ersetzen(word):

# Ausnahmeregeln
# ~~~~~~~~~~~~~~
#
# Für sz und sk gelten Regeln, welche die Herkunft der Wörter beachten.
# Diese und weitere spezielle Fälle, welche Lang-S vor Trennstellen verlangen
# sind in der Liste `Ausnahmen Lang-S`_ gesammelt::

    for (ausnahme, ersetzung) in ausnahmen_lang_s:
        if ausnahme in word:
            word = word.replace(ausnahme, ersetzung)
            # print("ſ-Ausnahme", ersetzung, word, file=sys.stderr)

# Allgemeine Regeln
# ~~~~~~~~~~~~~~~~~
#
# ſ steht im Silbenanlaut::

    word = re.sub(r'(^|[-<>=·.])s([^<=])', r'\1ſ\2', word)

# ſ steht im Inlaut als stimmhaftes s zwischen Vokalen
# (gilt auch für ungetrenntes ss zwischen Selbstlauten, z.B. Hausse, Baisse)::

    word = re.sub(r'([AEIOUYÄÖÜaeiouäöüé])s([aeiouyäöüé])', r'\1ſ\2', word)
    word = re.sub(r'([AEIOUYÄÖÜaeiouäöüé])ss([aeiouyäöüé])', r'\1ſſ\2', word)


# Doppel-S statt ß
# ~~~~~~~~~~~~~~~~
#
# Wenn kein ß vorhanden ist (GROSSSCHREIBUNG) und in der Schweiz wird ss
# statt ß geschrieben.
#
# Bei Großschreibung ist keine s/ſ-Unterscheidung nötig.
#
# Für die Handhabung des Ersatz-Doppel-S in der Schweiz im Fraktursatz  sind
# keine Regeln  oder Belege bekannt.  Eine Stichprobennahme in den
# `E-Newspaper Archives`__ zeigt, dass in deutschsprachigen Zeitungen
# 'ß'  im Fraktursatz (und auch noch einige Jahre nach dem Wechsel
# zur  Antiquaschrift) genutzt wurde.
#
# __ https://www.e-newspaperarchives.ch
#
# Seit 1996 wird auch am Wort-/Silbenende und vor t nach
# kurzem Vokal ss geschrieben. Der "Reformduden" empfielt im Fraktursatz die
# Schreibung "ſs" (die auch vor 1901 in Gebrauch war).
#
# Wir übernehmen diese Schreibung am Wort-/Silbenende::

    word = re.sub('ss($|[-=<>.])', r'ſs\1', word)

# Vor t schreiben wir nach kurzem Vokal Doppel-ſ::

    word = word.replace('sst', 'ſſt')

# Nach langem Vokal steht auch in de-1996 ein ß, in de-x-GROSS ist keine
# ſ-Wandlung nötig/möglich.
# TODO: in der Schweizer Orthographie müßte nach langem Vokal oder Zwielaut
# auch vor t ein ſs stehen (beißt -> beiſst). Allerdings wurde im Fraktursatz
# auch in der Schweiz das ß verwendet.
#
# Verbindungen und Digraphen
# ~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# ſ steht in den Verbindungen sp, st, sch und in Digraphen::

    word = word.replace('st', 'ſt')
    word = word.replace('sp', 'ſp')
    word = word.replace('sch', 'ſch')
    word = word.replace('sch', 'ſ[ch')

    word = word.replace('Ps', 'Pſ')  # Ψ
    word = re.sub(r'^ps', r'pſ', word) # ψ (ps am Wortanfang)
    word = re.sub(r'([-<>=·.])ps', r'\1pſ', word) # ψ (ps am Silbenanfang)


# ſ vor Trennstellen
# ~~~~~~~~~~~~~~~~~~
#
# Die Verbindungen ss, sp, st werden zu ſſ, ſp und ſt, auch wenn sie
# durch eine Nebentrennstelle (Trennung innerhalb eines Wortbestandteiles)
# getrennt sind. Das s bleibt rund im Auslaut, d.h. am Wortende und vor
# einer Haupttrennstelle (Trennung an der Grenze zweier Wortbestandteile
# (Vorsilbe<Stamm, Bestimmungswort=Grundwort).
#
# ::

    word = re.sub(r's([-.]+)ſ([aeiouyäöüé])', r'ſ\1ſ\2', word)
    word = re.sub(r's([-.]+)p([^h])', r'ſ\1p\2', word)
    word = re.sub(r'(^|[^s])s([-.]+)t', r'\1ſ\2t', word) # Reformschreibung

# ſ wird auch geschrieben, wenn der S-Laut nur scheinbar im Auslaut steht,
# weil ein folgendes unbetontes "e" ausfällt:
#
# "s-l" (Baſ-ler, pinſ-le, Kapſ-lung, Wechſ-ler, wechſ-le, Rieſ-ling),
# aber nicht bei
#
# | M.s-l:    Mus-lim, Mos-lem, ...
# | [iys]s-l: Gris-ly, Crys-ler, ... (ss-l siehe nächste Regel)
# | s-la:     Bra-tis-la-va, Gos-lar, Bres-lau
#
# ::

    word = re.sub(r'([^mM][^siy])s-l([^a])', r'\1ſ-l\2', word)

# "ss-l", aber nicht bei "...eiss-l" und "Ess-lingen" mit ss statt ß::

    word = re.sub(r'([^iE])ss-l', r'\1ſſ-l', word) # Droſſ-lung, ...

# Für weitere Fälle siehe auch `Ausnahmen Lang-S`_ (bereits oben ersetzt).
#
#
# Fremdwörter und Eigennamen mit Schluss-ß
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Der 1971er [Duden71]_ führt zu englischen Fremdwörtern mit Schluß-ß die
# österreichische Schreibung mit "ss" auf (Miß, engl. und österr. Schreibung
# Miss) wobei das Schluß-s nicht unterstrichen ist (also lang sein müßte?). So
# auch Boss, Business, Stewardess.
#
# Dagegen sagt [en.wiktionary.org]_: "the digraph «ss» was often written «ſs»
# rather than «ſſ»". Die Lang-S Seite der [wikipedia]_ zeigt Beispiele der
# englischen Schreibung "Congreſs".
#
# ::

    # TODO ſſ oder ſs (wie in de-1996)? :
    # if lang == 'de-1901':
    #     word = re.sub(r'ss$', r'ſſ', word)
    #     word = word.replace('ss=', 'ſſ=')
    #     word = word.replace('ss-ſch', 'ſſ-ſch')

    return word


# Ausnahmen Lang-S
# ----------------
#
# Teilstrings mit Lang-S vor Trennstelle::

ausnahmen_lang_s = [

# ſ wird geschrieben, wenn der S-Laut nur scheinbar im Auslaut steht,
# weil ein folgendes unbetontes e ausfällt::

    'Pilſ-ner',  # < Pilsen, aber Mes-ner, Meiss-ner (de-ch)
    'Klauſ-ner', # < Klause, aber Gleiss-ner (de-ch)
    'riſſ-ne',   # ge<riss-ne, ... (de-ch)
    'oſſ-ne',    # ge<schoss-ne, ge<schloss-ne, ... (de-ch)
    'er<leſ-ne', # auserlesne
    'unſ-r',     # unsre, unsrige, ...
    'ſſl',       # Röſſl
    'ſl',        # Beiſl, Häuſl
    'kreiſ-le',
    'Wieſn',
    'Schiſſ-la-weng',
    'ſſ-re',     # bessre, wässre, ...
    'Pſſſt',     # im Duden pst!

# ſ steht in Abkürzungen, wenn es im abgekürzten Wort steht
# (Abſ. - Abſatz/Abſender, (de)creſc. - (de)creſcendo, daſ. - daſelbst ...)
# ::

    # 'Abſ'       # Absatz (Konflikt mit "ABS")
    'creſc',      # creſcendo
    'Berg=aſſ',   # Bergaſſeſſor
    # 'daſ'       # daſelbst (Konflikt mit "das")
    'Diſſ',       # Diſſertation
    'fran-zöſ',   # französſisch
    'ins<geſ',    # insgeſamt
    'Maſſ',       # Maſſachuſetts (Konflikt mit "Maſs" in sz-Ersatzschreibung)
    'WiſſZeitVG', # Wiſſenſchaft...

# Alternativtrennung, wo beide Fälle ſ verlangen::

    'er<.]ſat',   # Kin[-der=/d=er<.]satz, ...
    'er<.]ſät',   # Kin[-der=/d=er<.]sätze, ...
    'eſ[-ſer=/',  # Mes[-ser=/s=er<.]satz
    'ſ[-ter=/t',  # Tes[-ter=/t=er<.]ken-nung
    '[ſ-/=ſ]t',   # Bon[s-/=s]ta-pel
]

# Fremdwörter und Eigennamen
# --------------------------
#
# Schreibung nach Regeln der Herkunftssprache. Dabei ist zu bedenken, daß zu
# der Zeit, als das lange ſ im Antiquasatz noch üblich war (bis ca. 1800), die
# Rechtschreibung freier gehandhabt wurde und mehrfach Wandlungen unterworfen
# war [West06]_.
#
# Im Deutschen werden im Fraktursatz nicht eingedeutschte Fremdwörter
# lateinischen und romanischen Ursprungs in Antiqua mit rund-s geschrieben.
#
# Im Englischen gilt:
#
#   The long, medial, or descending ess, as distinct from the short or
#   terminal ess. In Roman script, the long ess was used everywhere except at
#   the end of words, where the short ess was used, and frequently in what is
#   now the digraph «ss», which was often written «ſs» rather than «ſſ»
#   [en.wiktionary.org]_. See also [Typefounder08]_ and [West06]_.
#
# ::

ausnahmen_lang_s.extend(
    [

# Digraphen::

     'ſh',       # (englisch)
     'Cſar',     # Cs -> Tsch (Csardas, ... ungarisch)
     'ſz',       # polnisch, ungarisch (Liszt, Puszta)

# ts am Silbenanfang (chinesisch, japanisch, griechisch)::

     'Tſa', 'Tſe', 'tſe', 'tſi', 'Tſu', 'tſu',

# In vielen (aber nicht allen) Fremdwörtern steht ſc/ſz trotz Trennzeichen::

     'ſ-ce-',     # Fluo-res-ce-in
     'ſ-ze-',     # Aszese, aszetisch, Damaszener, vis-ze-ral, ...
     'ſ-zen',     # Adoleszenz, Aszendent, ...
     'ſ-zes',     # Abs-zess (de-1996), ...
     'ſ-zet',     # As-zet
     # ſ-zi (aber Dis-zi-plin):
     'ſ-zil-l' ,  # Oszillation, Oszilloskop, ...
     'Aſ-zi',     # Aszites
     'aſ-zi',     # fasziniert, lasziv
     'bſ-zö',     # obszön
     'gnoſ-zie',  # rekognoszieren,
     'reſ-zi',    # fluoreszieren, phosporeszieren, Fluo-res-cin, ...
     'le-biſ-zi', # Plebiszit,
     'viſ-zi',    # Mukoviszidose

# ſ steht in der Endung sk in Wörtern und Namen slawischen Ursprungs
#
# ::

     'Gdanſk',
     'owſk', # Litowſk
     'Minſk',
     'Mur-manſk',
     'ſi-birſk',
     'Smo-lenſk',

# Iſlam/Islam, Iſmael/Ismael, Iſrael/Israel: uneinheitlich
#
# - Duden 1972: Iſlam (in Österreich Islam)
# - Duden 1934: Preußen Iſlam, Österreich Islam, Bayern beides
# - Brockhaus 1894-1896 und Meyer 1885-1892: Islam, Israel
#
# ::

     'Iſ-lam',
     'Iſ-la-m',
     'iſ-la-m',
     'Iſ-ma-e',
     'Iſ-ra-e',
     'iſ-ra-e',
    ])

# ſſ steht in vollständig assimilierten Präfixen auf s (dis-, as-).
# Die Wortliste zeichnet diese mit "normaler Trennung aus (dis-so-nant, ...)
#
# Wandel der Einträge in (Ausnahme, Ersetzung)-Tupel::

ausnahmen_lang_s = [(exception.replace('ſ', 's'), exception)
                    for exception in ausnahmen_lang_s]


# s-Regeln
# --------
#
# Test auf verbliebene Unklarheiten
#
# Wenn ein Wort "s" nur an Stellen enthält wo die Regeln rundes S vorsehen,
# ist die automatische Konversion abgeschlossen.
#
# Ausnahmen und spezielle Regeln
#
# Liste von Teilstrings, welche stets rund-s behalten ::

ausnahmen_rund_s = [

# Abkürzungen::

    'Ausg',  # Ausgang, Ausgabe
    'ausſchl',
    'desgl', # des<gleichen
    'Esc',   # engl. Escape
    'hrsg',  # herausgegeben
    'Hrsg',  # Herausgeber
    'insb',

# ausgelassenes flüchtiges e::

    'Dresd-ne',   # Dresd-ner/Dresd-ne-rin

# s steht auch in einigen Fremdwörtern vor z::

    'is-zi-pl',   # Disziplin (Duden 1971, aber Duden (1934) Diſziplin)
    'mas-ze-ner', # Damaszener
    'on<fis-zie', # konfiszieren, ...
    # 'le-bis-z', # Plebiszit (Mayer 1885), Duden (1934 und 1971) Plebiſzit

# ss, sc im Auslaut uneinheitlich (vgl. `Fremdwörter und Eigennamen`_)::

    'Disc',       # uneinheitlich (auch Diſc aber eingedeutscht Disk)
    '=disc',      # Radiodiscjockey
    # 'Gauss',    # "Briefwechsel zwischen C.F. Gauss und H.C. Schumacher,
                  # herausg. von C.A.F. Peters"
                  # aber Boſſ, Busineſſ, Dreſſ, Miſſ
    ]


def is_complete(word):

# Kurztest: schon alles gewandelt?::

    if 's' not in word:
        return True

# Ersetze s an Stellen, wo es rund zu schreiben ist, durch ς (Schluss-Sigma)
# und teste auf verbliebene Vorkommen.
#
# Einzelfälle mit rundem S (substrings)::

    for fall in ausnahmen_rund_s:
        if fall in word:
            # print('s-Ausnahme', fall, word)
            word = word.replace(fall, fall.replace('s','ς'))

# s steht am Wortende, auch in Zusammensetzungen (vor Haupttrennstellen)::

    word = re.sub(r's($|[=<>])', r'ς\1', word)

# Einige ältere Quellen schreiben ss am Schluss von Fremdwörtern oder Namen
# (Gauss). Andere schreiben ſs oder ſſ. (Vgl. `Fremdwörter und Eigennamen
# mit Schluss-ß`_) Wir verwenden ſs (TODO oder?)::

    #word = re.sub(r'ss(=|$)', r'ςς\1', word)

# s steht am Silbenende (vor Nebentrennstellen), wenn kein p, t, z oder ſ
# folgt (in der traditionellen Schreibung wird st nicht getrennt)::

    word = re.sub(r'ss?([·.\-][^ptzſ])', r'ς\1', word) # konservativ

# s steht auch vor Nebentrennstellen, wenn sch folgt::

    word = word.replace('s-ſch','ς-ſch')

# s steht nach Vorsilben (wie aus<) auch wenn s, p, t, oder z folgt::

    word = word.replace('s<','ς<')

# s steht vor Trennstellen am Suffixanfang
# auch wenn s, p, t, oder z folgt (Ols>sen, Jonas>son, Wachs>tum)::

    word = word.replace('s>','ς>')

# s steht meist im Inlaut vor k, n, w (aber: siehe Lang-ſ-Ausnahmen)::

    word = re.sub(r's([knw])', r'ς\1', word)

# s steht in der Verbindung sst, die in der Schweiz und
# bei fehlendem ß (GROSS) für ßt steht::

    # TODO: nur nach Zwielaut und langem Vokal.
    # word = word.replace('ſst', 'ſςt')
    # word = word.replace('ſs-t', 'ſς-t')

# s steht als zweiter Buchstabe im ersetzten ß::

    word = word.replace('-ſs', '-ſς') # traditionelle Orthographie

# und suche nach übrigen Vorkommen::

    return 's' not in word




# Aufruf von der Kommandozeile
# ============================
#
# ::

if __name__ == '__main__':

# Pfad zu "../../../wortliste" unabhängig vom Arbeitsverzeichnis::

    default_wortliste = os.path.relpath(os.path.join(
                            os.path.dirname(skriptdirname), 'wortliste'))

# Optionen::

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('INFILES', nargs='*',
                        help=u'Eingabedatei(en) im "Wortliste-Format" '
                        '("-" für Standardeingabe), '
                        'Vorgabe "%s".'%default_wortliste,
                        default=[default_wortliste])
    parser.add_argument('-l', '--language', dest='language',
                      help='Sprachvariante(n) (kommagetrennte Liste von '
                      'ISO Sprachtags), Vorgabe "de-1901"',
                      default='de-1901')
    parser.add_argument('-d', '--drop-homonyms', action="store_true",
                      default=False,
                      help='Bei mehrdeutigen Wörtern, die sich nur in '
                      'Lang-S-Schreibung unterscheiden, nimm nur das erste.')
    parser.add_argument('-w', '--wortliste', action='store_true',
                        help='Generiere eine neue "wortliste" mit '
                        'Einträgen in Orthographievariante "de-Latf".')

    args = parser.parse_args()


# Angabe der Sprachvariante nach [BCP47]_ (Reformschreibung 'de' oder
# 'de-1996', Schweiz 'de-CH', ...)::

    lang = args.language

    if args.wortliste:
        lang = 'de-1901,de-1996'

# Iterator::

    entries = (WordEntry(line) for line in filelines(args.INFILES))


# Hauptschleife
# =============
#
# Konvertiere die Wörter der Trennliste und sortiere die Ergebnisse in
# Listen::

    no_of_words = 0       # Gesamtwortzahl der gewählten Sprache(n)
    irreversible = []     # Rückkonversion ungleich Original (Fehler)
    unterschiedlich = []  # Unterschiedliche Wandlung in de-1901 und de-1996
    offen = []  # Der Algorithmus kann die Schreibweise (noch) nicht ermitteln

# Iteration über alle Zeilen der Wortliste::

    for entry in entries:

        word = entry.get(lang)  # Wort mit Trennstellen
        if not word:  # Wort existiert nicht in der Sprachvariante
            # print('not in lang', entry, file=sys.stderr)
            continue
        no_of_words += 1

# Trivialfall:
#
# Wörter ohne Binnen-s¹ müssen nicht konvertiert werden. Damit wird die
# Wortliste ungefähr um die Hälfte kürzer
#
# ¹Abkürzungen können auf ſ enden. Beispiel: «franzöſ.»
#
# ::

        if ('s' not in word
            or 's' not in word[:-1] and not entry.comment):
            if args.wortliste:
                print(entry)
            else:
                print(entry[0])
            continue

# Wandel in Orthographie für Fraktursatz ("de-Latf") mit s/ſ-Unterscheidung::

        latf_word = s_ersetzen(word)

        latf_key = join_word(latf_word) # evt. noch mit [s/ſ]-Alternative
        # print(lang, latf_word, str(entry))
        entry.set(latf_word, lang) # Rückschreiben
        entry[0] = latf_key

# Tests auf Korrektheit und Vollständigkeit der Wandlung::

        if latf_word.replace('ſ', 's') != word:
            entry.comment = latf_word.replace('ſ', 's') + " != " + word
            irreversible.append(entry)
            continue

        if not is_complete(latf_word):
            offen.append(entry)
            continue

# Behandeln der restlichen Felder falls neue Wortliste gewünscht:
#
# * Löschen der Felder mit ß-Ersatzschreibung.
# * Ergänzen von de-1996 falls verschieden.
#
# ::

        if args.wortliste:
            while len(entry) > 4:
                entry.pop()
            if len(entry) > 2 and entry[2] and entry[3]:
                entry[3] = s_ersetzen(entry[3])
                # Teste Konsistenz der Wandlung:
                if (join_word(entry[3]) != entry[0]
                    and '[ſer/ser]' not in join_word(entry[3])):
                    entry.comment = f'{entry[0]} != {join_word(entry[3])}'
                    latf_key = join_word(entry[3])
                    unterschiedlich.append(entry)


        if '/' not in latf_key:
            if args.wortliste:
                print(entry)
            else:
                print(latf_key)
            continue

# Mehrdeutigkeiten [ſ/s] oder [s/ſ] auflösen::

        # print('/', latf_word, file=sys.stderr)
        # 1. Alternative:
        entry1 = WordEntry(str(entry))
        for i, field in enumerate(entry):
            entry1[i] = re.sub(r'\[(.*[sſ].*)/.*[sſ].*\]', r'\1', field)
        entry1.prune()
        if args.wortliste:
            print(entry1)
        else:
            print(entry1[0])
        if args.drop_homonyms:
            continue

        # 2. Alternative
        for i, field in enumerate(entry):
            entry[i] = re.sub(r'\[.*[sſ].*/(.*[sſ].*)\]', r'\1', field)

        # Mehrdeutigkeit nur in 'de-1996:
        latf_key = re.sub(r'\[.*[sſ].*/(.*[sſ].*)\]', r'\1', latf_key)
        if entry[0] != latf_key:
            # print('/2', entry[0], latf_key, file=sys.stderr)
            entry[0] = latf_key
            entry[1] = None
            entry[2] = None
            unterschiedlich.append(entry1)

        if args.wortliste:
            print(entry)
        else:
            print(entry[0])


# Auswertung
# ==========
#
# ::

    sys.stdout.flush() # prevent mixing data with diagnostic output
    print(f"# Gesamtwortzahl {lang} {no_of_words}", file=sys.stderr)
    print(f"# erkannte Konvertierungsfehler: {len(irreversible)}",
          file=sys.stderr)
    for entry in irreversible:
        print(entry, file=sys.stderr)
    print(f"# noch offen/unklar: {len(offen)}", file=sys.stderr)
    for entry in offen:
        print(entry, file=sys.stderr)
    print(f"# uneinheitliche Wandlungen: {len(unterschiedlich)}",
          file=sys.stderr)
    for entry in unterschiedlich:
        print(entry, file=sys.stderr)


# Diskussion
# ==========
#
# Für gebrochene Schriften gibt es den `ISO Sprachtag`_
#
#   :Latf: Latin (Fraktur variant)
#
# (Lateinisches Alphabet, gebrochen), e.g. "de-1901-Latf",
# "de-1996-Latf".
#
# .. _ISO Sprachtag: https://www.unicode.org/iso15924/iso15924-codes.html
#
# Statistik
# ---------
#
# Gesamtwortzahl (traditionelle Rechtschreibung):
# 487715 (de-1901), 487924 (de-1996)
# erkannte Konvertierungsfehler: 0
# noch offen/unklar: 0
#
# Die Wörter der Trennliste wurden nach den Regeln des Dudens in
# die Schreibung mit langem `S` (ſ) konvertiert (wobei ungefähr die Hälfte der
# Wörter kein kleines `s` enthält womit die Konversion trivial wird).
#
# Für eine beschränke Anzahl offener Fälle wurden Ausnahmeregeln und Ausnahmen
# implementiert.
#
# Das Resultat muß noch auf nicht erfaßte Ausnahmen und Sonderfälle geprüft
# werden. Fehlentscheidungen sind nicht auszuschließen.
#
#
# Offene Fälle
# ------------
#
# Wörter mit identischer Schreibung ohne Lang-S
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Einige mehrdeutige Zusammensetzungen unterscheiden sich in der
# Lang-S-Schreibung, z.B.
#
# * Wach[s/ſ]tube:  Wach-Stube / Wachs-Tube
# * Ga[s/ſ]traſſe:  Gas-Trasse / Gast-Rasse
# * Schiff[ſ/s]tau: Schiffs-Tau / Schiff-Stau
#
# Im Normalfall schreibt s2long-s.py beide Varianten in die Ausgabedatei. Die
# Option --drop_homonyms kann verwendet werden, wenn dies nicht erwünscht ist.
#
# Unklare Schreibung
# ~~~~~~~~~~~~~~~~~~
#
# * Ersetztes SZ in reformierter Rechtschreibung, wenn ein Selbstlaut folgt
#   (z.B. "Straſ-ſe" oder "Straſ-se).
#
#   - Während in 1901-er Rechtschreibung die Trennung vor dem "ss" erfolgt
#     (was Ersetzung mit "ſs" impliziert) wäre bei Trennung "wie normales
#     Doppel-S" dann ein rundes S am Silbenanfang.
#
#     Korrekte Fallunterscheidung geht nur bei Betrachtung der Nachbarfelder.
#
# * Tonarten (As-Dur oder Aſ-Dur)
#
#   - Im Fraktur-Duden steht As *in Antiqua* mit rundem s, also
#     keine Aussage zur Schreibung in Fraktur.
#   - Im 1971-er [Duden71]_ steht As (Tonart) ohne Unterstreichung des `s`,
#     das wäre Lang-S, obgleich am Wortende! Ebenso für Es, Cis, ...
#
#   Wir brauchen keine Ausnahme, da korrekterweise die Tonart immer in Antiqua
#   zu setzen ist.
#
#
# Quellen
# =======
#
# .. [Duden71] `Der Große Duden` 16. Auflage, VEB Bibliographisches Institut
#    Leipzig, 1971
#
#    Kennzeichnet im Stichwortteil rundes „s“ durch Unterstreichen.
#
# .. [Duden34] `Der Große Duden`, Leipzig 1934
#
#    In Fraktur.
#
# .. [wikipedia] Langes s
#    https://de.wikipedia.org/wiki/Langes_s
#
# .. [en.wiktionary.org]
#    https://en.wiktionary.org/wiki/%C5%BF
#
# .. [Typefounder08]
#    https://typefoundry.blogspot.com/2008/01/long-s.html
#
# .. [West06] Andrew West, `The rules for long s`, 2006
#    https://www.babelstone.co.uk/Blog/2006/06/rules-for-long-s.html
#
# .. [BCP47]  A. Phillips und M. Davis, (Editoren.),
#    `Tags for Identifying Languages`, https://www.rfc-editor.org/rfc/bcp/bcp47.txt
#
# .. Links:
#
# .. _Wortliste der deutschsprachigen Trennmustermannschaft:
#    https://mirrors.ctan.org/language/hyphenation/dehyph-exptl/projektbeschreibung.pdf
