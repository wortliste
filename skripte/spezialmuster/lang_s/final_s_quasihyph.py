#!/usr/bin/env python3
# :Copyright: © 2011 Günter Milde.
#             Released without warranty under the terms of the
#             GNU General Public License (v. 2 or later)
# :Id: $Id:  $

u"""Filter zum Wandeln von Wörtern mit Rund- und Lang-S in
Pseudo-Trennbeispiele (ausſagen -> aus-sagen)."""

import sys

words = (word.replace(u's', u's-').replace(u'ſ', u's').replace(u'-\n', u'\n')
         for word in sys.stdin)

sys.stdout.writelines(words)
