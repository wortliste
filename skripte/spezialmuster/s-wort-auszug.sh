# s-wort-auszug.sh
# Skript mit einem Parameter: Dateiname einer Wortliste im Langformat

if [ $# -lt 1 ]
then
	echo "Dateiname fehlt."
	exit 1
fi

# Funktion mit folgenden Parametern:
# $1: regulärer Ausdruck, der auf die Ausgabe von "s2long-s.py" angewandt werden soll
# $2: Dateiname einer Wortliste im Langformat
# $3: Sprachkürzel
# $4: Endung der Ausgabedatei
grepauszug() {
	skripte/spezialmuster/lang_s/s2long-s.py -l $3 < $2 2> /dev/null \
	| grep $1 \
	| sed 's/ſ/s/g' > s-woerter/words.$3.$4
}

# Funktion mit folgenden Parametern:
# $1: regulärer Ausdruck, der auf Einträge im Kurzformat angewandt werden soll
# $2: Dateiname einer Wortliste im Kurzformat
# $3: Endung der Ausgabedatei
allsprachauszug() {
	grepauszug $1 $2 de-1901 $3
	grepauszug $1 $2 de-CH-1901 $3
	grepauszug $1 $2 de-1996 $3
	grepauszug $1 $2 de-CH-1996 $3
}

mkdir -p s-woerter

# Suche nach Wörtern mit Lang-s (außer vor oder nach Lang-s oder Rund-s und außer vor t)
echo "ſ ..."
allsprachauszug '[^ſs]ſ[^ſst]' $1 long-s

# Suche nach Wörtern mit Rund-s im Wortinnern (außer vor oder nach Lang-s und außer vor t)
echo "s im Innern ..."
allsprachauszug '[^ſ]s[^ſt]' $1 round-s-inside

# Suche nach Wörtern mit Rund-s am Wortende (außer nach Lang-s)
echo "s am Ende ..."
allsprachauszug '[^ſ]s$' $1 round-s-end

# Suche nach Wörtern mit Doppel-lang-s (außer vor t)
echo "ſſ ..."
allsprachauszug 'ſſ[^t]' $1 double-long-s

# Suche nach Wörtern mit Lang-s gefolgt von Rund-s im Wortinnern (außer vor Lang-s und t)
echo "ſs im Innern ..."
allsprachauszug 'ſs[^ſt]' $1 long-s-round-s-inside

# Suche nach Wörtern mit Lang-s gefolgt von Rund-s am Wortende
echo "ſs am Ende ..."
allsprachauszug 'ſs$' $1 long-s-round-s-end

# Suche nach Wörtern mit Lang-s gefolgt von t (außer nach Lang- oder Rund-s)
echo "ſt ..."
allsprachauszug '[^ſs]ſt' $1 long-s-t

# Suche nach Wörtern mit Rund-s gefolgt von t (außer nach Lang-s)
echo "st ..."
allsprachauszug '[^ſ]st' $1 round-s-t

# Suche nach Wörtern mit Lang-s gefolgt von Rund-s gefolgt von Lang-s (außer vor t)
echo "ſsſ ..."
allsprachauszug 'ſsſ[^t]' $1 long-s-round-s-long-s

# Suche nach Wörtern mit Doppel-lang-s gefolgt von t
echo "ſſt ..."
allsprachauszug 'ſſt' $1 double-long-s-t

# Suche nach Wörtern mit Lang-s gefolgt von Rund-s gefolgt von t
echo "ſst ..."
allsprachauszug 'ſst' $1 long-s-round-s-t

# Suche nach Wörtern mit Rund-s gefolgt von Lang-s gefolgt von t (außer nach Lang-s)
echo "sſt ..."
allsprachauszug '[^ſ]sſt' $1 round-s-long-s-t

# Suche nach Wörtern mit Lang-s gefolgt von Rund-s gefolgt von Lang-s gefolgt von t
echo "ſsſt ..."
allsprachauszug 'ſsſt' $1 long-s-round-s-long-s-t
