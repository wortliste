# ligaturwortauszug.sh
# Skript mit einem Parameter: Dateiname einer Wortliste im Langformat

if [ $# -lt 1 ]
then
	echo "Dateiname fehlt."
	exit 1
fi

# Funktion mit folgenden Parametern:
# $1: regulärer Ausdruck, der auf Einträge im Kurzformat angewandt werden soll
# $2: Dateiname einer Wortliste im Langformat
# $3: Sprachkürzel
# $4: Endung der Ausgabedatei
grepauszug() {
	skripte/wortliste/sprachauszug.py -l $3 -s 'morphemgrenzen,einfach' < $2 \
	| grep $1 \
	| sed 's/-//g' > ligaturwoerter/words.$3.$4
}

# Funktion mit folgenden Parametern:
# $1: regulärer Ausdruck, der auf Einträge im Kurzformat angewandt werden soll
# $2: Dateiname einer Wortliste im Kurzformat
# $3: Endung der Ausgabedatei
allsprachauszug() {
	grepauszug $1 $2 de-1901 $3
	grepauszug $1 $2 de-CH-1901 $3
	grepauszug $1 $2 de-1996 $3
	grepauszug $1 $2 de-CH-1996 $3
}

mkdir -p ligaturwoerter

# Suche nach Wörtern mit ch, aber nicht sch
echo "Ligatur ch ..."
# mit Ligatur: chaotisch, machen, Rauch, syn-chron, Dom-chor
allsprachauszug '^ch\|[^s-]ch\|[^s]-ch' $1 ch.lig
# ohne Ligatur: Comic-heft, comic-haft
allsprachauszug '[^s]c-h' $1 ch.nolig

# Suche nach Wörtern mit sch
echo "Ligatur sch ..."
# mit Ligatur: schön, rauschen, Tisch, Dach-schaden, Ge-schenk, Wirt-schaft
allsprachauszug 'sch' $1 sch.lig
# ohne Ligatur: Einkaufs-chef, aus-checken, nass-chemisch
allsprachauszug 'sc-h\|s-ch' $1 sch.nolig

# Suche nach Wörtern mit ck, aber nicht ck
echo "Ligatur ck ..."
# mit Ligatur: meckern, Reck
allsprachauszug 'ck' $1 ck.lig
# ohne Ligatur: Traffic-kosten
allsprachauszug 'c-k' $1 ck.nolig

# Suche nach Wörtern mit ff, aber nicht ffi, ffl, fft
echo "Ligatur ff ..."
# mit Ligatur: pfiffst, Schiffe, schliff, Stoff-wechsel, Off-set, treff-lich,
# Schiffahrt (nur AR), schifförmig (nur AR)
allsprachauszug '[^f]ff[^filt-]\|ff$\|ff-[^filt-]' $1 ff.lig
# ohne Ligatur: Brief-freund, Auf-fahrt, fünf-fach, Schiff-fahrt (nur NR)
allsprachauszug 'f-f[^ilt]' $1 ff.nolig

# Suche nach Wörtern mit fi, aber nicht ffi
echo "Ligatur fi ..."
# mit Ligatur: finden, pfiff, Chefin, aus-pfiff, Chemie-firma
allsprachauszug '^fi\|[^f-]fi\|[^f]-fi' $1 fi.lig
# ohne Ligatur: Dorf=idylle
allsprachauszug '[^f]f-i' $1 fi.nolig

# Suche nach Wörtern mit ffi
echo "Ligatur ffi ..."
# mit Ligatur: affig, staffieren, Offizier, Baustoffirma (nur AR)
allsprachauszug 'ffi' $1 ffi.lig
# ohne Ligatur: Zellstoff-industrie, Wahlkampf-finale
allsprachauszug 'ff-i\|f-fi' $1 ffi.nolig

# Suche nach Wörtern mit fl, aber nicht ffl
echo "Ligatur fl ..."
# mit Ligatur: flach, pflanzen, Persiflage, schaufle (oder ohne Ligatur?), Ge-flecht
allsprachauszug '^fl\|[^f-]fl\|[^f]-fl' $1 fl.lig
# ohne Ligatur: Kauf-leute, auf-lösen, behilf-lich
allsprachauszug '[^f]f-l' $1 fl.nolig

# Suche nach Wörtern mit ffl
echo "Ligatur ffl ..."
# mit Ligatur: knifflig (oder ohne Ligatur?), Mufflon, Souffleur
allsprachauszug 'ffl' $1 ffl.lig
# ohne Ligatur: Griff-loch, off-line, stoff-lich, Tief-flug, auf-flammen
allsprachauszug 'ff-l\|f-fl' $1 ffl.nolig

# Suche nach Wörtern mit ft, aber nicht fft
echo "Ligatur ft ..."
# mit Ligatur: oft, Landschaft, Hälfte, Stifte
allsprachauszug '[^f]ft' $1 ft.lig
# ohne Ligatur: Brief-träger, Auf-trag
allsprachauszug '[^f]f-t' $1 ft.nolig

# Suche nach Wörtern mit fft
echo "Ligatur fft ..."
# mit Ligatur: schafft, schaffte (oder ohne Ligatur?)
allsprachauszug 'fft' $1 fft.lig
# ohne Ligatur: Stoff-tasche
allsprachauszug 'ff-t' $1 fft.nolig

# Suche nach Wörtern mit ll
echo "Ligatur ll ..."
# mit Ligatur: trällern, Stilleben (nur AR), Fall, Gesell-schaft, Schall-pegel
allsprachauszug '[^l]ll[^l-]\|[^l-]ll$\|ll-[^l]' $1 ll.lig
# ohne Ligatur: Bastel-laden, beispiel-los, Still-leben (nur NR)
allsprachauszug 'l-l' $1 ll.nolig

# Suche nach Wörtern mit ss, aber nicht ssch, ssi, sst
echo "Ligatur ss ..."
# mit Ligatur: Raſſel, Riſs (nur NR), Meſs-gerät, Dreſs-code
allsprachauszug 'ss[^it-]\|ss$\|ss-[^cist]\|ss-c[^h]' $1 ss.lig
# ohne Ligatur: Erhaltungs-ſatz, Aus-ſendung, Hilfs-ſcanner
allsprachauszug 's-s[^cit]\|s-sc[^h]' $1 ss.nolig

# Suche nach Wörtern mit si, aber nicht ssi
echo "Ligatur si ..."
# mit Ligatur: ſingen, Muſik, quaſi, Queck-ſilber, Vor-ſitz
allsprachauszug '^si\|[^s-]si\|[^s]-si' $1 si.lig
# ohne Ligatur: Rettungs-insel, Des-infektion
allsprachauszug '[^s]s-i' $1 si.nolig

# Suche nach Wörtern mit ssi
echo "Ligatur ssi ..."
# mit Ligatur: Ruſſin, Gaſſi
allsprachauszug 'ssi' $1 ssi.lig
# ohne Ligatur: Meſs-instrument (nur NR), Rats-ſitz, Aus-ſiedler
allsprachauszug 'ss-i\|s-si' $1 ssi.nolig

# Suche nach Wörtern mit st, aber nicht sst
echo "Ligatur st ..."
# mit Ligatur: ſterben, Angeſtellter, Kontraſt, faſten, Meiſter
# nur AR: Anschluß-ſtelle, Miß-ſtand
stAusdruckMitLig='^st\|^[^#]*[^s-]st\|^[^#]*[^s]-st'
allsprachauszug $stAusdruckMitLig $1 st.lig
# ohne Ligatur: Friedens-taube, Aus-tritt, Wachs-tum
stAusdruckOhneLig='^[^#]*[^s]s-t'
allsprachauszug $stAusdruckOhneLig $1 st.nolig

# Suche nach Wörtern mit sst
echo "Ligatur sst ..."
# mit Ligatur: faſſt (nur NR), faſſten (nur NR), Pſſſt
sstAusdruckMitLig='^[^#]*sst'
allsprachauszug $sstAusdruckMitLig $1 sst.lig
# ohne Ligatur: Meſs-technik (nur NR), Miſs-ton (nur NR), Mittags-ſtunde, Aus-ſtellung
sstAusdruckOhneLig='^[^#]*\(ss-t\|s-st\)'
allsprachauszug $sstAusdruckOhneLig $1 sst.nolig

# Suche nach Wörtern mit tt
echo "Ligatur tt ..."
# mit Ligatur: Matte, Brett, Wett-lauf, Ballettanz (nur AR)
allsprachauszug '[^t]tt[^t-]\|[^t-]tt$\|tt-[^t]' $1 tt.lig
# ohne Ligatur: Blut-tat, ent-tarnen, Papst-tum, Schutt-transport, Ballett-tanz (nur NR)
allsprachauszug 't-t' $1 tt.nolig

# Suche nach Wörtern mit tz
echo "Ligatur tz ..."
# mit Ligatur: spritzen, Schutz, Ersatz-bus, ersatz-los
allsprachauszug 'tz' $1 tz.lig
# ohne Ligatur: Fahrt-zeit, Ent-zug, Wett-zettel
allsprachauszug 't-z' $1 tz.nolig
