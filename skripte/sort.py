#!/usr/bin/env python3
# :Copyright: © 2012, 2019 Günter Milde.
# :Licence:   This work may be distributed and/or modified under
#             the conditions of the `LaTeX Project Public License`,
#             either version 1.3 of this license or (at your option)
#             any later  version.
# :Version:   0.3 (2019-06-14)

# sort.py
# *******
#
# ::

"""
Sortiere eine oder mehrere Dateien im "Wortliste-Format".

Filter:
  ./sort.py <../wortliste > ../wortliste.sortiert

Zusammenfügen:
  ./sort.py liste.c liste.a liste.b > liste.abc

Einsortieren (zusammenfügen und wieder aufteilen):
   ./sort.py neu.todo wl-* --split -o wl-

Einsortieren und Patch erstellen:
  ./sort.py ../wortliste neu.todo --diff -o ../wortliste.patch
"""

usage = '%(prog)s [Optionen] [Eingangsdatei(en)]\n' + __doc__

# Alle Eingabedateien werden in eine Liste gelesen und dann sortiert.
# Das spezielle Argument '-' steht für die Standardeingabe.
#
# Mit Option -o kann die Ausgabe in eine/mehrere Datei(en) gelenkt werden.
# Das spezielle Argument '-' steht für die Standardausgabe (Vorgabe).
#
# Es wird wahlweise nach Duden oder nach der bis März 2012 für die Wortliste
# genutzten Regel sortiert. Voreinstellung ist Dudensortierung.
#
# Siehe auch Optionen


import unicodedata, sys, argparse, os

# path for local Python modules
sys.path.insert(0, os.path.join(os.path.dirname(__file__), 'lib'))

from py_wortliste.wortliste import WordEntry, udiff, sortkey_duden
from py_wortliste.split_wortliste import split_a_z, write_a_z

# sortkey_wl
# ----------
#
# Sortierschlüssel für den früher genutzten Algorithmus,
# d.h Emulation von:
#
# * Sortieren nach gesamter Zeile
# * mit dem Unix-Aufruf `sort -d`
# * und locale DE.
#
# ::

def sortkey_wl(entry):
    # Sortieren nach gesamter Zeile
    key = str(entry)

    # Ersetzungen:
    ersetzungen = {ord(u'ß'): u'ss'} # ß -> ss
    # Feldtrenner und Trennzeichen ignorieren (Simulation von `sort -d`)
    for char in u';-·=|[]{}':
        ersetzungen[ord(char)] = None
    key = key.translate(ersetzungen)

    # Akzente/Umlaute weglassen:
    key = unicodedata.normalize('NFKD', key) # Akzente mit 2-Zeichen-Kombi
    key = key.encode('ascii', 'ignore').decode('ascii') # ignoriere nicht-ASCII Zeichen
    # Großschreibung ignorieren
    key = key.lower()

    return key


# Aufruf von der Kommandozeile
# ============================
#
# ::

if __name__ == '__main__':

# Optionen::

    parser = argparse.ArgumentParser(usage=usage)
    parser.add_argument('infiles', nargs='*',
                        help='Eingangsdatei(en), Vorgabe: - (Standardeingabe)',
                        default=['-'])
    parser.add_argument('-o', '--outfile',
                      help=u'Ausgangsdatei, Vorgabe: - (Standardausgabe)',
                      default='-')
    parser.add_argument('-p', '--diff', action='store_true', default=False,
                      help=u'Erstelle Patch im "unified diff" Format. '
                        'Vergleicht die erste Eingangsdatei mit dem Resultat.')
    parser.add_argument('-s', '--split',
                      help=u'Aufteilen in Dateien "OUTFILEa" bis "OUTFILEz".'
                            'Vorgabe: False (nicht splitten)',
                      action="store_true", default=False)
    parser.add_argument('-u', '--unsorted', action='store_true', default=False,
                      help=u'Überspringe die Sortierung.')
    parser.add_argument('-d', '--dump', action="store_true", default=False,
                      help=u'Für Rückwärtskompatibilität. Obsolet, ignoriert.')
    parser.add_argument('--legacy-sort', action="store_true",
                      help=u'alternative (obsolete) Sortierordnung',
                      default=False)

    args = parser.parse_args()

    if args.legacy_sort:
        sortkey = sortkey_wl
    else:
        sortkey = sortkey_duden

    if args.split and args.diff:
        print('Aufteilen nach a-z für Patch nicht implementiert.')
        sys.exit()
    if args.split and args.outfile == '-':
        print('Aufteilen nach a-z auf Standardausgabe nicht möglich.')
        sys.exit()

# Einlesen in eine Liste::

    filenames = ', '.join(arg.replace('-', '<stdin>') for arg in args.infiles)
    infiles = [sys.stdin if arg=='-' else open(arg) for arg in args.infiles]

    # Vergleichsdatei bei patch ist erste Datei
    wordlist = list(infiles.pop(0))
    if args.diff:
        wordlist_pre = wordlist[:]

    # Restliche Dateien anhängen
    # verschachtelte Listen entflechten [i for lst in lsts for i in lst]
    wordlist += [line for infile in infiles for line in infile]

# Aufteilen::

    if args.split:
        lists = split_a_z(wordlist)
        if not args.unsorted:
            for l in lists.values():
                l.sort(key=sortkey)
        write_a_z(lists, args.outfile)
        sys.exit()

# Sortieren::

    if not args.unsorted:
        wordlist.sort(key=sortkey)

# Patch erstellen::

    if args.diff:
        output = udiff(wordlist_pre, wordlist,
                       filenames, filenames+'-sortiert')
        if not output:
            print('keine Änderungen')
            sys.exit()
    else:
        output = ''.join(wordlist)

# Ausgabe::

    if args.outfile == '-':
        outfile = sys.stdout
    else:
        outfile = open(args.outfile, 'w')

    outfile.write(output)
