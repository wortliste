LC_COLLATE=de_DE.UTF8

#     $ sh prepare-wordlist.sh < Textdatei
#
# listet alle Wörter mit mindestens vier Buchstaben, die keine römischen
# Zahlen sind, aus einer gegebenen Textdatei auf und …
#
#     $ sh prepare-wordlist.sh < Textdatei | grep -Fixvf Prüfliste
#
# … prüft sie gegen eine ebenfalls gegebene Prüfliste von bekannten Wörtern.
# Eine solche Prüfliste kann beispielsweise mit
#
#   perl skripte/wortliste/extract-tex.pl wortliste | sed 's/-//g' > wortliste.ref
#
# erzeugt werden.


p=$(echo $0 | sed "s|\(.*\)/.*|\1|")  # der Pfad zu den Skripten

sed -f $p/strippunct.sed \
| sed '/..../!d' \
| sort -i \
| uniq -i
