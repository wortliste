#! /usr/bin/perl -w
#
# wl-new2old.pl
#
# Dieses Perl-Skript konvertiert eine gegebene Liste von Einträgen im neuen
# Wortlistenformat (mit bis zu fünf Feldern) ins alte Format (mit bis zu
# acht Feldern).
#
# Aufruf:
#
#   wl-new2old.pl [--debug] [liste1 liste2 ...] > ausgabe
#
# Die Eingabedateien müssen in UTF-8 kodiert sein.
#
# Sind keine Eingabedateien angegeben, verwendet das Skript ebenfalls die
# Standardeingabe.  Beispiel:
#
#   wl-new2old.pl < reformiert > wortliste.erzeugt
#
# Die Ausgabe erfolgt unsortiert.
#
# Im folgenden Beispiel befinden sich die Eingabedateien im Ordner »neu«.
# Eine sortierte Wortliste kann mit
#
#   wl-new2old.pl neu/* \
#   | wlsort.pl \
#   > wortliste
#
# erzeugt werden.
#
# Option »--debug« gibt einen Dump mit mehr Informationen aus, der explizite
# und implizite Felder anzeigt.

# Wir verwenden »<<>>« statt »<>« aus Sicherheitsgründen.
require 5.22.0;

# Diese Datei wird, bevor sie ausgeführt wird, mithilfe des
# »M4filter«-Moduls vom GNU M4 Makroprozessor gefiltert.
#
# Die Datei »M4filter.pm« befindet sich im gleichen Verzeichnis wie dieses
# Skript; wir müssen daher das »lib«-Verzeichnis entsprechend adjustieren.
#
use File::Basename;
use lib dirname (__FILE__);
use M4filter;

# Wir wollen › und ‹ als M4-Quotes verwenden.
changequote(`›', `‹')

# Hauptgrund für die Verwendung von M4 ist das Fehlen von Inline-Funktionen
# in Perl.  Da Subroutinen das Skript deutlich verlangsamen, benützen wir
# stattdessen die M4-Funktionalität.
#
# Anmerkungen:
#
# * Argumente in M4-Makros sollen normalerweise von › und ‹ umschlossen
#   sein, um vorzeitige Expansion zu verhindern.  Der Perl-Code in dieser
#   Datei verwendet keines der beiden Zeichen, was Zweideutigkeiten
#   verhindert.
#
# * »$1«, »$2«, ... sind Argumente in M4-Makros, aber auch Perl verwendet
#   diese Konstrukte in regulären Ausdrücken.  Perl jedoch erlaubt die
#   alternative Syntax »${1}«, »${1}«, ..., die wir daher verwenden, um
#   Zweideutigkeiten zu verhindern.
#
# * Zur besseren Lesbarkeit sind Namen von M4-Makros immer in
#   Großbuchstaben; das verhindert auch Kollisionen im Code, da nur
#   Kleinbuchstaben abgearbeitet werden – es gibt zwar großbuchstabige
#   Einträge in der Wortliste, doch sind sie nicht für dieses Skript
#   relevant.
#
# * Manche Schlüsselwörter sind identisch in M4 und Perl (z.B. »shift«).
#   Derzeit allerdings sind keine von diesen in Verwendung.  Falls sich das
#   einmal ändert, müssen die betroffenen Befehle (auf der M4-Seite) mittels
#   »defn« umdefiniert werden.
#
#
# Mit Subroutinen statt »M4filter« ist das Skript rund 20% langsamer.


use 5.010001;
use strict;
use warnings;
use utf8;                                 # String-Literals direkt als UTF-8.
use open qw(:std :utf8);
use feature qw(unicode_strings);
use Getopt::Long qw(:config no_ignore_case
                            no_auto_abbrev
                            no_getopt_compat);
use List::MoreUtils qw(first_index);


my $debug = "";
GetOptions("debug" => \$debug);


# Analyse der Wortbestandteile
# ----------------------------
#
# Wir verwenden M4-Makros, um folgende Trennvarianten abzudecken:
#
#   reformierte Rechschreibung,
#   traditionelle Rechtschreibung,
#
#   reformierte Rechtschreibung, Versalform,
#   traditionelle Rechtschreibung, Versalform,
#
#   traditionelle schweizerdeutsche Rechtschreibung.
#
# Beachte: Die Wortliste hat keine separaten Einträge für traditionelle
# schweizerdeutsche Versalformen; diese fallen entweder mit den
# traditionellen Versalformen oder den traditionellen schweizerdeutschen
# Formen zusammen.
#
# Beachte weiters, daß sich Einträge für reformierte und traditionelle
# Rechtschreibung teilweise widersprechen und daher separat behandelt werden
# müssen; die Einträge für traditionelle deutsche und schweizerdeutsche
# Rechtschreibung dagegen nicht.  Aus diesem Grund ist die Wortliste für
# traditionelle schweizerdeutsche Rechtschreibung einfach eine Obermenge der
# Wortliste für die traditionelle deutsche Rechtschreibung; etwaige
# »Fehleinträge« der traditionellen schweizerdeutschen Rechtschreibung (z.B.
# »süsssauer« statt dem korrekten »süssauer«) werden nicht entfernt.


# M4-Makros mit angehängtem »_« sind für Wörter ohne Trennstellenmarker.


# »s-t« -> »-st« (ref. -> trad.)
#                (ref. -> trad. versal)
#                (ref. -> trad. schweiz.)
#
define(›S_T‹,
  ›if ($1 =~ /s \W+ t/x) {
     # Trennstelle zwischen »s« und »th(eta)« auch in traditioneller
     # Rechtschreibung (z.B. An<äs-the-sie)!

     # Kno-ten=äs-te -> Kno-ten=äste
     # Äs-te         -> Äste
     $1 =~ s/(?: (?<= \W \w )
                 | (?<= ^ \w ) )
             s-t
             (?! h )
            /st/xg;

     # Tes[-ter=/t=er<.]ken-nung -> Te[-ster=/st=er<.]ken-nung
     $1 =~ s/(?<= \w [^s] )
             s \[ \W+
                  t
                  (?! h )
                  (.*?)
               \/
                  t
                  (.*?)
               \]
            /[-st${1}\/st${2}]/xg;

     #  fas-te  -> fa-ste
     # (fass-te -> fass-te)
     $1 =~ s/(?<= \w [^s] )
             s-t
             (?! h )
            /-st/xg;
   }‹)

# Nichts zu tun.
define(›S_T_‹, ›‹)


# »-ck« -> »{ck/k-k}« (ref. -> trad.)
#                     (ref. -> trad. versal)
#                     (ref. -> trad. schweiz.)
#
define(›CK‹,
  ›if ($1 =~ /ck/) {
     # Dru[-cker=/ck=er<.]zeug>nis -> Dru[{ck/k-k}er=/ck=er<.]zeug>nis
     $1 =~ s/\[ \W+
                ck
                (.*?)
             \/
                ck
                (.*?)
             \]
            /[{ck\/k-k}${1}\/ck${2}]/xg;

     # He-cke -> He{ck/k-k}e
     $1 =~ s/[^\w\/\[\]\{\}]+
             ck
            /{ck\/k-k}/xg;

     # Stau[=be-/b=e]cken -> Stau[=b/b=]e{ck/k-k}en
     $1 =~ s/\[ (.*?) (.)
                -
             \/
                (.*?) \g{2}
             \]
             ck
             (?= [aeiouäöüy] )
            /[${1}\/${3}]${2}{ck\/k-k}/xg;

     #  Ecke       -> E{ck/k-k}e
     #  Buch=ecker -> Buch=e{ck/k-k}er
     # (Eck        -> Eck)
     # (Vier=eck   -> Vier=eck)
     # (blockt     -> blockt)
     $1 =~ s/(?: (?<= [^\w\/\[\]\{\}] \w )
                 | (?<= ^ \w ) )
             ck
             (?= [aeiouäöüy] )
            /{ck\/k-k}/xg;
   }‹)

# Nichts zu tun.
define(›CK_‹, ›‹)


# »-ß« -> »s-s« (ref. -> ref. versal)
#               (ref. -> trad. schweiz.)
#
define(›BINDESTRICH_SZ_1‹,
  ›# hei-ße -> heis-se
   $1 =~ s/\W+
           ß
          /s-s/xg;

   #  äße      -> äs-se
   #  auf<aßen -> auf<as-sen
   # (auf<aß   -> auf<aß)
   # (auf<aßt  -> auf<aßt)
   $1 =~ s/(?: (?<= \W \w )
               | (?<= ^ \w ) )
           ß
           (?= [aeiouäöüy] )
          /s-s/xg‹)

define(›BINDESTRICH_SZ_1_‹,
  ›$1 =~ s/\W+
           ß
          /ss/xg;

   $1 =~ s/(?: (?<= \W \w )
               | (?<= ^ \w ) )
           ß
           (?= [aeiouäöüy] )
          /ss/xg‹)


# »-ß« -> »-ss« (ref. -> trad. versal)
#
define(›BINDESTRICH_SZ_2‹,
  ›# hei-ße -> hei-sse
   $1 =~ s/-ß
          /-ss/xg‹)

define(›BINDESTRICH_SZ_2_‹,
  ›$1 =~ s/-ß
          /ss/xg‹)


# »ß« -> »ss« (ref. -> ref. versal)
#             (ref. -> trad. versal)
#             (ref. -> trad. schweiz.)
#
# Diese Regel ist nach den spezielleren ß-Regeln weiter oben anzuwenden.
#
define(›SZ‹,
  ›# heiß   -> heiss
   # büß-te -> büss-te
   $1 =~ s/ß
          /ss/xg;

   # Wir markieren Trennstellen nach »en« und »er« als ungünstig.
   #
   # Ge<fäss=er<kran-kung -> Ge<fäss=er<.kran-kung
   $1 =~ s/ss \W+ (?: en | er ) \K
           ([<-]) \.?
          /${1}./xg‹)

define(›SZ_‹,
  ›$1 =~ s/ß
          /ss/xg‹)


# »ss« -> »ß« (ref. -> trad.)
#
define(›SS‹,
  ›#  biss=fest -> biß=fest
   #  esst      -> eßt
   #  Hass      -> Haß
   #  Rössl     -> Rößl
   # (Fa-desse  -> Fa-desse)
   $1 =~ s/ss
           (?= (?: [tl]
                   | \W
                   | $ ) )
          /ß/xg;

   # Wir entfernen Ungünstigkeitsmarker nach »en« und »er«.
   #
   # Ab<fluß=er<.hö-hung -> Ab<fluß=er<hö-hung
   $1 =~ s/ß \W+ (?: en | er ) [<-] \K
           \.
          //xg‹)

define(›SS_‹,
  ›$1 =~ s/ss
           (?= (?: [tl]
                   | \W
                   | $ ) )
          /ß/xg‹)


# »xx-x« -> »{xx/xx-x}« (ref. -> trad.)
#                       (ref. -> trad. versal)
#                       (ref. -> trad. schweiz.)
#
# Buchstabe »x« ist irgendein Konsonant außer »s«.
#
define(›XX_X‹,
  ›if ($1 =~ /(.) \g{1} \W+ \g{1}/x) {
     #  Bau=stoff==fir-ma -> Bau=sto{ff/ff==f}ir-ma
     # (Griff=flä-che     -> Griff=flä-che)
     $1 =~ s/([b-df-hj-np-rtv-z]) \g{1} (\W+) \g{1}
             (?= [aeiouäöüy] )
            /{${1}${1}\/${1}${1}${2}${1}}/xg;

     # Kipp=phä-no-men -> Ki{pp/pp=p}hä-no-men
     $1 =~ s/([prt]) \g{1} (\W+) \g{1}
             (?= h [aeiouäöüy] )
            /{${1}${1}\/${1}${1}${2}${1}}/xg;

     # Wir markieren Trennstellen nach einen Buchstaben als ungünstig.
     #
     # Ab<fa{ll/ll=l}a-ger -> Ab<fa{ll/ll=l}a-.ger
     $1 =~ s/([b-df-hj-np-rtv-z]) \g{1} \W+ \g{1} } [aeiouäöüy] \K
             ([<-]) \.?
            /${2}./xg;
   }‹)

define(›XX_X_‹,
  ›if ($1 =~ /(.) \g{1} \W+ \g{1}/x) {
     $1 =~ s/([b-df-hj-np-rtv-z]) \g{1} \W+ \g{1}
             (?= [aeiouäöüy] )
            /${1}${1}/xg;

     $1 =~ s/([prt]) \g{1} \W+ \g{1}
             (?= h [aeiouäöüy] )
            /${1}${1}/xg;
   }‹)


define(›ENTFERNE_MARKER‹,
  ›# Es gilt
   #
   #   {»Variante A«/»Variante B«} -> »Variante A«
   #   [»Variante A«/»Variante B«] -> »Variante A«
   #
   # Beispiele:
   #
   #   Ab<fa{ll/ll=l}a-.ger -> Ab<falla-.ger
   #   Dru[-cker=/ck=er<.]zeug>nis -> Dru-cker=zeug>nis
   #
   # Beachte, daß »{.../...}« bei reformierter Rechtschreibung nicht
   # auftreten kann (»[.../...]« dagegen schon).
   #
   $1 =~ s/\{ (.*?) \/ .*? \}/${1}/gx;
   $1 =~ s/\[ (.*?) \/ .*? \]/${1}/gx;

   $1 =~ s/\W//g‹)


# Makros zum Erzeugen von Wortformen aus der reformierten Trennung.
# Wiederum sind die Versionen mit angehängtem »_« im Namen für Wörter ohne
# Trennstellenmarker.

define(›REF‹, ›‹)

define(›REF_‹,
   ›ENTFERNE_MARKER(›$1‹)‹)


define(›TRAD‹,
  ›S_T(›$1‹);
   CK(›$1‹);
   SS(›$1‹);
   XX_X(›$1‹)‹)

define(›TRAD_‹,
  ›S_T_(›$1‹);
   CK_(›$1‹);
   SS_(›$1‹);
   XX_X_(›$1‹);
   ENTFERNE_MARKER(›$1‹)‹)


define(›REF_VERSAL‹,
  ›BINDESTRICH_SZ_1(›$1‹);
   SZ(›$1‹)‹)

define(›REF_VERSAL_‹,
  ›BINDESTRICH_SZ_1_(›$1‹);
   SZ_(›$1‹);
   ENTFERNE_MARKER(›$1‹)‹)


define(›TRAD_VERSAL‹,
  ›S_T(›$1‹);
   CK(›$1‹);
   BINDESTRICH_SZ_2(›$1‹);
   SZ(›$1‹);
   XX_X(›$1‹)‹)

define(›TRAD_VERSAL_‹,
  ›S_T_(›$1‹);
   CK_(›$1‹);
   BINDESTRICH_SZ_2_(›$1‹);
   SZ_(›$1‹);
   XX_X_(›$1‹);
   ENTFERNE_MARKER(›$1‹)‹)


define(›TRAD_SCHWEIZ‹,
  ›S_T(›$1‹);
   CK(›$1‹);
   BINDESTRICH_SZ_1(›$1‹);
   SZ(›$1‹);
   XX_X(›$1‹)‹)

define(›TRAD_SCHWEIZ_‹,
  ›S_T_(›$1‹);
   CK_(›$1‹);
   BINDESTRICH_SZ_1_(›$1‹);
   SZ_(›$1‹);
   XX_X_(›$1‹);
   ENTFERNE_MARKER(›$1‹)‹)


# Makros zum Erzeugen von Wortformen aus der traditionellen Trennung.

define(›TRAD_T‹, ›‹)

define(›TRAD_T_‹,
  ›ENTFERNE_MARKER(›$1‹)‹)


define(›TRAD_VERSAL_T‹,
  ›BINDESTRICH_SZ_2(›$1‹);
   SZ(›$1‹)‹)

define(›TRAD_VERSAL_T_‹,
  ›BINDESTRICH_SZ_2_(›$1‹);
   SZ_(›$1‹);
   ENTFERNE_MARKER(›$1‹)‹)


define(›TRAD_SCHWEIZ_T‹,
  ›BINDESTRICH_SZ_1(›$1‹);
   SZ(›$1‹)‹)

define(›TRAD_SCHWEIZ_T_‹,
  ›BINDESTRICH_SZ_1(›$1‹);
   SZ(›$1‹);
   ENTFERNE_MARKER(›$1‹)‹)


# Konvertiere Argument zu Großbuchstaben (unter Auslassung von »ß«).
#
define(›GROSS‹,
  ›translit(›$*‹,
            ›abcdefghijklmnopqrstuvwxyzäöü‹,
            ›ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ‹)‹)


# Konvertiere ersten Buchstaben des Arguments zu einem Großbuchstaben.  Der
# Code hier ist eine verhübschte Version der Beispieldatei »capitalize2.m4«
# aus der M4-Distribution.
#
define(›_ARG1‹,
  ›$1‹)
define(›_ZU_ALT‹,
  ›changequote(›»‹, ›«‹)‹)
define(›_VON_ALT‹,
  ›changequote(»›«, »‹«)‹)
define(›_GROSS_ALT‹,
  ›translit(»$*«,
            »abcdefghijklmnopqrstuvwxyzäöü«,
            »ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ«)‹)
define(›_KLEIN_ALT‹,
  ›translit(»$*«,
            »ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ«,
            »abcdefghijklmnopqrstuvwxyzäöü«)‹)
define(›_GROSS_ANFANG_ALT‹,
  ›regexp(»$1«,
          »^\(\w\)\(\w*\)«,
          »_GROSS_ALT(»»\1««)_KLEIN_ALT(»»\2««)«)‹)
define(›GROSS_ANFANG‹,
  ›_ARG1(_ZU_ALT()patsubst(»»$*««,
                           »\w+«,
                           _VON_ALT()›«_$0_ALT(»\&«)»‹_ZU_ALT())_VON_ALT())‹)


# Ein Makro, um einen Eintrag im »einträge«-Hash zu setzen.
#
# $1 ist der Präfix (z.B. »ref«)
# $2 ist die Feldkonstante (z.B. »Ref«)
# $3 ist der Flagwert (z.B. »Ref_abgeleitet«)
#
# Verwendete Variablen:
#   $$1_wort (z.B. »$ref_wort«)
#   $$1_wort_lc (z.B. »$ref_wort_lc«)
#   $$1_trennung (z.B. »$ref_trennung«)
#   %einträge
#
define(›EINTRAG‹,
  ›@{$einträge{$$1_wort_lc}}[Wort] //= $$1_wort;
   @{$einträge{$$1_wort_lc}}[$2, $2_] = ($$1_trennung, $3)‹)


# Ein Makro, um Kommentare im »einträge«-Hash zu setzen.
#
# $1 ist der Präfix (z.B. »ref«)
# $2 ist der Flagwert (z.B. »Ref_abgeleitet«)
#
# Verwendete Variablen:
#   $$1_wort_lc (z.B. »$ref_wort_lc«)
#   $kommentar
#   %einträge
#
define(›KOMMENTAR‹,
  ›if (!@{$einträge{$$1_wort_lc}}[Kommentar_]
       || @{$einträge{$$1_wort_lc}}[Kommentar_] < $2) {
     @{$einträge{$$1_wort_lc}}[Kommentar, Kommentar_] = ($kommentar, $2);
   }‹)


# Sekundärmakros, die abgeleitete Wortformen setzen.
#
# $1 ist der Präfix (z.B. »ref«)
# $2 ist das Trennmakro (z.B. »REF«),
# $3 ist das Wortmakro (z.B. »REF_«)
# $4 ist die Feldkonstante (z.B. »Ref«)
# $5 ist das ableitende Wort
#
# Verwendete Variablen:
#   $$1_wort (z.B. »$ref_wort«)
#   $$1_wort_lc (z.B. »$ref_wort_lc«)
#   $$1_trennung (z.B. »$ref_trennung«)
#   %einträge
#
define(›_REF_ABGELEITET‹,
  ›$$1_wort = $5;
   $3(››$$1_wort‹‹);

   $$1_trennung = $5;
   $2(››$$1_trennung‹‹);

   $$1_wort_lc = lc $$1_wort;
   if (!@{$einträge{$$1_wort_lc}}[$4_]) {
     EINTRAG(›$1‹, ›$4‹, ›Ref_abgeleitet‹);
   }‹)


define(›_TRAD_ABGELEITET‹,
  ›$$1_wort = $5;
   $3(››$$1_wort‹‹);

   $$1_trennung = $5;
   $2(››$$1_trennung‹‹);

   $$1_wort_lc = lc $$1_wort;
   if (!@{$einträge{$$1_wort_lc}}[$4_]
       || @{$einträge{$$1_wort_lc}}[$4_] <= Trad_abgeleitet) {
     EINTRAG(›$1‹, ›$4‹, ›Trad_abgeleitet‹);
   }‹)


# Sekundärmakro, das eine explizite Wortform setzt.
#
# $1 ist der Präfix (z.B. »ref«)
# $2 ist das Trennmakro (z.B. »REF«),
# $3 ist das Wortmakro (z.B. »REF_«)
# $4 ist die Feldkonstante (z.B. »Ref«)
# $5 ist das ableitende Wort
#
# Verwendete Variablen:
#   %einträge
#
# Lokale Variablen:
#   $$1_wort (z.B. »$ref_wort«)
#   $$1_wort_lc (z.B. »$ref_wort_lc«)
#   $$1_trennung (z.B. »$ref_trennung«)
#
define(›_EINTRAG_EXPLIZIT‹,
  ›$$1_trennung = $5;

   $$1_wort = $5;
   ENTFERNE_MARKER(›$$1_wort‹);

   $$1_wort_lc = lc $$1_wort;
   EINTRAG(›$1‹, ›$4‹, ›Explizit‹)‹)


# Die Hauptmakros sind nur dazu da, die notwendigen Makronamen aus dem
# ersten Argument zu konstruieren.  $2 ist das ableitende Wort.
#
define(›REF_ABGELEITET‹,
  ›_$0(›$1‹,
       ›GROSS(›$1‹)‹,
       ›GROSS(›$1_‹)‹,
       ›GROSS_ANFANG(›$1‹)‹,
       ›$2‹)‹)


define(›TRAD_ABGELEITET‹,
  ›_$0(›$1‹,
       ›GROSS(›$1_T‹)‹,
       ›GROSS(›$1_T_‹)‹,
       ›GROSS_ANFANG(›$1‹)‹,
       ›$2‹)‹)


define(›EINTRAG_EXPLIZIT‹,
  ›_$0(›$1‹,
       ›GROSS(›$1‹)‹,
       ›GROSS(›$1_‹)‹,
       ›GROSS_ANFANG(›$1‹)‹,
       ›$2‹)‹)


# Zum Debuggen.  Das zweite Argument von »format« darf nicht von › und ‹
# umgeben sein.

define(›DEBUG‹,
  ›print   "  format(›%-14s‹, patsubst(›$1‹, ›_‹, ›. ‹):) "
         . $$1_wort
         . " "
         . $$1_trennung
         . "\n"‹)


# Hauptroutine
# ------------

# Alle Einträge kommen in ein Hash, wobei der Schlüssel das zu trennende
# Wort in Kleinschreibung ist (wir verwenden Perls »lc«-Funktion).  Die
# Elemente sind Arrays mit folgender Struktur:
#
#   Wort              Wort ungetrennt
#   Ref               reformierte Trennung
#   Trad              traditionelle Trennung
#   Ref_versal        reformierte versale Trennung
#   Trad_versal       traditionell versale Trennung
#   Trad_schweiz      traditionelle schweizer Trennung
#   Kommentar         Kommentar, falls vorhanden, sonst ""
#
#   Ref_              s.u.
#   Trad_             s.u.
#   Ref_versal_       s.u.
#   Trad_versal_      s.u.
#   Trad_schweiz_     s.u.
#   Kommentar_        s.u.
#
# Die sechs Felder endend mit »_« sind numerische Flags mit folgenden Werten:
#
#   Ref_abgeleitet    abgeleitet von reformierter Trennung
#   Trad_abgeleitet   abgeleitet von traditioneller Trennung
#   Explizit          explizit gesetzt
#
# Alle Trennungen in einem Element haben die gleiche (ungetrennte) Wortform
# im Gegensatz zur Liste im Kurzformat, wo in einer Zeile verschiedene
# Wörter auftreten können.
#
my %einträge;

use constant {
  Wort => 0,
  Ref => 1,
  Trad => 2,
  Ref_versal => 3,
  Trad_versal => 4,
  Trad_schweiz => 5,
  Kommentar => 6,

  Ref_ => 7,
  Trad_ => 8,
  Ref_versal_ => 9,
  Trad_versal_ => 10,
  Trad_schweiz_ => 11,
  Kommentar_ => 12,
};

use constant {
  Ref_abgeleitet => 0,
  Trad_abgeleitet => 1,
  Explizit => 2,
};


# Folgender Algorithmus wird für das Setzen von Kommentaren angewandt.
#
# * Der Eintrag für das erste nicht-leere Feld einer Zeile wird explizit mit
#   einem Kommentar gesetzt (auch wenn er leer sein sollte).
#
# * Abgeleitete Kommentare werden gesetzt, falls sie nicht-leer sind und
#   kein expliziter Eintrag vorhanden ist.


while (<<>>) {
  # Variablen, welche von »REF_ABGELEITET«, »TRAD_ABGELEITET« und
  # »EINTRAG_EXPLIZIT« benötigt werden.
  my ($ref_wort, $ref_wort_lc, $ref_trennung,
      $ref_versal_wort, $ref_versal_wort_lc, $ref_versal_trennung,
      $trad_wort, $trad_wort_lc, $trad_trennung,
      $trad_versal_wort, $trad_versal_wort_lc, $trad_versal_trennung,
      $trad_schweiz_wort, $trad_schweiz_wort_lc, $trad_schweiz_trennung);

  chop;

  # Reine Kommentarzeilen werden ignoriert.
  next if (/^ \s* \#/x);

  # Analysiere Zeile.
  /^ ([^\#]+?)
     \s*
     (\# .* )? $/x;

  my @trennungen = split(";", ${1});
  my $kommentar = ${2} // "";

  # Reformierte Rechtschreibung.
  my $ref_trennung_explizit = $trennungen[0];

  if ($ref_trennung_explizit ne "-1-") {
    EINTRAG_EXPLIZIT(›ref‹, ›$ref_trennung_explizit‹);

    # Leite reformierte Wortform aus der reformierten Trennung ab.
    REF_ABGELEITET(›ref_versal‹, ›$ref_trennung_explizit‹);

    KOMMENTAR(›ref‹, ›Explizit‹);
    if ($kommentar && @trennungen == 1) {
      KOMMENTAR(›ref_versal‹, ›Ref_abgeleitet‹);
    }

    if ($debug) {
      print   "\n"
            . "explizit:\n";
      DEBUG(›ref‹);
      print "abgeleitet:\n";
      DEBUG(›ref_versal‹);
    }

    # Leite traditionelle Wortformen aus der reformierten Trennung ab,
    # wenn kein traditioneller Eintrag vorhanden ist.
    if (@trennungen == 1) {
      REF_ABGELEITET(›trad‹, ›$ref_trennung_explizit‹);
      REF_ABGELEITET(›trad_versal‹, ›$ref_trennung_explizit‹);
      REF_ABGELEITET(›trad_schweiz‹, ›$ref_trennung_explizit‹);

      if ($kommentar) {
        KOMMENTAR(›trad‹, ›Ref_abgeleitet‹);
        KOMMENTAR(›trad_versal‹, ›Ref_abgeleitet‹);
        KOMMENTAR(›trad_schweiz‹, ›Ref_abgeleitet‹);
      }

      if ($debug) {
        DEBUG(›trad‹);
        DEBUG(›trad_versal‹);
        DEBUG(›trad_schweiz‹);
      }
    }

    $kommentar = "";
  }

  next if @trennungen == 1;

  # Traditionelle Rechtschreibung.
  my $trad_trennung_explizit = $trennungen[1] eq "-2-"
                                 ? "" : $trennungen[1];

  EINTRAG_EXPLIZIT(›trad‹, ›$trad_trennung_explizit‹);
  TRAD_ABGELEITET(›trad_versal‹, ›$trad_trennung_explizit‹);
  TRAD_ABGELEITET(›trad_schweiz‹, ›$trad_trennung_explizit‹);

  if ($kommentar) {
    KOMMENTAR(›trad‹, ›Explizit‹);
    $kommentar = "";
  }

  if ($debug && $trad_trennung_explizit) {
    print "explizit:\n";
    DEBUG(›trad‹);

    print "abgeleitet:\n";
    DEBUG(›trad_versal‹);
    DEBUG(›trad_schweiz‹);
  }

  next if @trennungen == 2;

  # Reformierte Rechtschreibung, Versalform.
  my $ref_versal_trennung_explizit = $trennungen[2] eq "-3-"
                                       ? "" : $trennungen[2];

  EINTRAG_EXPLIZIT(›ref_versal‹, ›$ref_versal_trennung_explizit‹);

  if ($kommentar) {
    KOMMENTAR(›ref_versal‹, ›Explizit‹);
    $kommentar = "";
  }

  if ($debug && $ref_versal_trennung_explizit) {
    print "explizit:\n";
    DEBUG(›ref_versal‹);
  }

  next if @trennungen == 3;

  # Traditionelle Rechtschreibung, Versalform.
  my $trad_versal_trennung_explizit = $trennungen[3] eq "-4-"
                                        ? "" : $trennungen[3];

  EINTRAG_EXPLIZIT(›trad_versal‹, ›$trad_versal_trennung_explizit‹);

  if ($kommentar) {
    KOMMENTAR(›trad_versal‹, ›Explizit‹);
    $kommentar = "";
  }

  if ($debug && $trad_versal_trennung_explizit) {
    print "explizit:\n";
    DEBUG(›trad_versal‹);
  }

  next if @trennungen == 4;

  # Traditionelle Rechtschreibung, schweizerdeutsch.
  my $trad_schweiz_trennung_explizit = $trennungen[4] eq "-5-"
                                         ? "" : $trennungen[4];

  EINTRAG_EXPLIZIT(›trad_schweiz‹, ›$trad_schweiz_trennung_explizit‹);

  if ($kommentar) {
    KOMMENTAR(›trad_schweiz‹, ›Explizit‹);
    $kommentar = "";
  }

  if ($debug && $trad_schweiz_trennung_explizit) {
    print "explizit:\n";
    DEBUG(›trad_schweiz‹);
  }
}


# Wir iterieren jetzt über alle Einträge, um sie auszugeben.
foreach my $schlüssel (keys %einträge) {
  my ($ref_trennung_lc,
      $trad_trennung_lc,
      $ref_versal_trennung_lc,
      $trad_versal_trennung_lc,
      $trad_schweiz_trennung_lc);

  my ($wort,
      $ref_trennung,
      $trad_trennung,
      $ref_versal_trennung,
      $trad_versal_trennung,
      $trad_schweiz_trennung,
      $kommentar) = @{$einträge{$schlüssel}};

  # Ignoriere leere Einträge.
  next unless $ref_trennung
              || $trad_trennung
              || $ref_versal_trennung
              || $trad_versal_trennung
              || $trad_schweiz_trennung;

  print $wort;

  $ref_trennung //= "";
  $trad_trennung //= "";
  $ref_versal_trennung //= "";
  $trad_versal_trennung //= "";
  $trad_schweiz_trennung //= "";

  $ref_trennung_lc = lc $ref_trennung;
  $trad_trennung_lc = lc $trad_trennung;
  $ref_versal_trennung_lc = lc $ref_versal_trennung;
  $trad_versal_trennung_lc = lc $trad_versal_trennung;
  $trad_schweiz_trennung_lc = lc $trad_schweiz_trennung;

  if ($ref_trennung_lc
      && $ref_trennung_lc eq $trad_trennung_lc
      && (!$ref_versal_trennung_lc
          || ($trad_trennung_lc eq $ref_versal_trennung_lc
              && $ref_versal_trennung_lc eq $trad_versal_trennung_lc
              && $trad_versal_trennung_lc eq $trad_schweiz_trennung_lc))) {
    print   ";"
          . $ref_trennung; # Feld 2
  }
  else {
    print   ";"
          . "-2-"
          . ";"
          . ($trad_trennung ? $trad_trennung
                            : "-3-")
          . ";"
          . ($ref_trennung ? $ref_trennung
                           : "-4-");

    if ($ref_trennung_lc ne $ref_versal_trennung_lc
        || $trad_trennung_lc ne $trad_versal_trennung_lc
        || $trad_trennung_lc ne $trad_schweiz_trennung_lc) {
      if ($ref_versal_trennung_lc eq $trad_versal_trennung_lc
          && $ref_versal_trennung_lc eq $trad_schweiz_trennung_lc) {
        if ($ref_versal_trennung) {
          print   ";"
                . $ref_versal_trennung; # Feld 5
        }
      }
      else {
        print   ";"
              . "-5-"
              . ";"
              . ($trad_versal_trennung ? $trad_versal_trennung
                                       : "-6-")
              . ";"
              . ($ref_versal_trennung ? $ref_versal_trennung
                                      : "-7-");

        if ($trad_versal_trennung_lc ne $trad_schweiz_trennung_lc) {
          print   ";"
                . ($trad_schweiz_trennung ? $trad_schweiz_trennung
                                          : "-8-");
        }
      }
    }
  }

  print " " . $kommentar if $kommentar;

  print "\n";
}

# EOF
