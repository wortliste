#! /usr/bin/perl -w
#
# wlsort.pl
#
# Dieses Perl-Skript sortiert eine Wortlisten-Datei (entweder im alten oder
# neuen Format des »wortlisten«-Projekts) zeilenweise.
#
# Die Eingabe auf stdin muß UTF-8-kodiert sein; die Ausgabe erfolgt auf
# stdout.
#
# Die angewandte Sortierung ist die Duden-Sortierung für Deutsch, basierend
# auf dem Zeichenrepertoire von ISO-8859-15 (latin9) plus »ſ«.
#
# Beispiel:
#
#   wlsort.pl < eingabe > ausgabe

# Wir verwenden »<<>>« statt »<>« aus Sicherheitsgründen.
require 5.22.0;

use strict;
use warnings;
use utf8;                              # String-Literals direkt als UTF-8.
use open qw(:std :utf8);
use feature 'unicode_strings';

my %wortliste;

while (<<>>) {
  # Bestimme den Schlüsselwert einer Zeile für die Hash-Tabelle; wir
  # verwenden das erste nicht-leere Feld (ohne Trennzeichen).
  #
  # Beispiele:
  #
  #   abartigste;-2-;ab<ar-tig-ste;ab<ar-tigs-te  ->  abartigste
  #   -1-;an<ge<rauht                             ->  angerauht
  #
  my $wort = $_;

  # Entferne Kommentar.
  $wort =~ s/\# .*+ $//x;

  # {»Variante A«/»Variante B«} -> »Variante A«
  # [»Variante A«/»Variante B«] -> »Variante A«
  #
  # Es genügt, den ersten Treffer zu bearbeiten.
  $wort =~ s/\{ ([^\/]*+) \/ [^}]*+ \}/$1/x;
  $wort =~ s/\[ ([^\/]*+) \/ [^]]*+ \]/$1/x;

  # Entferne alle Nicht-Buchstaben außer »;«.
  $wort =~ s/[^\p{Alpha};]//g;

  # Reduziere auf erstes nicht-leeres Feld.
  $wort =~ /^ ;*+ ([^;]++)/x;
  $wortliste{$1} = $_;
}

foreach my $erstes_feld
        (map {
	   # Retourniere das originale Wort (also das vierte Element)
	   # nach dem Suchvorgang.
	   (split("\0"))[3]
         }

         sort

         map {
           # Damit das Sortieren möglichst schnell abläuft, verwenden
           # wir einerseits »sort« ohne eine explizite Subroutine (das
           # bedeutet im besonderen, daß wir uns auf eine reine
           # lexikographische Sortierung beschränken müssen),
           # andererseits »tr///« zum Erstellen der Sortierschlüssel,
           # soweit wie möglich.  Als Abbildung benutzen wir daher
           # passende Unicode-Zeichen, deren Werte ausschließlich zur
           # Sortierung verwendet werden.

           my $s1 = lc;
	   my $s2 = $s1;
	   my $s3 = $_;

	   my $hat_ligatur = /[ßÆæŒœ]/;

	   # Wir brauchen drei Sortierschlüssel, siehe weiter unten.
	   # Die Zeichen »ß«, »Æ«, »æ«, »Œ« und »œ« werden wie
	   # zweibuchstabige Sequenzen behandelt.
	   #
	   # Alle nicht explizit erwähnten Zeichen werden anhand ihres
	   # Unicode-Wertes sortiert.
	   #
	   # Beachte, daß beim Sortieren der Daten im »wortliste«-Projekt
	   # die tertiäre Sortierung nicht zum Einsatz kommt, da es keine
	   # Dopplungen gibt, die sich nur durch die Groß- und
	   # Kleinschreibung unterscheiden.

     	   # Die primäre Sortierung vergleicht Zeichen in
	   # Kleinschreibung ohne diakritischen Akzent (oder
	   # Äquivalente; wir bilden beispielsweise »ð« auf »d« ab).
	   #
           $s1 =~ tr[àáâãäåçðèéêëìíîïñòóôõöøšſþùúûüýÿž]
	            [aaaaaacdeeeeiiiinoooooosstuuuuyyz];

           if ($hat_ligatur) {
             $s1 =~ s/ß/ss/g;
             $s1 =~ s/æ/ae/g;
             $s1 =~ s/œ/oe/g;
           }

	   # Die sekundäre Sortierung ordnet die Akzente
	   # bzw. Äquivalente (zuerst Grundbuchstaben, dann Zeichen
	   # mit Diakritika).
	   #
	   # Die Formatierung des ersten und zweiten Arguments von
	   # »tr« muß identisch sein!
	   $s2 =~ tr
[aàáâãäå
b
cç
dð

eèéêë
fgh
iìíîï
jklm

nñ
oòóôõöø
pqr
sſš

tþ
uùúûü
vwx
yýÿ
zž]
[\x{200}\x{201}\x{202}\x{203}\x{204}\x{205}\x{206}
\x{210}
\x{220}\x{221}
\x{230}\x{231}

\x{240}\x{241}\x{242}\x{243}\x{244}
\x{250}\x{251}\x{252}
\x{260}\x{261}\x{262}\x{263}\x{264}
\x{270}\x{271}\x{272}\x{273}

\x{280}\x{281}
\x{290}\x{291}\x{292}\x{293}\x{294}\x{295}\x{296}
\x{300}\x{301}\x{302}
\x{310}\x{311}\x{313}

\x{320}\x{321}
\x{330}\x{331}\x{332}\x{333}\x{334}
\x{340}\x{341}\x{342}
\x{350}\x{351}\x{352}
\x{360}\x{361}];

	   if ($hat_ligatur) {
	     $s2 =~ s/ß/\x{310}\x{312}/g;
	     $s2 =~ s/æ/\x{200}\x{245}/g;
	     $s2 =~ s/œ/\x{290}\x{245}/g;
           }

	   # Die tertiäre Sortierung gibt die Groß- und
	   # Kleinschreibung an (zuerst Klein-, dann Großbuchstaben).
	   #
	   if ($hat_ligatur) {
	     $s3 =~ s/ß/ss/g;
	     $s3 =~ s/Æ/Ae/g;
	     $s3 =~ s/Œ/Oe/g;
	     $s3 =~ s/æ/ae/g;
	     $s3 =~ s/œ/oe/g;
           }

	   # Die Formatierung des ersten und zweiten Arguments von
	   # »tr« muß identisch sein!
	   $s3 =~ tr
[A-ZÀÁÂÃÄÅÇÐÈÉÊËÌÍÎÏÑÒÓÔÕÖØŠÞÙÚÛÜÝŸŽ
a-zàáâãäåçðèéêëìíîïñòóôõöøšþùúûüýÿž]
[a-zaaaaaacdeeeeiiiinoooooostuuuuyyz
A-ZAAAAAACDEEEEIIIINOOOOOOSTUUUUYYZ];

	   # Wir konstruieren jetzt den Sortierschlüssel (als
	   # Rückgabewert dieser »map«-Funktion).  Zu diesem Zweck
	   # hängen wir die Sortierschlüssel mit dem Wort zusammen
	   # (wobei das Wort am Schluß kommt), unter Verwendung von
	   # Nullbytes als Feldtrenner.
	   #
	   #   http://www.sysarch.com/Perl/sort_paper.html
	   #
	   # Wir verwenden Typ »a« für das Originalwort in »pack«,
	   # damit es unverändert bleibt und kein Padding angewandt
	   # wird.
	   pack("A*xA*xA*xa*", $s1, $s2, $s3, $_);
         }
         keys %wortliste) {
  print $wortliste{$erstes_feld};
}

# eof
