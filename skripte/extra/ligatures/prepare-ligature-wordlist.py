#!/usr/bin/env python3

"""
Make patgen wordlist for ligature patterns.

See LIESMICH.md for a detailed documentation (german).
"""


"""
The MIT License (MIT)

Copyright (c) 2016 Lukas Sommer

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

# Transformed to Python 3 by Günter Milde


import re
import argparse


def make_patgen_input():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        'input',
        help="wordlist (UTF8 encoded) in "
             "the file format of the Trennmuster project.")
    parser.add_argument(
        'output',
        help="wordlist (UTF8 encoded) as "
             "simple word list with a dash that marks all positions where "
             "ligatures have to be suppressed. This file can be used as "
             "input for patgen. Attention: Existing files will be "
             "overwritten.")
    parser.add_argument(
        'log',
        help="log (UTF8 encoded) of the operations. "
             "Attention: Existing files will be overwritten.")
    args = parser.parse_args()

    # open the files
    wordlist_file =open(args.input, "r")
    patgen_file =open(args.output, "w")
    log_file =open(args.log, "w")

    # initialize variables for log data
    double_meaning = []
    unclassified = []
    inconsistent = []

    # process data from the word list
    print("Please wait while processing data...")
    for line in wordlist_file:
        # remove comments
        simplified_line = line.split("#")[0]
        # test for non-classified things: U+00B7 MIDDLE DOT
        if "·" in simplified_line:
            unclassified.append(line)
            continue
        # remove whitespace
        simplified_line = simplified_line.strip()
        # remove information about special hyphenations following 1901
        # orthography (Not useful because most programs makes the
        # hyphenation automatically, but scripts access normally to the
        # non-hyphenated underlying data.)
        # We use *? instead of *, because * is greedy,
        # but *? is reluctant/non-greedy, which makes sure that we do not
        # match various blocks at the same time.
        simplified_line = re.sub(
            r"(\{)(.*?)/.*?\}",
            r"\2",
            simplified_line)
        # remove placeholder content of empty fields (like “-2-” for
        # an empty field at column 2)
        simplified_line = re.sub(
            r"\-[0-9]\-",
            r"",
            simplified_line)
        # remove hyphenations within morphemes (not useful for
        # ligature setting)
        simplified_line = simplified_line.replace("-", "")
        # remove markers for bad hyphenation quality
        # (not useful for ligature setting)
        simplified_line = simplified_line.replace(".", "")
        # use “-” as sign for morpheme boundaries (“<”, “>”, “=”)
        # (“-” is guaranteed to not occur in the string, because it is
        # yet filtered out), which will later also be recognized by patgen.
        simplified_line = simplified_line.replace("<", "-")
        simplified_line = simplified_line.replace(">", "-")
        simplified_line = simplified_line.replace("=", "-")
        # remove duplicate morpheme boundaries
        while simplified_line.count("--") > 0:
            simplified_line = simplified_line.replace("--", "-")
        # test for double meaning
        inconsistent_double_meaning = False
        stop_loop = False
        while not stop_loop:
            my_match_object = re.search(
                r"\[(.*?)/(.*?)\]",
                simplified_line)
            if my_match_object:
                # double meaning found
                if my_match_object.group(1) == my_match_object.group(2):
                    # ligature setting is identical for both meaning
                    # We substitute it by a single meaning.
                    simplified_line = re.sub(
                        r"\[(.*?)/(.*?)\]",
                        r"\1",
                        simplified_line)
                else:
                    # ligature setting is not identical for both meaning
                    inconsistent_double_meaning = True
                    stop_loop = True
            else:
                # no double meaning found
                stop_loop = True
        if inconsistent_double_meaning:
            double_meaning.append(line)
            continue
        # transform into an array
        my_array = simplified_line.split(";")
        # remove the first column (does not contain ligature data,
        # but only the non-hyphenated word)
        if len(my_array) > 0:
            my_array.pop(0)
        # remove empty fields
        while "" in my_array:
            my_array.remove("")
        # nothing to do if there is no data (like on empty lines)
        if len(my_array) == 0:
            continue
        # test if there are differences between the remaining fields
        if len(my_array) != my_array.count(my_array[0]):
            inconsistent.append(line)
            continue
        # write to file
        patgen_file.write(my_array[0].lower() + "\n")

    # close wordlist_file and patgen_file
    wordlist_file.close()
    patgen_file.close()

    # write log
    log_file.write(
        "The following entries "
        "with ambiguous morpheme boundaries "
        "have been ignored:\n")
    for entry in double_meaning:
        log_file.write(entry)
    log_file.write(
        "\n\nThe following entries "
        "with unclassified hyphenation points "
        "have been ignored:\n")
    for entry in unclassified:
        log_file.write(entry)
    log_file.write(
        "\n\nThe following entries "
        "with inconsistencies between different orthographies "
        "have been ignored:\n")
    for entry in inconsistent:
        log_file.write(entry)

    # close log file
    log_file.close()

    # exit
    print("Done.")


make_patgen_input()
