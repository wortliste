Erstelle eine Patgen-Wortliste für Ligaturmuster




Einführung
==========

Einige Buchstabenkombinationen sind eine Herausforderung für die Gestaltung von
Schriftarten. Ein gutes Beispiel ist die f‑i‑Kombination. Das kleine f ist oft
ausladend gestaltet, und so entsteht eine unschöne Lücke zwischen dem kleinen f
und dem kleinen i. Viele Schriftarten lösen dieses Problem durch Ligaturen. Die
Buchstaben werden verschmolzen; so wird die Lücke vermieden und das Schriftbild
wirkt harmonischer. Im Deutschen gibt es nun viele zusammengesetzte Wörter.
Ligaturen erschweren hier aber das Lesen. Deshalb wird bei deutschen Texten
*in solchen Fällen* keine Ligatur gesetzt.




Umsetzung
=========

Ligaturen werden überall dort gesetzt, wo sie nicht durch einen Bindehemmer
(Unicode-Zeichen U+200C) unterdrückt werden. Nun kann man natürlich die
Bindehemmer per Hand einfügen. Aber es ist komfortabler, wenn man – so wie auch
bei der Worttrennung – auf eine automatische Lösung zurückgreifen kann. Dieses
Skript erstellt aus einer Wortliste im Format des Trennmuster-Projekts eine
Wortliste für Patgen, um Ligaturmuster zu erzeugen. Die Muster markieren dann
die Stellen, an denen Ligaturen unterdrückt werden müssen.


Aufruf von der Kommandozeile
----------------------------
Das Skript muss mit drei Parametern aufgerufen werden.

* input: Wortliste im Format des Trennmuster-Projekts (UTF‑8-kodiert)

* output: Wortliste als einfache Wortliste, in der ein Bindestrich die Stellen
markiert, an denen Ligaturen unterdrückt werden müssen (UTF‑8-kodiert). Achtung:
Bereits existierende Dateien werden überschrieben.

* log: Protokoll der Operationen (UTF-8-kodiert). Achtung: Bereits existierende
Dateien werden überschrieben.

Das Trennmuster-Repository stellt neben der normalen Wortliste „wortliste“ noch
weitere Wortlisten für spezielle Zwecke, so wie „pre-1901“. Sie alle eignen
sich als Eingabe für dieses Skript.

Die Ausgabedatei des Skripts kann als Eingabedatei für Patgen verwendet werden.

* Eine Kopie der Datei daten/german.tr erstellen und den Inhalt der ersten
Zeile „ 2 2“ durch „ 1 1“ ersetzen (bei der Ligaturunterdrückung gibt es –
anders als bei der Silbentrennung – keinen Mindestabstand von zwei Zeichen zum
Wortanfang oder Wortende)

* Die beiden Dateien als Eingabe für
„skripte/trennmuster/make-full-pattern.sh“ verwenden, um mittels Patgen die
Muster zu erzeugen.




Eigenschaften
=============


Unterstützung für *alle* Ligaturen
----------------------------------
Das Skript schlägt einen Bindehemmer an allen Morphemgrenzen vor, und zwar
unabhängig davon, ob die unterdrückte Ligatur gebräuchlich ist. Somit wird beim
Wort „Auflage“ die häufig anzutreffende fl‑Ligatur durch einen Bindehemmer
unterdrückt. Aber auch beim Wort „Aufgabe“ wird zwischen f und g ein Bindehemmer
gesetzt, obwohl eine fg‑Ligatur nicht gebräuchlich ist. Dieses Vorgehen hat den
Nachteil, dass wohl viele Bindehemmer gesetzt werden, die keinen Effekt haben,
weil die Schriftart diese Ligaturen überhaupt nicht enthält. Dieses Vorgehen hat
aber den Vorteil, dass es im Grundsatz für alle Schriftarten funktioniert – auch
dann, wenn Schmuckligaturen verwendet werden oder die Schriftart exotische
Ligaturen enthält.


Keine Unterstützung für Unicode-Ligaturen
-----------------------------------------
Unicode enthält zwar eine Handvoll Codepoints für Ligaturen, dies aber nur aus
Gründen der Kompatibilität mit älteren Zeichensätzen. Diese Gruppe von Ligaturen
ist nicht vollständig; es gibt OpenType-Schriften, die weitaus mehr Ligaturen
enthalten. Die technisch saubere Lösung ist es, Smartfont-Technologie wie
OpenType für den Ligatursatz zu nutzen, und nicht die Ligatur-Codepoints. Dieses
Skript hat daher keine Unterstützung für Unicode-Ligaturen.


Keine Unterstützung für Unicode-Normalisierung
----------------------------------------------
Dieses Skript führt keine Unicode-Normalisierung durch. Wenn eine bestimmte
Uniocode-Normalisierungsform nötig ist, muss dies außerhalb dieses Skripts
sichergestellt werden. Das betrifft hauptsächlich die Groß- und Kleinbuchstaben
der deutschen Umlaute und einige wenige Buchstaben mit Akzent.


Groß- und Kleinschreibung
-------------------------
Das Skript wandelt alle Großbuchstaben in Kleinbuchstaben um, so dass das Muster
wie üblich nur mit Kleinbuchstaben erzeugt wird.

Hinweis: Viele Programmierbibliotheken wandeln das große Eszett (ẞ) nicht
korrekt in das kleine Eszett um. Das sollte bei der späteren Verwendung des
Musters bedacht werden.

Hinweis: Die Wortliste des Trennmuster-Projekts enthält kein langes S (ſ). Das
sollte bei der späteren Verwendung des Musters bedacht werden.


Nur ein einziges Muster für alle Rechtschreibvarianten
------------------------------------------------------
Die Ausgabedatei enthält sowohl die Einträge in alter wie die Einträge in neuer
Rechtschreibung, sowohl in der ß-Variante als auch in der ss-Variante.


Satzregeln
----------
Der Ligatursatz ist nicht Bestandteil der amtlichen Rechtschreibregeln, sondern
eine typographische Konvention.

* Grundsatz: Ligaturen werden innerhalb von Morphemen gesetzt. („Stoff“ mit
ff‑Ligatur) Zwischen Morphemen werden hingegen keine Ligaturen gesetzt.
(„Kopfläuse“ ohne fl‑Ligatur)

* Ausnahme: Wenn eine Abkürzung auf eine Ligatur endet, dann wird diese Ligatur
gesetzt, auch wenn sie über eine Morphemgrenze reicht. („Auflage“ ohne
fl‑Ligatur, aber „Aufl.“ mit fl‑Ligatur)

* Ausnahme: Bei Suffixen auf „i“ („-ig“, „-isch“ …) wird die Ligatur trotzdem
gesetzt. („häuf|ig“ mit fi‑Ligatur).

* Bei den anderen Suffixen gehen die Meinungen auseinander und es gibt keine
einheitliche Praxis. Dies betrifft:
    * Flexionssuffixe, die nicht mit einer Silbengrenze zusammenfallen: „[er]
    kauf|t“
    * Flexionssuffixe, die mit einer Silbengrenze zusammenfallen: „[er] kauf|te“
    * weitere Suffixe: „käuf|lich“

Die grundsätzliche Arbeitsweise dieses Skripts ist simpel: Überall, wo in der
Wortliste „<“, „>“ und „=“ auftreten, werden Ligaturen unterdrückt – nicht
jedoch dort, wo „-“ auftritt.

Die Wortliste enthält für jedes aufgenommene Wort alle Trennstellen, und diese
Trennstellen sind klassifiziert. Beispiel: Auf<la-ge. Die erste Trennstelle
ist eine Morphemgrenze; sie trennt die Vorsilbe „auf“ und das Wort „Lage“. Die
zweite Trennstelle innerhalb von „Lage“ ist hingegen keine Morphemgrenze. Bei
den Suffixen werden zwar einige als Morphemgrenzen gekennzeichnet
(„herz>lich“), nicht jedoch beispielsweise Flexionssuffixe („[Er] kauf-te
[Brot.]“).

Nicht jede Morphemgrenze ist auch eine Trennstelle. Morphemgrenzen, die keine
Trennstelle sind, werden in der Wortliste überhaupt nicht gekennzeichnet
(beispielsweise Flexionssuffixe: „[Er] kauft [Brot.]“ ohne gekennzeichnete
Morphemgrenze, obwohl „t“ ein Flexionssuffix für die Wurzel „Kauf“ ist).

Bei Ausfall eines Konsonanten in der alten Rechtschreibung (Schiffahrt) wird die
Ligatur nicht unterdrückt.

Im Ergebnis wird eine Ligatur dort unterdrückt, wo eine Morphemgrenze ist, die
gleichzeitig auch eine Trennstelle ist, mit Ausnahmen vor allem bei
Flexionssuffixen. Also wird „Aufteilung“ ohne ft‑Ligatur gesetzt, „[er] kaufte“
hingegen mit ft‑Ligatur. Weniger technisch ausgedrückt: Ligaturen werden
zwischen unverbunden gesprochenen, sinntragenden Einheiten unterdrückt.
Flexionssuffixe gelten als verbunden gesprochen.

Weiterführende Informationen:

* [Dokumentation von Selnolig](https://mirrors.ctan.org/macros/luatex/latex/selnolig/selnolig.pdf)
Selnolig bietet automatischen Ligatursatz für Tex, arbeitet aber nicht mit
Mustern, sondern mit Regeln. Die Dokumentation ist außergewöhnlich gut und
diskutiert viele Zweifelsfälle sehr detailliert.
* Einige Artikel auf *typografie.info*
    * [Wann setzt man im Deutschen Ligaturen und wann nicht?](https://www.typografie.info/3/faq.htm/wann-setzt-man-im-deutschen-ligaturen-und-wann-nicht-r28/)
    * [Ligaturen – Alles, was man wissen muss](https://www.typografie.info/3/artikel.htm/wissen/ligaturen-alles-was-man-wissen-muss-r96/)
    * [Tipps zum Ligatursatz in InDesign ](https://www.typografie.info/3/artikel.htm/wissen/tipps-zum-ligatursatz-in-indesign-r180/)
    * [Wie setzt man das lange s im klassischen Fraktursatz?](https://www.typografie.info/3/faq.htm/wie-setzt-man-das-lange-s-im-klassischen-fraktursatz-r12/)
    Hier geht es zwar nicht um Ligaturen, sondern um das lange S, aber
    es gibt durchaus Analogien.


Ignorierte Einträge
-------------------
Einige Einträge werden ignoriert:

* Uneindeutige Einträge (also Eintrage, die „[…]“ enthalten, weil sie bei
gleicher Schreibweise verschiedene Deutungen zulassen) werden dann ignoriert,
wenn der Ligatursatz für die beiden Deutungen unterschiedlich ist.

* Nicht vollständig klassifizierte Einträge (also Einträge, die U+00B7 MIDDLE
DOT „·“ enthalten) werden ignoriert.

* Inkonsistente Einträge (also Einträge, die nicht in allen
Rechtschreibvarianten dieselben Positionen für „<“, „>“ und „=“ aufweisen)
werden ignoriert.

Dies wird in der log-Datei protokolliert. In der Praxis werden nur sehr wenige
Einträge ignoriert.


Lizenz des Skripts und dieser Dokumentation
-------------------------------------------
The MIT License (MIT)

Copyright (c) 2016 Lukas Sommer

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
