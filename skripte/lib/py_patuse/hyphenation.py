#!/usr/bin/env python3
"""Hyphenation using a pure Python implementation of Frank Liang's algorithm.

    This module provides a class to hyphenate words.

    `Hyphenator.split_word(word)` takes a string (the word), and returns a
    list of parts that can be separated by hyphens.

    >>> from hyphenation import Hyphenator
    >>> hyphenator = Hyphenator(pattern_file='en-US.pat')
    >>> hyphenator.split_word("hyphenation")
    ['hy', 'phen', 'ation']
    >>> hyphenator.hyphenate_word("supercalifragilisticexpialidocious", '-')
    'su-per-cal-ifrag-ilis-tic-ex-pi-ali-do-cious'
    >>> hyphenator.hyphenate_word("project")
    'pro­ject'

    based on https://nedbatchelder.com/code/modules/hyphenate.py
    by Ned Batchelder, July 2007.

    Version 2.x © 2013, 2020 Günter Milde
"""

# Changelog:
#
# v 1    Original by Ned Batchelder
# v 2    Internationalization (external pattern files, Unicode)
# v 2.1  Converted to Python 3.
# v 2.2  Bugfix: remove spurious newlines in printout.
# v 2.3  More robust pattern reading. Support OpenOffice pattern files.
#        Sort globbing results before selecting the last pattern file.
#        Diagnostic options -v and --show-patterns.
#
# See also the independently developed https://pyphen.org/

import re, argparse, sys, os, glob

__version__ = '2.2 2020-04-07'

class Hyphenator(object):
    def __init__(self, pattern_file, exceptions=''):
        self.tree = {}
        with open(pattern_file) as lines:
            for pattern in self.yield_patterns(lines):
                self._insert_pattern(pattern)

        self.exceptions = {}
        for ex in exceptions.split():
            # Convert the hyphenated pattern into a point array for use later.
            self.exceptions[ex.replace('-', '')] = [0] + [ int(h == '-')
                                            for h in re.split(r"[^-]", ex) ]

    def yield_patterns(self, lines):
        """
        Yield hyphenation patterns from a sequence of strings
        (the content of a pattern file).
        Discard comments and metadata.

        TeX
          Patterns may be argument of ``\patterns{…}`` macro
          (expected on a separate line). Comment sign "%".
        OpenOffice/LibreOffice
          Patterns preceded by encoding and optional constants in UPPERCASE.
          Comment sign "#".

        >>> def extract_patterns(lines):
        ...   return ' '.join(hyphenator.yield_patterns(lines))
        >>> extract_patterns(['UTF-8',
        ...                   'LEFTHYPHENMIN 2',
        ...                   'a1 b2 # comment', 'c3'])
        'a1 b2 c3'

        >>> extract_patterns(['a1 b2 % comment', 'c3'])
        'a1 b2 c3'

        >>> extract_patterns(['% leading comment\n',
        ...                   '\message{German Lyrics patterns}\n',
        ...                   '\patterns{%\n',
        ...                   '.a2b3a\n'
        ...                   '}\n'])
        '.a2b3a'

        """
        oo_preamble = True  # check for OpenOffice preamble code

        for line in lines:
            # skip comments
            line = re.split('[%#]', line)[0]
            # skip leading CONSTANTS in OpenOffice patterns
            if oo_preamble:
                if line.upper() == line:
                    continue
                else:
                    oo_preamble = False
            # skip lines containing TeX code
            if re.match(r' *\\[a-zA-Z]', line):
                continue
            # remove brackets
            line = line.strip('{} \n')
            # there may be several patterns on a line:
            for word in line.split():
                yield word

    def _insert_pattern(self, pattern):
        # Convert the a pattern like 'a1bc3d4' into a string of chars 'abcd'
        # and a list of points [ 1, 0, 3, 4 ].
        chars = re.sub('[0-9]', '', pattern)
        points = [ int(d or 0) for d in re.split('[^0-9]', pattern) ]

        # Insert the pattern into the tree.  Each character finds a dict
        # another level down in the tree, and leaf nodes have the list of
        # points.
        t = self.tree
        for c in chars:
            if c not in t:
                t[c] = {}
            t = t[c]
        t[None] = points

    def split_word(self, word, lmin=2, rmin=2):
        """ Given a word, returns a list of pieces, broken at the possible
            hyphenation points.
        """
        # Short words aren't hyphenated.
        if len(word) < (lmin + rmin):
            return [word]
        # If the word is an exception, get the stored points.
        if word.lower() in self.exceptions:
            points = self.exceptions[word.lower()]
        else:
            work = '.' + word.lower() + '.'
            points = [0] * (len(work)+1)
            for i in range(len(work)):
                t = self.tree
                for c in work[i:]:
                    if c in t:
                        t = t[c]
                        if None in t:
                            p = t[None]
                            for j in range(len(p)):
                                points[i+j] = max(points[i+j], p[j])
                    else:
                        break
            # No hyphens in the first `lmin` chars or the last `rmin` ones:
            for i in range(lmin):
                points[i+1] = 0
            for i in range(rmin):
                points[-2-i] = 0
            # example for lmin == rmin == 2:
            # points[1] = points[2] = points[-2] = points[-3] = 0

        # Examine the points to build the pieces list.
        pieces = ['']
        for c, p in zip(word, points[2:]):
            pieces[-1] += c
            if p % 2:
                pieces.append('')
        return pieces

    def hyphenate_word(self, word, hyphen='­', lmin=2, rmin=2):
        """ Return `word` with (soft-)hyphens at the possible
            hyphenation points.
        """
        return hyphen.join(self.split_word(word, lmin, rmin))


    def hyphenate_text(self, text, hyphen='­', lmin=2, rmin=2):
        # Text zerlegen: finde (ggf. leere) Folgen von nicht-Wort-Zeichen
        # gefolgt von Wort-Zeichen. Der Iterator liefert Match-Objekte, mit
        # den Untergruppen 0: nicht-Wort und 1: Wort.
        tokens = re.finditer(r"([\W0-9_]*)(\w*)", text, flags=re.UNICODE)
        # Konvertierung und Zusammenfügen
        parts = [match.groups()[0] # nicht-Wort Zeichen
                 + self.hyphenate_word(match.groups()[1], hyphen, lmin, rmin)
                 for match in tokens]
        return ''.join(parts)


# default_pattern_file = '../../../muster/dehyphn-x/dehyphn-x-*.pat'
# default_pattern_file = '../../../muster/dehyphn-x-fugen/dehyphn-x-fugen-*.pat'

mydir = os.path.dirname(os.path.abspath(os.path.realpath(__file__)))
patterndir = os.path.normpath(os.path.join(mydir, '../../../muster'))

# print(mydir, patterndir)

lang = 'en'  # default language

pattern_files = {'en': os.path.join(mydir, 'en-US.pat'),
                 'de': os.path.join(patterndir,
                                    'dehyphn-x/dehyphn-x-*.pat'),
                 'de-1901': os.path.join(patterndir,
                                         'dehypht-x/dehypht-x-*.pat'),
                 'de-x-gesang': os.path.join(patterndir,
                                         'dehyphn-x-gesang/dehyphn-x-*.pat'),
                }

if __name__ == '__main__':

    usage = ('%(prog)s [options] [words to be hyphenated]\n'
             '%(prog)s [options] <input-file >output-file'
            )

    parser = argparse.ArgumentParser(usage=usage)
    parser.add_argument('WORDs', nargs='*')
    parser.add_argument('-f', '--pattern-file',
                        help='pattern file, default: depending on "--lang"',
                        default='')
    parser.add_argument('-e', '--exception-file',
                        help='file of hyphenated words (exceptions), '
                        'default: None',
                        default='')
    parser.add_argument('--hyphen',
                        help=r'hyphenation marker,'
                        r' default: SOFT HYPHEN (\u00AD)',
                        default='­')
    parser.add_argument('-l', '--lang',
                        help='language, default: "en".',
                        default='en')
    parser.add_argument('--lmin',
                        help='unhyphenated characters at start of word,'
                        ' default: 2',
                        default='2')
    parser.add_argument('--rmin',
                        help='unhyphenated characters at end of word,'
                        ' default: 2',
                        default='2')
    parser.add_argument('-t', '--test', action="store_true", default=False,
                        help='compare input and reconstruction,'
                        ' report differences')
    parser.add_argument('--self-test', action="store_true", default=False,
                        help='short, self-contained self-test ')
    parser.add_argument('-v', '--verbose', action="store_true", default=False,
                        help='print info about used patterns')
    parser.add_argument('--show-patterns', action="store_true", default=False,
                        help='print used patterns')

    args = parser.parse_args()

    hyphen = args.hyphen
    lmin = int(args.lmin)
    rmin = int(args.rmin)

    if args.exception_file:
        ex_file = open(exception_file)
        exceptions = ex_file.read()
        ex_file.close()
    elif args.self_test:
        exceptions = "present presents project projects reci-procity"
    else:
        exceptions = ''


    try:
        pattern_file = args.pattern_file or pattern_files[args.lang]
    except KeyError:
        print(f'No pattern file defined for language "{args.lang}".',
              file=sys.stderr)
        sys.exit()
    try:
        pattern_file = sorted(glob.glob(pattern_file))[-1]
    except IndexError:
        print(f'Pattern file {pattern_file} not found!', file=sys.stderr)
        sys.exit()

    hyphenator = Hyphenator(pattern_file, exceptions)
    del exceptions

    if args.WORDs:
        lines = args.WORDs
    else:
        lines = sys.stdin

    if args.self_test:
        import doctest
        doctest.testmod(verbose=True)
        sys.exit()

    if args.verbose:
        print('patterns:', pattern_file, file=sys.stderr)

    if args.show_patterns:
        with open(pattern_file) as raw_patterns:
            for pattern in hyphenator.yield_patterns(raw_patterns):
                print(pattern)
        sys.exit()

    if args.test:
        pairs = ((line.strip(),
                  hyphenator.hyphenate_text(line.replace('-', '').strip(),
                                        hyphen='-', lmin=lmin, rmin=rmin))
                  for line in lines)

        for (line, line2) in pairs:
            if line2 != line:
                # print repr(line), '->', repr(line2)
                print(line, '->', line2)
        sys.exit()

    line = ''  # allow check for trailing newline below
    for line in lines:
        print(hyphenator.hyphenate_text(line, hyphen=hyphen,
                                        lmin=lmin, rmin=rmin),
              end='')
    if not line.endswith('\n'):
        print()
