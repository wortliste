#!/usr/bin/env python3
# -*- coding: utf8 -*-
# :Copyright: © 2014 Günter Milde.
#             Released without warranty under the terms of the
#             GNU General Public License (v. 2 or later)
# :Id: $Id:  $

# long_s_conversion.py: Demonstrator für die Lang-S Wandlung
# =============================================================================

"""Rund-S nach Lang-S Wandlung über "hyphenation patterns"."""

import os, sys, codecs, re, argparse
from hyphenation import Hyphenator


# Konfiguration
# -------------

# Lang-s Pseudo-Trennmuster mit "s-" statt "ſ" (ausſagen -> auss-agen)
# (mit "make de-x-long-s" im Wurzelverzeichnis der wortliste generiert)::

default_pfile = os.path.normpath(os.path.join(os.path.dirname(__file__),
                                    '../../../de-long-s/de-x-long-s.pat'))

# Ausnahmen
# ---------

# ſ steht auch am Ende von Abkürzungen, wenn es im abgekürzten Wort steht
# (Abſ. - Abſatz/Abſender, (de)creſc. - (de)creſcendo, daſ. - daſelbst ...)
# s steht auch in der Mitte von Abkürzungen, wenn es im abgekürzten Wort steht
# (Ausg. - Ausgang/Ausgabe, Hrsg. - Herausgeber, ...)
# ::

exceptions = ('Abſ', # Abſatz/Abſender
              'beſ',  # beſonders
              'coſ',  # Ko<ſinus
            # u'daſ',   # da<ſelbſt (nicht von Artikel "das" zu unterscheiden!)
              'Diſſ',  # Diſſertation
              'Hſ',    # Handschrift
              'Maſſ',  # Maſſachuſetts
            # u'Miſſ',  # Miſſiſippi (nicht von Miſs (Frln.) zu unterscheiden)
              # TODO: N-Z
             )
exceptions = dict((ex.replace('ſ', 's'), ex) for ex in exceptions)

# Konvertierung mit Hyphenator::

def transformiere(wort, hyphenator):

    if 's' not in wort:
        return wort
    if wort in exceptions:
        return exceptions[wort]

    wort = hyphenator.hyphenate_word(wort, hyphen='-', lmin=1, rmin=1)

    # Wandle "s-" in "ſ" (auss-agen -> ausſagen):

    return wort.replace('s-', 'ſ').replace('S-', 'S')

def transformiere_text(text, hyphenator):
    # Text zerlegen: finde (ggf. leere) Folgen von nicht-Wort-Zeichen
    # gefolgt von Wort-Zeichen. Der Iterator liefert Match-Objekte, mit
    # den Untergruppen 0: nicht-Wort und 1: Wort.
    it = re.finditer(r"([\W0-9_]*)(\w*)", text, flags=re.UNICODE)
    # Konvertierung und Zusammenfügen
    parts = [match.groups()[0] # nicht-Wort Zeichen
             + transformiere(match.groups()[1], hyphenator)
             for match in it]
    return ''.join(parts)

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('TEXT', nargs='*',
                        help='text to be transformed (read from stdin if empty)')
    parser.add_argument('-f', '--pattern-file', dest='pattern_file',
                        help='Pattern file, Default "%s"' % default_pfile,
                        default=default_pfile)
    parser.add_argument('-t', '--test', action="store_true", default=False,
                        help='Vergleiche Eingabe mit Rekonstruktion, '
                        'Melde Differenzen.')

    args = parser.parse_args()

    h_Latf = Hyphenator(args.pattern_file)

    if args.TEXT:
        lines = [' '.join(args.TEXT)]
    else:
        lines = sys.stdin

    if args.test:
        for line in lines:
            line2 = transformiere_text(line.replace('ſ', 's'), h_Latf)
            if line2 != line:
                print(line.strip(), '->', line2, end=' ')
        sys.exit()

    for line in lines:
        print(transformiere_text(line, h_Latf), end=' ')
