#!/usr/bin/env python3
# :Copyright: © 2023 Günter Milde.
#             Released without warranty under the terms of the
#             GNU General Public License (v. 2 or later)
# :Id: $Id:  $

# hyphenate_NDF_words.py
# =======================================================================
#
# ::

"""Trenne Wörter mit combining characters mittels "hyphenation"-Algorithmus.

Eingabe: Wörter im wortliste-Format.

Ausgabe: Wörter, die unterschiedlich getrennt werden, wenn Buchstaben mit
         diakritischen Zeichen entweder als vorgefertigte Zeichen oder
         als Basisbuchstabe + kombinierende Zeichen werden.


Bsp: python3 hyphenate_NDF_words.py < wortliste

¹ Verwendet als Voreinstellung "Reformschreibungs" Pattern-Dateien, welche
  über das "make" Ziel `make pattern-refo` im
  Wurzelverzeichnis der Wortliste generiert werden können.

  Für Trennungen nach traditioneller Orthographie (de-1091) oder spezielle Trennungen
  (fugen, major, suffix) muss die Musterdatei über die Optionen angegeben
  werden.
"""

# Doctest: Beispiel für Anwendung als Python-Module
#
# >>> from hyphenate_NDF_words import *
#
# ::

import sys, os, glob, argparse, unicodedata

# path for local Python modules (parent dir of this file's dir)
sys.path.insert(0,
        os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# import patuse, trennstellenkategorisierung
from wortliste import (WordFile, WordEntry, ShortEntry,
                       sortkey_duden, run_filters)
from py_patuse.hyphenation import Hyphenator


# Optionen::

# Die neuesten Pattern-Dateien, welche über die "make"-Ziele
#
#     make pattern-refo
#     make major pattern-refo
#     make fugen pattern-refo
#     make suffix pattern-refo
#
# im Wurzelverzeichnis der wortliste generiert werden
# (--help zeigt das Erstelldatum) ::

# Pfad zum Wurzelverzeichnis ("../../../") unabhängig vom Arbeitsverzeichnis::

root_dir = os.path.relpath(
    os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(
        os.path.abspath(__file__))))) )

patterns = {}
for cat in ('', '-major', '-fugen', '-suffix'):
    try:
        ppath = os.path.join(root_dir, 'muster/dehyphn-x%s/dehyphn-x%s-*.pat'%(cat,cat))
        patterns[cat] = sorted(glob.glob(ppath))[-1]
    except IndexError:
        print('Keine Muster in "%s" gefunden\n'%ppath,
                '(`make %s pattern-refo` mag helfen).'%cat[1:])
        patterns[cat] = ''

parser = argparse.ArgumentParser(description = __doc__,
                                formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('-p', '--patterns',
                    help='Pattern-Datei (alle Trennstellen), '
                    'Vorgabe "%s"'%patterns[''], default=patterns[''])
parser.add_argument('-d', '--pattern-date', metavar='DATUM',
                    help='verwende Standard-Patterns mit Erstellungsdatum '
                    'DATUM.')
parser.add_argument('-k', '--kurzformat', action='store_true',
                    help='Eingabe ist im Kurzformat')
parser.add_argument('-l', '--language', metavar='SPRACHE',
                    help='Sprachvariante (für Auswahl bei Eingabe '
                            'im Wortliste-Format (Vorgabe: "de-1996").',
                    default="de-1996")
parser.add_argument('-u', '--ungefiltert', action='store_true',
                    help='Ausgabe aller Trennungen')
args = parser.parse_args()


if args.kurzformat:
    eintragsklasse = ShortEntry
else:
    eintragsklasse = WordEntry

if args.pattern_date:
    for cat in patterns:
        argvarname = ('patterns'+cat).replace('-', '_')
        argvar = getattr(args, argvarname)
        if argvar == patterns[cat]: # default unchanged
            ppath = os.path.join(root_dir,
                                    'muster/dehyphn-x%s/dehyphn-x%s-%s.pat'%
                                    (cat, cat, args.pattern_date))
            setattr(args, argvarname, ppath)

# Trenner-Instanzen::

hyphenator = Hyphenator(args.patterns)

print('## Trennung mit patgen Mustern:')
argvarname = 'patterns'.replace('-', '_')
print('#  %-10s %s'
        %('standard', getattr(args, argvarname)))

nr_words = 0
nr_word_diffs = 0
nr_getrennt_diffs = 0


# Trennen und Ausgabe::

# for line in ('Möhre', 'Binnenöse', 'sofort'):
for line in sys.stdin:
    if not line.strip() or line.startswith('#'):
        continue

    nr_words += 1
    word = eintragsklasse(line.strip()).key(lang=args.language)
    word_d = unicodedata.normalize('NFD', word)

    if word != word_d:
        nr_word_diffs += 1
    elif not args.ungefiltert:
        continue

    getrennt = hyphenator.hyphenate_word(word)
    getrennt_d = hyphenator.hyphenate_word(word_d)
    if getrennt != unicodedata.normalize('NFC', getrennt_d):
        nr_getrennt_diffs +=1
    elif not args.ungefiltert:
        continue

    print(word, getrennt, getrennt_d)


# Ausgabe::

if nr_words:
    percentile = nr_getrennt_diffs/nr_words * 100
else:
    percentile = 0

print("")
print(f'# {nr_words} Wörter, davon {nr_word_diffs} mit diakritischen Zeichen.')
print(f'# {nr_getrennt_diffs} ({percentile:.2f}%), unterschiedlich getrennt.')
