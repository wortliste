#!/usr/bin/env python3
# :Copyright: © 2014 Günter Milde.
#             Released without warranty under the terms of the
#             GNU General Public License (v. 2 or later)
# :Id: $Id:  $

# prepare_patch.py: Helfer für kleine Editieraufgaben
# ===================================================
# ::

u"""
Erstelle einen Patch für kleinere Korrekturen der Wortliste.
Ausgangspunkt sind Dateien mit einer Korrektur pro Zeile.
(Zeilen, die mit ``#`` starten, werden ignoriert.)

AKTION ist eine von:
  doppelte:         Einträge mit gleichem Schlüssel entfernen,
  fehleintraege:    Einträge entfernen,
  grossklein:       Großschreibung ändern,
  grossabgleich:    Großschreibung der Trennmuster wie erstes Feld,
  korrektur:        Einträge durch alternative Version ersetzen,
  neu¹:             Einträge hinzufügen,
  reformschreibung: Eintrag in "nur Reformschreibung" ändern,
  zusammenfassen¹:  Sprachvarianten zusammenfassen wenn gleich.

  ¹Für diese Aufgaben ist eine Hin-/Rückwandlung in das "Kurzformat" mit
   skripte/umformatierung.py besser geeignet.
"""

# Die ``<AKTION>.todo`` Dateien im Unterverzeichnis "tasks/" beschreiben das
# jeweils erforderliche Datenformat im Dateikopf.
#
# ::

import optparse, sys, os
from copy import copy, deepcopy


from wortliste import WordFile, WordEntry, join_word, udiff, sortkey_duden

def teste_datei(datei):
    """Teste, ob Datei geöffnet werden kann."""

    try:
        file = open(datei, 'r')
        file.close()
    except:
        sys.stderr.write("Kann '" + datei + u"' nicht öffnen\n" )
        sys.exit()


# Sprachvarianten
# ---------------
# Sprach-Tag nach [BCP47]_::

sprachvariante = 'de-1901'         # "traditionell"
# sprachvariante = 'de-1996'         # Reformschreibung
# sprachvariante = 'de-x-versal'      # ohne ß (Großbuchstaben und Kapitälchen)
# sprachvariante = 'de-1901-x-versal'   # ohne ß (Schweiz oder versal)
# sprachvariante = 'de-1996-x-versal' # ohne ß (Schweiz oder versal)
# sprachvariante = 'de-CH-1901'     # ohne ß (Schweiz) ("süssauer")


# Allgemeine Korrektur (z.B. Fehltrennung)
#
# Format:
#  * ein Wort mit Trennstellen (für "Sprachvariante"):
#  * vollständiger Eintrag (für Wörter mit Sprachvarianten).
#
# ::

def korrektur(wordfile, datei='tasks/korrektur.todo'):
    """Patch aus korrigierten Einträgen"""

    teste_datei(datei)

    korrekturen = {}
    for line in open(datei, 'r'):
        if line.startswith('#'):
            continue
        # Dekodieren, Zeilenende entfernen
        line = line.strip()
        if not line:
            continue
        # Eintrag ggf. komplettieren
        if ';' not in line:
            line = '%s;%s' % (join_word(line), line)
        entry = WordEntry(line)
        key = entry[0]
        entry.regelaenderungen() # teste auf Dinge wie s-t/-st

        korrekturen[key] = entry

    wortliste = list(wordfile)
    wortliste_neu = [] # korrigierte Liste

    for entry in wortliste:
        key = entry[0]
        if key in korrekturen:
            entry = korrekturen.pop(key)
        wortliste_neu.append(entry)

    if korrekturen:
        print(u"Die folgenden Einträge sind nicht in der Wortliste:")
        print(korrekturen) # übrige Einträge

    return (wortliste, wortliste_neu)


# Fehleinträge
# ------------
#::

def fehleintraege(wordfile, datei='tasks/fehleintraege.todo'):
    """Entfernen der Einträge aus einer Liste von Fehleinträgen """

# Fehleinträge aus Datei.
#
# Format:
#   Ein Eintrag/Zeile, mit oder ohne Trennzeichen
#
# ::

    teste_datei(datei)

    # Dekodieren, Zeilenende entfernen, Trennzeichen entfernen
    korrekturen = set(join_word(
                line.strip().replace(';', ' ').split()[0])
                      for line in open(datei, 'r')
                      if line.strip() and not line.startswith('#'))
    wortliste = list(wordfile)
    wortliste_neu = [] # korrigierte Liste
    for entry in wortliste:
        if entry[0] in korrekturen: # nicht kopieren
            korrekturen.discard(entry[0]) # erledigt
        else:
            wortliste_neu.append(entry)

    if korrekturen:
        print('nicht gefunden:')
        for w in korrekturen:
            print(w)

    return (wortliste, wortliste_neu)


# Groß-/Kleinschreibung ändern
# ----------------------------
#
# Umstellen der Groß- oder Kleinschreibung auf die Variante in der Datei
# ``grossklein.todo``
#
# Format:
#   ein Eintrag oder Wort pro Zeile, mit vorhandener Groß-/Kleinschreibung.
#
# ::

def grossklein(wordfile, datei='tasks/grossklein.todo'):
    """Groß-/Kleinschreibung umstellen"""

    teste_datei(datei)

    wortliste = list(wordfile)

    # Dekodieren, Feldtrenner zu Leerzeichen
    korrekturen = [line.replace(';',' ')
                   for line in open(datei, 'r')
                   if not line.startswith('#')]

    # erstes Feld, Trennzeichen entfernen
    korrekturen = [join_word(line.split()[0]) for line in korrekturen
                   if line.strip() and not line.startswith('#')]
    korrekturen = set(korrekturen)
    wortliste_neu = deepcopy(wortliste) # korrigierte Liste

    for entry in wortliste_neu:
        if entry[0] in korrekturen:
            korrekturen.discard(entry[0]) # gefunden
            # Anfangsbuchstabe mit geänderter Großschreibung:
            if entry[0][0].islower():
                anfangsbuchstabe = entry[0][0].title()
            else:
                anfangsbuchstabe = entry[0][0].lower()
            # Einträge korrigieren:
            for i in range(len(entry)):
                if entry[i].startswith('-'): # -2-, -3-, ...
                    continue
                entry[i] = anfangsbuchstabe + entry[i][1:]

    if korrekturen:
        print(korrekturen) # übrige Einträge

    return (wortliste, wortliste_neu)

# Anpassung der Großschreibung der Trennmuster an das erste Feld
# (ungetrenntes Wort). Siehe "wortliste.py" für einen Test auf Differenzen.
# (Differenzen sind größtenteils auf unvorsichtiges Ersetzen mit Texteditor
# zurückzuführen.)
# ::

def grossabgleich(wordfile):
    wortliste = list(wordfile)
    wortliste_neu = deepcopy(wortliste) # korrigierte Liste
    for entry in wortliste_neu:
        # Übertrag des Anfangsbuchstabens
        for i in range(1,len(entry)):
            if not entry[i]:
                continue
            entry[i] = entry[0][0] + entry[i][1:]
    return (wortliste, wortliste_neu)


# Sprachvariante ändern
# ---------------------
#
# Einträge mit allgemeingültiger (oder sonstwie mehrfacher) Sprachvariante
# in "nur in Reformschreibung" (allgemein) ändern.
#
# Format:
#   ein Wort/(Alt-)Eintrag pro Zeile.
#
# ::

def reformschreibung(wordfile, datei='tasks/reformschreibung.todo'):
    """Wörter die nur in (allgemeiner) Reformschreibung existieren"""

    teste_datei(datei)
    # Dekodieren, Zeilenende entfernen
    korrekturen = [line.strip()
                   for line in open(datei, 'r')
                   if not line.startswith('#')
                  ]
    # erstes Feld
    korrekturen = [line.split(';')[0] for line in korrekturen]
    korrekturen = set(korrekturen)

    wortliste = list(wordfile)
    wortliste_neu = [] # korrigierte Liste

    for entry in wortliste:
        if entry[0] in korrekturen:
            key = entry[0]
            wort = entry.get('de-1996')
            if 'ss' in wort:
                entry = WordEntry('%s;-2-;-3-;%s;%s' % (key, wort, wort))
            else:
                entry = WordEntry('%s;-2-;-3-;%s' % (key, wort))
            korrekturen.discard(key) # erledigt
        wortliste_neu.append(entry)

    if korrekturen:
        print(korrekturen) # übrige Einträge
    return wortliste, wortliste_neu


# Getrennte Einträge für Sprachvarianten
# --------------------------------------
#
# Korrigiere fehlende Spezifizierung nach Sprachvarianten, z.B.
#
# - System;Sy-stem
# + System;-2-;Sy-stem;Sys-tem
#
# ::

def sprachvariante_split(wordfile, alt, neu,
                         altsprache='de-1901', neusprache='de-1996'):

    wortliste = list(wordfile)
    wortliste_neu = [] # korrigierte Liste

    for entry in wortliste:
        if len(entry) == 2: # Allgemeine Schreibung
            altwort = entry.get(altsprache)
            neuwort = altwort.replace(alt, neu)
            if altwort != neuwort:
                entry = WordEntry('%s;-2-;3;4' % (join_word(altwort)))
                entry.set(altwort, altsprache)
                entry.set(neuwort, neusprache)
        wortliste_neu.append(entry)
    return (wortliste, wortliste_neu)



# Neueinträge prüfen und vorbereiten
# ----------------------------------
#
# Die in einer Datei (ein Neueintrag pro Zeile) gesammelten Vorschläge auf
# auf Neuwert testen (vorhandene Wörter werden ignoriert, unabhängig von der
# Groß-/Kleinschreibung) und einsortieren.
#
# Akzeptierte Formate:
#
#  * vollständiger Eintrag (für Wörter mit Sprachvarianten oder Kommentaren).
#
#  * ein Wort mit Trennstellen:
#
#    - Schlüssel wird generiert und vorangestellt (durch Semikolon getrennt).
#    - Test auf einfache/häufige Reformänderungen 1996:
#      s-t/-st, {ck/k-k}/c-k
#
# ::

def neu(wordfile, datei='tasks/neu.todo'):
    """Neueinträge prüfen und vorbereiten."""

    teste_datei(datei)
    neue = open(datei, 'r')
    print_korrekturhinweis = False

    wortliste = list(wordfile)
    wortliste_neu = copy(wortliste)
    # vorhandene Einträge:
    words = dict((entry.key().lower(), entry) for entry in wortliste)

    for line in neue:
        if line.startswith('#'):
            continue
        # Dekodieren, Zeilenende entfernen
        line = line.strip()
        if not line:
            continue
        # Eintrag ggf. komplettieren:
        if ';' not in line:
            line = '%s;%s' % (join_word(line), line)
        entry = WordEntry(line)
        key = entry.key().lower()

        # Test auf "Neuwert":
        old = words.get(key)
        if old:
            if options.force:
                wortliste_neu.remove(old)
            else:
                print(key, 'ignored: schon vorhanden',)
                # TODO: Einträge vergleichen:
                if entry == old and entry.comment == old.comment:
                    print('(identisch).')
                else:
                    print('(unterschiedlich)')
                    print(' -', old)
                    print(' +', entry)
                    print_korrekturhinweis = True
                continue

        entry.regelaenderungen() # teste auf Dinge wie s-t/-st
        wortliste_neu.append(entry)
        words[key] = entry

    if print_korrekturhinweis:
        print('Änderungen können mit "--force" erzwungen werden.')
    # Sortieren
    wortliste_neu.sort(key=sortkey_duden)

    return (wortliste, wortliste_neu)


def doppelte(wordfile, use_first=False):
    """Doppeleinträge entfernen (ohne Berücksichtigung der Großschreibung).

    Boolscher Wert `use_first` bestimmt, ob der erste oder der letzte von
    Einträgen mit gleichem Schlüssel in der Liste verbleibt.

    Die neue Liste ist sortiert.
    """
    wortliste = list(wordfile)
    worddict = {}
    for entry in wortliste:
        key = entry[0].lower()
        if use_first and key in worddict:
            continue
        worddict[key] = entry

    wortliste_neu = list(worddict.values()) # korrigierte Liste
    wortliste_neu.sort(key=sortkey_duden)

    print(len(wortliste) - len(wortliste_neu), u"Einträge entfernt")
    return (wortliste, wortliste_neu)


def prune(wortliste):

    wortliste = list(wordfile)
    wortliste_neu = [] # korrigierte Liste

    for entry in wortliste:
        if len(entry) > 2:
            # Felder zusammenfassen:
            entry = copy(entry)
            entry.prune()
        wortliste_neu.append(entry)

    return (wortliste, wortliste_neu)


# Default-Aktion::

if __name__ == '__main__':

# Pfad zu "../../../wortliste" unabhängig vom Arbeitsverzeichnis::

    default_wortliste = os.path.relpath(os.path.join(
        os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(
            os.path.abspath(__file__))))),
        'wortliste'))

# Optionen::

    usage = '%prog [Optionen] AKTION\n' + __doc__

    parser = optparse.OptionParser(usage=usage)
    parser.add_option('-i', '--file', dest='wortliste',
                      help='Eingangsdatei, Vorgabe "%s"'%default_wortliste,
                      default=default_wortliste)
    parser.add_option('-k', '--todofile', dest='todo',
                      help='Korrekturdatei, Vorgabe "tasks/<AKTION>.todo"')
    parser.add_option('-o', '--outfile', dest='patchfile',
                      help='Ausgangsdatei, Vorgabe "%s.patch"'
                             % default_wortliste,
                      default=default_wortliste+'.patch')
    parser.add_option('-f', '--force',
                      help='überschreibe vorhandene Einträge (Aktion "neu").',
                      action="store_true", default=False)

    (options, args) = parser.parse_args()

    if args:
        aktion = args[0]
    else:
        print('Nichts zu tun: AKTION Argument fehlt.', '\n')
        parser.print_help()
        sys.exit()

# Die `Wortliste`::
    wordfile = WordFile(options.wortliste)

    # Da der Aufruf von `wortliste = list(wordfile)` lange dauert, wird er
    # in den Aktionsroutinen nach dem Test auf die Eingabedatei ausgeführt.

# Die Aufgabenliste::

    todofile = options.todo or "tasks/%s.todo" % aktion

# Behandeln::

    if aktion == 'neu':
        (wortliste, wortliste_neu) = neu(wordfile, todofile)
    elif aktion == 'doppelte':
        (wortliste, wortliste_neu) = doppelte(wordfile)
    elif aktion == 'fehleintraege':
        (wortliste, wortliste_neu) = fehleintraege(wordfile, todofile)
    elif aktion == 'grossklein':
        (wortliste, wortliste_neu) = grossklein(wordfile, todofile)
    elif aktion == 'grossabgleich':
        (wortliste, wortliste_neu) = grossabgleich(wordfile)
    elif aktion == 'korrektur':
        (wortliste, wortliste_neu) = korrektur(wordfile, todofile)
    elif aktion == 'reformschreibung':
        (wortliste, wortliste_neu) = reformschreibung(wordfile, todofile)
    elif aktion == 'zusammenfassen':
        (wortliste, wortliste_neu) = prune(wordfile)
    else:
        print('Unbekannte AKTION', '\n')
        parser.print_help()
        sys.exit()

    # (wortliste, wortliste_neu) = sprachvariante_split(wordfile,
    #                                                   'knien', 'kni-en')

# Patch erstellen::

    patch = udiff(wortliste, wortliste_neu, 'wortliste', 'wortliste-neu')
    if patch:
        # print(patch)
        patchfile = open(options.patchfile, 'w')
        patchfile.write(patch)
        print('Änderungen nach %s geschrieben' % options.patchfile)
    else:
        print('keine Änderungen')
