#!/usr/bin/env python3
# :Copyright: © 2014 Günter Milde.
#             Released without warranty under the terms of the
#             GNU General Public License (v. 2 or later)
# :Id:        $Id:  $

# Aussortieren von Wörtern aus einer Liste
# ===============================================================
#
# ::

"""Filtern von Wörtern aus einer Liste.

Eingabe: ein ungetrenntes Wort oder Eintrag im Wortliste-Format pro Zeile.

Ausgabe: Zeilen, die den Filterkriterien entsprechen.

Bsp: python filter_wortliste.py -n -a < german.dict > neu.todo
"""

# Optionen
# --------
#
#   -h, --help            kurze Hilfe
#
#   -i WORTLISTE, --file=WORTLISTE
#                         Vergleichsdatei, Vorgabe "../../../wortliste"
#
#   -v, --vorhandene      Wörter die in der Vergleichsliste
#                         wörtlich enthalten sind bei der Ausgabe
#                         überspringen.
#
#   -a, --ableitungen     Wörter, die sich durch einfache Ersetzungen¹
#                         von Zeichen am Wortende aus Wörtern der
#                         Eingabe bilden lassen bei der Ausgabe
#                         überspringen.
#
#                         Wenn -n und -a gemeinsam angegeben sind, ist die
#                         Vereinigung von Vergleichsliste und Eingabe die
#                         Basis für den Ausschluß ableitbarer Wörter.
#
#   -n, --nichtwoerter    Wörter aus der Liste 'zusatzlisten/nichtwoerter'
#                         überspringen.
#
#   -3, --kurze           Einträge mit 3 oder weniger Buchstaben weglassen
#
#   -u, --umschrift       Nur Wörter mit Nicht-ASCII-Zeichen, in Umschrift.
#
#   --verbose             Aussortierte Wörter nicht überspringen sondern
#                         als Kommentare ausgeben.
#
# ¹vgl. Abschnitt `endungen`_.
#
#
# Abhängigkeiten
# --------------
#
# Benötigte Python Module::

import argparse, os, random, re, sys

# path for local Python modules (parent dir of this file's dir)
sys.path.insert(0,
        os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from wortliste import WordFile, WordEntry, join_word, sortkey_duden, filelines

# Hilfsfunktion zum schnellen Extrahieren eines Schlüssels:
#
# >>> from filter_wortliste import get_key
# >>> get_key('Abitur')
# 'Abitur'
# >>> get_key('Abitur # mit Kommentar')
# 'Abitur'
# >>> get_key('Abitur# mit Kommentar')
# 'Abitur'
# >>> get_key('Abendabitur;-2-;A·bend=ab<i·tur;A·bend=a·b<i-tur # WordEntry-Format')
# 'Abendabitur'
# >>> get_key('A·bend=a·b<i-tur # Kurzformat')
# 'Abendabitur'
#
# ::

def get_key(line):
    """Return key from string in WordEntry, ShortEntry, or unhyphenated format.
    """
    entry = (line.split('#')[0]).strip()
    word = entry.split(';')[0]
    return join_word(word)


# filter_vorhandene()
# -----------------
#
# In der Wortliste vorhanden Einträge aussortieren::

def filter_vorhandene(lines, words):
    bleibt = True
    for line in lines:
        if not line:
            bleibt = True
        # Kommentarzeilen wie vorangehende Zeile behandeln,
        elif not line.startswith('#'):
            bleibt = True
            neukey = get_key(line)
            altkey = ''
            if neukey in words:
                altkey = neukey
                bleibt = False
            elif neukey.title() in words:
                altkey = neukey.title()
                bleibt = False
            elif neukey.lower() in words:
                altkey = neukey.lower()
                bleibt = False
        if bleibt:
            yield line
        elif args.verbose and not line.startswith('#'):
            yield '# %s ∃ %s' % (neukey, altkey)


# filter_nichtwoerter()
# ---------------------
#
# In der Datei "nichtwoerter" vorhande Einträge aussortieren::

def filter_nichtwoerter(lines, nichtwortdatei=''):
    words = set()
    for line in open(nichtwortdatei):
        key = get_key(line)
        if key:
            words.add(key)
    for line in filter_vorhandene(lines, words):
        yield line

# filter_zufaellig()
# ------------------
#
# Enträge zufällig, mit Wahrscheinlichkeit `p` aussortieren::

def filter_zufaellig(lines, p, restdatei):
    score = 0
    for line in lines:
        if not line:
            score = 0
        elif not line.startswith('#'):
            # Kommentarzeilen wie vorangehende Zeile behandeln
            score = random.random()
        if score > p:
            yield line
        elif restdatei:
            restdatei.write(line + '\n')

# endungen
# --------
#
# ``(<alt>, <neu>)`` Paare von Endungen. filter_ableitungen_ sortiert
# Wörter aus, wenn die Ersetzung <neu> → <alt> ein Wort ergibt, dass
# in der Vergleichsliste vorhanden ist.
#
# Die hier angegebenen Ableitungsregeln reduzieren die Wortliste
# (``../../../wortliste``) auf ca. 45%. Für die gegenwärtige Wortliste
# ist dies nicht gewollt, aber neu einzutragende Zusammensetzungen müssen
# nicht mit allen Ableitungsformen aufgenommen werden um korrekte Trennung
# zu erreichen. So läßt sich die Wortliste etwas kompakter und leichter
# handhabbar halten. ::

endungen = [
            ('', 'artig'),
            ('', 'chen'),
            ('', 'chens'),
            ('', 'd'),
            ('', 'de'),
            ('', 'dem'),
            ('', 'den'),
            ('', 'der'),
            ('', 'des'),
            ('', 'e'),
            # ('', 'en'),  # ! gehen ← geh + 'en'
            # ('', 'end'),  # ! -ende, -enden, -ender, -endes, ...
            ('', 'em'),
            ('', 'er'),  # ! -ere, -eren, -erer, -eres, ...
            ('', 'ere'),
            ('', 'eren'),
            ('', 'erer'),
            ('', 'eres'),
            ('', 'erin'),
            ('', 'ern'),
            ('', 'es'),
            # ('', 'ger'), # ! -gere, -geren, -geres, -gerste, ...
            # ('', 'heit'), # ! -heiten, -heits
            ('', 'iat'),
            ('', 'ie'),
            # ('', 'ig'), # ! -ige, -iger, ...
            ('', 'in'),
            ('', 'innen'),
            ('', 'ismus'),
            ('', 'ismen'),
            ('', 'ität'),
            # ('', 'keit'),
            # ('', 'lein'),
            # ('', 'los'),
            ('', 'losem'),
            ('', 'losen'),
            # ('', 'loser'), # ! losere, loserem, loseren, ...
            ('', 'loses'),
            ('', 'm'),
            ('', 'n'),
            ('', 'ne'),
            ('', 'nem'),
            ('', 'nen'),
            ('', 'ner'),
            ('', 'ner'),
            ('', 'ns'),
            ('', 'r'),
            ('', 's'),
            # ('', 'schaft'),
            ('', 'sche'),
            ('', 'slos'),
            ('', 'st'),
            ('', 'ste'),
            ('', 'stem'),
            ('', 'sten'),
            ('', 'ster'),
            ('', 'stes'),
            ('', 't'),
            ('', 'tte'),
            # ('', 'tum'),
            # ('', 'ung'),
            ('', 'ungen'),
            ('', 'ungs'),
            ('', 'weise'),

            # ! Achtung: Kürzen entfernt nicht nur Ableitungen.
            # (<weglassen>, <hinzufügen>), # Beispiele, Treffer in "wortliste"
            # ('bar', 't'), # 0
            # ('ben', 'bne'), # ebne ∃ eben (ben←bne) + 1
            # ('d', 'heit'), # Abwesenheit ∃ abwesend + 25 ! Feinheit ∃ Feind
            # ('el', 'le'), # bügle ∃ Bügel (el←le) + 180 ! Apple ∃ Appel
            ('el', 'elung'),
            ('en', 'barkeit'), # Dankbarkeit ∃ danken (en←barkeit) + 100
            ('en', 'em'), # anderem ∃ anderen (en←em) + 4200
            ('en', 'er'), # Abbeizer ∃ abbeizen (en←er) + 3800
            ('en', 'erin'),
            ('en', 'ern'),
            ('en', 'ern'),
            ('en', 'ers'),
            ('en', 'ung'),
            ('en', 'ungen'),
            ('en', 'es'), # # dickestes ∃ dickesten + 3783
            ('en', 'est'),
            ('en', 'et'),
            ('en', 'ete'),
            ('en', 'st'),
            ('en', 't'), # abbiegt ∃ abbiegen (en←t) + 7200
            ('en', 'te'), # abbuchte ∃ abbuchen (en←te) # 2900
            ('en', 'ten'), # abbuchte ∃ abbuchten
            ('en', 'ter'),
            ('en', 'tes'),
            ('en', 'tet'),
            ('en', 'test'),
            ('er', 'erei'), # Bäckerei ∃ Bäcker (er←erei) + 100
            ('er', 'em'),
            ('er', 'en'),
            ('er', 'ens'), # Absendens ∃ Absender (er←ens) + 360
            ('er', 'es'),
            # ('er', 'in'), # Göttin ∃ Götter (er←in) + 30 ! Hein ∃ Heer, mein ∃ Meer, hin ∃ her
            ('er', 'ung'), # Ablesung ∃ Ableser (er←ung) + 500
            ('er', 'ungen'), # Ablesungen ∃ Ableser + 200
            ('eren', 'tion'), # Addition ∃ addieren (eren←tion) + 9
            ('eren', 'sch'), # basisch ∃ basieren (eren←sch) + 30 ! logisch ∃ logieren
            ('ern', 'ere'), # abfedere ∃ abfedern (ern←ere) + 500
            ('ern', 'ernd'),
            ('ern', 'ers'),
            ('es', 'ste'), # starrste ∃ starres (es←ste) + 3
            ('ie', 'in'),
            ('ie', 'innen'),
            ('ie', 'isch'),
            ('in', 'e'), # Amtsärzte ∃ Amtsärztin (in←e) + 200 ! Allee ∃ allein
            ('in', 'en'), # Anwälten ∃ Anwältin (in←en) + 252 ! amen ∃ Amin
            ('ln', 'le'), # abhobele ∃ abhobeln (ln←le) + 300
            ('lt', 'le'), # abfeile ∃ abfeilt (lt←le) + 300
            ('mus', 'men'), # Organismen ∃ Organismus (mus←men) + 60
            ('mus', 'tik'), # Amerikanistik ∃ Amerikanismus (mus←tik)
            # ('n', ''), # Leiter ∃ Leitern ! besser Leiter bleibt!
            # ('n', 'er'), # Belagerer ∃ belagern (n←er) + 70 ! Bier ∃ bin, Feier ∃ fein, Gier ∃ Gin
            ('n', 'st'), # behexest ∃ behexen (n←st) # 670 # ! Suffix -ist
            ('n', 't'),  # bröselt ∃ bröseln
            ('n', 'te'),
            ('n', 'ten'),
            ('n', 'test'),
            ('n', 'ung'),
            # ('o','en'), # Risiken ∃ Risiko (o←en) + 50 ! essen ∃ Esso, harren ∃ Harro, lassen ∃ Lasso, wiesen ∃ wieso, Metren ∃ Metro,
            ('re', 'ste'), # absurdeste ∃ absurdere + 400
            # ('ren', 'rne'), # geschorne ∃ geschoren (ren←rne) + 1
            ('ren', 'rst'), # abhörst ∃ abhören (ren←rst) + 1000
            # ('ren', 'rt'), 0
            ('rn', 'rung'), # Kaperung ∃ Kapern (rn←rung) + 20
            ('rt', 're'), # abhöre ∃ abhört (rt←re) + 1000
            ('s', 'sere'), # blassere ∃ blass (s←sere) + 60
            ('s', 'sse'), # Albatrosse ∃ Albatros + 500
            ('s', 'sses'), # Atlasses ∃ Atlas +10 !Bisses ∃ bis
            ('sen', 'sne'), # abgerissne ∃ abgerissen + 2
            ('t', 'bar'), # ablegbar ∃ ablegt (t←bar) # 600
            ('t', 'bare'), # 570
            ('t', 'barem'), # 2
            ('t', 'baren'), # 570
            ('t', 'barer'), # 560
            ('t', 'bares'), # 590
            ('t', 'e'), # abbeiße ∃ abbeißt + 3200 ! Gehabe ∃ gehabt (t←e)
            ('t', 'n'), # abändern ∃ abändert (t←n) + 4000
            ('t', 'st'), # abbiegst ∃ abbiegt (t←st) + 6000
            # ('ten', 'mus'), ! Autismus ∃ Autisten
            ('t', 'tisch'),
            ('ten', 'tung'), # Abgeltung ∃ Abgelten + 35
            ('um', 'a'), # Aktiva ∃ Aktivum (um←a) + 65 ! Alba ∃ Album
            ('us', 'en'), # Globen ∃ Globus (us←en) + 40
            # ('ß', 'sse'), # Betonflußabrisse ∃ Betonflußabriß (ß←sse) + 0
           ]

substantivendungen = [
                      ('', 'en'),
                      ('', 'n'),
                     ]

# filter_ableitungen()
# --------------------
#
# Aussortieren von häufigen Ableitungen aus den Einträgen der `liste`.
#
# >>> from filter_wortliste import filter_ableitungen
# >>> words = set(('abbeißen', 'Wolle', 'Leiter'))
# >>> list(filter_ableitungen(['Ab<bei-ßens', 'Wollen', 'Leitern'], words))
# []
#
#
# ::

def filter_ableitungen(lines, words, verbose=False):
    for line in lines:
        if line.startswith('#'):
            yield line
            continue
        key = get_key(line)
        gibts_schon = False
        for alt, neu in endungen:
            if not key.endswith(neu):
                continue
            if neu:
                basis = key[:-len(neu)] + alt
            else:
                basis = key + alt
            if basis in words:
                gibts_schon = True
                break
            basis = basis.lower()
            if basis in words:
                gibts_schon = True
                break
            basis = basis.title()
            if basis in words:
                gibts_schon = True
                break
        # bei Substantiven
        if not gibts_schon:
            for alt, neu in substantivendungen:
                if not key.endswith(neu) and key.istitle():
                    continue
                if neu:
                    basis = key[:-len(neu)] + alt
                else:
                    basis = key + alt
                if basis in words:
                    gibts_schon = True
                    break

        if gibts_schon:
            # Ein aussortiertes Wort, soll für den Rest der Liste nicht mehr
            # als Basis einer Ableitung dienen:
            words.discard(key)
            if verbose:
                teile = line.split(' # ')
                teile.insert(1, '∃ %s (%s←%s)' % (basis, alt, neu))
                yield '# ' + ' # '.join(teile)
        else:
            yield line

# Wörter in Umschrift (Käse -> Kaese) ausgeben.
# ß wird separat behandelt.
#
# ::

umschrift = {# Umlaute
             ord('å'): 'aa',
             ord('ä'): 'ae',
             ord('æ'): 'ae',
             ord('ö'): 'oe',
             ord('ø'): 'oe',
             ord('ü'): 'ue',
             ord('Ä'): 'Ae',
             ord('Å'): 'Aa',
             ord('Æ'): 'Ae',
             ord('Ö'): 'Oe',
             ord('Œ'): 'Oe',
             ord('Ø'): 'Oe',
             ord('Ü'): 'Ue',
             # Akzente
             ord('à'): 'a',
             ord('á'): 'a',
             ord('â'): 'a',
             ord('ç'): 'c',
             ord('é'): 'e',
             ord('è'): 'e',
             ord('ê'): 'e',
             ord('ë'): 'e',
             ord('í'): 'i',
             ord('î'): 'i',
             ord('ï'): 'i',
             ord('ñ'): 'n',
             ord('ó'): 'o',
             ord('ô'): 'o',
             ord('Š'): 'S',
             ord('š'): 's',
            }

def umschreibe(lines, words=[], verbose=False):
    for line in lines:
        if line.startswith('#'):
            yield line
            continue
        key = get_key(line)
        if not re.search('[^ -~ß·]', key):
            continue
        teile = line.split(' # ')
        teile[0] = teile[0].translate(umschrift)
        if re.search('[^ -~ß·]', teile[0]):
            print('Keine Umschrift für', teile[0], file=sys.stderr)
            continue
        neukey = get_key(line)
        if neukey in words:
            if verbose:
                yield ('# ' + teile[0] + ' # schon vorhanden')
            continue
        yield ' # '.join(teile)


if __name__ == '__main__':

# Pfad zu "../../../wortliste" unabhängig vom Arbeitsverzeichnis::

    root_dir = os.path.relpath(os.path.dirname(os.path.dirname(
                               os.path.dirname(os.path.dirname(
                                         os.path.abspath(__file__))))))
    default_wortliste = os.path.join(root_dir, 'wortliste')

    default_nichtwortliste = os.path.join(root_dir, 'zusatzlisten',
                                          'nichtwoerter')

# Optionen::

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('INFILES', nargs='*', default=['-'],
                        help=u'Eingabedatei(en), Default: "-" (Standardeingabe)')
    parser.add_argument('-v', '--vorhandene', action='store_true',
                        help='In WORTLISTE vorhandene Wörter aussortieren.',
                        default=False)
    parser.add_argument('-a', '--ableitungen', action='store_true',
                        help='Ableitungen aussortieren',
                        default=False)
    parser.add_argument('-n', '--nichtwoerter', action='store_true',
                        help='Wörter der Liste "nichtwoerter" aussortieren',
                        default=False)
    parser.add_argument('-3', '--kurze', action='store_true',
                        help='Einträge mit 3 oder weniger Buchstaben aussortieren.',
                        default=False)
    parser.add_argument('-p', '--zufall', type=float,
                        help='Zufällig mit Wahrscheinlichkeit p aussortieren.',
                        default=False)
    parser.add_argument('-u', '--umschrift', action='store_true',
                        help='Wörter mit Nicht-ASCII-Zeichen, in Umschrift.',
                        default=False)
    parser.add_argument('-i', '--file', dest='wortliste',
                        help='Vergleichsdatei, Vorgabe "%s".'%default_wortliste,
                        default=default_wortliste)
    parser.add_argument('--verbose', action='store_true',
                        help='Nur auskommentieren.')
    parser.add_argument('-r', '--restdatei', type=argparse.FileType('w'),
                        help='Datei zum Sammeln der Aussortierten.')

    args = parser.parse_args()

# Ausgangsliste::

    lines = filelines(args.INFILES)

# Vergleichsmenge::

    words = set()

    if args.vorhandene:
        for line in open(args.wortliste):
            if line.startswith('#'):
                continue
            key = line.split(';')[0] # Vergleichsdatei immer im Langformat
            words.add(key)
        print('%s Wörter aus %s gelesen.'%(len(words), args.wortliste),
              file=sys.stderr)


# Filtern::

    if args.vorhandene and not args.umschrift:
        lines = [line for line in filter_vorhandene(lines, words)]

    if args.kurze:
        lines = [line for line in lines
                 if len(line.split(';')[0]) > 3]

    if args.nichtwoerter:
        lines = [line for line
                 in filter_nichtwoerter(lines, default_nichtwortliste)]

    if args.ableitungen:
        # Vergleichsmenge ergänzen:
        lines = list(lines) # Ein Generator würde sich "verbrauchen".
        for line in lines:
            if line.startswith('#'):
                continue
            key = get_key(line)
            words.add(key)

        lines = [line for line in filter_ableitungen(lines, words,
                                                     args.verbose)]

    if args.umschrift:
        if args.vorhandene:
            lines = [line for line
                     in umschreibe(lines, words, args.verbose)]
        else:
            lines = [line for line in umschreibe(lines)]

    if args.zufall:
        lines = [line for line
                 in filter_zufaellig(lines, args.zufall, args.restdatei)]

# Ausgabe::

    # Header
    if args.verbose:
        argv = list(sys.argv) # Kopie
        argv[0] = os.path.relpath(argv[0])
        print('# Gefiltert mit `%s`' % ' '.join(argv))

    for line in lines:
        print(line)
