#!/usr/bin/env python3
# :Copyright: © 2014 Günter Milde.
#             Released without warranty under the terms of the
#             GNU General Public License (v. 2 or later)
# :Id: $Id:  $

# split_wortliste.py: Helfer für kleine Editieraufgaben
# ===================================================
# ::

"""
Aufteilen einer Wortliste-Datei.
"""

# ::

import argparse, sys, os, re, unicodedata

# append path to the "py_wortliste" package
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from py_wortliste.wortliste import sortkey_duden, umschrift_skey, WordFile


# Gib den ersten Buchstaben einer Zeile (normalisiert auf a-z) zurück

# >>> from split_wortliste import startletter
# >>> startletter('Öfen')
# 'o'
# >>> startletter('-*-;ſoff')
# 's'

# ::

def startletter(line):

    match = re.search('[a-zäöüßæœſA-ZÄÖÜÆÅØ]', line)
    if not match:
        return ''
    sl = match.group(0).lower()
    sl = sl.translate(umschrift_skey)
    sl = unicodedata.normalize('NFKD', sl)
    return sl.encode('ascii', 'ignore').decode('utf8')

# Erzeuge ein Dictionary mit leeren Listen für die Buchstabne a bis z

# >>> from split_wortliste import make_az_lists
# >>> lists = make_az_lists()
# >>> lists['m']
# []
#
# ::

def make_az_lists():
    lists = {}
    for i in range(ord('a'), ord('z')+1):
        lists[chr(i)] = []
    return lists

# split_a_z
# ---------
#
# Sortiere eine Liste von Zeilen je nach Anfangsbuchstaben in
# Einzellisten. Gib ein Dictionary mit den Listen zurück. ::

def split_a_z(lines):

    lists = make_az_lists()
    # einsortieren:
    for line in lines:
        try:
            lists[startletter(line)].append(line)
        except IndexError:
            # print(line, startletter(line))
            continue
    return lists

# Sortiere eine Liste von Zeilen je nach Anfangsbuchstaben in
# Einzeldateien. Das Argument ``destination`` wird um die Endungen
# ``a`` … ``z`` ergänzt::

def write_a_z(lists, destination):
    for ch, lines in lists.items():
        print(ch, len(lines), end=' ')
        if not lines:
            continue
        outfile = open(destination+ch, 'w')
        outfile.writelines(line for line in lines)


# Default-Aktion::

if __name__ == '__main__':


# Optionen::

    usage = '%(prog)s [Optionen] destination\n' + __doc__

    parser = argparse.ArgumentParser(usage=usage)
    parser.add_argument('destination', help='Template für die Ausgangsdateien'
                        '(wird um "a"..."z" erweitert).')
    parser.add_argument('-i', '--wordfile',
                      help='Eingangsdatei, Vorgabe "../../../wortliste"',
                      default='../../../wortliste')
    parser.add_argument('-s', '--sort',
                      help='Einträge sortieren (Dudensortierung).',
                      action="store_true", default=False)

    args = parser.parse_args()

# Öffnen::

    wordfile = open(args.wordfile) # vorhandene Einträge

# Aufteilen und Schreiben::

    lists = split_a_z(wordfile.readlines())
    write_a_z(lists, args.destination)
