#!/usr/bin/env python3
# -*- coding: utf8 -*-
# :Copyright: © 2014 Günter Milde.
#             Released without warranty under the terms of the
#             GNU General Public License (v. 2 or later)
# :Id: $Id:  $

# Erweitern der Wortliste um Kombinationen von Teilwörtern
# ========================================================
#
# ::

"""Zerlegen von Komposita an den Wortfugen.
"""

# >>> from wortzerlegung import *
#
# ::

import codecs, collections, copy, argparse, os, re, sys, time
from wortliste import (WordFile, WordEntry, join_word,
                       toggle_case, sortkey_duden)


# Klasse für die rekursive Zerlegung eines Kompositums
#
# >>> print(Compound("Kunst==hand=wer-ker===markt"))
# (Kunst + (hand + wer-ker)) + markt
#
# >>> print(Compound("Ma-kro<=le-be=we-sen"))
# Ma-kro- + (le-be + we-sen)
# >>> print(Compound("Ur<=ur<=gross=va-ter"))
# Ur- + (ur- + (gross + va-ter))
# >>> print(Compound("E·lek-tro<==trieb==fahr=zeug"))
# E·lek-tro- + (trieb + (fahr + zeug))
# >>> print(Compound("drei==ein=halb===mil-li.o-nen===>fach"))
# ((drei + (ein + halb)) + mil-li.o-nen) + -fach
# >>> print(Compound("be<=gut=acht=>bar"))
# be- + (gut + acht) + -bar
# >>> print(Compound("An<=al-.pha=be-ten==>tum"))
# (An- + (al-.pha + be-ten)) + -tum
# >>> print(Compound("Brand==o·ber<=amts=rat"))
# Brand + (o·ber- + (amts + rat))
#
# >>> print(Compound("Kub<ok-ta<e·der=stumpf"))
# (Kub- + (ok-ta- + e·der)) + stumpf
# >>> print(Compound("Dop-pel=in<kli-no<<me-ter"))
# Dop-pel + ((in- + kli-no-) + me-ter)
#
# >>> print(Compound("ver<ein>heit>li-chen"))
# ver- + (ein + -heit) + -li-chen
# >>> print(Compound("Pi.e-zo<auf<neh-mer"))
# Pi.e-zo- + (auf- + neh-mer)
# >>> for i in Compound("Dop-pel=in<kli-no<<me-ter").walk():
# ...      print(repr(i))
# Compound("Dop-pel=in<kli-no<<me-ter")
# Compound("Dop-pel")
# 'Dop-pel'
# Compound("in<kli-no<<me-ter")
# PCompound("in<kli-no")
# Prefix('in')
# Compound("kli-no")
# 'kli-no'
# Compound("me-ter")
# 'me-ter'
#
# >>> for i in Compound("An<ge<legen>heit").walk(parts=False):
# ...      print(repr(i))
# Compound("An<ge<legen>heit")
# Compound("ge<legen")
# Compound("legen")
#
# >>> for i in Compound("An<ge<legen>heit").walk(compounds=False):
# ...      print(i)
# An-
# ge-
# legen
# -heit
#
# ::

class Compound(list):

    def __init__(self, word):
        self.source = word
        self.level = level = compound_level(word)
        sep = '='*level # (=, ==, ===, ...)

        # Affixe
        prefix = []
        suffix = []
        if ('<<' + sep) in word: # Wörter mit mehrteiligem Präfix
            prefix, word = word.split('<<'+sep, maxsplit=1)
            prefix = [PCompound(prefix)]
        elif ('<' + sep) in word[:-1]:
            prefix, word = word.split('<'+sep, maxsplit=1)
            prefix = [Prefix(prefix)]
        if (sep + '>') in word:
            word, suffix = word.rsplit(sep+'>', maxsplit=1)
            suffix = [Suffix(suffix)]

        if prefix or suffix:
            parts = prefix + [Compound(word)] + suffix
        else:
            if level: # rekursive Zerlegung
                parts = [Compound(part)
                         for part in word.split(sep)]
            else:
                parts = [word]
        list.__init__(self, parts)

    # Iterator über die Bestandteile:
    def walk(self, compounds=True, parts=True):
        if compounds:
            yield self
        for part in self:
            if isinstance(part, Compound):
                yield from part.walk(compounds, parts)
            elif parts:
                yield part

    def __str__(self):
        parts = (str(p) for p in self)
        parts = ('('+ p +')' if '+' in p else p for p in parts)
        return ' + '.join(parts)

    def __repr__(self):
        return 'Compound("%s")' % self.source


class PCompound(Compound):
    def __repr__(self):
        return 'PCompound("%s")' % self.source

    def __str__(self):
        return Compound.__str__(self) + '-'

# Wrapper für Präfix und Suffix:
#
# >>> print(Prefix("ab"), 'hängig', Suffix('keit'))
# ab- hängig -keit
# >>> print(repr(Prefix("zu")))
# Prefix('zu')
# >>> Prefix("auf").raw()
# 'auf'
#
# ::

class Prefix(str):
    # # Idempotente Klasse: Prefix(Prefix('ab')) == Prefix('ab')
    # def __new__(cls, value):
    #     if isinstance(value, cls):
    #         # print(value, "is already a Prefix")
    #         return value
    #     instance = super().__new__(cls, value)
    #     return instance
    #
    # # def split(self, *args, **nargs):
    # #     return [Prefix(s) for s in str.split(self, *args, **nargs)]

    def __str__(self):
        return self + '-'

    def __repr__(self):
        return 'Prefix(%s)' % str.__repr__(self)

    def raw(self):
        return str.__str__(self)

class Suffix(str):
    def __str__(self):
        return '-' + self

    def __repr__(self):
        return 'Suffix(%s)' % str.__repr__(self)

# Funktionen
# -----------
#
#
# Zahl der Ebenen der Zerlegungshierarchie:
#
#
# ::

def compound_level(word):
    try:
        return max(len(s) for s in re.findall('=+', word))
    except ValueError:
        return 0


# Iterator, gibt alle geordneten Teilkombinationen zurück
#
# >>> list(multisplitter('test', '='))
# ['test']
#
# >>> list(multisplitter('a=b', '='))
# ['a', 'a=b', 'b']
#
# >>> list(multisplitter('a=b=c', '='))
# ['a', 'a=b', 'a=b=c', 'b', 'b=c', 'c']
#
# >>> list(multisplitter('a=b=c=d', '='))
# ['a', 'a=b', 'a=b=c', 'a=b=c=d', 'b', 'b=c', 'b=c=d', 'c', 'c=d', 'd']
#
# >>> list(multisplitter('a=b==c', '=='))
# ['a=b', 'a=b==c', 'c']
# >>> list(multisplitter('a=b==c=de', '=='))
# ['a=b', 'a=b==c=de', 'c=de']
# >>> list(multisplitter('a=b==c=de', '==='))
# ['a=b==c=de']
#
# >>> list(multisplitter('er[<st/st=]ritt', '='))
# ['er[<st/st=]ritt']
# >>> list(multisplitter('Schiff[=s/s=]tau', '='))
# ['Schiff[=s/s=]tau']
# >>> list(multisplitter('a{ll/ll=l}ie-bend', '='))
# ['a{ll/ll=l}ie-bend']
# >>> list(multisplitter('Be[t=t/{tt/tt=t}]uch', '='))
# ['Be[t=t/{tt/tt=t}]uch']
#
# Mit `only_new` wird das Eingangswort nicht mit ausgegeben:
#
# >>> list(multisplitter('test', '=', only_new=True))
# []
# >>> list(multisplitter('a=b', '=', True))
# ['a', 'b']
# >>> list(multisplitter('a=b=c', '=', True))
# ['a', 'a=b', 'b', 'b=c', 'c']
# >>> list(multisplitter('a=b==c=de', '==', True))
# ['a=b', 'c=de']
# >>> list(multisplitter('a=b==c=de', '===', True))
# []
#
# ::

def multisplitter(wort, sep, only_new=False):
    specials = re.findall(r'\[.*%s.*\]|\{[^}]*%s[^}]*\}'%(sep,sep), wort)
    for sp in specials:
        wort = wort.replace(sp, sp.replace(sep, '*'))
    parts = wort.split(sep)
    length = len(parts)
    for start in range(length):
        for end in range(start+1, length+1):
            if only_new and end - start == length:
                continue
            part = sep.join(parts[start:end])
            if specials:
                part = part.replace('*', sep)
            yield part

# Gib eine Liste möglicher Zerlegungen eines Kompositums zurück.
# Berücksichtige dabei die Bindungsstärke bis zum Level 3
# ("===", zur Zeit höchste Auszeichnung in der Wortliste).
#
# >>> multisplit('test')
# ['test']
# >>> multisplit('a=b')
# ['a=b', 'a', 'b']
# >>> multisplit('a=b=c')
# ['a=b=c', 'a', 'a=b', 'b', 'b=c', 'c']
# >>> multisplit('a<b=c')
# ['a<b=c', 'a<b', 'c']
# >>> multisplit('a==b=c')
# ['a==b=c', 'a', 'b=c', 'b', 'c']
# >>> multisplit('a<=b=c')
# ['a<=b=c', 'b=c', 'b', 'c']
# >>> multisplit('a=b=>c')
# ['a=b=>c', 'a=b', 'a', 'b']
# >>> multisplit('a<==b=c==d')
# ['a<==b=c==d', 'b=c==d', 'b=c', 'b', 'c', 'd']
# >>> multisplit('a==b=c==>d')
# ['a==b=c==>d', 'a==b=c', 'a', 'b=c', 'b', 'c']
# >>> multisplit('a==b=c==d')
# ['a==b=c==d', 'a', 'a==b=c', 'b=c', 'b', 'c', 'b=c==d', 'd']
#
# >>> multisplit('test', only_new=True)
# []
# >>> multisplit('a=b', True)
# ['a', 'b']
# >>> multisplit('a=b=c', True)
# ['a', 'a=b', 'b', 'b=c', 'c']
# >>> multisplit('a<b=c', True)
# ['a<b', 'c']
# >>> multisplit('a==b=c', True)
# ['a', 'b=c', 'b', 'c']
# >>> multisplit('a<=b=c', True)
# ['b=c', 'b', 'c']
# >>> multisplit('a=b=>c', True)
# ['a=b', 'a', 'b']
# >>> multisplit('a<==b=c==d', True)
# ['b=c==d', 'b=c', 'b', 'c', 'd']
# >>> multisplit('a==b=c==>d', True)
# ['a==b=c', 'a', 'b=c', 'b', 'c']
# >>> multisplit('a==b=c==d', True)
# ['a', 'a==b=c', 'b=c', 'b', 'c', 'b=c==d', 'd']
#
# >>> for w in multisplit('Brenn=stoff==zel-len===an<trieb'):
# ...    print(w)
# Brenn=stoff==zel-len===an<trieb
# Brenn=stoff==zel-len
# Brenn=stoff
# Brenn
# Stoff
# Zel-len
# An<trieb
#
# >>> for w in multisplit('drei==ein=halb===mil-lio-nen===>fa-che'):
# ...    print(w)
# drei==ein=halb===mil-lio-nen===>fa-che
# drei==ein=halb===mil-lio-nen
# drei==ein=halb
# drei
# ein=halb
# ein
# halb
# mil-lio-nen
#
# Mit `only_new` wird das Eingangswort nicht mit ausgegeben:
#
# >>> multisplit('a=b=c', only_new=True)
# ['a', 'a=b', 'b', 'b=c', 'c']
# >>> for w in multisplit('drei==ein=halb===mil-lio-nen===>fa-che', True):
# ...    print(w)
# drei==ein=halb===mil-lio-nen
# drei==ein=halb
# drei
# ein=halb
# ein
# halb
# mil-lio-nen
# >>> for w in multisplit('amts=ärzt=>lich', True):
# ...    print(w)
# amts=ärzt
# amts
# ärzt
#
# ::

def multisplit(wort, only_new=False, level=3):

    if only_new:
        parts = []
    else:
        parts = [wort]
    if not level:
        return parts

    fuge = '=' * level
    istitle = wort[:2].istitle()

    # Globale Affixe abspalten:
    global_suffix = fuge + '>' # =>, ==>, ===>, ...
    global_prefix = '<' + fuge # <=, <==, <===, ...
    if global_suffix in wort or global_prefix in wort:
        wort = re.sub(global_suffix+'.*$', '', wort)
        wort = re.sub('^.*'+global_prefix, '', wort)
        if istitle:
            wort = wort[0].title() + wort[1:]
        parts.append(wort)

    if fuge not in wort:
        # print(level, wort, only_new)
        return multisplit(wort, only_new, level-1)

    # Zerlegen an Fugen mit `level`:
    for part in multisplitter(wort, fuge, only_new=True):
        # print(level,  part)
        if fuge in part:
            parts.append(part)
            continue
        parts.extend(multisplit(part, False, level-1))

    # Großschreibung beibehalten:
    istitle = wort[:2].istitle()
    if istitle:
        parts = [part[0].title() + part[1:] for part in parts]
    return parts

# Gib eine Liste von allen (sinnvollen) Zerlegungen eines WordEntry zurück
#
# >>> from wortliste import WordEntry
#
# >>> split_entry(WordEntry('Aachen;Aa-chen'))
# [WordEntry('Aachen;Aa-chen')]
# >>> aalbestand = WordEntry('Aalbestand;Aal=be<stand')
# >>> for e in split_entry(aalbestand):
# ...     e
# WordEntry('Aalbestand;Aal=be<stand')
# WordEntry('Aal;Aal')
# WordEntry('Bestand;Be<stand')
#
# >>> godi = WordEntry('Abendgottesdienste;-2-;Abend==got-tes=dien-ste;Abend==got-tes=diens-te')
# >>> for entry in split_entry(godi):
# ...     print(entry)
# Abendgottesdienste;-2-;Abend==got-tes=dien-ste;Abend==got-tes=diens-te
# Abend;Abend
# Gottesdienste;-2-;Got-tes=dien-ste;Got-tes=diens-te
# Gottes;Got-tes
# Dienste;-2-;Dien-ste;Diens-te
#
# Mit `only_new` wird das Ausgangswort weggelassen:
#
# >>> for entry in split_entry(godi, only_new=True):
# ...     print(entry)
# Abend;Abend
# Gottesdienste;-2-;Got-tes=dien-ste;Got-tes=diens-te
# Gottes;Got-tes
# Dienste;-2-;Dien-ste;Diens-te
#
# Achtung: Wenn ein Wort nur in einer Sprachvariante existiert, werden alle
# Zerlegungen auch nur in dieser Variante zurückgegeben,
# selbst wenn sie auch in anderen Sprachvarianten korrekt sind:
#
# >>> bb = WordEntry('Biberbettuch;-2-;Bi-ber==be[t=t/{tt/tt=t}]uch')
# >>> for entry in split_entry(bb):
# ...     print(entry)
# Biberbettuch;-2-;Bi-ber==be[t=t/{tt/tt=t}]uch
# Biber;-2-;Bi-ber
# Bettuch;-2-;Be[t=t/{tt/tt=t}]uch
#
# >>> bb = WordEntry('Biberbetttuch;-2-;-3-;Bi-ber==bett=tuch')
# >>> for entry in split_entry(bb):
# ...     print(entry)
# Biberbetttuch;-2-;-3-;Bi-ber==bett=tuch
# Biber;-2-;-3-;Bi-ber
# Betttuch;-2-;-3-;Bett=tuch
# Bett;-2-;-3-;Bett
# Tuch;-2-;-3-;Tuch
#
# Wenn die Zahl der Zerlegungen abweicht, wird ein Fehler erzeugt:
#
# >>> sa = WordEntry('Schrottanker;-2-;Schro[tt=/{tt/tt=t}]an-ker;Schrott=an-ker')
# >>> split_entry(sa)
# Traceback (most recent call last):
#   ...
# ValueError: unterschiedliche Zerlegungsanzahl für Schrottanker;-2-;Schro[tt=/{tt/tt=t}]an-ker;Schrott=an-ker
#
# ::

def split_entry(entry, only_new=False):

    entries = []

    for col in range(1, len(entry)):
        wort = entry[col]
        if '=' not in wort:
            continue # nichts zu splitten
        parts = multisplit(wort, only_new)

        # (leere) Einträge und Schlüssel erstellen
        if not entries:
            for part in parts:
                entries.append(copy.copy(entry))
                entries[-1][0] = join_word(part)
        # Einträge auffüllen
        for i in range(len(parts)):
            try:
                entries[i][col] = parts[i]
            except IndexError:
                raise ValueError('unterschiedliche Zerlegungsanzahl für %s'
                                 %entry)
    if entries:
        for e in entries:
            e.prune() # Zusammenfassen von Sprachvarianten
        return entries
    else:
        return [entry]



# Iteriere über die Wortliste-Einträge `entries` und
# gib ein Dictionary mit Teilwortkombinationen der Einträge zurück.
#
# Das Argument `scope` bestimmt die Auswahl der gesammelten Wörter:
#
#
#   alle:
#     alle Teile vor und nach Wortfugen,
#     auch "vollständige" (in `entries` vorhandene) Wörter,
#
#   teile:
#     Teile vor und nach Wortfugen, ohne in `entries` vorhandene Wörter.
#
#   :selbständige:
#     ohne vollständige Wörter und Teile die (wahrscheinlich)
#     keine sinnvollen Einträge für die Wortliste sind:
#
#     * kürzer als 3 Buchstaben,
#     * eingeschobenes "zu" (gegen=zu=halten)
#     * Kurzform ohne "-en" (z.B. Ab<löse=..., amts=ärzt=>lich, ...)
#     * Bindungs-S (z.B. "Ab<fahrts=...")
#     * "ung" + Bindungs-S (z.B. "Abrechnungs=grund")
#
# ::

def expand_words(entries, scope='teile', verbose=False):
    newentries = {}
    only_new = scope in ('teile', 'selbständige')

    if only_new:
        entries = list(entries) # Sicherstellen, dass noch einmal iteriert werden kann.
        keys = set(entry.key() for entry in entries)
    for entry in entries:
        if "Kurzwort" in entry.comment:
            continue
        try:
            parts = split_entry(entry, only_new) # Liste mit Teilworteinträgen
        except ValueError as err:
            if verbose:
                print(err, file=sys.stderr)
            continue
        for e in parts:
            key = e[0]
            if key in newentries:  # schon da
                ve = newentries[key]
                ve._duplicates += 1
                if (len(ve) == 2): # alle Sprachvarianten gleich
                    continue
                try:
                    ve.merge(e)
                except AssertionError as err:
                    if verbose:
                        print(err, file=sys.stderr)
                continue
            if only_new:
                if (key in keys
                    or key.lower() in keys
                    or key.title() in keys):
                    continue # in Originalverzeichnis vorhanden

            # Aussortieren von Einträgen die wahrscheinlich keine
            # selbständigen Wörter sind.
            if scope == 'selbständige':
                if len(key) <= 3:  # zu kurz
                    continue
                # eingeschobenes "zu" (gegen=zu=halten)
                if  key.startswith('zu=') or key.endswith('=zu'):
                    continue
                # Kurzform ohne "-en" (z.B. Ab<löse=..., amts=ärzt=>lich, ...)
                if key.lower() + 'en' in keys:
                    continue
                # Bindungs-S (z.B. "Ab<fahrts=...")
                if key.endswith('s') and key[:-1] in keys:
                    # aber: Achs=... (ach), ...
                    continue
                # "ung" + Bindungs-S (z.B. "Abrechnungsgrund")
                if key.endswith('ungs') and key[:-4] + 'en' in keys:
                    continue

            # Herkunft festhalten:
            e.comment = '< ' + entry.get('de-1996,de-1901,de-1901-x-versal,'
                                         'de-1996-x-versal,de-CH-1901')
            e._duplicates = 0

            # Entfernen des "Ungünstigkeitsmarkers" nach kurzen Vorsilben:
            for i in range(1, len(e)):
                if re.match('..<[.]', e[i]):
                    e[i] = e[i][:3] + e[i][4:]
            newentries[key] = e

    for entry in newentries.values():
        if entry._duplicates:
            entry.comment += ' +%s×' % entry._duplicates

    return newentries

# def exists(wort):
#     key = join_word(wort)
#     return (key.title() in words) or (key.lower() in words) or (len(wort)<4)


# Präfixe bestimmen::

def check_affix(affix):
    if '[' in affix or '/' in affix or ']' in affix:
       return '' # Spezialtrennung/Alternativen -> unsicher
    affix = affix.lower()
    # Entferne Unterdrücker und führende Trennzeichen
    affix = affix.replace('-.', '-').strip('.')
    affix = re.sub('^[·]', '', affix)
    # Entferne Alternativtrennung nach §113 (verblasste Morphologie):
    affix = re.sub('[·-]([^aeiouäöüy])$', r'\1', affix)
    affix = re.sub('[-]([aeiouäöüy])$', r'·\1', affix)
    return affix

# >>> words = ['Vor<silbe',
# ...          'Keine=vor<silbe',
# ...          'Globale<=vor<silbe',
# ...          'au-to<gen',
# ...          'Au-to=bahn',
# ...          'un<zu<ver<läs-sig',
# ...          'un<<gleich>för-mig',
# ...          'An<äs-the-si·o<<lo-gie',
# ...          'Pi.e-zo<auf<neh-mer',
# ...          'alt=an<.ge<se-hen',
# ...         ]
# >>> for word in words:
# ...   for prefix in split_prefixes(word):
# ...     print(prefix)
# vor
# vor
# globale
# vor
# au-to
# un
# zu
# ver
# un
# an<äs-the-si·o
# an
# pi.e-zo
# auf
# an
# ge
#
# ::

def split_prefixes(word):
    for part in Compound(word).walk():
        if isinstance(part, PCompound):
            yield check_affix(part.source)
        if isinstance(part, Prefix):
            yield check_affix(part)
    return


# >>> for word in words:
# ...   for suffix in split_suffixes(word):
# ...     print(suffix)
# för-mig
#
# ::

def split_suffixes(word):
    for part in Compound(word).walk(compounds=False):
        if isinstance(part, Suffix):
            yield check_affix(part)
    return

# Präfixe sammeln:
# `scope`:
#   * alle: alle Teile vom Wortanfang bis "<"
#   * neuwörter: keine Homonyme zu Einträgen in `entries`,
#   * wörter: Homonyme zu Einträgen und Teilwörtern.
#
# >>> entries = [WordEntry(join_word(word) + ';' + word)
# ...            for word in words]
# >>> for p in list_affixes(entries, split_fun=split_prefixes): print(p)
# ('vor', 'Vor<silbe', 2)
# ('globale', 'Globale<=vor<silbe', 0)
# ('au-to', 'au-to<gen', 0)
# ('un', 'un<zu<ver<läs-sig', 1)
# ('zu', 'un<zu<ver<läs-sig', 0)
# ('ver', 'un<zu<ver<läs-sig', 0)
# ('an<äs-the-si·o', 'An<äs-the-si·o<<lo-gie', 0)
# ('an', 'An<äs-the-si·o<<lo-gie', 1)
# ('pi.e-zo', 'Pi.e-zo<auf<neh-mer', 0)
# ('auf', 'Pi.e-zo<auf<neh-mer', 0)
# ('ge', 'alt=an<.ge<se-hen', 0)
#
# >>> for p in list_affixes(entries, split_fun=split_prefixes,
# ...                       scope='neuwörter'): print(p)
# ('vor', 'Vor<silbe', 2)
# ('globale', 'Globale<=vor<silbe', 0)
# ('un', 'un<zu<ver<läs-sig', 1)
# ('zu', 'un<zu<ver<läs-sig', 0)
# ('ver', 'un<zu<ver<läs-sig', 0)
# ('an<äs-the-si·o', 'An<äs-the-si·o<<lo-gie', 0)
# ('an', 'An<äs-the-si·o<<lo-gie', 1)
# ('pi.e-zo', 'Pi.e-zo<auf<neh-mer', 0)
# ('auf', 'Pi.e-zo<auf<neh-mer', 0)
# ('ge', 'alt=an<.ge<se-hen', 0)
#
# >>> list_affixes(entries, split_fun=split_prefixes, scope='wörter')
# [('au-to', 'au-to<gen (vs. Au-to=bahn)', 0)]
#
# ::

# Affixe sammeln::

def list_affixes(entries, split_fun=split_suffixes,
                 scope='alle', lang='de-1996'):
    affixes = {}
    if scope != 'alle':
        entries = list(entries)
    for entry in entries:
        word = entry.get(lang)
        for affix in split_fun(word):
            try:
                affixes[affix][1] += 1
            except KeyError:
                affixes[affix] = [word, 0]
    affixes.pop('', None)

    if scope != 'alle':
        words = dict((e.get('de-1996').lower(), e)
                     for e in expand_words(entries, scope='alle').values())
        # print(words)
        for affix in list(affixes):
            if ((scope == 'neuwörter' and affix in words)
                or (scope == 'wörter' and affix not in words)):
                del(affixes[affix])
            elif scope == 'wörter':
                affixes[affix][0] += ' (vs. %s)' % words[affix].comment[2:]

    return [(key, affixes[key][0], affixes[key][1]) for key in affixes]


if __name__ == '__main__':

# Pfad zu "../../../wortliste" unabhängig vom Arbeitsverzeichnis::

    default_wortliste = os.path.relpath(os.path.join(
        os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(
            os.path.abspath(__file__))))),
        'wortliste'))

# Optionen::

    usage = '%prog [Optionen]\n' + __doc__

    parser = argparse.ArgumentParser(description = __doc__,
                        # formatter_class=argparse.RawDescriptionHelpFormatter
                                    )
    parser.add_argument('WORTLISTE', nargs='?', # optionales Argument
                        help='Eingabedatei im "Wortliste-Format" '
                        '("-" für Standardeingabe), '
                        'Vorgabe "%s".'%default_wortliste,
                        default=default_wortliste)
    parser.add_argument('-k', '--komposita',
                        help='Liste Konstituenten zusammengesetzter Wörter '
                        '(Teile vor und nach \'=\'). '
                        '"alle": auch vollständige Wörter der Eingabe, '
                        '"teile": keine vollständigen Wörter der Eingabe, '
                        '"selbständige": keine Konstituenten mit Bindungs-s, '
                        'eingeschobenem »zu« oder fehlender Endung. '
                        'Vorgabe "teile".',
                        choices=['alle', 'teile', 'selbständige', ''],
                        default='teile')
    parser.add_argument('-p', '--praefixe',
                        help='Präfixe (statt Komposita) bestimmen. '
                        '"alle": alle Präfixe (Wortanfang bis \'<\', '
                        '"wörter": nur Homonyme zu Einträgen und Teilwörtern, '
                        '"neuwörter": keine Homonyme, '
                        '"": keine. '
                        'Vorgabe "".',
                        choices=['alle', 'neuwörter', 'wörter', ''],
                        default='')
    parser.add_argument('-s', '--suffixe',
                        help='Suffixe (statt Komposita) bestimmen. '
                        '"alle": alle Suffixe von \'>\' bis Wortende, '
                        '"wörter": nur Homonyme zu Einträgen und Teilwörtern, '
                        '"neuwörter": keine Homonyme, '
                        '"": keine. '
                        'Vorgabe "".',
                        choices=['alle', 'neuwörter', 'wörter', ''],
                        default='')
    parser.add_argument('-z', '--zerlegungshierarchie', action='store_true',
                        help='Gib die Zerlegungshierarchie aus.')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='Kommentare zur Herkunft anhängen.')
    parser.add_argument('--sort', choices=['duden', 'länge', 'anzahl', ''],
                        default='duden',
                        help='Sortieren der Ergebnisse (Default: nach Duden)')
    parser.add_argument('-l', '--language', metavar='SPRACHE,[SPRACHE...]',
                        help='Sprachvariante(n), ignoriert für "-k" '
                        '(Vorgabe: "de-1996").',
                        default="de-1996")

    args = parser.parse_args()


    if args.WORTLISTE == '-':
        wordfile = (WordEntry(line.rstrip()) for line in sys.stdin)
    else:
        wordfile = WordFile(args.WORTLISTE)


    sortkeys = {'duden':  lambda result : sortkey_duden(result[0]),
                'länge':  lambda result : len(result[0]),
                'anzahl': lambda result : result[2],
               }


# `Wortliste` einlesen::

    entries = wordfile

# Präfixe::

    if args.praefixe:
        # affixes = list_prefixes(entries, scope=args.praefixe)
        affixes = list_affixes(entries, split_fun=split_prefixes,
                                scope=args.praefixe)

    elif args.suffixe:
        affixes = list_affixes(entries, split_fun=split_suffixes,
                               scope=args.suffixe)

    elif args.zerlegungshierarchie:
        words = (entry.get('de-1996') for entry in entries)
        output = ['%s # %s' % (word, Compound(word)) for word in words]

# Teilwörter::

    else:
        parts = expand_words(entries, scope=args.komposita,
                             verbose=args.verbose)
        output = sorted(parts.values(), key=sortkey_duden)

# Affixe sortieren und formatieren::

    if args.praefixe or args.suffixe:
        output =  ['%s # < %s +%s×' % affix
                   for affix in sorted(affixes, key=sortkeys[args.sort])]

# Ausgabe::

    # Header
    # Modifikationszeit der Eingabdatei:
    if args.WORTLISTE != '-':
        mtime = ''
    else:
        mtime = time.localtime(os.path.getmtime(args.WORTLISTE))
        mtime = time.strftime(' vom %d.%m.%Y', mtime)
    argv = list(sys.argv) # Kopie
    argv[0] = os.path.relpath(argv[0])
    print('# Extract aus "%s"%s' % (args.WORTLISTE, mtime))
    print('# mit `%s`' % ' '.join(argv))

    for line in output:
        print(line)

    print('#', len(output), "Einträge", file=sys.stderr)
