#!/usr/bin/env python3
# :Copyright: © 2012, 2023 Günter Milde.
# :Licence:   This work may be distributed and/or modified under
#             the conditions of the `LaTeX Project Public License`,
#             either version 1.3 of this license or (at your option)
#             any later  version.
# :Version:   0.1 (2012-02-07)

# wortliste.py
# ************
# ::

"""Hilfsmittel für die Arbeit mit der `Wortliste`"""

# .. contents::
#
# Die hier versammelten Funktionen und Klassen dienen der Arbeit an und
# mit der freien `Wortliste der deutschsprachigen Trennmustermannschaft`_
# (wortliste_).
#
# Abhängigkeiten
# ==============
#
# Python 3 mit Standardmodulen::

import difflib
import re
import unicodedata
import copy
import itertools
import sys, os

# Module des lokalen "py_wortliste" Pakets::

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# Filter für Wahltrennungen (Re<s-pekt -> Re<spekt, …)::
from py_wortliste import stilfilter
from py_wortliste.stilfilter import morphemisch


# WordFile
# ========
#
# Klasse zum Lesen und Schreiben der `Wortliste`::

class WordFile():

# Initialisierung
# ----------------
#
# Das Argument ``format`` erwartet einen der Strings
#
#    :f8: das originale, maximal 8-spaltige Wortlisten-Format (Langform_),
#    :f5: das neue, maximal 5-spaltige Wortlisten-Format (Kurzform_), oder
#    :auto: bestimme das Format automatisch.
#
# ::

    def __init__(self, name, mode='r', encoding='utf8', format='f8'):

        self.name = name
        self.mode = mode
        self.encoding = encoding
        self.wordfile = open(name, mode=mode, encoding=encoding)
        self.seek = self.wordfile.seek
        # Dateiformat bestimmen und die Eintrags-Klasse setzen:
        self.set_entry_class(format)

# Klasse zum Verarbeiten der Zeilen setzen.
# Mit self.format == "auto", werden die ersten Zeilen der Datei untersucht
# und der Pointer zurückgesetzt.
#
# ::

    def set_entry_class(self, format, search_limit=1000):

        assert(format in ('f8', 'f5', 'auto'))
        self.format = format # default

        self.entry_class = WordEntry
        n = 0 # Zählindex

        # Format anhand des Dateiinhalts bestimmen:
        if format == 'auto':
            # print("auto-determining word file format")
            for e in self:
                # nur ein Feld oder erstes Feld leer oder mit Trennzeichen:
                if len(e) == 1 or e and re.search('[-=<>.·]', e[0]):
                    self.format = 'f5'
                    break
                # mehr als 5 Felder: 8-Felder-Format (Langform)
                elif len(e) > 5:
                    self.format = 'f8'
                    break
                if n > search_limit:
                    break
                n += 1
        if self.format == 'f5':
            self.entry_class = ShortEntry

        self.wordfile.seek(0)            # Dateizeiger zurücksetzen


# Iteration
# ---------
#
# Die spezielle Funktion `__iter__` wird aufgerufen wenn über eine
# Klasseninstanz iteriert wird.
# Sie liefert einen Iterator über die "geparsten" Zeilen (Datenfelder)
# ::

    def __iter__(self):
        entry_class = self.entry_class
        for line in self.wordfile:
            yield entry_class(line.rstrip())


# asdict()
# --------
#
# Gib ein `dictionary` (assoziatives Array) mit ungetrenntem Wort als
# Schlüssel (`key`) und den Datenfeldern als `value` zurück::

    def asdict(self, lowkey=False):
        words = {}
        for entry in self:
            key = entry.key()
            if key == '#': # reiner Kommentar
                try:
                    words['#'].append(entry.comment)
                except KeyError:
                    words['#'] = entry.comment
            elif lowkey:
                words[key.lower()] = entry
            else:
                words[key] = entry
        return words

# writelines()
# ------------
#
# Schreibe eine Liste von Strings (Zeilen ohne Zeilenendezeichen)
# in die Datei `destination`::

    def writelines(self, lines, destination, encoding=None):
        outfile = open(destination, 'w',
                       encoding=(encoding or self.encoding))
        outfile.writelines(lines)

# write_entries()
# ---------------
#
# Schreibe eine Liste von Einträgen (WordEntry_ oder ShortEntry_ Objekten) in
# die Datei `destination`::

    def write_entries(self, wortliste, destination, encoding=None):
        lines = (str(entry) for entry in wortliste)
        self.writelines(lines, destination, encoding)


# WordEntry
# =========
#
# Klasse für Einträge (Zeilen) der Wortliste_ in Langform_.
# Jede Zeile enthält einen Eintrag mit durch Semikolon „;“
# getrennten Feldern.
#
# Beispiel:
#
# >>> from wortliste import WordEntry
#
# >>> aalbestand = WordEntry('Aalbestand;Aal=be<stand # Test')
# >>> print(aalbestand)
# Aalbestand;Aal=be<stand # Test
#
# .. _Langform:
# .. _8-Spalten-Format:
#
# Bedeutung der Felder im 8-Spalten-Format (Langform):
#
# 1. Wort ungetrennt
# 2. Wort mit Trennungen, falls für alle Varianten identisch,
#    anderenfalls leer
# 3. Trennung nach traditioneller Rechtschreibung
# 4. Trennung nach reformierter Rechtschreibung (2006)
# 5. Trennung für Wortform, die entweder in der Schweiz oder mit
#    Großbuchstaben oder Kapitälchen benutzt wird und für traditionelle
#    und reformierte Rechtschreibung identisch ist, anderenfalls leer
# 6. Trennung für Wortform, die entweder in der Schweiz oder mit
#    Großbuchstaben oder Kapitälchen benutzt wird, traditionelle
#    Rechtschreibung (*an<stö-ssig-ste*)
# 7. Trennung für Wortform, die entweder in der Schweiz oder mit
#    Großbuchstaben oder Kapitälchen benutzt wird, reformierte
#    Rechtschreibung (2006). (*an<stös-sigs-te*)
# 8. Trennung nach (deutsch)schweizerischer, traditioneller Rechtschreibung
#    mit Trennung von „ss“, auch wenn es für „ß“ steht. (*an<stös-sig-ste*)
#
# ::

class WordEntry(list):

# Argumente
# ---------
#
# feldnamen, feldindizes
# ~~~~~~~~~~~~~~~~~~~~~~
#
# Benennung der Felder mit Sprachkürzeln (Tags nach [BCP47]_).
# (Die Zählung der Indizes beginnt in Python bei 0.)::

    feldnamen = (# Name    Index  Nr Erklärung
        'key',               # 0  1  Schlüssel
        'de-x-allgemein',    # 1  2  Deutsch, bei Reform 1996 unverändert
        'de-1901',           # 2  3  traditionell (Rechtschreibreform 1901)
        'de-1996',           # 3  4  reformierte Reformschreibung (1996)
        'de-CH-x-allgemein', # 4  5  ohne ß (Schweiz oder versal), unverändert
        'de-1901-x-versal',  # 5  6  ohne ß (Schweiz oder versal) traditionell
        'de-CH-1996',        # 6  7  ohne ß (Schweiz oder versal) reformiert
        'de-CH-1901',        # 7  8  ohne ß (Schweiz) traditionell
        )

    feldindizes = dict((key, i) for i ,key in enumerate(feldnamen))

    # Aliase
    feldindizes['de'] = feldindizes['de-1996']
    feldindizes['de-CH'] = feldindizes['de-CH-1996']

    feldindizes['de-x-versal'] = feldindizes['de-CH-1996']
    feldindizes['de-1996-x-versal'] = feldindizes['de-CH-1996']
    feldindizes['de-x-versal-allgemein'] = feldindizes['de-CH-x-allgemein']


# ersatzfelder
# ~~~~~~~~~~~~
#
# Bei der Auswahl eines Wortes in einer Rechtschreibvariante werden
# "generische" Felder gewählt, falls das "Originalfeld" fehlt. Die folgenden
# Tupel listen die Ersatzfelder für das ensprechende Originalfeld.
#
# Bsp: Ersatz-Felder für 'de-CH-1901':
#
# >>> for i in WordEntry.ersatzfelder[WordEntry.feldindizes['de-CH-1901']]:
# ...     print(WordEntry.feldnamen[i])
# de-1901-x-versal
# de-CH-x-allgemein
# de-1901
# de-x-allgemein
#
# ::

    #               Ersatzindizes  Index  Nr  Name
    ersatzfelder = ((),            # 0    1   key
                    (),            # 1    2   'de'
                    (1,),          # 2    3   'de-1901'
                    (1,),          # 3    4   'de-1996'
                    (1,),          # 4    5   'de-CH-x-allgemein'
                    (4, 2, 1),     # 5    6   'de-1901-x-versal'
                    (4, 3, 1),     # 6    7   'de-1996-x-versal'
                    (5, 4, 2, 1),  # 7    8   'de-CH-1901'
                   )


# comment
# ~~~~~~~
#
# Kommentar, Vorgabe ist ein leerer String::

    comment = ''


# Initialisierung
# ---------------
# ::


    def __init__(self, line, delimiter=';'):

        self.delimiter = delimiter

# eventuell vorhandenen Kommentar abtrennen und speichern::

        if '#' in line:
            line, comment = line.split('#', 1)
            self.comment = comment.strip()
            line = line.rstrip()
            # print(line, self.comment)

        if not line: # kein Inhalt
            return

# Zerlegen in Datenfelder, in Liste eintragen::

        for field in line.split(delimiter):
            if field.startswith('-'):
                self.append('')
            else:
                self.append(field)


# Rückverwandlung in String
# -------------------------
#
# Erzeugen eines Eintrag-Strings (ohne Zeilenendezeichen) aus der Liste der
# Datenfelder und dem Kommentar
#
# __str__()
# ~~~~~~~~~
#
# >>> str(aalbestand)
# 'Aalbestand;Aal=be<stand # Test'
#
# >>> grse = WordEntry('Grüße;Grü-ße')
# >>> str(grse)
# 'Grüße;Grü-ße'
#
# >>> leerkommentar = WordEntry('# Testkommentar ')
# >>> leerkommentar, str(leerkommentar)
# (WordEntry('# Testkommentar'), '# Testkommentar')
#
# ::

    def __str__(self):
        # Nummerieren leerer Felder:
        fields = [field or '-%d-' % (i+1)
                  for (i, field) in enumerate(self)]
        line = ';'.join(fields)
        if self.comment:
            line += ' # ' + self.comment
            if not self:
                line = line.lstrip()
        return line

# __repr__()
# ~~~~~~~~~~
# >>> print(type(repr(grse)), repr(grse))
# <class 'str'> WordEntry('Grüße;Grü-ße')
#
# ::

    def __repr__(self):
        s = '%s(\'%s\')' % (self.__class__.__name__, self)
        return s

# lang_index()
# ------------
#
# Bestimme den Index des zur Sprachvariante gehörenden Datenfeldes unter
# Verwendung der Ersatzfelder:
#
# >>> aalbestand.lang_index('de-x-allgemein')
# 1
# >>> aalbestand.lang_index('de-1901')
# 1
# >>> aalbestand.lang_index('de-1996')
# 1
# >>> aalbestand.lang_index('de-CH-x-allgemein')
# 1
# >>> aalbestand.lang_index('de-1901-x-versal')
# 1
# >>> aalbestand.lang_index('de-1996-x-versal')
# 1
# >>> abbeissen = WordEntry(
# ...     'abbeissen;-2-;-3-;-4-;-5-;ab<bei-ssen;ab<beis-sen;ab<beis-sen')
# >>> print(abbeissen.lang_index('de-x-allgemein'))
# None
# >>> print(abbeissen.lang_index('de-CH-x-allgemein'))
# None
# >>> abbeissen.lang_index('de-1901-x-versal')
# 5
# >>> abbeissen.lang_index('de-1996-x-versal')
# 6
# >>> abbeissen.lang_index('de-CH-1901')
# 7
# >>> urlaubstipp = WordEntry('Urlaubstipp;-2-;-3-;Ur<laubs=tipp')
# >>> print(urlaubstipp.lang_index('de-x-allgemein'))
# None
# >>> print(urlaubstipp.lang_index('de-1901'))
# None
# >>> print(urlaubstipp.lang_index('de-1996'))
# 3
# >>> print(urlaubstipp.lang_index('de-CH-x-allgemein'))
# None
# >>> print(urlaubstipp.lang_index('de-1901-x-versal'))
# None
#
# ::

    def lang_index(self, lang):

        if lang not in self.feldindizes:
            raise ValueError('Sprachvariante "%s" nicht in %s'
                             % (lang, self.feldindizes.keys()))

# Einfacher Fall: eine allgemeine Schreibweise::

        if len(self) == 2:
            if 'ß' in self[0] and ('CH' in lang or 'versal' in lang):
                return None
            else:
                return 1

# Spezielle Schreibung::

        try:
            i = self.feldindizes[lang]
            feld = self[i]
        except IndexError:
            if lang == 'de-CH-1901':
                return self.lang_index('de-1901-x-versal')
            # Allgemeine Schweiz/versal Schreibung:
            if i > 4 and len(self) == 5:
                return 4
            # versal == normal (kein sz):
            if len(self) == 4 and 'ß' not in self[0]:
                if lang in ('de-CH-1901', 'de-1901-x-versal'):
                    return self.lang_index('de-1901')
                elif lang in ('de-CH-1996', 'de-1996-x-versal'):
                    return self.lang_index('de-1996')
            return None  # Feld nicht vorhanden

        if not feld:  # leeres Feld
            return None

        return i

# key()
# -----
#
# Gib einen Schlüssel (ungetrenntes Wort) zurück.
#
# Bei Einträgen im Langform_ ist der Schlüssel gleich dem Inhalt des ersten
# Feldes. Die Funktion dient zur Kompatibilität mit ShortEntry_.
#
# >>> print(WordEntry('Fluss;Quatsch').key())
# Fluss
# >>> print(WordEntry('Dreß;-1-;Dreß;-3-').key())
# Dreß
#
# Das `lang` Argument wird ignoriert, da der Schlüssel (im Gegensatz zur
# Kurzform_) stets eindeutig ist:
#
# >>> print(WordEntry('Dreß;-1-;Dreß;-3-').key('de-1996'))
# Dreß
#
# Der Schlüssel eines leeren Eintrags ist ein leerer String, der eines leeren
# Kommentars das Kommentarzeichen:
#
# >>> WordEntry('').key(), WordEntry('# toller Kommentar').key()
# ('', '#')
#
# ::

    def key(self, lang=None):
        """Gib den Schlüssel (ungetrenntes Wort) zurück."""
        try:
            return self[0]
        except IndexError: # reiner Kommentar oder leerer Eintrag
            if self.comment:
                return '#'
            return ''


# getitem()
# ---------
#
# Gib Feld ``i`` oder Ersatzfeld zurück.
# ::

    def getitem(self, i):
        """Return item ``i`` or a subsititute"""
        try:
            return self[i]
        except IndexError: # Feld i nicht vorhanden
            pass
        # Ersatzregeln anwenden
        for _i in self.ersatzfelder[i]:
            try:
                word = self[_i]
            except IndexError: # Feld i nicht vorhanden
                continue       # nächsten `tag` versuchen
            # Spezialfall: in Versalschreibung ungültige Ersatz-Wörter
            if i >= 4 and _i <4 and 'ß' in word:
                return ''
            return word
        return ''

# .. _WordEntry.get():
#
# get()
# -----
#
# Trennmuster für eine oder mehrere Sprachvarianten ausgeben.
# Das Argument `tags` ist ein String mit einer Liste von BCP47-Sprachtags,
# die durch Kommata oder Doppelpunkt getrennt sind.
#
# >>> aalbestand.get('de-x-allgemein')
# 'Aal=be<stand'
# >>> aalbestand.get('de-1901')
# 'Aal=be<stand'
# >>> aalbestand.get('de-1996')
# 'Aal=be<stand'
# >>> aalbestand.get('de-CH-x-allgemein')
# 'Aal=be<stand'
# >>> aalbestand.get('de-1901-x-versal')
# 'Aal=be<stand'
# >>> aalbestand.get('de-1996-x-versal')
# 'Aal=be<stand'
# >>> aalbestand.get('de-CH-1901')
# 'Aal=be<stand'
#
# >>> weste = WordEntry('Weste;-2-;We-ste;Wes-te')
# >>> weste.get('de-x-allgemein')
# ''
# >>> weste.get('de-1901')
# 'We-ste'
# >>> weste.get('de-1996')
# 'Wes-te'
#
# Wenn mehr als eine Sprache angegeben ist, wird das erste nicht-leere
# Feld zurückgegeben:
#
# >>> abbeissen.get('de-x-allgemein')
# ''
# >>> abbeissen.get('de-CH-x-allgemein')
# ''
# >>> abbeissen.get('de-x-allgemein, de-CH-x-allgemein')
# ''
# >>> abbeissen.get('de-1901-x-versal')
# 'ab<bei-ssen'
# >>> abbeissen.get('de-x-allgemein, de-1901, de-1901-x-versal')
# 'ab<bei-ssen'
# >>> abbeissen.get('de-x-allgemein:de-1901:de-1901-x-versal')
# 'ab<bei-ssen'
# >>> abbeissen.get('de-CH-1901')
# 'ab<beis-sen'
#
# ::

    def get(self, tags):
        for lang in re.split('[,:] *', tags):
            word = self.getitem(self.feldindizes[lang])
            if word:
                break
        else:
            return ''
        return word


# set()
# -----
#
# Trennmuster für Sprachvariante setzen:
#
# >>> abbeissen.set('test', 'de-1901-x-versal')
# >>> print(abbeissen)
# abbeissen;-2-;-3-;-4-;-5-;test;ab<beis-sen;ab<beis-sen
#
# >>> abbeissen.set('test', 'de-1901')
# Traceback (most recent call last):
# ...
# IndexError: kann kein leeres Feld setzen
#
# >>> abbeissen.set('test2', 'de-1901, de-1901-x-versal')
# >>> print(abbeissen)
# abbeissen;-2-;-3-;-4-;-5-;test2;ab<beis-sen;ab<beis-sen
#
# >>> abbeissen.set('ab<bei-ssen', 'de-1901:de-1901-x-versal')
# >>> print(abbeissen)
# abbeissen;-2-;-3-;-4-;-5-;ab<bei-ssen;ab<beis-sen;ab<beis-sen
#
# ::

    def set(self, word, tags):
        for lang in re.split('[,:] *', tags):
            i = self.lang_index(lang)
            if i is None:
                continue
            if word is None:
                word = ''
            self[i] = word
            return
        raise IndexError("kann kein leeres Feld setzen")

# setitem()
# ---------
#
# Feld ``i`` setzen:
#
# >>> entry = WordEntry('test;test')
# >>> entry.setitem(4, 's-x')
# >>> print(entry)
# test;test;-3-;-4-;s-x
# >>> entry.setitem(3, 'sz')
# >>> print(entry)
# test;test;-3-;sz;s-x
#
# ::

    def setitem(self, i, word):
        while len(self) < i+1:
            self.append('')
        self[i] = word


# complete()
# ----------
#
# Alle Felder setzen:
#
# >>> print(WordEntry('Ruhe;Ru-he').completed())
# Ruhe;Ru-he;Ru-he;Ru-he;Ru-he;Ru-he;Ru-he;Ru-he
#
# >>> print(aalbestand, len(aalbestand))
# Aalbestand;Aal=be<stand # Test 2
# >>> print(aalbestand.completed())
# Aalbestand;Aal=be<stand;Aal=be<stand;Aal=be<stand;Aal=be<stand;Aal=be<stand;Aal=be<stand;Aal=be<stand # Test
#
# >>> auffrass = WordEntry('auffrass;-2-;-3-;-4-;auf-frass')
# >>> print(auffrass.completed())
# auffrass;-2-;-3-;-4-;auf-frass;auf-frass;auf-frass;auf-frass
#
# >>> fresssack= WordEntry('Fresssack;-2-;-3-;Fress=sack;Fress=sack')
# >>> print(fresssack.completed())
# Fresssack;-2-;-3-;Fress=sack;Fress=sack;Fress=sack;Fress=sack;Fress=sack
#
# >>> line = 'Flussschiffahrt;-2-;-3-;-4-;-5-;Fluss=schi{ff/ff=f}ahrt;-7-'
# >>> print(WordEntry(line).completed())
# Flussschiffahrt;-2-;-3-;-4-;-5-;Fluss=schi{ff/ff=f}ahrt;-7-;Fluss=schi{ff/ff=f}ahrt
#
# >>> line = 'Ackerstraße;-2-;A{ck/k-k}er=stra-ße;Acker=stra-ße'
# >>> ackerstrasse = WordEntry(line).completed()
# >>> print(ackerstrasse)
# Ackerstraße;-2-;A{ck/k-k}er=stra-ße;Acker=stra-ße;-5-;-6-;-7-;-8-
#
# ::

    def complete(self):
        for i in range(len(self),8):
            self.append(self.getitem(i))

# .. _WordEntry.completed():
#
# completed()
# -----------
#
# Gib eine vervollständigte Kopie des Eintrags zurück.
#
# ::

    def completed(self):
        """Return expanded copy of self."""
        neu = copy.copy(self)
        neu.complete()
        return neu

# .. _ShortEntry.prune():
#
# prune()
# -------
#
# Eintrag kürzen.
#
# Felder für Sprachvarianten zusammenfassen, wenn gleich:
#
# >>> aalbestand.prune()
# >>> print(aalbestand)
# Aalbestand;Aal=be<stand # Test
# >>> auffrass.prune()
# >>> print(auffrass)
# auffrass;-2-;-3-;-4-;auf-frass
# >>> entry = WordEntry('distanziert;-2-;di-stan-ziert;di-stan-ziert')
# >>> entry.prune()
# >>> print(entry)
# distanziert;di-stan-ziert
# >>> entry = WordEntry('Gauss;-2-;Gauss;Gauss;Gauss')
# >>> entry.prune()
# >>> print(entry)
# Gauss;Gauss
# >>> fresssack.prune()
# >>> print(fresssack)
# Fresssack;-2-;-3-;Fress=sack;Fress=sack
#
# >>> masse = WordEntry('Masse;Mas-se;Mas-se;Mas-se;Mas-se;Mas-se;Mas-se;Mas-se')
# >>> masse.prune()
# >>> print(masse)
# Masse;Mas-se
#
# Aber nicht, wenn die Trennstellen sich unterscheiden:
#
# >>> abenddienste = WordEntry(
# ...    'Abenddienste;-2-;Abend=dien-ste;Abend=diens-te')
# >>> abenddienste.prune()
# >>> print(abenddienste)
# Abenddienste;-2-;Abend=dien-ste;Abend=diens-te
# >>> ackerstrasse.prune()
# >>> print(ackerstrasse)
# Ackerstraße;-2-;A{ck/k-k}er=stra-ße;Acker=stra-ße
# >>> entry = WordEntry(
# ...    'Schlammmasse;-2-;-3-;Schlamm=mas-se;-5-;-6-;Schlamm=mas-se;-8-')
# >>> entry.prune()
# >>> print(entry)
# Schlammmasse;-2-;-3-;Schlamm=mas-se
# >>> entry = WordEntry(
# ...    'Flussschiffahrt;-2-;-3-;-4-;-5-;Fluss=schi{ff/ff=f}ahrt;-7-')
# >>> entry.prune()
# >>> print(entry)
# Flussschiffahrt;-2-;-3-;-4-;-5-;Fluss=schi{ff/ff=f}ahrt;-7-
#
# ::

    def prune(self):
        if len(self) == 8:
            if self[7] == self[5]: # de-CH-1901 gleich versal-1901
                self.pop()
        if len(self) == 7:
            if self[6] == self[5]:
                self[4] = self[5] # umschreiben auf versal-allgemein
                self.pop()
                self.pop()
            elif (not(self[4] or self[5] or self[6])
                  or self[5] == self[2] and self[6] == self[3]):
                # alle leer oder ohne ß-Ersetzung
                self.pop()
                self.pop()
                self.pop()
        if len(self) == 5:
            if not self[4]:
                self.pop()
            elif self[1] == self[4]: # de-x-versal == de
                self.pop()
                self.pop()
                self.pop()
            elif self[2] == self[3] == self[4]:
                self[1] = self[2] # Umschreiben auf de (allgemein)
                self.pop()
                self.pop()
                self.pop()
        if len(self) == 4:
            if self[3] == self[2]: # de-1996 == de-1901
                self[1] = self[2] # Umschreiben auf de (allgemein)
                self.pop()
                self.pop()

        if len(self) > 6:
            self[4] = ''
        if len(self) > 2:
            self[1] = ''


# pruned()
# -----------
#
# Gib eine gekürzte Kopie des Eintrags zurück.
# ::

    def pruned(self):
        """Return pruned copy of self."""
        neu = copy.copy(self)
        neu.prune()
        return neu

# .. _WordEntry.merge():
#
# merge()
# -------
#
# Einträge zusammenfassen:
#
# >>> entry = WordEntry('Dienste;-2-;Dien-ste')
# >>> entry.merge(WordEntry('Dienste;-2-;-3-;diens-te'))
# >>> print(entry)
# Dienste;-2-;Dien-ste;Diens-te
# >>> entry = WordEntry('Abenddress;Abend=dress')
# >>> entry.merge(WordEntry('Abenddress;-2-;-3-;-4-;Abend=dress'))
# >>> print(entry)
# Abenddress;Abend=dress
# >>> entry = WordEntry('Gauss;Gauss')
# >>> entry.merge(WordEntry('Gauss;-2-;-3-;-4-;Gauss'))
# >>> print(entry)
# Gauss;Gauss
# >>> masse.merge(WordEntry('masse;-2-;-3-;-4-;-5-;Ma-sse;Mas-se;Mas-se'),
# ...             allow_alternatives=True)
# >>> print(masse)
# Masse;-2-;Mas-se;Mas-se;-5-;Ma[s-/-s]se;Mas-se;Mas-se
#
# ::

    def merge(self, other, allow_alternatives=False, prune=True,
              merge_comments=False, start=0, stop=None):
        self.complete()
        other.complete()
        islower = self.key().islower()
        conflict = False
        # `stop=None` oder `stop=0` bedeutet "keine Obergrenze".
        stop = stop or len(self)
        for i in range(start, stop):
            o_i = other[i]
            if not o_i:
                continue
            if islower != o_i.islower():
                o_i = toggle_case(o_i)
            s_i = self[i]
            if not s_i:
                self[i] = o_i
            elif s_i != o_i:
                if s_i.lower() == o_i.lower():
                    self[i] = toggle_case(o_i)
                elif allow_alternatives:
                    self[i] = alternatives(s_i, o_i) # '[%s/%s]' % (s_i, o_i)
                else:
                    conflict = True
        if merge_comments and not conflict and self.comment != other.comment:
            if allow_alternatives and self.comment and other.comment:
                self.comment += ' / ' + other.comment
            else:
                self.comment = self.comment or other.comment

        if conflict:
            raise AssertionError('Merge Error:\n %s\n %s'
                                 % (str(self), str(other)))

        if prune:
            other.prune()
            self.prune()


# regelaenderungen()
# ------------------
#
# Teste Felder auf Konsistenz mit den Regeländerungen der Orthographiereform
# 1996, ändere Unstimmigkeiten `in place`.
#
# Die neuere Funktion `ableitung1901()`_, deckt mehr Ausnahmen ab aber
# funktioniert nur in eine Richtung.
#
# Falls ein generisches Feld von Änderung betroffen ist, schreibe die
# korrekten Trennungen in die spezifischen Felder.
#
# >>> entry = WordEntry('Würste;Wür-ste')
# >>> entry.regelaenderungen()
# >>> print(entry)
# Würste;-2-;Wür-ste;Würs-te
# >>> entry = WordEntry('Würste;Würs-te')
# >>> entry.regelaenderungen()
# >>> print(entry)
# Würste;-2-;Wür-ste;Würs-te
# >>> entry = WordEntry('Hecke;He-cke')
# >>> entry.regelaenderungen()
# >>> print(entry)
# Hecke;-2-;He{ck/k-k}e;He-cke
# >>> entry = WordEntry('Ligusterhecke;Ligu-ster=he{ck/k-k}e')
# >>> entry.regelaenderungen()
# >>> print(entry)
# Ligusterhecke;-2-;Ligu-ster=he{ck/k-k}e;Ligus-ter=he-cke
#
# Bei Änderungen der Schreibung wird das entsprechende Feld ausgekreuzt:
#
# >>> entry = WordEntry('Hass;Hass')
# >>> entry.regelaenderungen()
# >>> print(entry)
# Hass;-2-;-3-;Hass;Hass
# >>> entry = WordEntry('fasst;fasst')
# >>> entry.regelaenderungen()
# >>> print(entry)
# fasst;-2-;-3-;fasst;fasst
# >>> entry = WordEntry('Missbrauch;Miss<brauch')
# >>> entry.regelaenderungen()
# >>> print(entry)
# Missbrauch;-2-;-3-;Miss<brauch;Miss<brauch
# >>> entry = WordEntry('schlifffest;schliff=fest')
# >>> entry.regelaenderungen()
# >>> print(entry)
# schlifffest;-2-;-3-;schliff=fest
#
# Bei gleicher, nicht betroffener Trennung werden Felder zusammengefasst:
#
# >>> entry = WordEntry('austoben;-2-;aus<to-ben;aus<to-ben')
# >>> entry.regelaenderungen()
# >>> print(entry)
# austoben;aus<to-ben
#
# Achtung: nicht narrensicher -- Ausnahmen und Mehrdeutigkeiten werden nicht
# alle erkannt:
#
# >>> entry = WordEntry('Boss;Boss # engl.') # korrekt
# >>> entry.regelaenderungen()
# >>> print(entry)
# Boss;Boss # engl.
#
# >>> entry = WordEntry('Ästhesie;Äs-the-sie') # Trennung von "sth" erlaubt
# >>> entry.regelaenderungen()
# >>> print(entry)
# Ästhesie;Äs-the-sie
#
# >>> entry = WordEntry('Fluß;Fluß') # in de-1996 nur Fluss
# >>> entry.regelaenderungen()
# >>> print(entry)
# Fluß;Fluß
#
# >>> entry = WordEntry('Abfallager;-2-;Ab<fa{ll/ll=l}a-.ger;-4-') # Dreikonsonantenregel
# >>> entry.regelaenderungen()
# >>> print(entry)
# Abfallager;-2-;-3-;-4-
#
# ::

    def regelaenderungen(self):
        wort01 = self.get('de-1901')
        wort96 = self.get('de-1996')
        w_versal = None

        if wort01 is None or wort96 is None or 'engl.' in self.comment:
            return

        # Trennregeländerungen:

        # Trennung von ck:
        wort01 = wort01.replace('-ck', '{ck/k-k}')
        wort96 = wort96.replace('{ck/k-k}', '-ck')

        # Trenne nie st:
        wort01 = re.sub('(?<!s)s-t(?!h)', '-st', wort01)
        wort96 = wort96.replace('-st', 's-t')

        # kein Schluss-ss und sst in de-1901 (ungetrenntes "ss" nur in Ausnahmen)
        # aber: 'ßt' und Schluß-ß auch in de-1996 möglich (langer Vokal)
        if 'ss' in wort01:
            w_versal = wort01
            wort01 = None

        # Dreikonsonantenregel:
        if wort01 and re.search(r'(.)\1=\1', wort01):
            wort01 = None

        # Speichern:
        if wort01 == wort96: # keine Regeländerung im Wort
            if len(self) > 2:
                self.prune()
            return

        if wort01 is None:
            self.extend( ['']*(4-len(self)) )
            self[1] = ''
            self[2] = ''
            self[3] = wort96
        else:
            self.extend( ['']*(4-len(self)) )
            self[1] = ''
            self[2] = wort01
            self[3] = wort96
        if w_versal:
            self.append(w_versal)


# ShortEntry
# ==========
#
# Klasse für Einträge (Zeilen) der Wortlisten im `5-Felder-Format`_ (Kurzform).
#
# ::

class ShortEntry(WordEntry):
    """Entry of the German hyphenation database file (5 fields)."""

# .. _Kurzform:
#
# 5-Felder-Format
# ---------------
#
# Ein vollständiger Eintrag enthält fünf, durch Semikolon getrennte, Felder
# für die Sprachvarianten `de`, `de-1901`, `de-CH`, `de-1901-x-versal` und
# `de-CH-1901` (Tags nach [BCP47]_).
#
# Jedes Feld enthält ein Wort mit Kennzeichnung der Trennstellen im Format
# der `Wortliste der deutschsprachigen Trennmustermannschaft`_, z.B.
# ``Pro<zent=zah-len``.
#
# Felder können weggelassen werden, wenn sich der Inhalt aus einem
# allgemeineren Feld gewinnen lässt (siehe `Ersatzregeln`_).
#
#
# Feldbelegung
# ~~~~~~~~~~~~
#
# Die Felder beschreiben die Schreibung und Trennung eines Wortes gemäß
# der Sprachvarianten:
#
# 1. `de`: `aktuelle Rechtschreibung`_ wie sie in Deutschland und Österreich
#    angewendet wird.
#
#    Beispiele: ``Diens-te``, ``ba-cken``, ``Grü-ße``, ``Schluss=satz``
#
# 2. `de-1901`: `traditionelle Rechtschreibung`_ wie sie in Deutschland und
#    Österreich von 1901 bis 1996 gültig war.
#
#    Beispiele: ``Dien-ste``, ``ba{ck/k-k}en``, ``Grü-ße``, ``Schluß=satz``
#
# 3. `de-CH` oder `de-x-versal`: aktuelle Rechtschreibung wie sie in der
#    Schweiz und bei ß-Ersatzschreibung angewendet wird.
#
#    Beispiele: ``ba-cken``, ``Grüs-se``, ``Schluss=satz``
#
# 4. `de-1901-x-versal`: traditionelle Rechtschreibung mit
#    ß-Ersatzschreibung wie sie in Deutschland und Österreich
#    angewendet wurde (keine Trennung von „ss“ als Ersatz für „ß“).
#
#    Beispiele: ``ba{ck/k-k}en``, ``Grü-sse``, ``Schluss=satz``
#
# 5. `de-CH-1901`: traditionelle Rechtschreibung wie sie in der Schweiz
#    angewendet wurde (Trennung von „ss“ auch wenn es für „ß“ steht aber
#    keine Dreikonsonantenregel für „ss=s“).
#
#    Beispiele: ``ba{ck/k-k}en``, ``Grüs-se``, ``Schluss=satz``
#
#
# Ersatzregeln
# ~~~~~~~~~~~~
#
# Feld 1 (`de`) ist ein Pflichtfeld, die anderen Felder können weggelassen
# werden, wenn sich der Inhalt über regelmäßige Transformationen_ aus einem
# anderen Feld gewinnen lässt:
#
# ==================== ============= =======================
# Feld                 Quelle        Transformation
# ==================== ============= =======================
# 1 de
# 2 de-1901            1 de          „Rechtschreibreversion_“
# 3 de-CH              1 de          SZ-Ersatz_
# 4 de-1901-x-versal   2 de-1901     SZ-Ersatz_
# 5 de-CH-1901         2 de-1901     SZ-Ersatz_
# ==================== ============= =======================
#
# Die Ersetzung erfolgt rekursiv (d.h. wenn Feld 2 nicht gegeben ist, können
# die Felder 4 und 5 aus Feld 1 mit „Rechtschreibreversion_“ und SZ-Ersatz_
# bestimmt werden).
#
# Wenn ein Eintrag in einer Sprachvariante leer bleiben soll, muss das
# zugehörige Feld entsprechend markiert („ausgekreuzt“) werden.
# (Da die ß-Ersatzschreibung für alle Wörter definiert ist, ist in einer
# vollständigen Liste das Auskreuzen der Spalten 3…5 nicht erforderlich.)
#
# Beispiele:
#   | ``-1-;de<pla-ziert``,
#   | ``auf<wän-dig;-2-;``.
#
#
# Implementiert in `ShortEntry.getitem()`_.
#
#
# Rechtschreibreversion
# """""""""""""""""""""
#
# Überführung eines Wortes in die Sprachvariante de-1901 durch
# Anwendung der mit der Rechtschreibreform 1996 entfallenen Regeln:
#
# st-Trennung_:
#   Trenne nie "st". (Aber "s-th" ist trennbar.)
# ck-Trennung_:
#   Trenne "ck" als "k-k".
# Schluss-ss_:
#   "ß", "ßt" und "ßl" statt "ss", "sst" und "ssl" am Silbenende.
# Dreikonsonantenregel_:
#   Von drei gleichen Konsonanten vor einem Selbstlaut entfällt einer,
#   taucht aber bei Worttrennung wieder auf.
#
# Abweichende Änderungen müssen als Ausnahmen explizit erfasst werden.
#
# Implementiert in `ableitung1901()`_.
#
# SZ-Ersatz
# """""""""
# Ersetze 'ß' mit 'ss', trenne je nach Sprachvarietät (siehe Feldbelegung_).
#
# Implementiert in `versalschreibung()`_.
#
#
#
# Beispiel
# --------
#
# Durch die Ersatzregeln reicht für die meisten Einträge die Angabe des ersten
# Feldes, z.B.
#
# >>> from wortliste import ShortEntry
# >>> entry = ShortEntry('Pass=stra-ße')
#
# Eintrag vervollständigen mit `ShortEntry.complete()`_:
#
# >>> entry.complete(); print(entry)
# Pass=stra-ße;Paß=stra-ße;Pass=stras-se;Pass=stra-sse;Pass=stras-se
#
# Eintrag kürzen mit `ShortEntry.prune()`_:
#
# >>> entry.prune(); print(entry)
# Pass=stra-ße
#
# Wort mit Unregelmäßigkeiten:
#
# >>> entry = ShortEntry('Boss;Boss;Boss;Boss;Boss # en.')
# >>> entry.prune(); print(entry)
# Boss;Boss # en.
#
#
# Argumente
# ---------
#
# feldnamen
# ~~~~~~~~~
#
# Tupel der Sprachbezeichner für die Felder im 5-Felder-Format_::

    feldnamen = (# Name   Index Nr Erklärung
        'de',               # 0 1  Deutsch, aktuell
        'de-1901',          # 1 2  traditionell (Reform 1901)
        'de-CH',            # 2 3  ohne ß (Schweiz/versal) aktuell
        'de-1901-x-versal', # 3 4  ohne ß (versal) traditionell
        'de-CH-1901',       # 4 5  ohne ß (Schweiz) traditionell
        )

# feldindizes
# ~~~~~~~~~~~
#
# Zuordnung von Sprachbezeichnern nach [BCP47]_ zu den Feldern.
#
# In Python beginnt die Indexzählung mit Null::

    feldindizes = dict((key, i) for i, key in enumerate(feldnamen))

# Aliase (vgl. auch BCP47_)::

    feldindizes.update({
        'de-DE':            feldindizes['de'],
        'de-AT':            feldindizes['de'],
        'de-1996':          feldindizes['de'],
        'de-DE-1996':       feldindizes['de'],
        'de-AT-1996':       feldindizes['de'],
        'de-DE-1901':       feldindizes['de-1901'],
        'de-AT-1901':       feldindizes['de-1901'],
        'de-CH-1996':       feldindizes['de-CH'],
        'de-x-versal':      feldindizes['de-CH'],
        'de-1996-x-versal': feldindizes['de-CH'],
        })


# Schlüssel-Cache
# ~~~~~~~~~~~~~~~
# ::

    _key = None


# Initialisierung
# ---------------
#
# Das Argument `line` ist eine Zeichenkette (`str`) im
# `5-Felder-Format`_ oder eine Instanz der Klasse WordEntry_.
#
# >>> print(ShortEntry('Diens-te'))
# Diens-te
# >>> tpp = ShortEntry('Ur<laubs=tipp;-2-')
# >>> print(tpp)
# Ur<laubs=tipp;-2-
#
# >>> print(abenddienste)
# Abenddienste;-2-;Abend=dien-ste;Abend=diens-te
# >>> print(ShortEntry(abenddienste))
# Abend=diens-te
#
# Achtung: Ein Lang-Eintrag mit nur zwei Feldern kann unregelmäßig sein und
# daher im Kurzformat mehrere Felder benötigen (siehe Tests)
# ::

    def __init__(self, line, delimiter=';', prune=True):

        if isinstance(line, WordEntry):
            self.comment = line.comment  # Kommentar
            self._key = line.getitem(0)  # Schlüssel cachen
            if not line:
                return
            self.append(line.getitem(3)) # Deutsch, aktuell (Reform 1996)
            self.append(line.getitem(2)) # "traditionell" (Reform 1901)
            if len(line) > 4:
                self.append(line.getitem(6)) # ohne ß (Schweiz oder versal) "aktuell"
                self.append(line.getitem(5)) # ohne ß (Schweiz oder versal) "traditionell"
                self.append(line.getitem(7)) # ohne ß (Schweiz) "traditionell" ("süssauer")
            elif 'ß' in self._key: # auskreuzen
                self.append('')
                self.append('')
                self.append('')
        else:
            WordEntry.__init__(self, line, delimiter)

        if prune: # Felder zusammenfassen
            self.prune()

# Tests
# ~~~~~
# >>> drs = str(ShortEntry('-1-;Dreß'))
# >>> print(drs)
# -1-;Dreß
# >>> print(ShortEntry('# Testkommentar'))
# # Testkommentar
#
# In der Voreinstellung werden optionale Felder mit `prune()` gekürzt.
# Mit der Option `prune=False` bleiben alle übergebenen Felder erhalten:
#
# >>> print(ShortEntry('Fluss;Fluß'))
# Fluss
# >>> print(ShortEntry('Fluss;Fluß', prune=False))
# Fluss;Fluß
#
# Wird dem Konstruktor eine WordEntry_-Instanz übergeben, so wird diese in das
# Kurzformat überführt:
#
# >>> print(ShortEntry(WordEntry('heute;heu-te')))
# heu-te
#
# Bei Ausnahmen von der regelmäßigen Worttrennung werden bei Bedarf
# zusätzliche Spalten belegt:
#
# >>> print(ShortEntry(WordEntry('Amnesty;Am-nes-ty # en.')))
# Am-nes-ty;Am-nes-ty # en.
# >>> bss = ShortEntry(WordEntry('Boss;Boss # engl.'))
# >>> print(bss)
# Boss;Boss # engl.
#
# Bei Wörtern mit unterschiedlicher Schreibung in den verschiedenen
# Sprachvarianten werden Felder, die im übergebenen `WordEntry` nicht belegt
# sind als leer markiert:
#
# >>> print(urlaubstipp)
# Urlaubstipp;-2-;-3-;Ur<laubs=tipp
# >>> print(ShortEntry(urlaubstipp))
# Ur<laubs=tipp;-2-
# >>> print(ShortEntry(WordEntry('Abfalllager;-2-;-3-;Ab<fall=la-ger')))
# Ab<fall=la-ger;-2-
#
# >>> dresz = WordEntry('Dreß;-2-;Dreß;-4-')
# >>> print(ShortEntry(dresz))
# -1-;Dreß;-3-;-4-;-5-
# >>> g_ebene = WordEntry('Gaußebene;Gauß=ebe-ne')
# >>> print(ShortEntry(g_ebene))
# Gauß=ebe-ne;Gauß=ebe-ne;-3-;-4-;-5-
# >>> print(auffrass)
# auffrass;-2-;-3-;-4-;auf-frass
# >>> print(ShortEntry(auffrass))
# -1-;-2-;auf-frass;auf-frass;auf-frass
# >>> fraesse = WordEntry('frässe;-2-;-3-;-4-;-5-;frä-sse;fräs-se;fräs-se')
# >>> frs = ShortEntry(fraesse)
# >>> print(frs)
# -1-;-2-;fräs-se;frä-sse;fräs-se
# >>> loesz = WordEntry('Lößboden;Löß=bo-den')
# >>> print(ShortEntry(loesz))
# Löß=bo-den;Löß=bo-den;-3-;-4-;-5-
# >>> loess = WordEntry('Lössboden;-2-;-3-;Löss=bo-den;Löss=bo-den')
# >>> print(ShortEntry(loess))
# Löss=bo-den;-2-;Löss=bo-den;Löss=bo-den;Löss=bo-den
#
# >>> print(ShortEntry(WordEntry('Fussballliga;-2-;-3-;-4-;-5-;-6-;Fuss=ball==li-ga')))
# -1-;-2-;Fuss=ball==li-ga
# >>> print(ShortEntry(WordEntry('Fussballiga;-2-;-3-;-4-;-5-;Fuss=ba{ll/ll=l}i-.ga;-7-')))
# -1-;-2-;-3-;Fuss=ba{ll/ll=l}i-.ga;Fuss=ba{ll/ll=l}i-.ga
# >>> messignal = WordEntry('Messignal;-2-;-3-;-4-;-5-;-6-;-7-;Me{ss/ss=s}i-.gnal')
# >>> print(ShortEntry(messignal))
# -1-;-2-;-3-;-4-;Me{ss/ss=s}i-.gnal
#
# >>> ShortEntry(WordEntry('# nur Kommentar'))
# ShortEntry('# nur Kommentar')
#
#
# key()
# -----
#
# Gib einen Schlüssel (ungetrenntes Wort) zurück.
#
# Bei Einträgen im Kurzformat ist der Schlüssel nicht eindeutig: durch
# Transformationen_ kann ein Wort in verschiedenen Schreibungen vorkommen.
#
# Standardmäßig wird das erste nichtleere Feld ohne Trennzeichen verwendet:
#
# >>> print(ShortEntry('Fluss;Fluß').key())
# Fluss
# >>> print(ShortEntry('-1-;Dreß').key())
# Dreß
#
# Die Auswahl kann über das `lang` Argument gesteuert werden:
#
# >>> print(ShortEntry('-1-;Dreß').key('de-CH-1901'))
# Dress
#
# Der Schlüssel eines leeren Eintrags ist ein leerer String, der eines leeren
# Kommenars das Kommentarzeichen:
#
# >>> ShortEntry('').key(), ShortEntry('# toller Kommentar').key()
# ('', '#')
#
# ::

    def key(self, lang=None):
        """Erstelle einen Schlüssel (ungetrenntes Wort)."""
        if lang:
            return join_word(self.get(lang))
        if self._key:
            return self._key
        for f in self:
            if f:
                self._key = f
                return join_word(f)
        if self.comment:
            return '#'  # reiner Kommentar
        return ''       # leerer Eintrag


# .. _`ShortEntry.getitem()`:
#
# getitem()
# ---------
#
# Gib Feld `i` zurück. Wende bei Bedarf die Ersatzregeln_ an.
#
# >>> entry = ShortEntry('Bu-ße')
# >>> print(entry.getitem(1), entry.getitem(2), entry.getitem(4))
# Bu-ße Bus-se Bus-se
#
# >>> entry.getitem(5)
# Traceback (most recent call last):
#     ...
# IndexError: ShortEntry "Bu-ße" hat nur 5 Felder
#
# Mit `subsititute=True` wird der Feldinhalt ignoriert und immer nach
# Ersatzregel bestimmt:
#
# >>> entry = ShortEntry('Bu-ße;Reue;Bu-sse')
# >>> print(entry.getitem(1), entry.getitem(1, substitute=True))
# Reue Bu-ße
#
# ::


    def getitem(self, i, substitute=False):
        """Return item ``i`` or a subsititute"""
        if not substitute:
            try:
                return self[i]
            except IndexError as e: # Feld i nicht vorhanden
                if not self:
                    raise ValueError('leerer Eintrag')
                if i > 4:
                    raise IndexError(f'ShortEntry "{self}" hat nur 5 Felder')

        # Rekursion: wähle das generischere Feld
        ersatzfelder = (None, 0, 0, 1, 1)
        word = self.getitem(ersatzfelder[i])

        # Bei leerem Feld ("ausgekreuzt") ist keine Transformation nötig
        if not word:
            return ''

        # Rechschreibreform  (s-t, ck, Dreikonsonantenregel)
        if i == 1: # de-1901
            return ableitung1901(word)

        # Versalschreibung:  ß -> ss
        if 'ß' in word:
            word = versalschreibung(word, self.feldnamen[i])
        return word


# get()
# -----
#
# Gib Trennmuster für Sprachvariante zurück (ggf. über Transformationen_).
#
# Beispiele:
#
# ohne Transformation
#
# >>> aalbst = ShortEntry('Aal=be<stand # Test')
# >>> aalbst.get('de')
# 'Aal=be<stand'
# >>> aalbst.get('de-1996')
# 'Aal=be<stand'
# >>> aalbst.get('de-1901')
# 'Aal=be<stand'
# >>> aalbst.get('de-x-versal')
# 'Aal=be<stand'
# >>> aalbst.get('de-1901-x-versal')
# 'Aal=be<stand'
# >>> aalbst.get('de-1996-x-versal')
# 'Aal=be<stand'
# >>> aalbst.get('de-CH-1901')
# 'Aal=be<stand'
#
# st und ck:
#
# >>> dst = ShortEntry('Diens-te')
# >>> print(dst.get('de'), dst.get('de-1901'), dst.get('de-1996'))
# Diens-te Dien-ste Diens-te
# >>> print(ShortEntry('Es-te').get('de-1901'))
# E·ste
# >>> sck = ShortEntry('Stre-cke')
# >>> print(sck.get('de'), sck.get('de-1901'))
# Stre-cke Stre{ck/k-k}e
#
# Versalschreibung:
#
# >>> abus = ShortEntry('ab<bü-ßen')
# >>> print(abus.get('de'), abus.get('de-CH'))
# ab<bü-ßen ab<büs-sen
# >>> print(abus.get('de-1901-x-versal'), abus.get('de-CH-1901'))
# ab<bü-ssen ab<büs-sen
#
# >>> print(ShortEntry('passt').get('de-1901'))
# paßt
# >>> print(ShortEntry('passt').get('de-1901-x-versal'))
# passt
# >>> rs = ShortEntry('-1-;-2-;Russ') # versal für Ruß
# >>> print(rs.get('de-x-versal'))
# Russ
# >>> rs.get('de-1901-x-versal')
# ''
#
# >>> entry = ShortEntry('süß=sau-er')
# >>> print(entry.get('de-CH'), entry.get('de-1901-x-versal'), entry.get('de-CH-1901'))
# süss=sau-er süss=sau-er süss=sau-er
#
# >>> ShortEntry('Pro<zess=en-.de').get('de-CH-1901')
# 'Pro<zess=en-.de'
# >>> pstr = ShortEntry('Pass=stra-ße')
# >>> print(pstr.get('de-1996'), pstr.get('de-1901'))
# Pass=stra-ße Paß=stra-ße
# >>> print(pstr.get('de-x-versal'), pstr.get('de-1901-x-versal'))
# Pass=stras-se Pass=stra-sse
#
# Test: Wenn keine Ersatzschreibung vorliegt, wird auch in traditioneller
# Versalschreibung s-s getrennt:
#
# >>> print(ShortEntry('Bu-ße').get('de-1901-x-versal'))
# Bu-sse
# >>> print(ShortEntry('Bus-se').get('de-1901-x-versal'))
# Bus-se
#
# Geerbt von `WordEntry.get()`_.
#
# .. _ShortEntry.complete():
#
# complete()
# ----------
#
# Eintrag vervollständigen (alle 5 Felder ausfüllen).
#
# >>> dst.complete()
# >>> print(dst)
# Diens-te;Dien-ste;Diens-te;Dien-ste;Dien-ste
# >>> aalbst.complete()
# >>> print(aalbst)
# Aal=be<stand;Aal=be<stand;Aal=be<stand;Aal=be<stand;Aal=be<stand # Test
# >>> abus.complete()
# >>> print(abus)
# ab<bü-ßen;ab<bü-ßen;ab<büs-sen;ab<bü-ssen;ab<büs-sen
# >>> bss.complete()
# >>> print(bss)
# Boss;Boss;Boss;Boss;Boss # engl.
# >>> frs.complete()
# >>> print(frs)
# -1-;-2-;fräs-se;frä-sse;fräs-se
# >>> tpp.complete()
# >>> print(tpp)
# Ur<laubs=tipp;-2-;Ur<laubs=tipp;-4-;-5-
#
# >>> entry = ShortEntry('# toller Hecht')
# >>> entry.complete()
# >>> print(entry)
# # toller Hecht
#
# ::

    def complete(self):
        for i in range(len(self), 5):
            try:
                field = self.getitem(i)
            except ValueError: # leerer Eintrag
                return
            self.append(field)


# completed()
# -----------
#
# Gib eine vervollständigte Kopie des Eintrags zurück.
#
# >>> entry = ShortEntry('-1-;-2-;sties-se').completed()
# >>> entry
# ShortEntry('-1-;-2-;sties-se;-4-;-5-')
# >>> [field for field in entry]
# ['', '', 'sties-se', '', '']
# >>> print(pstr.completed())
# Pass=stra-ße;Paß=stra-ße;Pass=stras-se;Pass=stra-sse;Pass=stras-se
# >>> print(ShortEntry('Miss<er<.folg').completed())
# Miss<er<.folg;Miß<er<.folg;Miss<er<.folg;Miss<er<.folg;Miss<er<.folg
#
# Geerbt von `WordEntry.completed()`_.
#
#
# prune()
# -------
#
# Eintrag kürzen.
#
# Felder weglassen, wenn sich der Inhalt durch Ersatzregeln_ gewinnen läßt:
#
# >>> aalbst.prune()
# >>> print(aalbst)
# Aal=be<stand # Test
# >>> dst.prune()
# >>> print(dst)
# Diens-te
# >>> abus.prune()
# >>> print(abus)
# ab<bü-ßen
# >>> bss.prune()
# >>> print(bss)
# Boss;Boss # engl.
# >>> tpp.prune()
# >>> print(tpp)
# Ur<laubs=tipp;-2-
#
# Auch das „Auskreuzen“ wird weitergereicht:
#
# Wenn ein Wort in "traditioneller" Rechtschreibung nicht existiert, reicht
# das "Auskreuzen" von Feld 2:
#
# >>> entry = ShortEntry('Tipp;-2-;Tipp;-4-;-5-')
# >>> entry.prune()
# >>> print(entry)
# Tipp;-2-
#
# Wenn ein Wort in aktueller Rechtschreibung nicht existiert, reicht das
# "Auskreuzen" von Feld 1:
#
# >>> entry = ShortEntry('-1-;Rauh=nacht;-3-;Rauh=nacht')
# >>> entry.prune()
# >>> print(entry)
# -1-;Rauh=nacht
#
# Felder werden nicht gekürzt, wenn die Rekonstruktion einen anderen Wert
# ergäbe:
#
# >>> frs.prune()
# >>> print(frs)
# -1-;-2-;fräs-se;frä-sse;fräs-se
#
# >>> entry = ShortEntry('-1-;Dreß;-3-;-4-;-5-')
# >>> entry.prune()
# >>> print(entry)
# -1-;Dreß;-3-;-4-;-5-
#
# Mit ``drop_sz=True`` werden die drei letzten Felder (ß-Ersatzschreibung)
# stets gekürzt:
#
# * ß-Ersatzschreibung ist immer definiert, daher kann eigentlich nichts falsch
#   gemacht werden.
#
# * Die "nachlässige" Wandlung garantiert nicht die exakte Reproduktion der
#   Ausgangsliste nach einem "Rundtrip":
#
#   Mit `ShortEntry.complete()`_ werden die letzten Spalten ausgefüllt, auch
#   wenn sie im Original "ausgekreuzt" waren.
#
#   Bei Wandlung ins Langformat können zusätzliche Einträge mit
#   ß-Ersatzschreibung entstehen.
#
#   Die Auszeichnung von Mehrdeutigkeiten (z.B. „Mas-se/Ma-sse“ in
#   de-1901-x-versal) geht verloren.
#
# >>> entry.prune(drop_sz=True)
# >>> print(entry)
# -1-;Dreß
# >>> entry.complete()
# >>> print(entry)
# -1-;Dreß;-3-;Dress;Dress
#
# ::

    def prune(self, drop_sz=False):
        if len(self) == 1: # bereits kompakt
            return
        if len(self) > 5:
            raise ValueError(f'ShortEntry mit mehr als 5 Feldern: "{self}"')
        if drop_sz:
            while len(self) > 2:
                self.pop()
        for i in range(len(self)-1, 0, -1):
            wort = self.pop()
            rekonstruktion = self.getitem(i)
            if wort != rekonstruktion:
                # print(tag, repr(self), wort, rekonstruktion)
                self.append(wort)
                return
        if len(self) == 1 and not self[0]:
            self.pop()


# merge()
# -------
#
# Einträge zusammenlegen.
#
# Das als Argument übergebene WordEntry Objekt wird in dem eigenen Eintrag
# eingegliedert.
#
# Leere („ausgekreuzte“) Felder werden überschrieben:
#
# >>> entry = ShortEntry('be<wusst;-2-')
# >>> entry.merge(ShortEntry('-1-;be<wußt'))
# >>> print(entry)
# be<wusst
#
# Identische Felder bleiben erhalten. Alle Felder die über Transformationen_
# rekonstruiert werden können werden entfernt:
#
# >>> entry = ShortEntry('Maß=nah-me # < nehmen')
# >>> entry.merge(ShortEntry('-1-;-2-;Mass=nah-me'))
# >>> print(entry)
# Maß=nah-me # < nehmen
# >>> entry = ShortEntry('-1-;-2-;Mass=nah-me')
# >>> entry.merge(ShortEntry('Maß=nah-me # < nehmen'), merge_comments=True)
# >>> print(entry)
# Maß=nah-me # < nehmen
#
# Es sei den die Option ``prune`` ist ``False``:
#
# >>> entry.merge(ShortEntry('Maß=nah-me'), prune=False)
# >>> print(entry)
# Maß=nah-me;Maß=nah-me;Mass=nah-me;Mass=nah-me;Mass=nah-me # < nehmen
#
# Bei Konflikten wird ein AssertionError erzeugt:
#
# >>> entry = ShortEntry('be<wusst;-2-')
# >>> entry.merge(ShortEntry('-1-;-2-;ver<bor-gen'))
# Traceback (most recent call last):
#     ...
# AssertionError: Merge Error:
#  be<wusst;-2-;be<wusst;-4-;-5-
#  -1-;-2-;ver<bor-gen;-4-;-5-
#
# Geerbt von `WordEntry.merge()`_
#
#
# wordentries()
# -------------
#
# Gib eine Liste von WordEntry_ Instanzen (8-Spalten-Format_) zurück.
#
# >>> mse = ShortEntry('Mas-se')
# >>> mse.wordentries()
# [WordEntry('Masse;Mas-se')]
#
# Im Langformat gibt es für jede Schreibung einen separaten Eintrag:
#
# >>> mas = ShortEntry('Ma-ße')
# >>> for e in mas.wordentries():
# ...     print(e)
# Maße;Ma-ße
# Masse;-2-;-3-;-4-;-5-;Ma-sse;Mas-se;Mas-se
#
# >>> entry = ShortEntry('Löss')
# >>> for e in entry.wordentries():
# ...     print(e)
# Löss;-2-;-3-;Löss;Löss
# Löß;-2-;Löß;-4-
#
# ::

    def wordentries(self, prune=True):

        if not self:
            if self.comment: # leerer Kommentar
                return [WordEntry('# '+self.comment)]
            return [WordEntry('')]

        entries = [] # liste für WordEntry Einträge (Rückgabeobjekt)
        words = {} # dictionary für WordEntry Einträge
        # Zuordnung der Indizes: ShortEntry[s] == WordEntry[l]
        indices = (3, 2, 6, 5, 7)

        for s,l in enumerate(indices):
            word = self.getitem(s)
            if not word:
                continue # Leerfelder überspringen
            # Schlüssel:
            key = join_word(word)
            # WordEntry Instanz heraussuchen oder erzeugen:
            try:
                entry = words[key]
            except KeyError:
                entry = WordEntry(key)
                entry.comment = self.comment
                words[key] = entry
                entries.append(entry)
            # Eintrag in entry[j]:
            entry.setitem(l, word)

        # Auffüllen und Komprimieren
        for entry in entries:
            while len(entry) < 8:
                entry.append('')
            if prune:
                entry.prune()
        return entries


# Hilfsfunktion für Tests:
#
# >>> def print_langform(line, prune=True):
# ...   for e in ShortEntry(line).wordentries(prune):
# ...       print(e)
#
# Eine Schreibung:
#
# >>> print_langform('ba-den')
# baden;ba-den
# >>> print_langform('Wes-te')
# Weste;-2-;We-ste;Wes-te
# >>> print_langform('Toll-patsch;-2-')
# Tollpatsch;-2-;-3-;Toll-patsch
# >>> print_langform('-1-;rau-he')
# rauhe;-2-;rau-he;-4-
#
# Unterschiedliche Schreibungen:
#
# >>> print_langform('Ab<fall=la-ger # Rechtschreibänderung')
# Abfalllager;-2-;-3-;Ab<fall=la-ger # Rechtschreibänderung
# Abfallager;-2-;Ab<fa{ll/ll=l}a-.ger;-4- # Rechtschreibänderung
#
# >>> print_langform('Ab<guss')
# Abguss;-2-;-3-;Ab<guss;Ab<guss
# Abguß;-2-;Ab<guß;-4-
#
# >>> print_langform('groß')
# groß;groß
# gross;-2-;-3-;-4-;gross
#
# >>> print_langform('gro-ßen')
# großen;gro-ßen
# grossen;-2-;-3-;-4-;-5-;gro-ssen;gros-sen;gros-sen
#
# >>> print_langform('Spaß')
# Spaß;Spaß
# Spass;-2-;-3-;-4-;Spass
# >>> print_langform('Spass')
# Spass;-2-;-3-;Spass;Spass
# Spaß;-2-;Spaß;-4-
# >>> print_langform('spa-ßen')
# spaßen;spa-ßen
# spassen;-2-;-3-;-4-;-5-;spa-ssen;spas-sen;spas-sen
#
# >>> print_langform('ab<bü-ßen')
# abbüßen;ab<bü-ßen
# abbüssen;-2-;-3-;-4-;-5-;ab<bü-ssen;ab<büs-sen;ab<büs-sen
#
# >>> print_langform('Biss')
# Biss;-2-;-3-;Biss;Biss
# Biß;-2-;Biß;-4-
# >>> print_langform('Boss;Boss # engl.')
# Boss;Boss # engl.
#
# >>> print_langform('Pass=sys-tem')
# Passsystem;-2-;-3-;Pass=sys-tem;-5-;Pass=sy-stem;Pass=sys-tem
# Paßsystem;-2-;Paß=sy-stem;-4-
#
# >>> print_langform('Press=saft')
# Presssaft;-2-;-3-;Press=saft;Press=saft
# Preßsaft;-2-;Preß=saft;-4-
#
# >>> print_langform('Pro<gramm==maß=nah-me')
# Programmmaßnahme;-2-;-3-;Pro<gramm==maß=nah-me
# Programmaßnahme;-2-;Pro<gra{mm/mm==m}aß=nah-me;-4-
# Programmmassnahme;-2-;-3-;-4-;-5-;-6-;Pro<gramm==mass=nah-me
# Programmassnahme;-2-;-3-;-4-;-5-;Pro<gra{mm/mm==m}ass=nah-me;-7-
#
# >>> print_langform('Pass=stra-ße')
# Passstraße;-2-;-3-;Pass=stra-ße
# Paßstraße;-2-;Paß=stra-ße;-4-
# Passstrasse;-2-;-3-;-4-;-5-;Pass=stra-sse;Pass=stras-se;Pass=stras-se
#
# >>> print_langform('Pro<zess=en-.de;Pro<zeß=en-de;Pro<zess=en-.de')
# Prozessende;-2-;-3-;Pro<zess=en-.de;Pro<zess=en-.de
# Prozeßende;-2-;Pro<zeß=en-de;-4-
#
# Explizit nur eine Schreibung:
#
# >>> print_langform('Abend=dress;Abend=dress')
# Abenddress;Abend=dress
# >>> print_langform('-1-;Abend=dreß')
# Abenddreß;-2-;Abend=dreß;-4-
# Abenddress;-2-;-3-;-4-;-5-;Abend=dress;-7-
#
# Nur Versalschreibung:
#
# >>> print_langform('-1-;-2-;Fuss;Fuss;Fuss')
# Fuss;-2-;-3-;-4-;Fuss
# >>> line = 'Fussballiga;-2-;-3-;-4-;-5-;Fuss=ba{ll/ll=l}i-.ga'
# >>> print(ShortEntry(WordEntry(line), prune=False))
# -1-;-2-;-3-;Fuss=ba{ll/ll=l}i-.ga;Fuss=ba{ll/ll=l}i-.ga
# >>> print_langform('-1-;-2-;-3-;Fuss=ba{ll/ll=l}i-.ga;Fuss=ba{ll/ll=l}i-.ga')
# Fussballiga;-2-;-3-;-4-;-5-;Fuss=ba{ll/ll=l}i-.ga;-7-
# >>> print_langform('-1-;-2-;Fuss=ball==li-ga')
# Fussballliga;-2-;-3-;-4-;-5-;-6-;Fuss=ball==li-ga
#
# Nur Kommentar:
#
# >>> print_langform('# holla')
# # holla
#
#
# Transformationen
# ================
#
# ableitung1901()
# ---------------
#
# Ableitung von de-1901 aus de-1996 (Reversion der Reform 1996).
#
# Mit keep_key == True werden nur Änderungen vorgenommen, die die
# Schreibung des (ungetrennten) Wortes nicht verändern.
#
# >>> from wortliste import ableitung1901
#
# ::

def ableitung1901(wort, keep_key=False):
    """Reverse regular changes of the 1996 orthography reform."""


# Trennregeländerungen
# ~~~~~~~~~~~~~~~~~~~~
#
# Diese Regeln ändern nicht das Wort, nur die Trennmöglichkeiten.
#
# Alternativtrennungen
# """"""""""""""""""""
# (siehe Trennstile.txt)::

    # wort = fremdwortsilben(wort) # Wahltrennungen wie no-b-le
    wort = morphemisch(wort)       # Wahltrennungen wie He-li-ko<p-ter

# st-Trennung
# """""""""""
#
# K75: Trenne nie st.
#
# Ersetze 's-t' mit '-st':
#
# >>> ableitung1901('Diens-te')
# 'Dien-ste'
# >>> print(ableitung1901('wuss-te', keep_key=True))
# wuss-te
#
# Aber Trennung von s-theta erlaubt (nach K74):
#
# >>> print(ableitung1901('Äs-thet'))
# Äs-thet
#
# ::

    wort = re.sub('(?<!s)s-t(?!h)', '-st', wort)

# Keine Trennung nach nur einem Buchstaben am Wortanfang:
#
# >>> print(ableitung1901('Es-te'))
# E·ste
# >>> print(ableitung1901('Nord=os-ten'))
# Nord=o·sten
# >>> print(ableitung1901('Po-ly<es-ter'))
# Po-ly<e·ster
#
# Im Wort sind Einvokalsilben erlaubt:
#
# >>> print(ableitung1901('the>is-tisch'))
# the>i-stisch
# >>> print(ableitung1901('neu-es-te'))
# neu-e-ste
#
# ::

    wort = re.sub('((?<=[=<].)|(?<=^.))-', r'·', wort)


# ck-Trennung
# """""""""""
#
# K76: Trenne 'ck' als 'k-k'.
#
# Ersetze '-ck' mit '{ck/k-k}':
#
# >>> ableitung1901('Ha-cke')
# 'Ha{ck/k-k}e'
# >>> ableitung1901('A·cker')
# 'A{ck/k-k}er'
# >>> ableitung1901('Got-tes=a·cker')
# 'Got-tes=a{ck/k-k}er'
# >>> ableitung1901('Aal=beck')
# 'Aal=beck'
# >>> ableitung1901('be<spick-te')
# 'be<spick-te'
#
# ::

    wort = re.sub('[-·]ck', '{ck/k-k}', wort)

    if keep_key:
        return wort


# Rechtschreibänderungen
# ~~~~~~~~~~~~~~~~~~~~~~
#
# Diese Regeln ändern die Schreibung des ungetrennten Worts (und somit den
# Schlüssel im Langformat der Wortliste).
#
# Schluss-ss
# """"""""""
#
# K38: Kein "ss" und "sst" am Silbenende (ungetrenntes "ss" nur in Ausnahmen)
# (Andererseit ist 'ßt' und Schluss-ß auch in de-1996 möglich (langer Vokal).)
#
# Ersetze ungetrenntes 'ss' mit 'ß':
#
# >>> print(ableitung1901('passt'))
# paßt
# >>> print(ableitung1901('Hass'))
# Haß
# >>> print(ableitung1901('Fass=brau-se'))
# Faß=brau-se
# >>> print(ableitung1901('wuss-te'))
# wuß-te
#
# ß steht für inlautendes ss, wenn ein 'e' ausfällt (und der Ausfall nicht
# durch Apostroph angedeutet wird)
#
# >>> print(ableitung1901('wäss-rig'))
# wäß-rig
# >>> print(ableitung1901('an<ge<mess-ner'))
# an<ge<meß-ner
# >>> print(ableitung1901('duss-lig'))
# duß-lig
# >>> print(ableitung1901('bissl'))
# bißl
#
# Keine Wandlung zu "ß":
# getrenntes Doppel-s (s-s)
#
# >>> print(ableitung1901('Was-ser'))
# Was-ser
#
# Vokal folgt (Fremdwörter):
# >>> print(ableitung1901('Com-tesse'))
# Com-tesse
#
# Großbuchstabe folgt
#
# >>> print(ableitung1901('WissZeitVG')) # Abkürzung
# WissZeitVG
#
# Drei oder mehr 's' = Lautmalerei → erhalten:
#
# >>> print(ableitung1901('pssst'))
# pssst
#
# ::

    wort = re.sub('(?<=[^s])ss(?=[^aeiouyäöüA-Zs]|$)', 'ß', wort)

# Unterdrückung der Trennstelle nach "…ß=er" und "…ß=en" nicht nötig:
#
# >>> print(ableitung1901('hass=er<.füllt'))
# haß=er<füllt
# >>> print(ableitung1901('Guss=er-.ker'))
# Guß=er-ker
#
# ::

    wort = re.sub('ß(=+)e([rn][-<]+)\.', r'ß\1e\2', wort)

# Dreikonsonantenregel
# """"""""""""""""""""
#
# K78: Zusammensetzungen, bei denen von drei zusammenstoßenden gleichen
# Konsonanten einer entfällt (K15), schreibt man bei Silbentrennung wieder
# mit allen drei Konsonanten.
#
# Ersetze 'xx=x' mit '{xx/xx=x}' (für alle Konsonanten vor Selbstlaut)
#
# >>> print(ableitung1901('Kipp=pflug'))
# Kipp=pflug
# >>> print(ableitung1901('Kipp=punkt'))
# Ki{pp/pp=p}unkt
# >>> print(ableitung1901('Ab<fall=la-ger'))
# Ab<fa{ll/ll=l}a-.ger
# >>> print(ableitung1901('All<lie-be'))
# A{ll/ll<l}ie-be
# >>> print(ableitung1901('hell>licht'))
# he{ll/ll>l}icht
# >>> print(ableitung1901('Pro<gramm==maß=nah-me'))
# Pro<gra{mm/mm==m}aß=nah-me
#
# ::

    wort = re.sub(r'([bfglmnprt])\1([<=>]+)\1(?=[aeiouyäöü])',
                  r'{\1\1/\1\1\2\1}', wort)

# Unterdrücken der Trennung nach nur einem Buchstaben::

    wort = re.sub(r'(?<=[<=>].}[aeiouyäöü])([-<])\.?', r'\1.', wort)

    return wort

# Tests:
#
# Ein-Vokal-Silben auch schon 1901 erlaubt:
#
# >>> print(ableitung1901('ver<knäu-e-le'))
# ver<knäu-e-le
#
# versalschreibung()
# ------------------
# Ersetze 'ß' mit 'ss', trenne je nach Sprachvarietät `lang`:
#
# >>> from wortliste import versalschreibung
# >>> print(versalschreibung('paßt'))
# passt
# >>> print(versalschreibung('Dar-ßer'))
# Dars-ser
# >>> print(versalschreibung('bü-ßen', 'de-CH'))
# büs-sen
# >>> print(versalschreibung('bü-ßen', 'de-1996-x-versal'))
# büs-sen
# >>> print(versalschreibung('bü-ßen', 'de-CH-1901'))
# büs-sen
# >>> print(versalschreibung('bü-ßen', 'de-1901-x-versal'))
# bü-ssen
# >>> print(versalschreibung('ä·ßen', 'de-CH-1901'))
# äs-sen
# >>> print(versalschreibung('ä·ßen', 'de-1901-x-versal'))
# ä·ssen
# >>> print(versalschreibung('auf<äßen', 'de-CH-1901'))
# auf<äs-sen
# >>> print(versalschreibung('auf<eßt'))
# auf<esst
# >>> print(versalschreibung('Groß=se-gel', 'de-1901-x-versal'))
# Gross=se-gel
# >>> print(versalschreibung('Groß=se-gel', 'de-CH-1901'))
# Gross=se-gel
# >>> print(versalschreibung('Paß=sy-ste-me', 'de-CH-1901'))
# Pass=sy-ste-me
# >>> print(versalschreibung('Pro<zeß=en-de'))
# Pro<zess=en-.de
# >>> print(versalschreibung('Pro<zess=en-.de'))
# Pro<zess=en-.de
# >>> print(versalschreibung('Pro<zeß=en<er-gie'))
# Pro<zess=en<.er-gie
# >>> print(versalschreibung('Fluß==sy-stem'))
# Fluss==sy-stem
# >>> print(versalschreibung('Meß==sen-der'))
# Mess==sen-der
#
# ::

def versalschreibung(wort, lang='de'):

    if not 'ß' in wort:
        return wort

    wort = wort.replace('ß', 'ss')

# Trennung von Ersatz-ss in de-CH und de-1996 nach Sprechsilbenregel::

    if '1901-x-versal' not in lang:
        wort = wort.replace('·ss', 's-s') # a·ßen -> as-sen
        # wort = re.sub('(?<=[aeiouyäöü])-\.?ss', 's-s', wort)
        wort = re.sub('-\.?ss(?=[aeiouyäöü])', 's-s', wort)
        wort = re.sub('(?<=^[aeiouyäöü])ss(?=[aeiouyäöü])', 's-s', wort)
        wort = re.sub('(?<=[=<][aeiouyäöü])ss(?=[aeiouyäöü])', 's-s', wort)

# Unterdrückung irreführender Trennung::

    wort = re.sub('ss(=+)(en|er)([<-])\.?', r'ss\1\2\3.', wort)

# Dreikonsonantenregel für Ersatz-ss in de-CH-1901::

    if 'CH-1901-x-dreikonsonanten' in lang:
        wort = re.sub('ss(=+)s(?=[aeiouyäöü])', r'{ss/ss\1s}', wort)
        # Unterdrücken der Trennung nach nur einem Buchstaben und irreführender Trennungen
        wort = re.sub(r'(?<=[=>]s}[aeiouyäöü])([-<])\.?', r'\1.', wort)
        # wort = re.sub(r'(?<===s}[aeiouyäöü])([-<])\.?', r'\1.', wort) # Reißverschus=sy-.stem
        wort = re.sub('(?<=[=>]s})(en|er)([<-])\.?', r'\1\2.', wort)

    return wort


# Kurzformat in Langformat
# ------------------------
#
# Zusätzlich benötigte Felder werden automatisch erzeugt. Ein Kurzeintrag kann
# mehrere Langeinträge ergeben:
#
# >>> from wortliste import short2long
# >>> for line in short2long(["Diens-te", "Ge<biss"]):
# ...     print(line)
# Dienste;-2-;Dien-ste;Diens-te
# Gebiss;-2-;-3-;Ge<biss;Ge<biss
# Gebiß;-2-;Ge<biß;-4-
#
# >>> for line in short2long(["Ge<schoss", "Ge<schoß # österr."]):
# ...     print(line)
# Geschoss;-2-;-3-;Ge<schoss;Ge<schoss
# Geschoß;Ge<schoß # österr.
#
# short2long()
# ~~~~~~~~~~~~
# ::

def short2long(lines, sort=True, prune=True):
    """Convert sequence of lines in ShortEntry format to WordEntry instances.
    """

    # Sammeln in Liste und Dictionary:
    words = {} # zum Zusammenfassen
    entries = []

    for line in lines:
        shortentry = ShortEntry(line)
        shortentry.complete()
        for entry in shortentry.wordentries(prune=False):
            key = entry.key().lower()
            try: # Eintrag mit gleichem Schlüssel vorhanden?
                altentry = words[key]
            except KeyError: # nein -> neuer Eintrag
                words[key] = entry
                entries.append(entry)
                continue
            try:
                if entry[3]: # de-1996 non-empty
                    entry.merge(altentry, prune=False, allow_alternatives=True)
                    # Alternativen Eintrag "in-place" ersetzen:
                    for i, word in enumerate(entry):
                        altentry[i] = word
                    altentry.comment = entry.comment
                else:
                    altentry.merge(entry, prune=False, allow_alternatives=True)
            except AssertionError as e:
                sys.stderr.write(str(e)+'\n')
                entries.append(entry)
            except IndexError: # Leerer Eintrag (Kommentar)
                entries.append(entry)


    if prune:
        for entry in entries:
            entry.prune()

    if sort:
        entries.sort(key=sortkey_duden)

    return entries

# Tests:
#
# Kommentare bleiben erhalten:
#
# >>> for line in short2long(['Aal=an-geln', '# toller Kommentar'],
# ...                        sort=False):
# ...     print(line)
# Aalangeln;Aal=an-geln
# # toller Kommentar
#
# Beim Sortieren werden Kommenare an den Beginn geschrieben.
# >>> for line in short2long(['# erster Kommentar',
# ...                         'Aal=an-geln',
# ...                         '# zweiter Kommentar']):
# ...     print(line)
# # erster Kommentar
# # zweiter Kommentar
# Aalangeln;Aal=an-geln
#
# >>> for line in short2long(['Fluss=schiff=fahrt']):
# ...     print(line)
# Flussschiffahrt;-2-;-3-;-4-;-5-;Fluss=schi{ff/ff=f}ahrt;-7-
# Flußschiffahrt;-2-;Fluß=schi{ff/ff=f}ahrt;-4-
# Flussschifffahrt;-2-;-3-;Fluss=schiff=fahrt
#
#
# Langformat in Kurzformat
# ------------------------
#
# Optionale Felder werden weggelassen, wenn sie mit dem automatisch erzeugten
# Inhalt übereinstimmen:
#
# >>> from wortliste import long2short
# >>> for entry in long2short(["Dienste;-2-;Dien-ste;Diens-te"]):
# ...     print(entry)
# Diens-te
#
# Zusammengehörige Einträge werden zusammengefasst:
#
# >>> for line in long2short(["Großanlass;-2-;-3-;Groß=an<lass",
# ...                         "Großanlaß;-2-;Groß=an<laß;-4-",
# ...                         "Grossanlass;-2-;-3-;-4-;Gross=an<lass"]):
# ...     print(line)
# Groß=an<lass
#
# Sonderfälle
# ~~~~~~~~~~~
#
# Bei einigen Wörtern ist die ß-ss-Beziehung nicht eindeutig:
#
# =======  ========  =========
# de1901   de-1996   de-CH
# =======  ========  =========
# Maße     Maße      Masse
# Masse    Masse     Masse
# Geschoß  Geschoß   Geschoss
# Geschoß  Geschoss  Geschoss
# =======  ========  =========
#
# Daher kann es vorkommen, dass ein Langform-Eintrag Beiträge von
# verschiedenen Kurzformen erhält. So erzeugen, z.B. sowohl
# „Masse“ als auch „Maße“ einen Langeintrag mit Schlüssel „Masse“:
#
# >>> for entry in short2long(['Mas-se']): print(entry)
# Masse;Mas-se
# >>> for entry in short2long(['Ma-ße']): print(entry)
# Masse;-2-;-3-;-4-;-5-;Ma-sse;Mas-se;Mas-se
# Maße;Ma-ße
#
# In der Langform werden Alternativen in ein Feld geschrieben:
#
# >>> for entry in short2long(['Mas-se', 'Ma-ße']):
# ...     print(entry)
# Masse;-2-;Mas-se;Mas-se;-5-;Ma[s-/-s]se;Mas-se;Mas-se
# Maße;Ma-ße
#
# Sind die Alternativen bereits in der Quelle, bleiben sie in der Kurzform
# erhalten:
#
# >>> ml = ['Masse;-2-;Mas-se;Mas-se;-5-;Ma[-s/s-]se;Mas-se;Mas-se',
# ...       'Maße;Ma-ße']
# >>> for entry in long2short(ml):
# ...     print(entry)
# Mas-se;Mas-se;Mas-se;Ma[-s/s-]se
# Ma-ße;Ma-ße;Mas-se;Ma[-s/s-]se
#
# Zurück in die Langform:
#
# >>> for entry in short2long(['Mas-se;Mas-se;Mas-se;Ma[-s/s-]se',
# ...                            'Ma-ße;Ma-ße;-3-;Ma[-s/s-]se']):
# ...     print(entry)
# Masse;-2-;Mas-se;Mas-se;-5-;Ma[-s/s-]se;Mas-se;Mas-se
# Maße;Ma-ße
#
# long2short()
# ~~~~~~~~~~~~
# ::

def long2short(lines, prune=True, drop_sz=False):
    """Convert sequence of 8-column lines to ShortEntry instances."""
    words = {}           # Einträge mit `de`
    words_x = {}         # Einträge ohne `de`
    words_merged = set() # Einträge die vollständig in andere einsortiert wurden
    entries = []         # Rückgabewert: Liste der Kurzeinträge

# Zeilen Einlesen, Wandeln und Sammeln::

    for line in lines:
        longentry = WordEntry(line)
        entry = ShortEntry(longentry, prune=False)
        key = entry.key().lower() # Schlüssel ohne Großschreibung

        if not entry: # reiner Kommentar oder leerer Eintrag
            if entry.comment:
                entries.append(entry)
            continue

        # Einträge mit leerem ersten Feld werden später einsortiert:
        if prune and not entry[0]:
            # print(key,  "in words_x eintragen")
            words_x[key] = entry
            continue

        # Eintrag in `dictionary` und Liste:
        words[key] = entry
        entries.append(entry)

# Straffen (Weglassen von Feldern/Einträgen wenn möglich),
# es sei denn, der Aufruf erfolgte mit `prune=False`::

    if not prune:
        return entries

    for entry in entries:

        # Auffüllen:
        i = 1
        while i < len(entry):
        # for i in range(1, len(entry)):
            if not entry[i]:  # Feld ausgekreuzt
                key_i = join_word(entry.getitem(i, substitute=True)).lower()
                # print("Auffüllen", i, key_i, str(entry))
                try:
                    co_entry = words_x[key_i]
                    entry.merge(co_entry, prune=False, start=1)
                    words_merged.add(key_i)
                    # del words_x[key_i]
                except (KeyError, AssertionError):
                    try:
                        co_entry = words[key_i]
                        entry.merge(co_entry, prune=False, start=1)
                        words_merged.add(key_i)
                    except (KeyError, AssertionError):
                        pass
            i += 1

# Anhängen aller Einträge mit leerem `de`-Feld, die nicht in
# einen  zugehörigen Eintrag einsortiert wurden an die Liste::

    for key, entry in words_x.items():
        # print(key, str(entry))
        if key in words_merged:
            continue
        # ggf. ergänzen:
        if len(entry) > 2 and not entry[3]:  # de-1901-x-versal
            key = join_word(entry.getitem(3, substitute=True)).lower()
            try:
                co_entry = words_x[key]
                entry.merge(co_entry, prune=False, start=3)
                words_merged.add(key)
            except (KeyError, AssertionError):
                try:
                    co_entry = words[key]
                    entry.merge(co_entry, prune=False, start=3)
                    # print(key, str(co_entry))
                except (KeyError, AssertionError):
                    pass

        entries.append(entry)

    for entry in entries:
        entry.prune(drop_sz)

    return entries

# Tests:
#
# Separate Einträge ß-Schreibungen zusammenfassen:
#
# Ein von 2 ß wird zu ss in de-1996:
#
# >>> for entry in long2short([
# ...       'Passstrasse;-2-;-3-;-4-;-5-;Pass=stra-sse;Pass=stras-se;Pass=stras-se',
# ...       'Passstraße;-2-;-3-;Pass=stra-ße',
# ...       'Paßstraße;-2-;Paß=stra-ße;-4-']):
# ...     print(entry)
# Pass=stra-ße
#
# ß in de-1996:
#
# >>> for line in long2short([
# ...       "Grossserie;-2-;-3-;-4-;-5-;Gross=se-rie;Gross=se-rie",
# ...       "Großserie;Groß=se-rie"]):
# ...     print(line)
# Groß=se-rie
#
# kein ß in de-1996:
#
# >>> for line in long2short([
# ...       "Basssaite;-2-;-3-;Bass=sai-te;-5-;Bass=sai-te;Bass=sai-te",
# ...       "Baßsaite;-2-;Baß=sai-te;-4-"]):
# ...     print(line)
# Bass=sai-te
#
# ß und Dreikonsonantenregel:
#
# >>> for line in long2short([
# ...       "Flussschiffahrt;-2-;-3-;-4-;-5-;Fluss=schi{ff/ff=f}ahrt;-7-",
# ...       "Flußschiffahrt;-2-;Fluß=schi{ff/ff=f}ahrt;-4-",
# ...       "Flussschifffahrt;-2-;-3-;Fluss=schiff=fahrt"]):
# ...     print(line)
# Fluss=schiff=fahrt
#
# Zusätzliche Variante (Fremdwort vs. Lehnwort) in de-1901:
#
# >>> for entry in short2long(['Boss;Boss # en.']): print(entry)
# Boss;Boss # en.
# >>> for entry in long2short(['Boss;Boss # en.']): print(entry)
# Boss;Boss # en.
# >>> for entry in long2short(['Boss;Boss # en.', 'Boß;-2-;Boß;-3- # < en.']):
# ...     print(entry)
# Boss;Boss # en.
# -1-;Boß # < en.
#
# Alternativschreibung in de-1996 (Geschoß, Löß):
#
# >>> for entry in long2short(['Geschoss;-2-;-3-;Ge<schoss;Ge<schoss',
# ...                          'Geschoß;Ge<schoß # österr. auch de-1996']):
# ...     print(entry)
# Ge<schoss
# Ge<schoß # österr. auch de-1996
#
# Eigennamen auf -ss:
#
# >>> for entry in long2short(['Vossstrasse;-2-;-3-;-4-;-5-;Voss=stra-sse;Voss=stras-se;Voss=stras-se',
# ...                          'Vossstraße;Voss=stra-ße',
# ...                          'Voßstraße;Voß=stra-ße']):
# ...     print(entry)
# Voss=stra-ße;Voss=stra-ße
# Voß=stra-ße
#
# Kommentare:
#
# >>> long2short(['# toller Kommentar'], prune=False)
# [ShortEntry('# toller Kommentar')]
# >>> long2short(['# toller Kommentar'], prune=True)
# [ShortEntry('# toller Kommentar')]
#
#
# Hilfsfunktionen
# ===============
#
# join_word()
# -----------
#
# Trennzeichen entfernen::

def join_word(wort, assert_complete=False):

# Einfache Trennzeichen:
#
# ===  ===================================  ==========================
#  =   Trennstelle an Wortfugen             Wort=fu-ge
#  <   Trennstelle nach Präfix              Vor<sil-be
#  >   Trennstelle vor Suffix               Freund>schaf-ten
# \-   Nebentrennstelle                     ge-hen
#  .   unerwünschte/ungünstige Trennstelle  Fett=em<.bo-li.en
#  ·   Randtrennstelle für Notentext        O·bo·e
# ===  ===================================  ==========================
#
# ::

    wort = re.sub('[-=<>.·]', '', wort)

# Spezielle Trennungen für die traditionelle Rechtschreibung
# (siehe ../../dokumente/README.wortliste)::

    if '{' in wort or '}' in wort:
        wort = wort.replace('{ck/kk}',  'ck')
        wort = wort.replace('{ck/k',  'k')
        wort = wort.replace('k}',     'k')
        # Konsonanthäufungen an Wortfuge: '{xx/xxx}' -> 'xx':
        wort = re.sub(r'\{(.)\1/\1\1\1\}', r'\1\1', wort)
        # schon getrennt: ('{xx/xx' -> 'xx' und 'x}' -> 'x'):
        wort = re.sub(r'\{(.)\1/\1\1$', r'\1\1', wort)
        wort = re.sub(r'^(.)\}', r'\1', wort)

# Trennstellen in doppeldeutigen Wörtern::

    if '[' in wort or ']' in wort:
        wort = re.sub(r'\[(.*)/\1\]', r'\1', wort)
        # schon getrennt:
        wort = re.sub(r'\[([^/\[]+)$', r'\1', wort)
        wort = re.sub(r'^([^/\]]+)\]', r'\1', wort)

# Test auf verbliebene komplexe Trennstellen::

    if assert_complete:
        for spez in '[{/}]':
            if  spez in wort:
                raise AssertionError('Spezialtrennung %s, %s' %
                                     (wort, wort))

    return wort

# zerlege()
# ---------
#
# Zerlege ein Wort mit Trennzeichen in eine Liste von Silben und eine Liste
# von Trennzeichen)
#
# >>> from wortliste import zerlege
#
# >>> zerlege('Haupt=stel-le')
# (['Haupt', 'stel', 'le'], ['=', '-'])
# >>> zerlege('Ge<samt=be<triebs=rats==chef')
# (['Ge', 'samt', 'be', 'triebs', 'rats', 'chef'], ['<', '=', '<', '=', '=='])
# >>> zerlege('an<stands>los')
# (['an', 'stands', 'los'], ['<', '>'])
# >>> zerlege('An<al.pha-bet')
# (['An', 'al', 'pha', 'bet'], ['<', '.', '-'])
#
# ::

def zerlege(wort):
    silben = re.split('[-·._<>=]+', wort)
    trennzeichen = re.split('[^-·._|<>=]+', wort)
    return silben, [tz for tz in trennzeichen if tz]

# alternatives()
# --------------
#
# Gib einen String mit Trennmarkierung für abweichende Trennungen in
# mehrdeutigen Wörtern zurück:
#
# >>> from wortliste import alternatives
# >>> alternatives('Mas-se', 'Ma-sse')
# 'Ma[s-/-s]se'
# >>> alternatives('Ma-sse', 'Mas-se')
# 'Ma[-s/s-]se'
#
# ::

def alternatives(wort1, wort2):
    # convert to lists
    wort1 = [c for c in wort1]
    wort2 = [c for c in wort2]
    pre = []
    post = []
    for c1, c2 in zip(wort1, wort2):
        if c1 != c2:
            break
        pre += c1
    for c1, c2 in zip(wort1.__reversed__(), wort2.__reversed__()):
        if c1 != c2:
            break
        post += c1
    post.reverse()
    return ''.join(pre + ['['] + wort1[len(pre):-len(post)] + ['/']
                  + wort2[len(pre):-len(post)] + [']'] + post)


# toggle_case()
# -------------
#
# Großschreibung in Kleinschreibung wandeln und umgekehrt
#
# Diese Version funktioniert auch für Wörter mit Trennzeichen (während
# str.title() nach jedem Trennzeichen wieder groß anfängt)
#
# >>> from wortliste import toggle_case
# >>> toggle_case('Ha-se')
# 'ha-se'
# >>> toggle_case('arm')
# 'Arm'
# >>> toggle_case('frei=bier')
# 'Frei=bier'
# >>> toggle_case('L}a-ger')
# 'l}a-ger'
#
# Keine Änderung bei Wörtern mit Großbuchstaben im Inneren:
#
# >>> toggle_case('USA')
# 'USA'
# >>> toggle_case('iRFD')
# 'iRFD'
#
# >>> toggle_case('gri[f-f/{ff/ff')
# 'Gri[f-f/{ff/ff'
# >>> toggle_case('Gri[f-f/{ff/ff')
# 'gri[f-f/{ff/ff'
#
# ::

def toggle_case(wort):
    try:
        key = join_word(wort, assert_complete=True)
    except AssertionError:
        key = wort[0]
    if key.istitle():
        return wort.lower()
    elif key.islower():
        return wort[0].upper() + wort[1:]
    else:
        return wort

# sortkey_duden()
# ---------------
#
# Schlüssel für die alphabetische Sortierung gemäß Duden-Regeln.
#
# >>> from wortliste import sortkey_duden
# >>> sortkey_duden(["Abflußröhren"])
# 'abflussrohren a*bflu*szroehren'
# >>> sortkey_duden(["Abflußrohren"])
# 'abflussrohren a*bflu*szro*hren'
# >>> sortkey_duden(["Abflussrohren"])
# 'abflussrohren'
#
# >>> s = sorted([["Abflußröhren"], ["Abflußrohren"], ["Abflussrohren"]],
# ...            key=sortkey_duden)
# >>> print(', '.join(e[0] for e in s))
# Abflussrohren, Abflußrohren, Abflußröhren
#
# Umschreibung
#
# Ligaturen auflösen und andere "normalisierende" Ersetzungen für den
# (Haupt-)Sortierschlüssel (Akzente werden über ``unicodedata.normalize``
# entfernt)::

umschrift_skey = {
                  ord('æ'): 'ae',
                  ord('œ'): 'oe',
                  ord('ø'): 'o',
                  ord('ſ'): 's',
                 }

# "Zweitschlüssel" zur Unterscheidung von Umlauten/SZ und Basisbuchstaben::

umschrift_subkey = {
                    ord('a'): 'a*',
                    ord('å'): 'aa',
                    ord('ä'): 'ae',
                    ord('o'): 'o*',
                    ord('ö'): 'oe',
                    ord('ø'): 'oe',
                    ord('u'): 'u*',
                    ord('ü'): 'ue',
                    ord('ß'): 'sz',
                   }

# Argument ist ein Eintrag im WordEntry oder ShortEntry Format oder
# ein (Unicode) String:
#
# >>> print(sortkey_duden(weste)) # WordEntry
# weste
# >>> print(sortkey_duden(dst)) # ShortEntry
# dienste
#
# >>> print(sortkey_duden('Stra-ße'))
# strasse stra*sze
# >>> print(sortkey_duden(['Büh-ne']))
# buhne buehne
# >>> print(sortkey_duden('Weiß=flog;Weiß=flog;-3-;-4-;-5-'))
# weissflog weiszflo*g
# >>> print(sortkey_duden('-1-;Meß=sen-der;-3-;-4-;-5-'))
# messsender meszsender
# >>> print(sortkey_duden('As-sen # (geogr. und Eigen-) Name\n'))
# assen
# >>> print(sortkey_duden('aßen;aßen;-3-;-4-;-5-\n'))
# assen a*szen
# >>> print(sortkey_duden('äßen\n'))
# assen aeszen
#
# ::

def sortkey_duden(entry):

# ggf. ungetrenntes Wort extrahieren oder generieren::

    if isinstance(entry, list):
        try:
            key = entry.key()
        except AttributeError:
            key = entry[0]
            if len(entry) == 1:  # ein Muster pro Zeile, siehe z.B. pre-1901
                key = join_word(key)
    else:
        match = re.search('^[-0-9;]*([^;\s]+)', entry) # erstes volles Feld
        if match:
            key = match.group(1)
        else:
            key = ''
        key = join_word(key)
        key = re.sub('[0-9;]', '', key)

# Großschreibung ignorieren:
#
# Der Duden sortiert Wörter, die sich nur in der Großschreibung unterscheiden
# "klein vor groß" (ASCII sortiert "groß vor klein"). In der
# `Trennmuster-Wortliste` kommen Wörter nur mit der häufiger anzutreffenden
# Großschreibung vor, denn der TeX-Trennalgorithmus ignoriert Großschreibung.
# ::

    key = key.lower()

# Ersetzungen:
#
# ß -> ss ::

    skey = key.replace('ß', 'ss')

# Restliche Akzente weglassen: Wandeln in Darstellung von Buchstaben mit
# Akzent als "Grundzeichen + kombinierender Akzent". Anschließend alle
# nicht-ASCII-Zeichen ignorieren::

    skey = skey.translate(umschrift_skey)
    skey = unicodedata.normalize('NFKD', skey)
    skey = skey.encode('ascii', 'ignore').decode()

# "Zweitschlüssel" für das eindeutige Einsortieren von Wörtern mit
# gleichem Schlüssel (Masse/Maße, waren/wären, ...):
#
# * "*" nach aou für die Unterscheidung Grund-/Umlaut
# * ß->sz
#
# ::

    if key != skey:
        subkey = key.translate(umschrift_subkey)
        skey = '%s %s' % (skey,subkey)

# Gib den Sortierschlüssel zurück::

    return skey



# udiff()
# -------
#
# Vergleiche zwei Sequenzen von `WordEntries` (genauer: alle Objekte, die
# sich sinnvoll zu Unicode wandeln lassen).
#
# Beispiel:
#
# >>> from wortliste import udiff
# >>> print(udiff([abbeissen, aalbestand], [abbeissen,dresz], 'alt', 'neu'))
# --- alt
# +++ neu
# @@ -1,2 +1,2 @@
#  abbeissen;-2-;-3-;-4-;-5-;ab<bei-ssen;ab<beis-sen;ab<beis-sen
# -Aalbestand;Aal=be<stand # Test
# +Dreß;-2-;Dreß;-4-
# <BLANKLINE>
# >>> udiff([abbeissen, aalbestand], [abbeissen, aalbestand], 'alt', 'neu')
# ''
#
# ::

def udiff(a, b, fromfile='', tofile='',
          fromfiledate='', tofiledate='', n=1):

    a = [str(entry).rstrip() for entry in a]
    b = [str(entry).rstrip() for entry in b]

    diff = '\n'.join(difflib.unified_diff(a, b, fromfile, tofile,
                        fromfiledate, tofiledate, n, lineterm=''))
    if diff:
        diff += '\n'
    return diff


# run_filters()
# -------------
#
# Anwenden der Trennstile auf ein Wort:
#
# >>> from wortliste import run_filters
#
# >>> run_filters(['keine_flattervokale'], 'Psy-ch<i-a-trie')
# 'Psy-ch<ia-trie'
# >>> run_filters(['keine_flattervokale'], 'An<woh-ner=in<.i-ti-a-ti-ve')
# 'An<woh-ner=in<.i-tia-ti-ve'
#
# Test: Trennungen wie bisher im 'dehyphen-exptl' TeX-Paket:
#
# >>> run_filters(['morphemisch', 'standard'], 'Psy-ch<i-a-trie')
# 'Psych<ia-trie'
# >>> run_filters(['morphemisch', 'standard'], 'An<woh-ner=in<.i-ti-a-ti-ve')
# 'An<woh-ner=initia-ti-ve'
#
# >>> run_filters(['standard'], 'Text=il<..lu-stra-ti-.on')
# 'Text=illu-stra-tion'
#
# >>> run_filters(['morphemisch'], 'Psy-ch<i-a-trie')
# 'Psych<i·a-trie'
# >>> run_filters(['morphemisch'], 'An<woh-ner=in<.i-ti-a-ti-ve')
# 'An<woh-ner=in<.i·ti-a-ti-ve'
#
# >>> run_filters(['morphemisch'], 'ge-r<i-a-tri-sche')
# 'ger<i·a-tri-sche'
# >>> run_filters(['syllabisch','regelsilben'], 'ge-r<i-a-tri-sche')
# 'ge-ri-at-ri-sche'
#
# >>> run_filters(['regelsilben'], 'abs-trakt')
# 'abst-rakt'
#
# >>> run_filters(['morphemisch'], 'Fern=ab<.i-tur')
# 'Fern=ab<.i·tur'
# >>> run_filters(['syllabisch','regelsilben'], 'Fern=ab<.i-tur')
# 'Fern=abi-tur'
# >>> run_filters(['morphemisch', 'standard'], 'Fern=ab<.i-tur')
# 'Fern=abitur'
#
# ::

def run_filters(styles, word):
    """Apply a sequence of hyphenation `styles` to `word`."""

    for style in styles:
        try:
            word = getattr(stilfilter, style)(word)
        except AttributeError:
            if style:
                raise ValueError('Trennstil %s nicht definiert. Siehe "--stilliste"' % style)
    return word


# normalize_language_tag(tag)
# ---------------------------
#
# Normalisierung und Expansion von Sprachtags nach [BCP47]_
#
# >>> from wortliste import normalize_language_tag
# >>> normalize_language_tag('de_AT-1901')
# ['de-AT-1901', 'de-AT', 'de-1901', 'de']
#
# >>> normalize_language_tag('de')      # Deutsch, allgemeingültig
# ['de']
# >>> normalize_language_tag('de_1901') # traditionell (Reform 1901)
# ['de-1901', 'de']
# >>> normalize_language_tag('de_1996') # reformiert (Reform 1996)
# ['de-1996', 'de']
# >>> normalize_language_tag('de_CH')   # ohne ß (Schweiz oder versal)
# ['de-CH', 'de']
# >>> normalize_language_tag('de-x-versal') # versal
# ['de-x-versal', 'de']
# >>> normalize_language_tag('de-1901-x-versal') # versal
# ['de-1901-x-versal', 'de-1901', 'de-x-versal', 'de']
# >>> normalize_language_tag('de_CH-1996') # Schweiz traditionell (süssauer)
# ['de-CH-1996', 'de-CH', 'de-1996', 'de']
#
# Feldnamen in `WordEntry`:
#
# 'de-x-allgemein':      Deutsch, bei Reform 1996 unverändert
# 'de-1901':             traditionell (Rechtschreibreform 1901)
# 'de-1996':             reformierte Reformschreibung (1996)
# 'de-CH-x-allgemein':   ohne ß (Schweiz oder versal), unverändert
# 'de-1901-x-versal':    ohne ß (Schweiz oder versal) traditionell
# 'de-CH-1996':          ohne ß (Schweiz oder versal) reformiert
# 'de-CH-1901':          ohne ß (Schweiz) traditionell
#
# ::

def normalize_language_tag(tag):
    """Return a list of normalized combinations for a `BCP 47` language tag.
    """
    # normalize:
    tag = tag.replace('_', '-')
    # split (except singletons, which mark the following tag as non-standard):
    tag = re.sub(r'-([a-zA-Z0-9])-', r'-\1_', tag)
    taglist = []
    subtags = [subtag.replace('_', '-') for subtag in tag.split('-')]
    base_tag = [subtags.pop(0)]
    # find all combinations of subtags
    for n in range(len(subtags), 0, -1):
        # for tags in unique_combinations(subtags, n):
        for tags in itertools.combinations(subtags, n):
            taglist.append('-'.join(base_tag+list(tags)))
    taglist += base_tag
    return taglist


# Iteratoren
# ==========
#
# filelines
# ---------
#
# Iterator über mehrere Eingabedateien.
# Ergibt Sequenz von Zeilen als Unicode-Strings::

def filelines(names):
    for name in names:
        if name=='-':
            infile = sys.stdin
        else:
            infile = open(name, encoding='utf-8')
        for line in infile:
            yield line.rstrip()


# sprachauszug
# ------------
#
# Iterator über Worliste-Datei(en)
#
# Ausgabe: Nach Trennregeln der Sprachvariante getrenntes Wort.
#
#
# ::

def sprachauszug(infiles, language="de-1996", entry_class=WordEntry, verbose=False):

    for line in filelines(infiles):

# Zeile lesen und in WordEntry oder ShortEntry Objekt wandeln::

        if not line or line.startswith('#'):
            if verbose:
                yield line
            continue
        entry = entry_class(line)

# Wort in der gewünschten Sprachvarietät aussuchen::

        word = entry.get(language)
        if not word:
            if verbose:
               yield ('# ' + line)
            continue

        yield word


# Tests
# =====
#
# Teste Übereinstimmung des ungetrennten Wortes in Feld 1 mit den
# Trennmustern nach Entfernen der Trennmarker. Schreibe Inkonsistenzen auf die
# Standardausgabe.
#
# Das Argument ist ein Iterator über die Einträge (Klasse `WordEntry`). ::

def test_keys(wortliste):
    print("Teste Schlüssel-Trennmuster-Übereinstimmung:")
    is_OK = True
    for entry in wortliste:
        if isinstance(entry, ShortEntry):
            print("Wortliste im Kurzformat: überspringe Schlüssel-Test.")
            return
            # Test der Übereinstimmung ungetrenntes/getrenntes Wort
        # für alle Felder:
        key = entry.key()
        for wort in entry[1:]:
            if not wort: # leere Felder
                continue
            if key != join_word(wort):
                is_OK = False
                print("  key '%s' != join_word('%s')" % (key, wort),)
                if key.lower() == join_word(wort).lower():
                    print("  Abgleich der Großschreibung mit"
                          "`prepare-patch.py grossabgleich`.", end=' ')
    if is_OK:
        print("OK")


# Finde Doppeleinträge (teste, ob jeder Schlüssel nur einmal vorkommt).
# Schreibe Inkonsistenzen auf die Standardausgabe.
#
# Das Argument ist ein Iterator über die Einträge (Klasse `WordEntry`). ::

def test_uniqueness(wortliste):

    doppelte = 0
    words = {}
    for entry in wortliste:
        key = entry.key()
        if key in words:
            doppelte += 1
            print("da ", str(words[key]))
            print("neu", str(entry))
        words[key] = entry
    print("%d Doppeleinträge." % doppelte)

# Teste die Wandlung einer Zeile im "wortliste"-Format in eine
# ``WordEntry``-Instanz und zurück::

def test_str_entry_str_conversion(wordfile):
    OK = 0
    for line in filelines([wordfile.name]):
        entry = WordEntry(line)
        if line == str(entry):
            OK +=1
        else:
            print('-', line,)
            print('+', str(entry))

    print(OK, "Einträge rekonstruiert")


# Teste Vervollständigung und Zusammenfassung von Einträgen::

def test_completion_pruning(entries):
    reko = []
    for entry in entries:
        new = copy.copy(entry)
        new.complete()
        new.prune()
        reko.append(new)
    patch = udiff(entries, reko, 'wortliste', 'neu')
    if patch:
        print(patch)
    else:
        print("alle Einträge rekonstruiert")

def test_doppelte(entries):
        doppelte = 0
        langs = ("de", "de-1901", "de-CH", "de-1901-x-versal", "de-CH-1901")
        for lang in langs:
            words[lang] = {}

        for entry in entries:
            entry.complete()
            for lang in langs:
                word = entry.get(lang)
                key = join_word(word)
                if word and (key in words[lang]):
                    oldword = words[lang][key].get(lang)
                    if oldword and (word != oldword):
                        doppelte += 1
                        print("%-16s" % lang, oldword, '!=', word)
                        print(" "*18, str(words[lang][key].pruned()))
                        print(" "*18, str(entry.pruned()))
                words[lang][key] = entry
        print("%d Doppeleinträge." % doppelte)



# Aufruf von der Kommandozeile
# ============================
#
# ::

if __name__ == '__main__':

    print("Test der Werkzeuge und inneren Konsistenz der Wortliste")

# Ein WordFile Dateiobjekt::

    try:
        wordfile = WordFile(sys.argv[1], format='auto')
    except IndexError:
        wordfile = WordFile('../../../wortliste')
        # wordfile = WordFile('../../../wlst', format='auto') # wortliste im Kurzformat
        # wordfile = WordFile('neu.todo')
        # wordfile = WordFile('neu-kurz.todo', format='f5')
        # wordfile = WordFile('korrektur.todo', format='auto')

# Format bestimmen::

    print("Datei: '%s'" % wordfile.name, "Format:", wordfile.format)

# Liste der Datenfelder (die Klasseninstanz als Argument für `list` liefert
# den Iterator über die Felder, `list` macht daraus eine Liste)::

    wordlist = list(wordfile)
    print(len(wordlist), "Zeilen")
    # print(wordlist[0], type(wordlist[0]))

# Ein Wörterbuch (dict Instanz)::

    wordfile.seek(0)            # Pointer zurücksetzen
    words = wordfile.asdict()
    print(len(words), "Wörterbucheinträge")

# Test auf Doppeleinträge::

    if len(words) != len(wordlist) and wordfile.format != "f5":
        test_uniqueness(wordlist)

# Teste Schlüssel-Trennmuster-Übereinstimmung::

    if isinstance(wordlist[0], WordEntry):
        test_keys(wordlist)

# Teste Eintrags-Konsistenz im Kurzformat::

    # if isinstance(wordlist[0], ShortEntry):
    #     test_doppelte(wordlist)

# Teste Komplettieren/Zusammenfassen der Einträge::

    # test_completion_pruning(wordlist)

# Sprachauswahl::

    # Sprachtags:
    #
    # sprache = 'de-1901' # traditionell
    sprache = 'de-1996' # Reformschreibung
    # sprache = 'de-x-versal'      # ohne ß (Schweiz oder versal) allgemein
    # sprache = 'de-1901-x-versal' # ohne ß (Schweiz oder versal) "traditionell"
    # sprache = 'de-1996-x-versal' # ohne ß (Schweiz oder versal) "reformiert"
    # sprache = 'de-CH-1901'       # ohne ß (Schweiz) "traditionell" ("süssauer")
    #
    # worte = [entry.get(sprache) for entry in wordlist]
    # worte = [wort for wort in worte if wort]
    # print(len(worte), "Einträge für Sprachvariante", sprache)

# Zeilenrekonstruktion::

    # test_str_entry_str_conversion(wordfile)


# Teste Konsistenz der Auszeichnung::

    # schwankungsfaelle = {}
    # words = [entry.get(sprache) for entry in wordlist]
    # for word in words:
    #     if '.' in word:
    #         match = re.search('[<=]([^<=]*[aeiouyäöü]\.[aeiouyäöü])[^<=]', word)
    #         if not match:
    #             match = re.search('^(.*[aeiouyäöü]\.[aeiouyäöü])[^<=]', word)
    #         if match:
    #             key = match.group(1).lower()
    #             key = key[:-2] + '-' + key[-1]
    #             schwankungsfaelle[key] = word
    # print(len(schwankungsfaelle), "Schwankungsfälle (von </= bis eins nach .)")
    # # for key, value in schwankungsfaelle.items():
    # #     print("%15s" % key, ":", value)
    # print("Inkonsistenzien:")
    # for word in words:
    #     for key in schwankungsfaelle:
    #         if (word.startswith(key.lower())
    #             or '=' + key in word
    #             or '<' + key in word):
    #             print(key[:-2] + '*' + key[-1], schwankungsfaelle[key], word)


# Quellen
# =======
#
# .. [BCP47] A. Phillips und M. Davis, (Editoren.),
#    `Tags for Identifying Languages`, https://www.rfc-editor.org/rfc/bcp/bcp47.txt
#
# .. _aktuelle Rechtschreibung:
#
# .. [Rechtschreibregeln] Rat für deutsche Rechtschreibung,
#    `Deutsche Rechtschreibung – Regeln und Wörterverzeichnis`,
#    https://www.rechtschreibrat.com/regeln-und-woerterverzeichnis/
#
# .. _traditionelle Rechtschreibung:
#
# .. [Duden1991] Wissenschaftlicher Rat der Dudenredaktion (Editoren),
#    `Duden: Rechtschreibung der deutschen Sprache`,
#    Dudenverlag Mannheim, 1991.
#
# .. _Wortliste der deutschsprachigen Trennmustermannschaft:
#    ../../../dokumente/README.wortliste
#
# .. _wortliste: ../../../wortliste
