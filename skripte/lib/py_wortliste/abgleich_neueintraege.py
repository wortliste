#!/usr/bin/env python3
# :Copyright: © 2014 Günter Milde.
#             Released without warranty under the terms of the
#             GNU General Public License (v. 2 or later)
# :Id:        $Id:  $

# Versuche Trennstellen neuer Wörter aus vorhandenen zu ermitteln
# ===============================================================
#
# ::

"""Trenne neue Wörter durch Ableitung von Einträgen der Wortliste.

Eingabe: 1 ungetrenntes Wort oder Eintrag im Wortliste-Format pro Zeile.

Ausgabe: Wortliste-Einträge (Vorschläge), sortiert nach:
  identisch (falls die Ableitung eindeutig und gleich der Vorgabe ist),
  eindeutige Zerlegung
  eindeutige Zerlegung (andere Großschreibung),
  mehrdeutige Zerlegung,
  Rest.

Bsp: python abgleich_neueintraege.py < dict-fail.txt > neu.todo

     (``neu.todo`` kann (nach Durchsicht!!) mit `prepare_patch.py neu`
     in die Wortliste eingepflegt werden.)
"""

# .. contents::
#
# Beispiel:
#
# >>> beispielliste = """\
# ... Füllventil;Füll=ven-til
# ... logistisch;-2-;lo-gi-stisch;lo-gis-tisch
# ... Störprozess;-2-;-3-;Stör=pro<zess
# ... Kauzahn;Kau=zahn
# ... Zahnweh;Zahn=weh
# ... Weh;Weh
# ... Kau;Kau # nur in Zusammensetzungen
# ... Atom;Atom
# ... Atome;Atome
# ... Atomen;Atomen
# ... Energie;En<er-gie
# ... Achterdeck;Ach-ter=deck
# ... jährl;jährl # Abk.
# ... Braten;Bra-ten
# ... Sauce;Sau-ce
# ... Gas;Gas
# ... Explosion;Ex<plo-sion
# ... """
#
# >>> from wortliste import WordEntry
# >>> from abgleich_neueintraege import *
# >>> bspliste = [WordEntry(line) for line in beispielliste.splitlines()]
# >>> bspwords = dict((entry[0], entry) for entry in bspliste)
#
# Abhängigkeiten::

import sys, os, argparse, re
from collections import defaultdict  # Wörterbuch mit Default

# path for local Python modules (parent dir of this file's dir)
sys.path.insert(0,
        os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from wortliste import (WordFile, WordEntry, ShortEntry,
                       join_word, toggle_case, sortkey_duden)
from wortzerlegung import expand_words, list_affixes, split_prefixes

# Funktionen
# -----------
#
# praefixabgleich()
# ~~~~~~~~~~~~~~~~~
#
# Übertrag von Praefixen auf Wörter ohne Präfix.
#
# >>> print(praefixabgleich('Abfüllventil', 'ab', bspwords ))
# Abfüllventil;Ab<füll=ven-til
# >>> print(praefixabgleich('psychologistisch', 'psy-cho', bspwords))
# psychologistisch;-2-;psy-cho<lo-gi-stisch;psy-cho<lo-gis-tisch
# >>> print(praefixabgleich('Entstörprozess', 'Ent', bspwords, verbose=True))
# Entstörprozess;-2-;-3-;Ent<stör=pro<zess # Ent< + Störprozess
#
# ::

def praefixabgleich(key, praefix, words, grossklein=False, verbose=False):

    if key.istitle():
        praefix = praefix.title()
    praekey = join_word(praefix)

    if not key.startswith(praekey):
        return None

    altkey = key[len(praekey):]

    if key.istitle() and not grossklein:
        altkey = altkey.title()
    if grossklein:
        altkey = toggle_case(altkey)

    try:
        altentry = words[altkey]
    except KeyError:
        # print 'missing', key
        return None

    entry = WordEntry(key)
    # print "fundum", key, str(entry)
    for wort in altentry[1:]:
        if wort:
            wort = '<'.join([praefix, wort.lower()])
        entry.append(wort)

    if verbose:
        entry.comment = praefix + '- + ' + altkey
    return entry

# Häufige zusammengesetzte Präfixe::

compound_prefixes = ['ab<zu',
                     'a<·syn',
                     'auf<zu',
                     'aus<zu',
                     'aus<zu',
                     'ein<zu',
                     'mit<zu',
                     'um<zu',
                     'un-ter<zu',
                     'weg<zu',
                    ]

# endungsabgleich()
# ~~~~~~~~~~~~~~~~~
#
# Übertrag von Einträgen auf Wörter mit anderer Endung:
#
# >>> print(endungsabgleich('Füllventile', 'l', '-le', bspwords, verbose=True))
# Füllventile;Füll=ven-ti-le # "l"->"-le"
#
# Das Argument `grossklein` sucht nach Vorkommen mit anderer Großschreibung:
#
# >>> print(endungsabgleich('Logismus', 'tisch', 'mus', bspwords))
# None
# >>> print(endungsabgleich('Logismus', 'tisch', 'mus', bspwords,
# ...                       grossklein=True, verbose=True))
# Logismus;Lo-gis-mus # "tisch"->"mus"
#
# Vorsicht mit ck und Abkürzungen (jährl. -> jährle);
#
# >>> print(endungsabgleich('Achterdecken', 'ck', '-cken', bspwords, verbose=True))
# Achterdecken;-2-;Ach-ter=de{ck/k-k}en;Ach-ter=de-cken # "ck"->"-cken"
# >>> print(endungsabgleich('jährle', 'l', '-le', bspwords))
# None
#
# ::

def endungsabgleich(key, alt, neu, words, grossklein=False, verbose=False,
                    kurzformat=False):

    lang = 'de-1996'

    if not key.endswith(join_word(neu)):
        return None
    oldkey = key[:-len(join_word(neu))] + join_word(alt)
    if grossklein:
        oldkey = toggle_case(oldkey)

    try:
        oldentry = words[oldkey]
    except KeyError:
        # print "missing:", oldkey
        return None
    if 'Abk.' in oldentry.comment:
        # Abkürzungen sind keine Basis für einen Endungsabgleich
        return None
    wort = oldentry.get(lang)
    if not wort or not wort.endswith(alt): # nicht da / andere Trennung
        # print "skipping", repr(wort)
        return None

    # neues Wort aus altem Eintrag ableiten:
    if alt:
        wort = wort[:-len(alt)]
    wort += neu
    if grossklein:
        wort = toggle_case(wort)
    if join_word(wort) != key:
        print("# Übertragungsproblem: %s → %s (%s,%s) %s"
              % (oldkey, key, alt, neu, str(entry)), file=sys.stderr)
        return None

    entry = WordEntry(key)
    # print("fundum", key, '←',  str(oldentry))

    entry.append(wort)

    if not kurzformat:
        entry.regelaenderungen() # bidirektionaler Sprachvariantenabgleich
    if verbose:
        entry.comment = '«%s» → «%s»' % (alt, neu)
    return entry


# endungen
# """"""""
# ``(<alt>, <neu>)`` Paare von Endungen.
#
# Problem:
#   mehr Endungspaare lösen mehr Fälle aber führen auch zu mehr Fehldeutungen.
#
# ::

endungen = [
            ('', '-de'),
            # ('', '-en'),
            # ('', '-er'),
            # ('', '-is-mus'),
            # ('', '-ität'),
            ('', '-lein'),
            ('', '-ne'),
            ('', '-nem'),
            ('', '-nen'),
            ('', '-ner'),
            ('', '-sche'),
            ('', '-tum'),
            # ('', '>chen'),   # ! dar-wins>chen, grabs>chen
            ('', '>fach'),
            ('', '>heit'),
            ('', '>keit'),
            ('', '>los'),
            ('', '>sam'),
            ('', '>schaft'),
            ('', '>schaft'),
            ('', '>wei-se'),
            # ('', 'd'),
            # ('', 'e'),
            # ('', 'e-rin'),
            # ('', 'er'),
            # ('', 'is-mus'),
            # ('', 'm'),
            # ('', 'n'),
            # ('', 'ner'),
            # ('', 'r'),
            # ('', 's'),
            # ('', 's-te'),
            # ('', 's-te'),
            # ('', 's>los'),
            # ('', 'st'),
            # ('', 't'),
            # ('', 't-te'),
            ('·a', '-as'),
            ('·e', '-es'),
            ('·i', '-is'),
            ('·o', '-os'),
            ('·u', '-us'),
            ('·ä', '-äs'),
            ('·ö', '-ös'),
            ('·ü', '-üs'),
            ('>li-che', '-tem'),
            ('>li-che', '-ten'),
            ('-al', 'a-le'),
            ('-an', 'a-ne'),
            ('-at', 'a-te'),
            ('-ben', 'b-ne'),
            # ('-che', 'ch'),
            ('-de', 'd'),
            ('-en', '>bar>keit'),
            # ('-en', 'e'),
            ('-en', '-e-ne'),
            ('-er', '-e-rei'),
            ('-er', '-e-rin'),
            ('-ern', '-e-re'),
            ('-ge', 'g'),
            ('-gen', 'g'),
            ('-in', 'i-ne'),
            ('-le', '-ler'),
            ('-on', 'o-nen'),
            ('-re', 'r'),
            ('-re', 'rt'),
            ('-ren', 'r-ne'),
            ('-ren', 'rt'),
            ('-ren', 'r-te'),
            ('-sche', 'sch'),
            ('-sen', 's-ne'),
            # ('-sten', 's-mus'), # nur de-1901
            ('-te','t'),
            ('-ten', '-mus'),
            ('-ten', '-ren'),
            ('-ten', '-tung'),
            ('-ter', '-te-ren'),
            ('-tern', 't-re'),
            ('-ti-on', '-tor'),
            ('-ös', 'ö-se'),
            ('a', 'ar'),
            ('a', 'as'),
            ('b', '-be'),
            ('b', '-ber'),
            ('>bar', 't'),
            ('bt', 'b-te'),
            ('-ce', '-cen'),
            # ('ch', '-che'), # ! Tis-che, ...
            # ('ch', '-cher'),
            ('ck', '-cke'),
            ('ck', '-cker'),
            ('ck', '-cken'),
            ('ck', '-ckes'),
            ('-cke', '-cker'),
            ('-cke', '-cken'),
            ('-cke', '-ckes'),
            ('d', '-de'),
            ('d', '-dem'),
            ('d', '-den'),
            ('d', '-der'),
            ('d', '-des'),
            ('d', '>heit'),
            # ('e', 'en'),     # ! Ak-ti.en, A·ri-en, A·ka-zi-en, O·bo-en
            ('e-ren', '-ti-on'),
            ('e-ren', 'sch'),
            # ('el', 'le'), # ! dun-kle
            # ('en', 'e'),
            ('en', 'em'),
            ('en', 'en-de'),
            ('en', 'end'),
            ('en', 'er'),
            ('en', 'es'),
            ('en', 'est'),
            # ('en', 't'), # ! ma-cht, wer-ft, ...
            # ('en', 'te'), # ! brä-chte, fi-schte, kap-pte, ...
            ('en', 'us'),
            ('end','en' ),
            # ('er', 'e'),
            ('er', 'e-rei'),
            ('er', 'ens'),
            ('er', 'in'),
            ('er', 'ung'),
            ('es', 'est'),
            # ('es', 's-te'), # ! se-li-ges !-> se-li-gs-te
            ('f', '-fe'),
            ('f', '-fer'),
            ('g', '-ge'),
            ('g', '-gen'),
            ('g', '-ger'),
            ('g', '-ger'),
            ('g', '-ges'),
            ('g', '-gung'),
            ('ie', 'e'),
            ('in', 'en'),
            ('isch', 'i-sche'),
            # ('k', '-ke'),       # ! Blick !-> Blic-ke
            # ('k', '-ken'),      # ! Blick !-> blic-ken
            # ('k', '-ker'),      # ! hock  !-> hoc-ker
            ('l', '-le'), # ! ä·qua-to-ri-a-le, ... (44:1000)
            ('l', '-len'),
            ('l', '-ler'),
            ('l', '-lis-mus'),
            ('ln', '-le'),
            ('lt', '-le'),
            ('m', '-me'),
            ('m', '-mer'),
            ('me', 'men'),
            ('mus', 'men'),
            ('mus', 'ten'),
            ('mus', 'tik'),
            # ('n', '-at'),   # ! Dor-er
            # ('n', '-er'),
            # ('n', '-ne'),
            # ('n', '-nen'),
            ('on', 'o-nis-mus'),
            ('n', '-nis-mus'),
            ('n', 'r'),
            ('n', 'st'),
            ('n', 't'),
            ('n','-ner'),
            ('n','-nist'),
            ('nd','n'),
            ('ne','ner'),
            # ('ne','n'),
            ('o','-on'),
            ('o','os'),
            ('o','en'),
            ('on','o-nen'),
            ('p', '-pe'),
            ('p', '-pen'),
            ('p', '-per'),
            ('ph', '-phen'),
            ('ph', '-phis-mus'),
            ('r', '-re'),
            ('r', '-rei'),
            ('r', '-ren'),
            ('r', '-rin'),
            ('r', '-ris-mus'),
            ('r', '-rung'),
            ('re', 'ste'),
            # ('ren', 'r-te'), # ! mo-de-rie-r-te
            # ('ren', 'rst'),  # ! de<kan-tie-rst, fil-trie-rst
            # ('ren', 'rt'),   # ! de<kan-tie-rt, ir-rt
            ('rn', '-re'),
            ('rn', '-rung'),
            ('rn', '-rung'),
            ('rt', '-re'),
            ('rt', 'r-te'),
            ('s', ''),
            ('s', '-se'),
            ('s', '-se-re'),
            ('s', '-se-res'),
            ('s', '-ser'),
            ('s', 's-se'),
            ('s', 's-ses'),
            ('sch', '-sche'),
            ('sch', '-schen'),
            ('sch', '-scher'),
            # ('st', '-ste'),  # ! nur de-1901
            # ('st', '-sten'), # ! nur de-1901
            ('st', 'n'),
            ('t', '>ba-re'),
            ('t', '>bar'),
            ('t', '-te'),
            ('t', '-ten'),
            ('t', '-ter'),
            ('t', '-tes'),
            ('t', '-tin'),
            ('t', '-tis-mus'),
            ('t', '-tist'),
            ('t', '-tis-tin'),
            # ('t', 'e'),
            ('t', 'n'),
            ('t', '-tum'),
            ('t', '-tums'),
            ('t', 'st'),
            ('te', 'le'),
            # ('te', 't'),
            ('um', 'a'),
            ('us', 'en'),
            ('v', '-ve'),
            ('v', '-ver'),
            ('v', '-vis-mus'),
            ('-ve', 'v'),
            ('z', '-ten'),
            ('z', '-ze'),
            ('z', '-zen'),
            ('z', '-zer'),
            ('ß', '-ße'),
            # ('ß', 's-se'), # ! -1-;Pro<zes-se
            ('ös', 'ö-se'),
            ('=öl', '=ö·le'),
           ]

# zerlege()
# ~~~~~~~~~
# Zerlege einen String mit von vorn bis hinten wandernder Bruchstelle:
#
# >>> from abgleich_neueintraege import zerlege
# >>> list(zerlege('wolle'))
# [('w', 'olle'), ('wo', 'lle'), ('wol', 'le'), ('woll', 'e')]
#
# ::

def zerlege(s):
    for i in range(1, len(s)):
        yield s[:i], s[i:]


# trenne_key()
# ~~~~~~~~~~~~
#
# Zerlege String, wenn die Teile in der Wortliste vorhanden sind, setze
# sie neu zusammen und übernimm die Trennmarkierer. Da alternative
# Zerlegungen möglich sind, wird eine Liste von Einträgen zurückgegeben:
#
# >>> for entry in trenne_key("Füllventilstörprozess", bspwords):
# ...     print(str(entry))
# Füllventilstörprozess;-2-;-3-;Füll=ven-til==stör=pro<zess
#
# Gleichberechtigte Alternativen werden ggf. erkannt:
#
# >>> trenne_key('Kauzahnweh', bspwords)
# [WordEntry('Kauzahnweh;Kau=zahn=weh')]
#
# Unterdrücke irreführende Trennungen in den Zusammensetzungen:
#
# >>> trenne_key('Atomenergie', bspwords)
# [WordEntry('Atomenergie;Atom=en<.er-gie')]
# >>> trenne_key('Bratensauce', bspwords)
# [WordEntry('Bratensauce;Bra-ten=sau-.ce')]
# >>> trenne_key('Gasexplosion', bspwords)
# [WordEntry('Gasexplosion;Gas=ex<.plo-sion')]
#
# ::

def trenne_key(key, words, endwords=set(),
               grossklein = False, verbose=False):
    entries = []
    sep = '='
    for k1, k2 in zerlege(key):
        # Typische Endungen überspringen (-bar, -ende, -innen, ...)
        if k2 in endwords:
            continue
        if grossklein:
            k1 = toggle_case(k1)
        if k1.istitle():
            k2 = k2.title()
        e1 = words.get(k1)
        e2 = words.get(k2) or words.get(toggle_case(k2))
        if not (e1 and e2):
            continue
        # zwei passende Teilwörter gefunden:
        if len(e1) != len(e2):
            # print str(e1), str(e2)
            # Länge (Sprachvarietäten) angleichen
            e1 = e1.completed()
            e2 = e2.completed()
        entry = WordEntry(key) # neuer Eintrag (nur Schlüssel)
        e12 = zip(e1,e2)
        next(e12) # drop first pair
        for w1, w2 in e12:
            if (not w1) or (not w2):
                # in einer Sprachvariante nicht definiert
                entry.append('')
                continue
            if grossklein:
                w1 = toggle_case(w1)
            w2 = w2.lower()
            # Unterdrückung irreführender Trennungen:
            # erste Silbe abtrennen
            _w2 = re.match('([^=]+?)([-<>])(.*)', w2)
            if _w2 and _w2 != 'zu' and (_w2.group(1) in ('sau', 'sex')
                    or len(_w2.group(1)) < 3 and k1+_w2.group(1) in words
                    or k1.endswith('s') and _w2.group(1) == 'ex'):
                w2 = _w2.expand('\\1\\2.\\3')
            # Zusammenfügen
            level = 1
            while (level*sep in w1) or (level*sep in w2):
                level += 1
            wort = (level*sep).join([w1, w2])
            # Eintrag ergänzen
            entry.append(wort)
        # Eintrag zusammenfassen und anhängen
        entry.prune()
        if verbose:
            entry.comment = '%s + %s' % (k1, k2)
        entries.append(entry)

        # Teste auf 3-teilige Composita und entferne die Wichtung:
        # ['Kau==zahn=weh', 'Kau=zahn==weh'] -> ['Kau=zahn=weh']
        if len(entries) == 2: # TODO auch mehr?
            teile = [[w for w in entry.get('de-1996').split('=') if w]
                     for entry in entries]
            if teile[0] == teile[1]:
                level = 1
                while level*sep in teile[0]:
                    level += 1
                entries = [entries[0]]
                entries[0][1] = entries[0][1].replace((level+1)*sep, level*sep)

    return entries

# Hilfsfunktionen und Klassen
# ---------------------------

# print_proposal()
# ~~~~~~~~~~~~~~~~
#
# Gib den Eintrag und sein Attribut `proposal` aus.
# ::

def print_proposal(entry):
    proposal = getattr(entry, "proposal", '')
    if proposal and len(proposal) > 1:
        print(' ' + str(entry))
        print('#' + str(proposal))
    else:
        print(str(entry))

# read_from_cache()
# ~~~~~~~~~~~~~~~~~

# Pfad zu "../../../wortliste" unabhängig vom Arbeitsverzeichnis::

default_wortliste = os.path.relpath(os.path.join(
            os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(
                os.path.abspath(__file__))))),
            'wortliste'))

cachedir = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                             'cache')


def check_cachefile(cachefile, masterfile):
    # Test, ob `masterfile` die "wortliste" ist:
    if default_wortliste != masterfile:
        return False

    # Test, ob `cachefile` neuer als `masterfile` ist::
    try:
        cache_mtime = os.path.getmtime(cachefile)
        master_mtime = os.path.getmtime(masterfile)
    except OSError:
        return False
    if os.path.getmtime(masterfile) <= cache_mtime:
        return True
    else:
        print('Cache "%s" zu alt oder nicht vorhanden.'%cachefile,
              file=sys.stderr)
        return False

# write_cachefile()
# ~~~~~~~~~~~~~~~~~
#
# Schreibe die Liste `lines` in `cachefile`.
# Füge dabei Zeilenenden hinzu.
# ::

def write_cachefile(cachefile, masterfile, lines):
    # Test, ob `masterfile` die "wortliste" ist:
    if default_wortliste != masterfile:
        return
    try:
        if not os.path.isdir(cachedir):
            os.mkdir(cachedir)
        header = '# Extract aus "%s"%s\n' %(masterfile,
                                            os.path.getmtime(masterfile))
    except FileNotFoundError:
        return

    cachefile = open(cachefile, 'w')
    cachefile.write(header)
    lines.append('') # ensure newline on last line
    cachefile.write('\n'.join(lines))
    cachefile.close()


if __name__ == '__main__':

# Optionen::

    # usage = '%prog [Optionen]\n' + __doc__

    parser = argparse.ArgumentParser(description=__doc__,
                                    formatter_class=argparse.RawDescriptionHelpFormatter)
    # parser.add_argument('INFILES', nargs='*', default=['-'],
    #                     help='Eingabedatei(en), Default: - (Standardeingabe).')
    parser.add_argument('-i', '--file', dest='wortliste',
                      help='Vergleichsdatei, Vorgabe "%s"'%default_wortliste,
                      default=default_wortliste)
    parser.add_argument('-k', '--kurzformat', action='store_true',
                      help='Ausgabe im Kurzformat')
    parser.add_argument('-e', '--nur-eindeutige', action='store_true',
                      help='Mehrdeutige Zerlegungen und Rest ignorieren')
    parser.add_argument('-v', '--verbose', action='store_true',
                      help='Herleitung in Kommentar schreiben')
    args = parser.parse_args()

    wordfile = WordFile(args.wortliste)


# Trenne neue Wörter durch Vergleich mit Kombinationen von Einträgen der
# Wortliste.
#
# `Wortliste` einlesen, Umschreiben in Dictionary, Aussortieren von
# Abkürzungen::

    words = dict()
    for entry in wordfile:
        if 'Abk.' in entry.comment:
            continue
        words[entry[0]] = entry

# Präfixe und Teilwörter und Kombinationen (siehe wortzerlegung.py)
# entweder vom "cache", oder "live" generiert::

    n_min = 5 # Mindestvorkommen in der Wortliste
    cache_prefixes = os.path.join(cachedir, 'präfixe-häufiger-%d'%n_min)

    if check_cachefile(cache_prefixes, args.wortliste):
        prefixes = (line.split() for line in open(cache_prefixes)
                    if not line.startswith('#'))
    else:
        print('Präfixe aus wortliste extrahieren... ',
              end='', file=sys.stderr, flush=True)
        prefixes = list_affixes(words.values(), split_fun=split_prefixes,
                                scope='alle')
        prefixes = [prefix for prefix in prefixes if prefix[2] > n_min]
        prefixes.sort(key=(lambda result : sortkey_duden(result[0])))
        # Cache schreiben
        lines =  ['%s # < %s +%s×' % prefix for prefix in prefixes]
        write_cachefile(cache_prefixes, args.wortliste, lines)
        print('fertig.', file=sys.stderr)

    prefixes = [prefix[0] for prefix in prefixes
                if len(prefix[0]) > 1]
    prefixes += compound_prefixes
    # Nach Länge sortieren, damit spezifischere zuerst probiert werden:
    prefixes.sort(key = len)
    prefixes.reverse()
    # print(prefixes)

    # Teilwörter und Kombinationen
    cache_teile = os.path.join(cachedir, 'teile')

    if check_cachefile(cache_teile, args.wortliste):
        constituents = WordFile(cache_teile).asdict()
    else:
        print('Teilwörter aus wortliste extrahieren... ',
              end='', file=sys.stderr, flush=True)
        constituents = expand_words(words.values(), scope='teile')
        lines = [str(entry) for entry
                 in sorted(constituents.values(), key=sortkey_duden)]
        write_cachefile(cache_teile, args.wortliste, lines)
        print('fertig.', file=sys.stderr)

    words.update(constituents)

# Aussortieren von (Teil-) Wörtern, die zu vielen "false positives" führen::

    for unwort in ('aa', 'ab', 'ache', 'alk', 'alpl', 'am', 'ans', 'as', 'auf',
                   'bio', 'che', 'chi', 'diens',
                   'ei', 'em', 'est', 'este', 'et',
                   'fas', 'fed', 'gest', 'ho', 'how',
                   'in', 'is', 'ion', 'isen', 'ja',
                   'kre', 'kir', 'ku',
                   'mc', 'nau', 'njet', 'och',
                   'shut', 'sion', 'so',
                   'tein', 'tele', 'tu', 'um', 'wa'):
        words.pop(unwort, None)
        words.pop(unwort.title(), None)

# Wörter, die auch als Endungen auftauchen::

    endwords = set(('esten', 'innen', 'enden', 'ion'))
    for alt, neu in endungen:
        for w in alt, neu:
            w = join_word(w)
            if w in words:
                endwords.add(w)

# Erstellen der neuen Einträge::

    neue = []
    neue_grossklein = []
    rest = []

    # Liste der Einträge (wird auch später für Vergleich gebraucht)
    proposals = [WordEntry(line.strip())
                 for line in sys.stdin
                 if line.strip() and not line.startswith('#')]

    for newentry in proposals:
        OK = False
        key = join_word(newentry.key())
        # print(key, newentry)
        # continue

# Test auf vorhandene (Teil-) Wörter:
#
# ::

        entry = words.get(key)
        if entry:
            if args.verbose:
                entry.comment = "vorhanden"
            neue.append(entry)
            continue
        # kleingeschrieben oder Großgeschrieben:
        entry = words.get(key.lower()) or words.get(key.title())
        if entry:
            if args.verbose:
                entry.comment = key + " vorhanden"
            neue_grossklein.append(entry)
            continue

# Endungsabgleich::

        for alt, neu in endungen:
            entry = endungsabgleich(key, alt, neu, words, grossklein=False,
                                    verbose=args.verbose,
                                    kurzformat=args.kurzformat)
            if entry:
                entry.comment = ", ".join((entry.comment, newentry.comment))
                neue.append(entry)
                OK = True
                break
        if OK:
            continue

        for alt, neu in endungen:
            entry = endungsabgleich(key, alt, neu, words, grossklein=True,
                                    verbose=args.verbose)
            if entry:
                entry.comment = ", ".join((entry.comment, newentry.comment))
                neue_grossklein.append(entry)
                OK = True
                # break
        if OK:
            continue

# Präfixabgleich::

        for prefix in prefixes:
            entry = praefixabgleich(key, prefix, words, grossklein=False,
                                    verbose=args.verbose)
            if entry:
                entry.comment = ", ".join((entry.comment, newentry.comment))
                neue.append(entry)
                OK = True
                break
            entry = praefixabgleich(key, prefix, words, grossklein=True,
                                    verbose=args.verbose)
            if entry:
                entry.comment = ", ".join((entry.comment, newentry.comment))
                neue_grossklein.append(entry)
                OK = True
                break
        if OK:
            continue

# Zerlegen und test auf Fugen::

        entries = trenne_key(key, words, endwords=endwords, grossklein=False,
                             verbose=args.verbose)
        for entry in entries:
            entry.comment = ", ".join((entry.comment, newentry.comment))
        if entries:
            neue.extend(entries)
            continue

        entries = trenne_key(key, words, endwords=endwords, grossklein=True,
                             verbose=args.verbose)
        for entry in entries:
            entry.comment = ", ".join((entry.comment, newentry.comment))
        if entries:
            neue_grossklein.extend(entries)
            continue

# Nicht gefundene Wörter::

        rest.append(newentry)

# ggf. in Kurzformat wandeln:

    if args.kurzformat:
        neue = [ShortEntry(entry) for entry in neue]
        neue_grossklein = [ShortEntry(entry) for entry in neue_grossklein]
        # rest = [ShortEntry(entry) for entry in rest]

# Mehrdeutige aussortieren::

    alle_neuen = {}
    doppelkeys = set()
    doppelkeys_gleich = defaultdict(int)

    # doppelte keys finden:
    for entry in neue + neue_grossklein:
        key = entry.key().lower()
        if key in alle_neuen:
            if entry == alle_neuen[key]:
                doppelkeys_gleich[key] += 1
            else:
                doppelkeys.add(key)
        alle_neuen[key] = entry

    # doppelte Einträge "verlegen":
    eindeutige = []
    eindeutige_grossklein = []
    doppelte = []

    for entry in neue:
        key = entry.key().lower()
        if key in doppelkeys:
            doppelte.append(entry)
        elif doppelkeys_gleich[key] > 0:
            doppelkeys_gleich[key] -= 1
        else:
            eindeutige.append(entry)

    for entry in neue_grossklein:
        key = entry.key().lower()
        if key in doppelkeys:
            doppelte.append(entry)
        elif doppelkeys_gleich[key] > 0:
            doppelkeys_gleich[key] -= 1
        else:
            eindeutige_grossklein.append(entry)


# Vergleich mit Original::

    identische = {}
    for proposal in proposals:
        key = proposal.key().lower()
        newentry = alle_neuen.get(key)
        if proposal == newentry:
            identische[key] = proposal
        else:
            if newentry:
                newentry.proposal = proposal

# Ausgabe::

    print('## Bestimmung von Trennstellen durch Abgleich mit %s.'
          %args.wortliste)
    print('#')
    print('#  identisch rekonstruiert:', len(identische))
    print('#  eindeutiger Zerlegungsvorschlag:', len(eindeutige))
    print('#  eindeutiger Zerlegungsvorschlag (andere Großschreibung):',
          len(eindeutige_grossklein))
    print('#  alternative Zerlegungsvorschläge:', len(doppelte))
    print('#  Rest:', len(rest))

    if identische:
        print('\n## identisch rekonstruiert:')
        for entry in sorted(list(identische.values()), key=sortkey_duden):
            print(entry)

    if eindeutige:
        print('\n## eindeutiger Zerlegungsvorschlag')
        for entry in eindeutige:
            if entry.key().lower() not in identische:
                print_proposal(entry)
    if eindeutige_grossklein:
        print('\n## eindeutiger Zerlegungsvorschlag (andere Großschreibung)')
        for entry in eindeutige_grossklein:
            if entry.key().lower() not in identische:
                print_proposal(entry)

    if doppelte and not args.nur_eindeutige:
        print('\n## alternative Zerlegungsvorschläge')
        for entry in doppelte:
            print_proposal(entry)

    if rest and not args.nur_eindeutige:
        print('\n## keine Zerlegung gefunden')
        for entry in rest:
            print_proposal(entry)
