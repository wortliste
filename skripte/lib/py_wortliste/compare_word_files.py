#!/usr/bin/env python3
# -*- coding: utf8 -*-
# :Copyright: © 2014 Günter Milde.
#             Released without warranty under the terms of the
#             GNU General Public License (v. 2 or later)
# :Id: $Id:  $

# compare_word_files.py: Vergleichen von Wort-Dateien
# ===================================================
# ::

"""Vergleichen zweier Dateien mit einem Wort/Zeile."""

# ::

import argparse, sys, os

from wortliste import filelines, join_word

# Default-Aktion::

if __name__ == '__main__':


# Optionen::

    parser = argparse.ArgumentParser(description = __doc__,
                                    formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('FILE1', help='erste Datei')
    parser.add_argument('FILE2', help='zweite Datei')
    parser.add_argument('-c', '--ignorecase', action="store_true",
                        help='Ignoriere Großschreibung (Vorgabe: Nein).',
                        default=False)
    args = parser.parse_args()

# Listen::

    words = {}
    new = []

# Einlesen Datei1::

    for line in filelines([args.FILE1]):
        words[join_word(line).lower()] = line

# Vergleich und Ausgabe:

    print('# Änderungen')
    for line in filelines([args.FILE2]):
        try:
            word = words.pop(join_word(line).lower())
        except KeyError:
            new.append(line)
            continue

        if args.ignorecase:
            if word.lower() != line.lower():
                print('-', word)
                print('+', line)
        elif word != line:
            print('-', word)
            print('+', line)

    print('\n# Neu')
    for i in new:
        print(i)

    print('\n# Gelöscht')
    words = list(words.values())
    words.sort()
    for i in words:
        print(i)
