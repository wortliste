#!/usr/bin/env python3
# -*- coding: utf8 -*-
# :Copyright: © 2018 Günter Milde.
#             Released without warranty under the terms of the
#             GNU General Public License (v. 2 or later)
# :Id: $Id:  $


# Stilfilter
# **********
#
# Funktionen, die einen Trennstil (oder einen Aspekt eines Trennstils)
# implementieren, z.B.
#
# >>> from stilfilter import syllabisch, morphemisch
# >>> print(syllabisch("Pä-d<a-go-gik"))
# Pä-da-go-gik
#
# Testfunktion:
#
# >>> def filtertest(filter, sample):
# ...     for wort in sample.split():
# ...         print(wort, '->', filter(wort))
#
# >>> filtertest(morphemisch, 'Pä-d<a-go-gik wo-r-an')
# Pä-d<a-go-gik -> Päd<a·go-gik
# wo-r-an -> wo-r-an
#
# .. contents::
#
# ::

"""Filter für Trennstile

Siehe dokumentation/Trennstile.txt.
"""

# Abhängigkeiten
# ==============
# ::

import re, sys

# Wahltrennungen
# --------------
#
# Alternativtrennungen in Fremdörtern nach §112 und §113.
#
# Regelwerk (1996) § 112:
#    In Fremdwörtern können die Verbindungen aus Buchstaben für einen
#    Konsonanten + l, n oder r entweder entsprechend § 110 getrennt werden,
#    oder sie kommen ungetrennt auf die neue Zeile.
#
# In der Wortliste sind nur die traditionell üblichen Trennungen nach
# Fremdwortsilben ausgezeichnet. Diese können automatisch in Regelsilben
# überführt werden.
#
# Daher sind die folgenden zwei Funktionen obsolet!
#
# _waehle_fremdwortsilben()
# ~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Bei Konsonantenclustern mit l, n oder r wähle Trennung nach Herkunft.
#
# >>> from stilfilter import _waehle_fremdwortsilben
# >>> fremdwoerter = ('no-b-le Zy-k-lus Ma-g-net Fe-b-ru-ar '
# ...                 'Hy-d-rant Ar-th-ri-tis ge-r<i-a-t-risch '
# ...                 'A·p-ri-ko-se Eth-ni-en')
# >>> filtertest(_waehle_fremdwortsilben, fremdwoerter)
# no-b-le -> no-ble
# Zy-k-lus -> Zy-klus
# Ma-g-net -> Ma-gnet
# Fe-b-ru-ar -> Fe-bru-ar
# Hy-d-rant -> Hy-drant
# Ar-th-ri-tis -> Ar-thri-tis
# ge-r<i-a-t-risch -> ge-r<i-a-trisch
# A·p-ri-ko-se -> A·pri-ko-se
# Eth-ni-en -> Eth-ni-en
#
# ::

def _waehle_fremdwortsilben(wort):
    """"fremdländische" Sprechsilben (no-ble, Hy-drant, Ma-gnet)."""
    return re.sub('([-·])([bcdfgkptv]|th)-(?=[lrn])', '\\1\\2', wort) # K86, K87


# _waehle_standardsilben()
# ~~~~~~~~~~~~~~~~~~~~~~~~
#
# Bei Konsonantenclustern mit l, n oder r wähle Trennung nach deutschen Regeln.
#
# >>> from stilfilter import _waehle_standardsilben
# >>> filtertest(_waehle_standardsilben, fremdwoerter)
# no-b-le -> nob-le
# Zy-k-lus -> Zyk-lus
# Ma-g-net -> Mag-net
# Fe-b-ru-ar -> Feb-ru-ar
# Hy-d-rant -> Hyd-rant
# Ar-th-ri-tis -> Arth-ri-tis
# ge-r<i-a-t-risch -> ge-r<i-at-risch
# A·p-ri-ko-se -> Ap-ri-ko-se
# Eth-ni-en -> Eth-ni-en
#
# ::

def _waehle_standardsilben(wort):
    """Standard-Sprechsilbentrennung (nob-le, Hyd-rant, Mag-net)."""
    return re.sub('[-·]([bcdfgkptv]|th)-(?=[lrn])', '\\1-', wort) # §112

# Tests:
#
# K86 Untrennbar sind in Fremdwörtern die Verbindungen von Verschluß- und
# Reibelauten mit l und r, ...
#
# >>> fremdwoerter = ('Pu-b-li-kum flexi-b-ler Zy-k-lone Qua-d-rat '
# ...                 'Spek-t-rum manö-v-rieren')
# >>> filtertest(_waehle_fremdwortsilben, fremdwoerter)
# Pu-b-li-kum -> Pu-bli-kum
# flexi-b-ler -> flexi-bler
# Zy-k-lone -> Zy-klone
# Qua-d-rat -> Qua-drat
# Spek-t-rum -> Spek-trum
# manö-v-rieren -> manö-vrieren
#
# "s-t-r" -> "s-tr"
#
# >>> fremdwoerter = 'Di<s-t-rikt Ma-gis-t-rat la-kus-t-risch'
# >>> filtertest(_waehle_fremdwortsilben, fremdwoerter)
# Di<s-t-rikt -> Di<s-trikt
# Ma-gis-t-rat -> Ma-gis-trat
# la-kus-t-risch -> la-kus-trisch
#
# K 87 Untrennbar ist die Konsonantenverbindung "gn".
#
# >>> fremdwoerter = 'Ma-g-net Pro-g-no-se Si-g-net'
# >>> filtertest(_waehle_fremdwortsilben, fremdwoerter)
# Ma-g-net -> Ma-gnet
# Pro-g-no-se -> Pro-gno-se
# Si-g-net -> Si-gnet
#
# Keine Übergeneralisierung:
#
# >>> woerter = 'Seg-ler bast-le Ad-ler'
# >>> filtertest(_waehle_fremdwortsilben, woerter)
# Seg-ler -> Seg-ler
# bast-le -> bast-le
# Ad-ler -> Ad-ler
#
# Mit Auszeichnung der "Randtrennstelle":
# >>> woerter = 'A·p-ri-kose i·g-no-rie-ren'
# >>> filtertest(_waehle_fremdwortsilben, woerter)
# A·p-ri-kose -> A·pri-kose
# i·g-no-rie-ren -> i·gno-rie-ren
#
#
# regelsilben()
# ~~~~~~~~~~~~~
#
# Mit regelsilben() werden alle Fremdwortsilbentrennungen nach §112 erkannt
# und zu Regelsilben gewandelt:
#
# >>> from stilfilter import regelsilben
# >>> fremdwoerter = ('no-ble Zy-klus Ma-gnet Fe-bru-ar '
# ...                 'Hy-drant Ar-thri-tis ge-r<i-a-trisch '
# ...                 'A·pri-ko-se')
# >>> filtertest(regelsilben, fremdwoerter)
# no-ble -> nob-le
# Zy-klus -> Zyk-lus
# Ma-gnet -> Mag-net
# Fe-bru-ar -> Feb-ru-ar
# Hy-drant -> Hyd-rant
# Ar-thri-tis -> Arth-ri-tis
# ge-r<i-a-trisch -> ge-r<i-at-risch
# A·pri-ko-se -> Ap-ri-ko-se
#
# ::

def regelsilben(wort):
    """Regel-Sprechsilbentrennung (nob-le, Hyd-rant, Mag-net)."""
    wort = re.sub('[-·]([bcdfgkptv]|th)(?=[lr][aeiouäöü])', '\\1-', wort)
    wort = re.sub('[-·]gn(?=[aeiouäöü])', 'g-n', wort)
    return wort

def trenne_gn(wort):
    """Regelsilben bei gn (Si-gnal -> Sig-nal)."""
    return re.sub('[-·]gn(?=[aeiouäöü])', 'g-n', wort)


# morphemisch()
# ~~~~~~~~~~~~~
#
# Entferne Alternativtrennungen nach §113 (verblasste Morphologie).
#
# Regelwerk (1996) §113:
#   Wörter, die sprachhistorisch oder von der Herkunftssprache her gesehen
#   Zusammensetzungen oder Präfigierungen sind, aber nicht mehr als solche
#   empfunden oder erkannt werden, kann man entweder nach § 108 oder nach
#   § 109 bis § 112 trennen.
#
# Ersetze, wenn zwischen Haupttrennstelle und Nebentrennstelle nur ein
# Buchstabe liegt , "ch" gilt als ein Buchstabe.
#
# (Die Haupttrennstelle kann vor oder nach der
# Nebentrennstelle liegen.)
#
# >>> from stilfilter import morphemisch
# >>> blasse = ('hi-n<auf wo-r=um Chry-s<an-the-me Hek-t<ar '
# ...           'He-li-ko<p-ter in-te-r<es-sant  au-to<ch-ton')
# >>> filtertest(morphemisch, blasse)
# hi-n<auf -> hin<auf
# wo-r=um -> wor=um
# Chry-s<an-the-me -> Chrys<an-the-me
# Hek-t<ar -> Hekt<ar
# He-li-ko<p-ter -> He-li-ko<pter
# in-te-r<es-sant -> in-ter<es-sant
# au-to<ch-ton -> au-to<chton
#
# Setze Randtrennstellen vor/nach Selbstlaut
# (bei Morphemgrenze oder Schwankungsfall zusätzlich zum Trennzeichen):
#
# >>> blasse2 = (' Di-a<gno-se Li-n<o-le-um Pä-d<a-go-gik '
# ...            'Psy-ch<i.a-trie A<·s-phalt E·x<a-men  A<·s-the-n=o-pie')
# >>> filtertest(morphemisch, blasse2)
# Di-a<gno-se -> Di·a<gno-se
# Li-n<o-le-um -> Lin<o·le-um
# Pä-d<a-go-gik -> Päd<a·go-gik
# Psy-ch<i.a-trie -> Psych<i.·a-trie
# A<·s-phalt -> A<·sphalt
# E·x<a-men -> Ex<a·men
# A<·s-the-n=o-pie -> A<·sthen=o·pie
#
# ::

def morphemisch(wort):
    """Bei Alternativen, Trennung nach Morphologie (hin<auf, Päd<ago-ge).

    Entferne Alternativtrennungen nach §113 (verblasste Morphologie).
    """
    # nach (mit Selbstlaut):
    wort = re.sub('([<=]+[.·]*[aeiouyäöü])[-]+[.]*', '\\1·', wort)
    wort = re.sub('([<=]+[.·]*[aeiouyäöü])[.]([^·])', '\\1.·\\2', wort)
    # nach
    wort = re.sub('([<=]+[.·]*([^>]|ch))[-]+[.]*', '\\1', wort)
    # vor (mit Selbstlaut):
    wort = re.sub('[-.]+([aeiouyäöü][<=]+)', '·\\1', wort)
    # vor:
    wort = re.sub('[-.]+((.|ch)[<=]+)', '\\1', wort)
    # vor (mit Randtrennung: E·x<a-men, Di·a<g-no-se)
    wort = re.sub('·(([^aeiouyäöü]|ch)[<=]+)', '\\1', wort)
    return wort


# syllabisch()
# ~~~~~~~~~~~~
#
# >>> from stilfilter import syllabisch
# >>> filtertest(syllabisch, blasse)
# hi-n<auf -> hi-nauf
# wo-r=um -> wo-rum
# Chry-s<an-the-me -> Chry-san-the-me
# Hek-t<ar -> Hek-tar
# He-li-ko<p-ter -> He-li-kop-ter
# in-te-r<es-sant -> in-te-res-sant
# au-to<ch-ton -> au-toch-ton
#
# >>> filtertest(syllabisch, blasse2)
# Di-a<gno-se -> Di-a-gno-se
# Li-n<o-le-um -> Li-no-le-um
# Pä-d<a-go-gik -> Pä-da-go-gik
# Psy-ch<i.a-trie -> Psy-chi.a-trie
# A<·s-phalt -> As-phalt
# E·x<a-men -> E·xa-men
# A<·s-the-n=o-pie -> As-the-no-pie
#
# ::

def syllabisch(wort):
    """Ignoriere "verblasste" Morphologie (hi-nauf, Hek-tar, Pä-da-go-ge)."""
    # nach
    wort = re.sub('[<=]+[.·]*(([^>]|ch)[-.]+[^·])', '\\1', wort)
    # vor (mit Selbstlaut)
    wort = re.sub('([-.]+[aeiouyäöü])[<=]+', '\\1-', wort)
    # vor
    wort = re.sub('([-.]+.)[<=]+[.]*', '\\1', wort)
    return wort


# Tests:
#
# K44 „ſ“ (langes s) steht in Fremdwörtern...
#
# >>> blasse = ('tran<s-pirieren tran<s-zendent ab<s-tinent '
# ...           'Ab<s-zess Pro-s<odie')
# >>> filtertest(morphemisch, blasse)
# tran<s-pirieren -> tran<spirieren
# tran<s-zendent -> tran<szendent
# ab<s-tinent -> ab<stinent
# Ab<s-zess -> Ab<szess
# Pro-s<odie -> Pros<odie
#
# Trennstellen können als ungünstig markiert sein:
#
# >>> blasse = ('Bür-ger=in<.i-ti-a-ti-ve Pä-..d<e-..rast')
# >>> filtertest(morphemisch, blasse)
# Bür-ger=in<.i-ti-a-ti-ve -> Bür-ger=in<.i·ti-a-ti-ve
# Pä-..d<e-..rast -> Päd<e·rast
# >>> filtertest(syllabisch, blasse)
# Bür-ger=in<.i-ti-a-ti-ve -> Bür-ger=ini-ti-a-ti-ve
# Pä-..d<e-..rast -> Pä-..de-..rast
#
# Vokalcluster ohne Haupttrennstelle nicht auflösen:
#
# >>> filtertest(morphemisch, 'Zy-klo>i-de Ka-li-um=i.·o-did')
# Zy-klo>i-de -> Zy-klo>i-de
# Ka-li-um=i.·o-did -> Ka-li-um=i.·o-did
#
# >>> filtertest(syllabisch, 'Zy-klo>i-de Ka-li-um=i.·o-did')
# Zy-klo>i-de -> Zy-klo>i-de
# Ka-li-um=i.·o-did -> Ka-li-um=i.·o-did
#
# Trennzeichen sind kein Buchstaben,
#
# >>> filtertest(morphemisch, 'aar=gau=>.i-sche')
# aar=gau=>.i-sche -> aar=gau=>.i-sche
#
# >>> filtertest(syllabisch, 'aar=gau=>.i-sche')
# aar=gau=>.i-sche -> aar=gau=>.i-sche
#
#
# Permissivität
# -------------
#
# Unterdrücken/Zulassen von regelkonformen Trennungen je nach Anwendungsfall.
# (Siehe Trennstile in sprachauszug.py.)
#
# _unguenstig()
# ~~~~~~~~~~~~~
#
# Entferne Randtrennstellen und explizit als ungünstig gekennzeichnete
# Trennstellen. Das Argument `level` bezeichnet die Zahl der
# Ungünstigmarker (Punkte) ab der eine Trennstelle entfernt wird.
#
# >>> from stilfilter import _unguenstig
# >>> _unguenstig('Text=il<..lu-stra-ti.on')
# 'Text=illu-stra-ti.on'
# >>> _unguenstig('Text=il<..lu-stra-ti.on', level=2)
# 'Text=illu-stra-ti.on'
# >>> _unguenstig('Text=il<..lu-stra-ti.on', level=3)
# 'Text=il<..lu-stra-ti.on'
# >>> print(_unguenstig('aar=gau=>.i-sche'))
# aar=gaui-sche
# >>> print(_unguenstig('A·dress=er<.hö-hung'))
# Adress=erhö-hung
# >>> print(_unguenstig('A·dress=er<.hö-hung', level=0))
# Adress=er<.hö-hung
#
# ::

def _unguenstig(wort, level=1):
    """Entferne ungünstige und Randtrennstellen ab `level`."""
    wort = re.sub('[=<>]*·', '', wort)
    if level:
        marker = r'\.' * level
        wort = re.sub('[-=<>]+%s+'%marker, '', wort)
        # TODO: einzelne Punkte (Schwankungsfälle)
        # wort = wort.replace('.', '-')
    return wort


# keine_schwankungsfaelle()
# ~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Schwankungsfälle, wo die Silbenzahl je nach Aussprache variiert werden im
# Gesangstext oft nicht getrennt und sind generell (leicht) ungünstige
# Trennungen.
#
# >>> from stilfilter import keine_schwankungsfaelle
# >>> filtertest(keine_schwankungsfaelle,
# ...            'ab<a·xi.al In<spek-ti.on Ar-me-.en Ak-ti.·e I.·od')
# ab<a·xi.al -> ab<a·xial
# In<spek-ti.on -> In<spek-tion
# Ar-me-.en -> Ar-me-.en
# Ak-ti.·e -> Ak-tie
# I.·od -> Iod
#
# ::

def keine_schwankungsfaelle(wort):
    """Entferne Schwankungsfall-Trennzeichen (Punkt zwischen Vokalen)."""
    return re.sub('([aeiouyäöüAEIOUYÖÄÜ])\\.[·]?([aeiouyäöü])', '\\1\\2', wort)


# keine_flattervokale()
# ~~~~~~~~~~~~~~~~~~~~~
#
# Traditionell enthalten die TeX-Trennmuster keine Trennstellen deren
# Abstand zur Nachbartrennstelle einen Buchstaben beträgt. Die Entscheidung
# „Flatterbuchstaben“ zu vermeiden folgt der Praxis im Wörterverzeichnis
# des „Einheitsdudens“ (Duden 1991) und Wahrig (1980).
#
# Bei einvokalischen Silben im Wortinneren, nimm die zweite:
#
# >>> from stilfilter import keine_flattervokale
# >>> einzelne = ('The-a-ter ge-ni.a-le')
# >>> filtertest(keine_flattervokale, einzelne)
# The-a-ter -> Thea-ter
# ge-ni.a-le -> ge-nia-le
#
# Allerdings nicht, wenn die zweite Trennstelle unterdrückt oder die erste
# eine Suffixtrennstelle ist:
#
# >>> einzelne = ('La-sal-le-a-.ner Athe>i-sten An<woh-ner=in<.i-ti-a-ti-ve')
# >>> filtertest(keine_flattervokale, einzelne)
# La-sal-le-a-.ner -> La-sal-le-a-.ner
# Athe>i-sten -> Athe>isten
# An<woh-ner=in<.i-ti-a-ti-ve -> An<woh-ner=in<.i-tia-ti-ve
#
# ::

def keine_flattervokale(wort):
    """Entferne Trennmarker vor Einzevokalsilben (Thea-ter)."""
    wort = re.sub('>([aeiouyäöü])[-]\.?', '>\\1', wort)
    wort = re.sub('(?<![<>=])[-.]+([aeiouyäöüëï])(?=[->][^.])', '\\1', wort)
    return wort


# standard()
# ~~~~~~~~~~
# Standardunterdrückung + keine Flattervokale
#
# >>> from stilfilter import standard
# >>> print(standard('aar=gau=>.i-sche'))
# aar=gaui-sche
#
# >>> bsp = ('La-sal-le-a-.ner Athe>i-sten An<woh-ner=in<.i-ti-a-ti-ve')
# >>> filtertest(standard, bsp)
# La-sal-le-a-.ner -> La-sal-le-aner
# Athe>i-sten -> Athe>isten
# An<woh-ner=in<.i-ti-a-ti-ve -> An<woh-ner=ini-tia-ti-ve
#
# ::

def standard(wort):
    """Entferne ungünstige Trennungen und Einvokalsilben."""
    return keine_flattervokale(_unguenstig(wort))

# permissiv()
# ~~~~~~~~~~~
# Aussortieren sehr ungünstiger (mit „..“ markierter) Trennstellen.
# ::

def permissiv(wort):
    """Aussortieren sehr ungünstiger (mit „..“ markierter) Trennstellen."""
    return _unguenstig(wort, level=2)

# inklusiv()
# ~~~~~~~~~~
# ::
def inklusiv(wort):
    """Zulassen aller Trennungen (auch ungünstige) außer Randtrennungen."""
    return _unguenstig(wort, level=0)


# righthyphenmin3()
# ~~~~~~~~~~~~~~~~~~
#
# „Es gilt als Buchdruckerregel, dass zwei Buchstaben am Wortende nur im
# Notfall abgetrennt werden“ (Duden 1934):
#
# >>> from stilfilter import righthyphenmin3
# >>> sample = ('fo-to<gra-fie-re Fe-ri-en=kurs'
# ...           ' dar<an dar<an=mach-te'
# ...           ' aben-teu-er>li-che hu-ma-no>id Amy-lo>id=er<kran-kung'
# ...           ' il--le-gal an-ti<zi-pier-te')
# >>> filtertest(righthyphenmin3, sample)
# fo-to<gra-fie-re -> foto<gra-fiere
# Fe-ri-en=kurs -> Fe-rien=kurs
# dar<an -> daran
# dar<an=mach-te -> daran=machte
# aben-teu-er>li-che -> aben-teuer>li-che
# hu-ma-no>id -> hu-ma-noid
# Amy-lo>id=er<kran-kung -> Amy-loid=er<kran-kung
# il--le-gal -> il--le-gal
# an-ti<zi-pier-te -> anti<zi-pierte
#
# ::

def righthyphenmin3(wort):
    # -, < und > am Schluss und vor =
    wort = re.sub('[-<>.]+(..$|..=)', '\\1', wort)
    # - vor < und >
    wort = re.sub('[-.]+(..[<>])', '\\1', wort)
    return wort

# hyphenmin3()
# ~~~~~~~~~~~~
#
# Keine Trennung nach nur 2 Buchstaben am Wortanfang oder -ende.
#
# Ausnahme:
#   morphemische oder günstige Trennung am Wortanfang
#   (z.B. Ab<bruch, Ei=sa-lat, il--le-gal):
#
# >>> from stilfilter import hyphenmin3
# >>> sample += ' Ab<druck=er<.laub>nis-se ab<er<.kannt un<=ge<recht=fer-tigt'
# >>> sample += ' be<ru-fen ab<be<ru-fen ab=zu=be<ru-fen'
# >>> sample += ' zu=zu=be<rei-ten un<zu<ver<läs-sig'
# >>> filtertest(hyphenmin3, sample)
# fo-to<gra-fie-re -> foto<gra-fiere
# Fe-ri-en=kurs -> Ferien=kurs
# dar<an -> daran
# dar<an=mach-te -> daran=machte
# aben-teu-er>li-che -> aben-teuer>liche
# hu-ma-no>id -> huma-noid
# Amy-lo>id=er<kran-kung -> Amy-loid=erkran-kung
# il--le-gal -> il--le-gal
# an-ti<zi-pier-te -> anti<zipierte
# Ab<druck=er<.laub>nis-se -> Ab<druck=erlaub>nisse
# ab<er<.kannt -> ab<erkannt
# un<=ge<recht=fer-tigt -> un<=gerecht=fer-tigt
# be<ru-fen -> be<rufen
# ab<be<ru-fen -> ab<berufen
# ab=zu=be<ru-fen -> ab=zu=beru-fen
# zu=zu=be<rei-ten -> zu=zu=berei-ten
# un<zu<ver<läs-sig -> un<zuver<läs-sig
#
# ::

def hyphenmin3(wort):
    """Entferne Trennungen im Abstand 2 vom Wortrand.

    Wie \\lefthyphenmin = \\righthyphenmin = 3, aber auch in Komposita
    und nicht bei "guten" Trennungen am Wortanfang.
    """
    # -, < und > am Schluss und vor =
    wort = re.sub('[-<>.]+(..$|..=)', '\\1', wort)
    # - und > am Anfang
    wort = re.sub('^(..)[->.][.]*([^-])', '\\1\\2', wort)
    # -, < und > nach =
    wort = re.sub('(=..)[-<>.]+', '\\1', wort)
    # - vor < und >
    wort = re.sub('[-.]+(..[<>])', '\\1', wort)
    # - nach < und >
    wort = re.sub('([<>]..)[-.]+', '\\1', wort)
    # < nach < (zuerst letztes von 3)
    wort = re.sub('<(..<..)[<.]+', '<\\1', wort)
    wort = re.sub('<(..)[<.]+', '<\\1', wort)
    return wort


# morphemgrenzen()
# ~~~~~~~~~~~~~~~~
#
# Entferne alle syllabischen Trennstellen, behalte Trennstellen an Wortfugen,
# nach Präfix und vor Suffix.
# (Entspricht in etwa dem Make-Ziel "major".)
#
# >>> from stilfilter import morphemgrenzen
# >>> filtertest(morphemgrenzen, sample)
# fo-to<gra-fie-re -> foto<grafiere
# Fe-ri-en=kurs -> Ferien=kurs
# dar<an -> dar<an
# dar<an=mach-te -> dar<an=machte
# aben-teu-er>li-che -> abenteuer>liche
# hu-ma-no>id -> humano>id
# Amy-lo>id=er<kran-kung -> Amylo>id=er<krankung
# il--le-gal -> illegal
# an-ti<zi-pier-te -> anti<zipierte
# Ab<druck=er<.laub>nis-se -> Ab<druck=er<.laub>nisse
# ab<er<.kannt -> ab<er<.kannt
# un<=ge<recht=fer-tigt -> un<=ge<recht=fertigt
# be<ru-fen -> be<rufen
# ab<be<ru-fen -> ab<be<rufen
# ab=zu=be<ru-fen -> ab=zu=be<rufen
# zu=zu=be<rei-ten -> zu=zu=be<reiten
# un<zu<ver<läs-sig -> un<zu<ver<lässig
#
# Anwendung: Ligaturaufbruch, Tests
# ::

def morphemgrenzen(wort):
    """Entferne syllabische Trennstellen."""
    # Randtrennungen:
    wort = re.sub('[=<>]*·', '', wort)
    # syllabische Trennungen
    wort = re.sub('-+[.]*', '', wort)
    # Schwankungsfälle (einzelner Punkt)
    wort = re.sub('(?<![<=>·])\\.+', '', wort)
    if '{' in wort:
        wort = wort.replace('{ck/kk}',  'ck')
    return wort


# haupttrennstellen
# """""""""""""""""
# Nur Trennstellen an Wortfugen und nach Präfix.
#
# >>> from stilfilter import haupttrennstellen
# >>> print(haupttrennstellen('Ü·ber<=licht=ge<schwin-dig>keit'))
# Über<=licht=ge<schwindigkeit
#
# Anwendung: Wortanalyse, Tests
# ::

def haupttrennstellen(wort):
    """Entferne Trennstellen außer an Wortfugen und nach Präfix."""
    # Randtrennungen:
    wort = re.sub('[=<>]*·', '', wort)
    # syllabische Trennungen und Suffixe
    wort = re.sub('[->]+', '', wort)
    # Schwankungsfälle und ungünstige entfernte Trennung (einzelner Punkt)
    wort = re.sub('(?<![<=·])\\.+', '', wort)
    return wort


# einfach()
# ~~~~~~~~~
# Ersetze die Trennstellenauszeichnung durch ein einfaches Trennzeichen.
#
# >>> from stilfilter import einfach
# >>> print(einfach('Rah-men=ver<ein>ba-rung'), einfach('Re<gis-ter=ari·e'))
# Rah-men-ver-ein-ba-rung Re-gis-ter-ari-e
# >>> print(einfach('Ab<fa{ll/ll=l}a-ger'), einfach('Dru{ck/k-k}er'))
# Ab-falla-ger Drucker
# >>> print(einfach('An-ti<=geld=wä-sche===vor<rich-tung'))
# An-ti-geld-wä-sche-vor-rich-tung
#
# Das Argument `alternative` beschreibt die Auflösung von Mehrdeutigkeiten:
# 1: erste Alternative, 2: zweite Alternative, 0 ungetrennt:
#
# >>> print(einfach('Au-ssen=ma[-s/s-]se'), einfach('An-dro[/>]id'))
# Au-ssen-masse An-droid
# >>> print(einfach('Au-ssen=ma[-s/s-]se', 1), einfach('An-dro[/>]id', 1))
# Au-ssen-ma-sse An-droid
# >>> print(einfach('Au-ssen=ma[-s/s-]se', 2), einfach('An-dro[/>]id', 2))
# Au-ssen-mas-se An-dro-id
#
# >>> print(einfach('auf<fangen', symbol='"|'))
# auf"|fangen
#
# ::

def einfach(wort, alternative=0 , symbol='-'):
    """Vereinheitliche Trennstellenmarker."""

# Spezielle Trennungen für die traditionelle Rechtschreibung
# (siehe ../../dokumente/README.wortliste)::

    if '{' in wort:
        wort = wort.replace('{ck/k-k}',  'ck')
        # Konsonanthäufungen an Wortfuge: '{xx/xxx}' -> 'xx':
        wort = re.sub(r'\{(.*)/.*\}', r'\1', wort)

# Trennstellen in doppeldeutigen Wörtern. `alternative` wählt
# 1: erste Alternative, 2: zweite Alternative, 0 ungetrennt ::

    if '[' in wort:
        if alternative:
            wort = re.sub(r'\[(.*)/(.*)]', r'\%s'%str(alternative), wort)
        else:
            match = re.search(r'\[(.*)/.*]', wort)
            kompromiss = re.sub('[-=<>.·]', '', match.group(1))
            wort = wort.replace(match.group(0), kompromiss)

# Trennstellen aller Kategorien::

    wort = re.sub('[-=<>.·]+', symbol, wort)

    return wort


# Hauptfunktion
# -------------
#
# ::

if __name__ == '__main__':

    import argparse

    from wortliste import WordEntry, ShortEntry, filelines, run_filters


# Optionen
# ~~~~~~~~
#
# ::

    parser = argparse.ArgumentParser(description = __doc__,
                                    formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('INFILES', nargs='*', default=['-'],
                        help='Eingabedatei(en), Default: - (Standardeingabe).')
    parser.add_argument('--stilliste', action="store_true",
                        help='Beschreibung der unterstützten Trennstile')
    parser.add_argument('-k', '--kurzformat', action="store_true",
                        help='Input ist Wortliste im Kurzformat. '
                        '(Default: Langformat)', default=False)
    parser.add_argument('-s', '--stil', metavar='STIL,[STIL...]',
                        help='Trennstil(e) (siehe --stilliste für Auswahl)',
                        default="")
    parser.add_argument('--test-filter', action='store_true',
                        help='Schreibe nur geänderte Einträge.')
    parser.add_argument('--output-filter', metavar='EXP',
                        help='Schreibe nur Einträge, die auf den regulären '
                        'Ausdruck EXP passen')
    parser.add_argument('-1', '--test-1er', action='store_true',
                        help='Schreibe nur Einträge mit „Flattertrennung“. '
                        'Überschreibt --output-filter.')

    args = parser.parse_args()

    if args.stilliste:
        help('stilfilter')
        sys.exit()

    if args.kurzformat:
        entry_class = ShortEntry
        start = 0
    else:
        entry_class = WordEntry
        start = 1

    if args.output_filter:
        output_regexp = args.output_filter
    else:
        output_regexp = ''

# Keine Ausgabe, wenn alle "Flatterbuchstaben" entfernt
# (überschreibt "--output-filter")::

    if args.test_1er:
        output_regexp = '[-<>=.][^-<>=./][-<>=.]'


# Iteration über Eingabe
# """"""""""""""""""""""
#
# ::

    for line in filelines(args.INFILES or '-'):

# Zeile lesen und in WordEntry oder ShortEntry Objekt wandeln::

        if not line or line.startswith('#'):
            continue
        entry = entry_class(line)

# Iteration über die Felder des Eintrags::

        changed = False
        for i in range(start,len(entry)):
            word = entry[i]
            if not word:
                continue

# Trennstil (Filter anwenden)::

            entry[i] = run_filters(args.stil.split(','), word)
            if entry[i] != word:
                changed = True

# für Testzwecke: nur Worte ausgeben, die durch Filter geändert wurden::

        if not changed and args.test_filter:
            continue

        out_line = str(entry)

# Nur Worte ausgeben, die auf den Ausgabefilter-Ausdruck passen::

        if output_regexp and not re.search(output_regexp, out_line):
            continue

# Auf Standardausgabe ausgeben::

        print(out_line)
