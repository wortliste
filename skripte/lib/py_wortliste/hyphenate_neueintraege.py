#!/usr/bin/env python3
# :Copyright: © 2014 Günter Milde.
#             Released without warranty under the terms of the
#             GNU General Public License (v. 2 or later)
# :Id: $Id:  $

# hyphenate_neueintraege.py: kategorisierte Trennung mit patgen-patterns.
# =======================================================================
#
# ::

"""Trenne Wörter mittels "hyphenation"-Algorithmus und patgen-patterns¹.

Eingabe: Ein ungetrenntes Wort oder Eintrag im Wortliste-Format pro Zeile.²

Ausgabe: Wortliste-Einträge (Neueintrag;Neu=ein-trag)
         ohne Unterscheidung von Sprachvarianten (!)³
         getrennt nach:

         identisch rekonstruiert
           wenn die vorhandene Trennmarkierung der ermittelten
           entspricht.
         mit Pattern getrennt
           wenn die Eingabe ungetrennt ist oder eine abweichende
           Trennmarkierung aufweist

Bsp: python hyphenate_neueintraege.py < missing-words.txt > neu.todo

     ``neu.todo`` kann (nach Durchsicht!!) mit `prepare_patch.py neu`
     in die Wortliste eingepflegt werden.³

¹ Verwendet als Voreinstellung "Reformschreibungs" Pattern-Dateien, welche
  über die "make" Ziele `make pattern-refo`, `make major pattern-refo`,
  `make fugen pattern-refo` und `make suffix pattern-refo` im
  Wurzelverzeichnis der Wortliste generiert werden können.

² Tip: mit `filter_wortliste.py -v < neue.txt > wirklich-neue.txt`
  können in der WORTLISTE vorhandene Wörter aussortiert werden.

³ `prepare_patch.py neu` nimmt auch eine Unterscheidung nach de-1901/de-1996
  anhand der wesentlichen Regeländerungen (-st/s-t, ck/c-k, ss/ß)
  vor. (Schweizer Spezialitäten und andere Grenzfälle müssen per Hand
  eingepflegt werden.)

  Für Trennungen nach traditioneller Orthographie (de-1091) müssen die
  Musterdateinen über die Optionen angegeben werden.
"""

# Doctest: Beispiel für Anwendung als Python-Module
#
# >>> from hyphenate_neueintraege import *
#
# ::

import sys, os, glob, argparse, re

# path for local Python modules (parent dir of this file's dir)
sys.path.insert(0,
        os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# import patuse, trennstellenkategorisierung
from wortliste import (WordFile, WordEntry, ShortEntry,
                       sortkey_duden, run_filters)
from py_patuse.hyphenation import Hyphenator


# Trenne mit Hyphenator
#
# :Eingabe: ungetrenntes Wort
# :Ausgabe: Tupel getrennter Wörter:
#
#           * kategorisierte Trennungen
#           * getrennt mit Standard-Mustern: '=<>-' -> '-'
#           * getrennt mit "Fugen"-Mustern:  '='    -> '-'
#           * getrennt mit "Major"-Mustern:  '=<>'  -> '-'
#           * getrennt mit "Suffix"-Mustern: '>'    -> '-'
#
# ::

def trenne(word, verbose=False):
    if not word:
        return ''
    parts_standard = h_standard.split_word(word)
    parts_fugen = h_fugen.split_word(word)
    parts_major = h_major.split_word(word)
    parts_suffix = h_suffix.split_word(word)
    if verbose:
        print('# standard',  parts_standard)
        print('# fugen', parts_fugen)
        print('# major', parts_major)
        print('# suffix', parts_suffix)

    getrennt = ['-'.join(parts_standard),
                '-'.join(parts_fugen),
                '-'.join(parts_major),
                '-'.join(parts_suffix)]

    parts = [] # Liste von Silben und Trennzeichen, wird am Ende zusammengefügt.
    p_major = '' # zum Vergleich mit parts_major
    p_fugen = ''
    p_suffix = ''
    # Kategorisierung der Trennstellen
    for part_standard in parts_standard[:-1]:
        parts.append(part_standard)
        p_major += part_standard
        p_fugen += part_standard
        p_suffix += part_standard
        if parts_fugen and p_fugen == parts_fugen[0]:
            parts_fugen.pop(0)
            p_fugen = ''
            try:
                parts_major.pop(0)
            except IndexError:
                pass
            p_major = ''
            parts.append('=')
        elif parts_suffix and p_suffix == parts_suffix[0]:
            parts_suffix.pop(0)
            p_suffix = ''
            parts.append('>')
        elif parts_major and p_major == parts_major[0]:
            parts_major.pop(0)
            p_major = ''
            parts.append('<')
        else:
            parts.append('-')
    parts.append(parts_standard[-1])

    # Alternative Kategorisierung über Zerlegung der Teilwörter/Wortteile:
    # word = '='.join(['<'.join([h_standard.hyphenate_word(part, '-')
    #                              for part in h_major.split_word(teilwort)])
    #                   for teilwort in h_fugen.split_word(word)])
    return [''.join(parts)] + getrennt

def print_proposal(entry, verbose=False):
    getrennt = getattr(entry, "hyphenated", '')
    if proposal and isinstance(proposal, ShortEntry) or len(proposal) > 1:
        print('  ' + str(entry))
    print('# ' + getrennt[0])
    if verbose:
        print('# ' + ', '.join(getrennt))
        # print('# ' + getrennt[-1])


# Hauptfunktion::

if __name__ == '__main__':

# Optionen::

# Die neuesten Pattern-Dateien, welche über die "make"-Ziele
#
#     make pattern-refo
#     make major pattern-refo
#     make fugen pattern-refo
#     make suffix pattern-refo
#
# im Wurzelverzeichnis der wortliste generiert werden
# (--help zeigt das Erstelldatum) ::

# Pfad zum Wurzelverzeichnis ("../../../") unabhängig vom Arbeitsverzeichnis::

    root_dir = os.path.relpath(
        os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(
            os.path.abspath(__file__))))) )

    patterns = {}
    for cat in ('', '-major', '-fugen', '-suffix'):
        try:
            ppath = os.path.join(root_dir, 'muster/dehyphn-x%s/dehyphn-x%s-*.pat'%(cat,cat))
            patterns[cat] = sorted(glob.glob(ppath))[-1]
        except IndexError:
            print('Keine Muster in "%s" gefunden\n'%ppath,
                  '(`make %s pattern-refo` mag helfen).'%cat[1:])
            patterns[cat] = ''

    parser = argparse.ArgumentParser(description = __doc__,
                                    formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('-p', '--patterns',
                        help='Pattern-Datei (alle Trennstellen), '
                        'Vorgabe "%s"'%patterns[''], default=patterns[''])
    parser.add_argument('--patterns_major',
                        help='Pattern-Datei (Trennstellen an Morphemgrenzen), '
                        'Vorgabe "%s"'%patterns['-major'],
                        default=patterns['-major'])
    parser.add_argument('--patterns_fugen',
                        help='Pattern-Datei (Trennstellen an Wortfugen), '
                        'Vorgabe "%s"'%patterns['-fugen'],
                        default=patterns['-fugen'])
    parser.add_argument('--patterns_suffix',
                        help='Pattern-Datei (Trennstellen vor Suffixen), '
                        'Vorgabe "%s"'%patterns['-suffix'],
                        default=patterns['-suffix'])
    parser.add_argument('-a', '--nur-abweichungen', action='store_true',
                        help='Nur bei abweichenden Standard-Trennungen ausgeben')
    parser.add_argument('-d', '--pattern-date', metavar='DATUM',
                        help='verwende Standard-Patterns mit Erstellungsdatum '
                        'DATUM.')
    parser.add_argument('-k', '--kurzformat', action='store_true',
                        help='Eingabe ist im Kurzformat')
    parser.add_argument('-l', '--language', metavar='SPRACHE',
                        help='Sprachvariante (für Auswahl bei Eingabe '
                             'im Wortliste-Format (Vorgabe: "de-1996").',
                        default="de-1996")
    parser.add_argument('-u', '--unsortiert', action='store_true',
                        help='Ausgabe ohne Sortierung')
    parser.add_argument('-v', '--verbose', action='store_true',
                      help='Zusatzinformation ausgeben')
    args = parser.parse_args()


    if args.kurzformat:
        eintragsklasse = ShortEntry
    else:
        eintragsklasse = WordEntry

    if args.pattern_date:
        for cat in patterns:
            argvarname = ('patterns'+cat).replace('-', '_')
            argvar = getattr(args, argvarname)
            if argvar == patterns[cat]: # default unchanged
                ppath = os.path.join(root_dir,
                                     'muster/dehyphn-x%s/dehyphn-x%s-%s.pat'%
                                     (cat, cat, args.pattern_date))
                setattr(args, argvarname, ppath)

# Trenner-Instanzen::

    h_standard = Hyphenator(args.patterns)
    h_major = Hyphenator(args.patterns_major)
    h_fugen = Hyphenator(args.patterns_fugen)
    h_suffix = Hyphenator(args.patterns_suffix)

    print('## Trennung mit patgen Mustern:')
    for cat in patterns:
        argvarname = ('patterns'+cat).replace('-', '_')
        print('#  %-10s %s'
              %(cat[1:] or 'standard', getattr(args, argvarname)))

# Erstellen der neuen Einträge::

    proposals = [eintragsklasse(line.strip())
                 for line in sys.stdin
                 if line.strip() and not line.startswith('#')]

# Trennen und ggf. unsortierte Ausgabe::

    results = []
    keys = set()

    for proposal in proposals:
        key = proposal.key(lang=args.language)
        getrennt = trenne(key)
        # getrennt = trenne(key, verbose=args.verbose)
        try:
            word = getrennt[0]
        except IndexError:
            print("no hyphenation of", proposal, getrennt, file=sys.stderr)
            continue
        if not word:
            continue

        # unsortierte Ausgabe
        if args.unsortiert:
            if key not in keys:
                keys.add(key)
                print('# ' + word)
            if isinstance(proposal, ShortEntry) or len(proposal) > 1:
                print('  ' + str(proposal))
        else:
            proposal.hyphenated = getrennt
            results.append(proposal)

    if args.unsortiert:
        sys.exit()

# Vergleich mit Original
#
# Abweichungen von "Proposal" (in gewählter Sprachvariante) wenn der
# entprechende Trennstilfilter auf Proposal angewendet wird::

    x_standard = []  # Abweichung der Standard-Trennstellen
    x_major = []     # Abweichung bei major ("=" "<" ">")
    x_fugen = []     # Abweichung bei fugen ("=")
    x_suffix = []    # Abweichung bei suffix (">")
    ok = []          # Keine Unterschiede bei der Trennung (bis auf Wichtung)

    ungetrennt = []  # Eingabe war ungetrennt

# ::

    for proposal in results:
        (wort, standard, fugen, major, suffix) = proposal.hyphenated

        if (not proposal
            or (args.kurzformat == False and len(proposal) == 1)):
            ungetrennt.append(wort)
            continue

# Anwenden der Trennstile auf Proposals und Prüfen auf Übereinstimmung

# >>> run_filters(['morphemisch', 'standard'], 'Brau-e-rei=zelt')

# ::

        p_generisch = run_filters(['morphemisch', 'standard'],
                                  proposal.get(args.language))

        if standard != run_filters(['einfach'], p_generisch):
            x_standard.append(proposal)
            continue

        p_major = run_filters(['morphemgrenzen', 'einfach'], p_generisch)
        if major != p_major:
            if args.verbose:
                proposal.hyphenated.append('major: %s != %s'%(major, p_major))
            x_major.append(proposal)
            continue

        p_fugen = re.sub('([-<.]+=*|=*>+)', '', p_generisch)
        p_fugen = run_filters(['einfach'], p_fugen)
        if fugen != p_fugen:
            if args.verbose:
                proposal.hyphenated.append('fugen: %s != %s'%(fugen, p_fugen))
            x_fugen.append(proposal)
            continue

        p_suffix = re.sub('[-<=.]+', '', p_generisch)
        if suffix != run_filters(['einfach'], p_suffix):
            if args.verbose:
                proposal.hyphenated.append('suffix: %s != %s'%(suffix, p_suffix))
            x_suffix.append(proposal)
            continue

        else:
            ok.append(proposal)

# Ausgabe::

    if ungetrennt:
        print('\n## Trennung mit Pattern (Eingabe ohne Trennungen)')
        for entry in sorted(ungetrennt, key=sortkey_duden):
            print(entry)
    if x_standard:
        print('\n## Abweichung bei Standard-Trennstellen')
        for entry in sorted(x_standard, key=sortkey_duden):
            print_proposal(entry, args.verbose)
    if not args.nur_abweichungen:
        if x_major:
            print('\n## Abweichung bei major ("=" "<" ">")')
            for entry in sorted(x_major, key=sortkey_duden):
                print_proposal(entry, args.verbose)
        if x_fugen:
            print('\n## Abweichung bei fugen ("=")')
            for entry in sorted(x_fugen, key=sortkey_duden):
                print_proposal(entry, args.verbose)
        if x_suffix:
            print('\n## Abweichung bei suffix (">")')
            for entry in sorted(x_suffix, key=sortkey_duden):
                print_proposal(entry, args.verbose)
        if ok:
            print('\n## Gleiche Trennung außer ggf. Wichtung/Unterdrückung:')
            for entry in sorted(ok, key=sortkey_duden):
                print_proposal(entry, args.verbose)

    if len(ok) or len(x_suffix) or len(x_fugen) or len(x_major) or len(x_standard):
        print('\n## Statistik')
        print('#', len(ok), 'gleich (evt. Unterschiede in Wichtung/Unterdrückung)')
        print('#', len(x_suffix), 'Abweichung bei suffix (">")')
        print('#', len(x_fugen), 'Abweichung bei fugen ("=")')
        print('#', len(x_major), 'Abweichung bei major ("=" und "<")')
        print('#', len(x_standard), 'Abweichung der Standard-Trennstellen')
        print('#', len(ungetrennt), 'Vorgabe ohne Trennung')
