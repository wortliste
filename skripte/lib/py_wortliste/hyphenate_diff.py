#!/usr/bin/env python3
# :Copyright: © 2014 Günter Milde.
#             Released without warranty under the terms of the
#             GNU General Public License (v. 2 or later)
# :Id: $Id:  $

# hyphenate_neueintraege.py: kategorisierte Trennung mit patgen-patterns.
# =======================================================================
#
# ::

"""Vergleiche die Trennung von Wörtern mittels "hyphenation"-Algorithmus.

Test/Vergleich zweier patgen-Mustersätze¹.

Eingabe: Ein ungetrenntes Wort oder Eintrag im Wortliste-Format pro Zeile.

Ausgabe: ungetrenntes Wort (Eintrag # Ein-trag;Eint-rag)
         wenn die Trennung nach den zwei Mustersätzen verschieden ist.

Bsp: python hyphenate_diff.py < kandidaten.txt > kandidaten2.txt

¹ Verwendet als Voreinstellung "Reformschreibungs" Pattern-Dateien, welche
  über die "make" Ziele `make pattern-refo` im
  Wurzelverzeichnis der Wortliste generiert werden können.

  Für Trennungen nach traditioneller Orthographie (de-1091) müssen die
  Musterdateinen über die Optionen angegeben werden.
"""

# Doctest: Beispiel für Anwendung als Python-Module
#
# >>> from hyphenate_neueintraege import *
#
# ::

import sys, os, glob, argparse, re

# path for local Python modules (parent dir of this file's dir)
sys.path.insert(0,
        os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# import patuse, trennstellenkategorisierung
from wortliste import (WordFile, WordEntry, ShortEntry,
                       sortkey_duden, run_filters)
from py_patuse.hyphenation import Hyphenator


def print_proposal(entry, verbose=False):
    getrennt = getattr(entry, "hyphenated", '')
    if proposal and isinstance(proposal, ShortEntry) or len(proposal) > 1:
        print('  ' + str(entry))
    print('# ' + getrennt[0])
    if verbose:
        print('# ' + ', '.join(getrennt))
        # print('# ' + getrennt[-1])


# Hauptfunktion::

if __name__ == '__main__':

# Optionen::

# Die neuesten Pattern-Dateien, welche über das "make"-Ziel
#
#     make pattern-refo
#
# im Wurzelverzeichnis der wortliste generiert wurde
# (--help zeigt das Erstelldatum) ::

# Pfad zum Wurzelverzeichnis ("../../../") unabhängig vom Arbeitsverzeichnis::

    root_dir = os.path.relpath(
        os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(
            os.path.abspath(__file__))))) )

    ppath = os.path.join(root_dir, 'muster/dehyphn-x/dehyphn-x-*.pat')
    try:
        current_patterns = sorted(glob.glob(ppath))[-1]
        other_patterns = sorted(glob.glob(ppath))[-2]
    except IndexError:
        print('Keine Muster in "%s" gefunden\n'%ppath,
              '(`make pattern-refo` mag helfen).')
        sys.exit()

    parser = argparse.ArgumentParser(description = __doc__,
                        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('-p', '--patterns',
                        help='Mustersatz-Datei (Standard-Trennstil), '
                        'Vorgabe "%s"'%current_patterns, default=current_patterns)
    parser.add_argument('--other_patterns',
                        help='Alternativer Mustersatz zum Vergleich, '
                        'Vorgabe "%s"'%other_patterns, default=other_patterns)
    parser.add_argument('-d', '--pattern-date', metavar='DATUM',
                        help='verwende Mustersatz mit Erstellungsdatum '
                        'DATUM.')
    parser.add_argument('-k', '--kurzformat', action='store_true',
                        help='Eingabe ist im Kurzformat')
    parser.add_argument('-l', '--language', metavar='SPRACHE',
                        help='Sprachvariante (für Auswahl bei Eingabe '
                             'im Wortliste-Format (Vorgabe: "de-1996").',
                        default="de-1996")
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='Zusatzinformation ausgeben')
    args = parser.parse_args()


    if args.kurzformat:
        eintragsklasse = ShortEntry
    else:
        eintragsklasse = WordEntry

    if args.pattern_date:
        if args.other_patterns == other_patterns: # default unchanged
            args.other_patterns = os.path.join(root_dir,
                        'muster/dehyphn-x/dehyphn-x-%s.pat'% args.pattern_date)

# Trenner-Instanzen::

    h_current = Hyphenator(args.patterns)
    h_other = Hyphenator(args.other_patterns)

    print('## Trennung mit patgen Mustern:')
    print('# aktuelle Muster ', args.patterns)
    print('# Vergleichsmuster', args.other_patterns)
    print()
    print('# Wort # alte Trennung  neue Trennung')

# Erstellen der neuen Einträge::

    entrys = (eintragsklasse(line.strip())
             for line in sys.stdin
             if line.strip() and not line.startswith('#'))

# Trennen und Ausgabe::

    for entry in entrys:
        key = entry.key(lang=args.language)
        current = h_current.hyphenate_word(key, hyphen='-')
        other   = h_other.hyphenate_word(key, hyphen='-')

        if current == other:
            if args.verbose:
                print('#', key, current)
        else:
            print(key, '#', other, '≠', current)

# print('\n## Statistik')
# print('#', len(ok), 'gleich')
# print('#', len(ungetrennt), 'ungleich')
