#!/usr/bin/env python3
# -*- coding: utf8 -*-
# :Copyright: © 2014 Günter Milde.
#             Released without warranty under the terms of the
#             GNU General Public License (v. 2 or later)
# :Id: $Id:  $

# edit_wortliste.py: Helfer für kleine Editieraufgaben
# ===================================================
# ::

"""
Korrekturen der Wortliste.

Bsp:  edit_wortliste.py < neu.todo > wortliste.neu

Dabei ist ``neu.todo`` eine Datei mit einem Eintrag pro Zeile.

* ein Wort mit Trennstellen (für "allgemeingültige" Trennungen),
  optional mit Kommentar

* vollständiger Eintrag (für Wörter mit Sprachvarianten), z.b.
  "Zysten;-2-;Zy-sten;Zys-ten"

Zeilen, die mit ``#`` starten, werden ignoriert.

Einträge im "Kurzformat" werden nach Sprachabgleichsregeln erweitert:

- Schlüssel wird generiert und vorangestellt (durch Semikolon getrennt).
- Test auf einfache/häufige Reformänderungen 1996: s-t/-st, {ck/k-k}/c-k

Die Zeile "Zys-ten" und die Zeile "Zy-sten" liefern also beide das
Ergebnis "Zysten;-2-;Zy-sten;Zys-ten".

Die Einträge werden in die Wortliste einsortiert. Schon vorhandene Einträge
können mit der Option "-f" überschrieben werden, ansonsten wird eine
Fehlermeldung ausgegeben.
"""

# ::

import optparse, sys, os, codecs
#from copy import copy, deepcopy

from wortliste import WordFile, WordEntry, join_word, sortkey_duden


# Sprachvarianten
# ---------------
# Sprach-Tag nach [BCP47]_::

# sprachvariante = 'de-1901'         # "traditionell"
# sprachvariante = 'de-1996'         # Reformschreibung
# sprachvariante = 'de-x-versal'      # ohne ß (Großbuchstaben und Kapitälchen)
# sprachvariante = 'de-1901-x-versal'   # ohne ß (Schweiz oder GROSS)
# sprachvariante = 'de-1996-x-versal' # ohne ß (Schweiz oder GROSS)
# sprachvariante = 'de-CH-1901'     # ohne ß (Schweiz) ("süssauer")


# Neueinträge prüfen und vorbereiten
# ----------------------------------
#
# Die Vorschläge auf auf Neuwert testen (vorhandene Wörter werden ignoriert,
# unabhängig von der Groß-/Kleinschreibung) und einsortieren.
#
# Akzeptierte Formate:
#
#  * vollständiger Eintrag (für Wörter mit Sprachvarianten oder Kommentaren).
#
#  * ein Wort mit Trennstellen:
#
#    - Schlüssel wird generiert und vorangestellt (durch Semikolon getrennt).
#    - Test auf einfache/häufige Reformänderungen 1996:
#      s-t/-st, {ck/k-k}/c-k
#
# ::

def neu(wortliste, datei=None):
    """Neueinträge prüfen und vorbereiten."""
    # vorhandene Einträge
    words = dict((entry[0], entry) for entry in wortliste)
    # neue Einträge
    if not datei:
        neue = sys.stdin
    else:
        neue = open(datei, 'r')
    newentries = []

    print_korrekturhinweis = False

    for line in neue:
        if line.startswith('#'):
            continue
        # Dekodieren, Zeilenende entfernen
        line = line.decode('utf8').strip()
        if not line:
            continue
        # Eintrag ggf. komplettieren:
        if ';' not in line:
            line = '%s;%s' % (join_word(line), line)
        entry = WordEntry(line)
        key = entry[0]

        # Test auf "Neuwert":
        if key.lower() in words or key.title() in words:
            old = words.get(key.lower()) or words.get(key.title())
            if not options.force:
                sys.stderr.write('%s ignored: schon vorhanden'%key)
                # Einträge vergleichen:
                if entry == old and entry.comment == old.comment:
                    sys.stderr.write('(identisch).\n')
                else:
                    sys.stderr.write('(unterschiedlich)\n')
                    sys.stderr.write(' - %s\n' %old)
                    sys.stderr.write(' + %s\n' %entry)
                    print_korrekturhinweis = True
                continue

        entry.regelaenderungen() # teste auf Dinge wie s-t/-st
        newentries.append(entry)

    if print_korrekturhinweis:
        sys.stderr.write(
            'Änderungen können mit "--force" erzwungen werden.\n')
    return newentries


# Default-Aktion::

if __name__ == '__main__':

# Optionen::

    usage = '%prog [Optionen]\n' + __doc__

    parser = optparse.OptionParser(usage=usage)
    parser.add_option('-i', '--wordfile', dest='wortliste',
                      help='Eingangsdatei, Vorgabe "../../../wortliste"',
                      default='../../../wortliste')
    parser.add_option('-k', '--todofile', dest='todo',
                      help='Korrekturdatei, Vorgabe stdin')
    parser.add_option('-o', '--outfile', dest='patchfile',
                      help='Ausgangsdatei, Vorgabe "stdout"',
                      default='')
    parser.add_option('-f', '--force',
                      help='überschreibe vorhandene Einträge.',
                      action="store_true", default=False)

    (options, args) = parser.parse_args()

# Einlesen::

    wortliste = list(WordFile(options.wortliste)) # vorhandene Einträge

# Behandeln::

    wortliste += neu(wortliste, options.todo)

# Sortieren und ausgeben::

    wortliste.sort(key=sortkey_duden)
    for entry in wortliste:
        print(str(entry))
