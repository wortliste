# -*- coding: utf-8 -*-
#
# Dieses AWK-Skript hilft beim Synchronieren der Listen der gültigen
# Buchstaben in den Dateien `daten/german.tr` und
# `skripte/lua/helper_words.lua`.
#
# Die Datei `german.tr` verzeichnet die in der Patgen-Eingabe
# verwendeten gültigen Buchstaben in Form einer Translate-Datei (siehe
# `man patgen`).  Zwar kann Patgen nur maximal 241 verschiedene
# Buchstaben handhaben – 2^8 Zeichen abzüglich der Zahlen 0-9, vier
# Syntax-Zeichen (standardmäßig » .-*«) und ein internes
# Escape-Zeichen –, jedoch können diese Buchstaben extern durch
# Zeichenketten dargestellt werden, wobei keine davon eine Teilmenge
# einer anderen Zeichenkette sein darf.  Erfreulicherweise erfüllt die
# UTF-8-Kodierung alle diese Bedingungen, was uns
# Kodierungskonvertierungen erspart.
#
# Für die Validierung der Wortliste wird ebenfalls eine Liste der
# gültigen Buchstaben benötigt.  Das Skript `helper_words.lua` enthält
# eine solche in Form eines LPEG-Musters in der Kodierung UTF-8 (siehe
# dort Muster `letter`).  Die Grundformen der lateinischen Buchstaben
# werden durch einen Intervallausdruck abgedeckt, die restlichen
# Buchstaben werden explizit aufgezählt.
#
# Dieses Skript erstellt aus einer Translate-Datei den
# LPEG-Teilausdruck, der die akzentuierten und Sonderbuchstaben abdeckt.
# Es wird der GAWK-Mechanismus FIELDWIDTHS zum Zugriff auf feste Spalten
# verwendet.  Ein beispielhafter Aufruf:
#
# $ LANG=de_DE.UTF_8 gawk -f tr2lpeg.awk < daten/german.tr > out.lua
#
# Die Ausgabe kann in die entsprechende Lua-Quelldatei eingefügt werden.
#


BEGIN {
    # Feste Spaltenbreiten.
    FIELDWIDTHS = "1 1 1 1"
    # Einzug in der Ausgabe.
    indent = "      "
}

# Überlese erste Zeile mit Hyphenmins.
NR == 1 { next }
# Überlese Kommentarzeilen.
/^%/ { next }
# Überlese Leerzeilen.
/^$/ { next }

# Überlese Zeilen mit Grundformen lateinischer Buchstaben.
$2 ~ /[a-z]/ && $4 ~ toupper($2) { next }

# Behandle Buchstaben mit Versalform.
$4 != "" {
    printf "%s+ P\"%s\" + P\"%s\"\n", indent, $2, $4
    next
}

# Behandle Buchstaben ohne Versalform.
$2 != "" && $3 == "" {
    # Sonderbehandlung ß.
    match($2, "ß"); if (RLENGTH != -1) { printf "%s+ P\"%s\" / _property_has_eszett\n", indent, $2 }
    # Fertig.
    next
}

# Alle verbleibenden Zeilen.
{
    printf "%s: unbekanntes Format in Zeile %d\n", FILENAME, FNR > "/dev/stderr"
    exit 1
}
