skripte/trennmuster/

Dieses Verzeichnis enthält Skripte, die für die Erzeugung, Anwendung,
Dokumentation oder Analyse von Trennmustern bestimmt sind.

apply-pattern.pl
  Dieses Perl-Skript wendet die TeX-Trennmusterdatei $1 auf den Datenstrom
  an, wobei $2 als Translationsdatei benutzt wird (das ist diejenige Datei,
  die `patgen' als viertes Argument benötigt).

  Folgende Zeichen werden vor der Weiterverarbeitung aus der Eingabe
  herausgefiltert:

    · - = |

  Ist Option `-1' nicht gegeben, werden Trennungen direkt nach dem ersten
  und vor dem letzten Buchstaben in der Ausgabe entfernt, wie z.B. bei
  deutschen Trennungen erforderlich.

  Dieses Skript benützt patgen, nicht TeX!  Die Trennmusterdatei darf daher
  keine TeX-Konstrukte (Makros u.ä.) enthalten.

  Aufruf:  perl apply-pattern.pl trennmuster german.tr < eingabe > ausgabe

diff-patgen-input.awk
  Dieses Skript liest eine DIFF-Datei der Patgen-Eingabelisten (siehe
  Skript diff-patgen-input.sh) und zerlegt sie in Wörter,

   * die neu hinzugefügt,
   * die entfernt,
   * deren Trennung korrigiert und
   * deren Klein- und Großschreibung korrigiert

  wurde.  Die Wörter werden in Dateien der Form <Eingabedatei>.<ext>
  gespeichert.  <ext> ist entsprechend 'added', 'removed', 'case' oder
  'hyph'.  Beim Aufruf des Skripts muss die Variable 'ftr' mit dem Namen
  der Translate-Datei für Patgen vorbelegt werden:
    gawk -v ftr=<translate datei> ...


diff-patgen-input.sh
  Dieses Skript erzeugt Differenzbilder (diff) für die
  Patgen-Eingabelisten zwischen zwei angegebenen Commits.  Wird nur ein
  Commit angegeben, wird als Zielcommit "master" verwendet.  Die
  Ausgabedateien werden als Dateien

    dehyph*-x/<Start-Commit-Hash>-<Ziel-Commit-Hash>.diff

  in Verzeichnissen gespeichert, die der jeweiligen Rechtschreibung
  entsprechen.  Start- und Ziel-Commit können in jeder gültigen
  Git-Syntax angegeben werden.  Für die Dateinamen werden die
  entsprechenden abgekürzten alphanumerischen Commit-Hashes
  verwendet.

make-full-pattern.sh

  Dieses Skript generiert deutsche Trennmuster.

  Aufruf:

    bash make-full-pattern.sh [-p] words.hyphenated german.tr

  Die Option -p wird für Trennmuster für Primär- und Sekundärtrennstellen
  benötigt.
