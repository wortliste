#!/bin/bash
# -*- coding: utf-8 -*-

#
# Dieses Skript generiert deutsche Trennmuster.
#
# Aufruf:
#
#   bash make-full-pattern.sh [-p] words.hyphenated german.tr
#
#
# Eingabe: -p                 Option für Trennmuster für Primär- und
#                             Sekundärtrennstellen, verwendet andere
#                             patgen-Parameter.
#          words.hyphenated   Liste von getrennten Wörtern.
#          german.tr          Translationsdatei für patgen.
#
# Ausgabe: pattmp.[1-8]       patgen-Resultate.
#          pattern.[0-8]      Trennmuster -- pattern.8 ist die finale
#                             Trennmusterdatei.
#          pattern.[1-8].log  Log-Dateien.
#          pattern.rules      Die patgen-Parameter in kompakter Form.
#


# Die Parameter für patgen für die Level eins bis acht.

hyph_start_finish[1]='1 1'
hyph_start_finish[2]='2 2'
hyph_start_finish[3]='3 3'
hyph_start_finish[4]='4 4'
hyph_start_finish[5]='5 5'
hyph_start_finish[6]='6 6'
hyph_start_finish[7]='7 7'
hyph_start_finish[8]='8 8'

if [ "$1" = "-p" ]
then
  shift # $1 <- $2, $2 <- $3
  # Parameterwerte für Trennmuster für Primärtrennstellen
  pat_start_finish[1]='5 9'
  pat_start_finish[2]='6 11'
  pat_start_finish[3]='7 13'
  pat_start_finish[4]='8 15'
  pat_start_finish[5]='9 17'
  pat_start_finish[6]='10 19'
  pat_start_finish[7]='11 21'
  pat_start_finish[8]='12 35'
else
  # Parameterwerte für gewöhnliche Trennmuster
  pat_start_finish[1]='1 3'
  pat_start_finish[2]='2 4'
  pat_start_finish[3]='3 5'
  pat_start_finish[4]='4 6'
  pat_start_finish[5]='5 12'
  pat_start_finish[6]='6 12'
  pat_start_finish[7]='7 12'
  pat_start_finish[8]='8 12'
fi

good_bad_thres[1]='2 3 1'
good_bad_thres[2]='1 5 1'
good_bad_thres[3]='1 6 1'
good_bad_thres[4]='1 7 1'
good_bad_thres[5]='1 8 1'
good_bad_thres[6]='1 9 1'
good_bad_thres[7]='1 9 1'
good_bad_thres[8]='1 9 1'


# Erzeuge leere Startmuster, lösche Datei mit patgen-Parametern.
rm -f pattern.0 pattern.rules
touch pattern.0

for i in 1 2 3 4 5 6 7 8; do

  # Erzeuge Muster des aktuellen Levels.  Steuereingaben werden patgen
  # mittels einer Pipe übergeben.
  printf "%s\n%s\n%s\n%s" "${hyph_start_finish[$i]}" \
                          "${pat_start_finish[$i]}" \
                          "${good_bad_thres[$i]}" \
                          "y" \
  | patgen $1 pattern.$(($i-1)) pattern.$i $2 \
  | tee pattern.$i.log

  # Sammle verwendete patgen-Parameter in Datei.
  printf "%%   %s | %s | %s\n" "${hyph_start_finish[$i]}" \
                               "${pat_start_finish[$i]}" \
                               "${good_bad_thres[$i]}" \
  >> pattern.rules

done

# eof
