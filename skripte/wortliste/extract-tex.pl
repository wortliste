#! /usr/bin/perl -w
#
# extract-tex.pl
#
# Dieses Perl-Skript extrahiert einfache Wortlisten aus der
# »wortliste«-Datenbank im Langformat (oder ähnlichen Dateien mit
# gleichem Dateiformat), die beispielsweise als Eingabedateien für
# »patgen« verwendet werden können.
#
# Aufruf:
#
#   perl extract-tex.pl [Optionen...] [Liste1 Liste2 ...] > input.patgen
#
# Die Eingabedateien müssen in UTF-8 kodiert sein; ist keine
# Eingabedatei angegeben, verwendet das Skript die Standardeingabe.
# Beispiele:
#
#   perl extract-tex.pl -l < ../wortliste > wortliste.ref.latin9
#   perl extract-tex.pl -t ../wortliste > wortliste.trad.utf8
#
# Die Aufrufe
#
#   perl extract-tex.pl -1 ...
#   perl extract-tex.pl -t -1 ...
#   perl extract-tex.pl -s -1 ...
#   perl extract-tex.pl -G -S ...
#
# liefern jeweils die gleiche Ausgabe wie
#
#   sprachauszug.py -l de-1996,de-1996-x-versal ...
#   sprachauszug.py -l de-1901,de-1901-x-versal ...
#   sprachauszug.py -l de-CH-1901,de-1901 ...
#   sprachauszug.py -l de-1996,de-1996-x-versal \
#                   -s morphemisch,keine_schwankungsfaelle,einfach ...  [*]
#
# [*] »sprachauszug.py« gibt hier zusätzlich auch Wörter mit
#     Mehrdeutigkeiten aus.
#
#
# Optionen
# --------
#
# -t
# -s  Option »-t« wählt die traditionelle deutsche Rechtschreibung
#     aus, Option »-s« die traditionelle (deutsch)schweizerische
#     Rechtschreibung.  Wenn weder »-s« noch »-t« gesetzt ist, wird
#     die reformierte deutsche Rechtschreibung ausgewählt.
#
# -G  Gib zusätzlich Gesangstrennstellen aus, z.B. »A-bend«, wobei
#     Ungünstigkeitsmarker (mit Ausnahme von ».·«) ignoriert werden.
#     Einträge mit speziellen Trennungen oder Doppeldeutigkeiten
#     werden nicht verwendet.
#
# -x  Ignoriere Optionen »-g«, »-G«, »-S«, »-u« sowie »-1« und gib die
#     sprachspezifischen Felder unbearbeitet aus (inklusive
#     Kommentare).
#
# -g  Gib Wörter mit gewichteten Trennstellen aus.  Optional kann ein
#     ganzzahliges Argument angegeben werden: Wert 0 gibt alle
#     gewichtete Trennstellen aus inklusive »-« (das ist der
#     Standardwert), Wert 1 nur die Trennstellen mit der höchsten
#     Wichtung (ohne »-«), Wert 2 die Trennstellen mit der höchsten
#     und zweithöchsten Wichtung (ohne »-«), usw.
#
#     Beachte, dass bei nahe beieinanderstehenden Trennstellen derzeit
#     keine zusätzliche Wichtung vorgenommen wird.  Beispielsweise ist
#     in dem Wort
#
#       ab<be<ru-fen
#
#     die Trennung »abbe-rufen« schlecht, weil ganz nahe der optimalen
#     Trennstelle (nach »ab«).  Das Skript gibt trotzdem diese
#     Trennstelle als zweitbeste aus.
#
# -u  Verhindere die Ausgabe von Wörtern mit Markern für unerwünschte
#     Trennungen (z.B. »An<=al-.pha=bet«).  Wenn nicht gesetzt, werden
#     als ungünstig markierte Trennstellen entfernt
#     (z.B. »An<=alpha=bet«).
#
# -U  Gib Wörter mit Nicht-ASCII-Zeichen auch in Umschrift aus (z.B.
#     »loe-sen«, »Haen-de«).  Ausgenommen davon sind Wörter mit »ß«,
#     weil die entsprechenden Formen mit »ss« bereits in der Wortliste
#     enthalten sind.
#
# -1  (Ziffer 1) Verhindere einbuchstabige Trennungen.  Ist die Option
#     gesetzt, wird die erste dieser Trennungen unterdrückt, falls
#     beide Trennstellen gleichwertig sind (z.B. »eu-ro-päi-sche«
#     statt »eu-ro-pä-i-sche«), anderenfalls bleibt die stärkere
#     erhalten (z.B. »päd<ago-gisch« statt »pä-d<a-go-gisch«).
#
#     Gesangstrennstellen sind von dieser Option nicht betroffen; es
#     ist daher nicht sinnvoll, gleichzeitig »-G« zu verwenden.
#
# -S  Entferne Schwankungsfälle (z.B. »Sta-tion« statt »Sta-ti-on«).
#
# -v  Verhindere die Ausgabe von Versalformen, wo »ß« durch »ss«
#     ersetzt ist.
#
# -l  (Kleinbuchstabe L) Konvertiere die Ausgabe von UTF-8 nach
#     latin-9.
#
#
# Options-Beispiele für den Eintrag »I·n<i-ti.a-ti.on«:
#
#   -1 -S    In-itia-tion
#   -1       In-itia-ti-on
#   (keine)  In-iti-a-ti-on
#   -G -S    In-i-tia-tion
#   -G       In-i-ti-a-ti-on


# Wir verwenden »<<>>« statt »<>« aus Sicherheitsgründen.
require 5.22.0;

use strict;
use warnings;
use English '-no_match_vars';
use utf8;                         # String-Literals direkt als UTF-8.
use open qw(:std :utf8);
use Getopt::Long qw(:config bundling);


my ($opt_g, $opt_G,
    $opt_l,
    $opt_s, $opt_S,
    $opt_t,
    $opt_u, $opt_U,
    $opt_v,
    $opt_x,
    $opt_1);
$opt_g = -1;

GetOptions("g:i" => \$opt_g, "G" => \$opt_G,
           "l"   => \$opt_l,
           "s"   => \$opt_s, "S" => \$opt_S,
           "t"   => \$opt_t,
           "u"   => \$opt_u, "U" => \$opt_U,
           "v"   => \$opt_v,
           "x"   => \$opt_x,
           "1"   => \$opt_1);


my $prog = $0;
$prog =~ s@.*/@@;


# Kodierung:
binmode(STDOUT, ":encoding(iso-8859-15)") if $opt_l;


# Einige Konstanten für reguläre Ausdrücke, um die Lesbarkeit zu
# erhöhen.
my $Marker = qr/[.·<>=-]/x;
my $Buchstabe = qr/(?: [^.·<>=-] | ch)/x;
my $Vokal = qr/[aeiouäëïöüy]/x;
# Konsonant: nicht Vokal, aber Buchstabe.
my $Konsonant = qr/(?! $Vokal ) $Buchstabe/x;


sub entferne_marker {
  my $arg = shift;
  $arg =~ s/$Marker//g;
  return $arg;
}

# Wenn Option »-U« gesetzt ist, müssen wir erkennen können, ob Wörter
# in Umschrift in der Wortliste existieren.  Wir benutzen dafür zwei
# Hashes.
my %wortliste;
my %wortliste_umschrift;

while (<<>>) {
  # Gebe Kommentarzeilen direkt aus, falls verlangt.
  if (/^ \s* \#/x) {
    print if $opt_x;
    next;
  }

  chop;

  # Isoliere Kommentare.
  s/(\# .*) $//x;

  my $kommentar = $1 // "";

  # Entferne Leerzeichen aller Art.
  s/\s+//g;

  my @feld = split(';');
  next if $#feld < 1;

  # reformiert:           Felder 2, 4, 5, 7
  # traditionell:         Felder 2, 3, 5, 6
  # traditionell Schweiz: Felder 2, 3, 5, 6, 8
  #
  # Beachte: Feld n hat Index n-1.
  my $zeile = "";
  $zeile = $feld[2] if defined $feld[2]
                       && $feld[2] ne "-3-" && ($opt_t || $opt_s);
  $zeile = $feld[3] if defined $feld[3]
                       && $feld[3] ne "-4-" && !($opt_t || $opt_s);
  if (!$zeile) {
    # Wir nehmen Versalformen nur dann, wenn es keine normalen Formen
    # (in Feld 2 oder 3) gibt.
    $zeile = $feld[4] if defined $feld[4]
                         && $feld[4] ne "-5-" && !$opt_v;
    $zeile = $feld[5] if defined $feld[5]
                         && $feld[5] ne "-6-" && ($opt_t || $opt_s) && !$opt_v;
    $zeile = $feld[6] if defined $feld[6]
                         && $feld[6] ne "-7-" && !($opt_t || $opt_s) && !$opt_v;
  }

  $zeile = $feld[7] if defined $feld[7] && $opt_s;

  if (!$zeile) {
    $zeile = $feld[1];
  }

  next if $zeile eq "-2-";

  if (!$opt_x) {
    # Entferne spezielle Trennungen.
    next if $opt_G and $zeile =~ /\{/; # \}
    $zeile =~ s|\{ (.*?) / .*? \}|$1|gx;

    # Entferne Doppeldeutigkeiten.
    next if $opt_G and $zeile =~ /\[/;
    $zeile =~ s|\[ (.*?) / .*? \]|entferne_marker($1)|egx;

    # Hier der Algorithmus, um die verbliebenen Markierungen in
    # Trennstellen aufzulösen.  Die Schritte sind in der gegebenen
    # Reihenfolge abzuarbeiten.
    #
    # Dieses Skript implementiert ausschließlich den morphemischen
    # Trennstil (siehe Punkt 1), unter weiterer Anwendung der Regeln 2
    # bis 6.
    #
    # (1) Auflösung von Wahlmöglichkeiten zwischen morphemischem und
    #     syllabischem Trennstil (einer der beiden Stile muß gewählt
    #     werden).  Ungünstigkeitsmarker und Gesangstrennstellen
    #     werden in diesem Schritt nicht berücksichtigt (wohl aber
    #     entfernt, wenn die entsprechende Trennstelle entfällt).
    #
    #     (a) Die Bezeichnungen
    #
    #           <x-  und  -x<
    #
    #         sind Kurzschreibungen für
    #
    #           {<x/x-}  und  {x</-x}  (morphemisch/syllabisch)   ,
    #
    #         wobei »x« ein Konsonant oder »ch« ist.  Diese Regel gilt
    #         nicht für die Suffixe »>x-« und »-x>«.
    #
    #         Gleicherweise sind
    #
    #           =x-  und  -x=
    #
    #         Kurzschreibungen für
    #
    #           {=x/x-}  und  {x=/-x}  (morphemisch/syllabisch)   .
    #
    #     (b) Die Bezeichnungen
    #
    #           <i-  und  -i<
    #
    #         sind Kurzschreibungen für
    #
    #           {<i·/i-}  und  {·i</-i}  (morphemisch/syllabisch)   ,
    #
    #         wobei »i« ein Vokal ist (einschließlich »y«).  Diese
    #         Regel gilt nicht für die Suffixe »>i-« und »-i>«.  Die
    #         Zusammensetzungen »=i-« und »-i=« werden gegenwärtig
    #         nicht beachtet, da sie in der Wortliste nicht vorkommen
    #         (Beispiel: Ei-se-n=a-ch-er Motorenwerke).
    #
    #         Beispielsweise bleibt die Markierung
    #
    #             al-ge-bra>i-sche
    #
    #         in diesem Schritt unverändert; wegen »>« gibt es keine
    #         Wahlmöglichkeit.
    #
    # (2) Behandle (angehängte) ».«-Marker, falls ungünstige
    #     Trennstellen unterdrückt werden sollen.
    #
    # (3) Entferne Flattervokale (also Einbuchstaben-Silben), falls
    #     verlangt.  Beachte, daß »ch« wie ein Buchstabe behandelt
    #     wird und auch Schwankungsfälle (».«) berücksichtigt werden.
    #     Gesangstrennstellen dagegen werden ignoriert (aber
    #     gegebenenfalls entfernt).
    #
    #     Die folgenden beiden Regeln beziehen sich auf die
    #     Trennstellen unmittelbar vor und nach dem Flattervokal.
    #
    #     (a) Ist eine Trennstelle »stärker« als die andere, wird die
    #         stärkere Trennstelle genommen (z.B. ist »>« stärker als
    #         »-«, »-« stärker als ».«).
    #
    #     (b) Sind die Trennstellen gleich stark, wird die rechte
    #         Trennstelle genommen.
    #
    # (4) Entferne Gesangstrennstellen (»·«), falls verlangt.
    #     Beachte, daß die Markierung für Gesangstrennstellen, ähnlich
    #     Ungünstigkeitsmarkern, auch zu anderen Markern treten kann
    #     (die dann ebenfalls entfernt werden).
    #
    #     Die Markierung ».·« wird in jedem Fall entfernt.
    #
    #     Falls Gesangstrennstellen berücksichtigt werden sollen und
    #     Situationen wie »·x<« auftreten (wobei »x« ein Konsonant
    #     oder »ch« ist), wird die »stärkere« Trennstelle genommen.
    #
    # (5) Entferne restliche Schwankungsfälle, falls verlangt.
    #
    # (6) Alle verbliebenen Markierungen werden zu »-« aufgelöst.
    #
    #
    # Beispiele:
    #   Re<s-tau-rant
    #   Re<stau-rant  (1a, morphemisch)
    #   Re-stau-rant  (6)
    #
    #   Re<s-tau-rant
    #   Res-tau-rant  (1a, syllabisch)
    #
    #   Mon-t=re-al
    #   Mont=re-al  (1a, morphemisch)
    #   Mont-re-al  (6)
    #
    #   Mon-t=re-al
    #   Mon-tre-al  (1a, syllabisch)
    #
    #   Ge-r<i.a-trie
    #   Ger<i.a-trie  (1a, morphemisch)
    #   Ger<ia-trie   (3)
    #   Ger-ia-trie   (6)
    #
    #   Ge-r<i.a-trie
    #   Ge-ri.a-trie  (1a, syllabisch)
    #   Ge-ria-trie   (3)
    #
    #   Ärz-te=i·n<.i-ti.a-ti-ve
    #   Ärz-te=i·n<.i·ti.a-ti-ve  (1b, morphemisch)
    #   Ärz-te=i·ni·ti.a-ti-ve    (2)
    #   Ärz-te=i·ni·tia-ti-ve     (3a)
    #   Ärz-te=initia-ti-ve       (4)
    #   Ärz-te-initia-ti-ve       (6)
    #
    #   Ärz-te=i·n<.i-ti.a-ti-ve
    #   Ärz-te=i·ni-ti.a-ti-ve  (1b, syllabisch)
    #   Ärz-te=i·ni-tia-ti-ve   (3a)
    #   Ärz-te=ini-tia-ti-ve    (4)
    #   Ärz-te-ini-tia-ti-ve    (6)
    #
    #   Di-a<s-po-ra
    #   Di·a<s-po-ra  (1b, morphemisch)
    #   Di·a<spo-ra   (1a)
    #   Dia<spo-ra    (4)
    #   Dia-spo-ra    (6)
    #
    #   Di-a<s-po-ra
    #   Di-as-po-ra  (1b, syllabisch)
    #
    #   Kaf-ka=ken-.ner
    #   Kaf-ka=kenner  (2)
    #   Kaf-ka-kenner  (6)
    #
    #   al-ge-bra>i-sche
    #   al-ge-bra>ische  (3a)
    #   al-ge-bra-ische  (6)
    #
    #   Ru-i-ne
    #   Rui-ne  (3b)
    #
    #   A<·s-phalt
    #   A<·sphalt  (1a, morphemisch)
    #   Asphalt    (4)
    #
    #   A<·s-phalt
    #   As-phalt  (1a, syllabisch)
    #
    #   ge-ni.al
    #   ge-nial  (5)
    #
    #   Ak-ti.·e
    #   Ak-tie  (4)
    #
    #   A·b<i-tur
    #   A·b<i·tur  (1b, morphemisch)
    #   Ab<i·tur   (4, mit Gesangstrennstellen)
    #   Ab-i-tur   (6)
    #
    #   A·b<i-tur
    #   A·bi-tur  (1b, syllabisch)
    #   A-bi-tur  (6, mit Gesangstrennstellen)

    # Schritt 1a: »<x-« wird zu »<x«.
    $zeile =~ s/[<=] [.·]* $Konsonant \K - \.*//gx;
    #             »-x<« wird zu »x<«.
    $zeile =~ s/- \.* (?= $Konsonant \.* [<=] )//gx;

    # Schritt 1b: »<i-« wird zu »<i·«.
    $zeile =~ s/< \.* $Vokal \K -/·/gx;
    #             »-i<« wird zu »·i<«.
    $zeile =~ s/- (?= \.* $Vokal < )/·/gx;

    # Ausgabe von Wörtern mit unerwünschten Trennungen?
    next if $opt_u and $zeile =~ /\./;

    # Schritt 2: »a<.b« wird zu »ab«.
    #
    # Beachte, daß die Kombination »·.« zwar nicht in der Wortliste
    # vorkommt, sehr wohl jedoch als Ergebnis von Schritt 1b.
    $zeile =~ s/[·<>=-]+ \.+//gx if !$opt_G;

    if ($opt_1) {
      # Schritt 3a: »a-i>c« wird zu »ai>c«.
      $zeile =~ s/[-.]+ (?= $Vokal [<>=] )//gx;
      #             »a.i-c« wird zu »ai-c«.
      $zeile =~ s/\.+ (?= $Vokal - )//gx;
      #             »a<i-c« wird zu »a<ic«.
      $zeile =~ s/[<>=] [·<>=]* $Vokal \K [-.]+//gx;
      #             »a-i.c« wird zu »a-ic«.
      $zeile =~ s/- $Vokal \K \.+//gx;

      # Schritt 3b: »a-i-c« wird zu »ai-c«.
      $zeile =~ s/- (?= $Vokal - )//gx;
    }

    # Schritt 4:
    $zeile =~ s/\.·//g;
    if ($opt_G) {
      # »a·x<c« wird zu »ax<c«.
      $zeile =~ s/(?<! <) ·+ (?= $Konsonant < )//gx;
    }
    else {
      # »a<·b« wird zu »ab«.
      $zeile =~ s/$Buchstabe \K $Marker* ·+//gx;
    }

    # Schritt 5:
    if ($opt_S) {
      # »a.b« wird zu »ab«.
      $zeile =~ s/\.//gx;
    }
    else {
      # »a.b« wird zu »a-b«.
      $zeile =~ s/\./-/gx;
    }

    if ($opt_g > 0) {
      # Berechne Wichtungen.  Wir verwenden folgende Werte:
      #
      #   0   Wortteil
      #   1   -
      #   2   --
      #   3   <, >
      #   4   <<
      #   5   =
      #   6   <=, =>
      #   7   ==
      #   8   <==, ==>
      #   9   ===
      #   ..
      #
      # Bei mehrfachem Auftreten von »<« hat das am meisten links
      # stehende den höchsten Rang.  Bei mehrfachem Auftreten von »>«
      # hat das am meisten rechts stehende den höchsten Rang.
      # Beispiele:
      #
      #   Nach<denk>lich>keit
      #       ^         ^
      #   Mit<ver<ant-wort>lich>keit
      #      ^                 ^
      #
      # Gleiches gilt für Ketten mit »=>« u.ä:
      #
      #   Ei-gen=wirt>schaft=>lich=>keit
      #                           ^^

      my $marker;
      my $wichtung;

      # Wir zerlegen mit »split« unter Beibehaltung der Begrenzer.
      my @zerlegung = split /([<>=-]+)/, $zeile;

      # Wir speichern die Wichtungen und deren Ränge als Felder, und
      # zwar als
      #
      #   wichtung * 10 + rang
      #
      # Das funktioniert nur für bis zu zehn Rängen, doch sollte das
      # ausreichend sein.
      my @wichtungen = (0) x @zerlegung;

      # Für jede Wichtungs-»Ebene« mit »>« und »<« protokollieren wir
      # den Rang.  Wir initialisieren die ersten drei Ebenen, die
      # stets Null sind.
      my @ränge = (0, 0, 0);

      # Erster Durchgang: Ermittle Wichtungswerte und Ränge von links
      # nach rechts.

      # Wir starten beim ersten Marker (mit Index 1).
      foreach my $i (1 .. ($#zerlegung - 1)) {
        # Ignoriere Nicht-Marker-Felder.
        next if not $i % 2;

        $marker = $zerlegung[$i];

        # Verwende einen Rang bei gleichen Präfix-Wichtungen: »<« ist
        # rechtsbindend.  Bei Auftreten einer höheren Wichtung werden
        # die Ränge für niedrigere Wichtungen zurückgesetzt.
        #
        # Die Marker »=«, »>« und »<« werden aus
        # Geschwindigkeitsgründen stets einzeln behandelt.
        if ($marker eq "-") {
          $wichtung = 1;
        }
        elsif ($marker eq "--") {
          $wichtung = 2;
        }
        elsif ($marker eq "<") {
          $wichtung = 3;
          $ränge[3] = $ränge[3] ? $ränge[3] - 1 : 9;
        }
        elsif ($marker eq "<<") {
          $wichtung = 4;
          $ränge[4] = $ränge[4] ? $ränge[4] - 1 : 9;
          $ränge[3] = 0;
        }
        elsif ($marker eq ">") {
          next;
        }
        elsif ($marker eq "=") {
          $wichtung = 5;
          @ränge[3 .. 5] = (0) x 3;
        }
        elsif ($marker =~ /^(<=+)$/) {
          $wichtung = length($1) * 2 + 2;
          $ränge[$wichtung] = $ränge[$wichtung] ? $ränge[$wichtung] - 1 : 9;
          @ränge[3 .. ($wichtung - 1)] = (0) x ($wichtung - 3);
        }
        elsif ($marker =~ /^(=+>)$/) {
          next;
        }
        elsif ($marker =~ /^(=+)$/) {
          $wichtung = length($1) * 2 + 3;
          @ränge[3 .. $wichtung] = (0) x ($wichtung - 2);
        }
        else {
          warn "Zeile $INPUT_LINE_NUMBER:"
               . " unbekannter Marker »$marker« behandelt als »-«\n";
          $wichtung = 1;
        }

        $wichtungen[$i] = 10 * $wichtung + $ränge[$wichtung];
      }

      # Zweiter Durchgang: Ermittle Wichtungswerte und Ränge von
      # rechts nach links.
      @ränge = (0, 0, 0);

      foreach my $i (reverse(1 .. ($#zerlegung - 1))) {
        # Ignoriere Nicht-Marker-Felder.
        next if not $i % 2;

        $marker = $zerlegung[$i];

        # Verwende einen Rang bei gleichen Suffix-Wichtungen: »>« ist
        # linksbindend.  Bei Auftreten einer höheren Wichtung werden
        # die Ränge für niedrigere Wichtungen zurückgesetzt.
        if ($marker eq ">") {
          $wichtung = 3;
          $ränge[3] = $ränge[3] ? $ränge[3] - 1 : 9;
        }
        elsif ($marker eq "=") {
          @ränge[3 .. 5] = (0) x 3;
          next;
        }
        elsif ($marker =~ /^(=+>)$/) {
          $wichtung = length($1) * 2 + 2;
          $ränge[$wichtung] = $ränge[$wichtung] ? $ränge[$wichtung] - 1 : 9;
          @ränge[3 .. ($wichtung - 1)] = (0) x ($wichtung - 3);
        }
        elsif ($marker =~ /^(=+)$/) {
          $wichtung = length($1) * 2 + 3;
          @ränge[3 .. $wichtung] = (0) x ($wichtung - 2);
          next;
        }
        else {
          next;
        }

        $wichtungen[$i] = 10 * $wichtung + $ränge[$wichtung];
      }

      # Nach diesen beiden Durchgängen schauen für die drei Beispiele
      # von oben die kombinierten Werte für Wichtungen und Ränge so
      # aus:
      #
      #    0    1   2    3   4     5    6      7   8     9   10
      #   ------------------------------------------------------
      #   Nach  <  denk  >  lich   >  keit
      #    0   39   0   38   0    39    0
      #
      #   Mit   <  ver   <  ant    -  wort     >  lich   >  keit
      #    0   39   0   38   0    10    0     38   0    39    0
      #
      #   Ei    -  gen   =  wirt   >  schaft  =>  lich  =>  keit
      #    0   10   0   50   0    39    0     68   0    69    0

      # Sortiere Indexfeld für Marker mit absteigender Wichtung.
      my @wichtungsindices =
        sort {
          -($wichtungen[$a] <=> $wichtungen[$b]);
        } (0 .. $#zerlegung);

      my $g = 0;
      my $wichtung_vorher = 0;

      # Die folgende Schleife konvertiert Wichtungen und Ränge zu den
      # finalen Wichtungswerten, die vom Argument der Option »-g«
      # kontrolliert werden.
      #
      #    0    1   2    3   4     5    6      7   8     9   10
      #   ------------------------------------------------------
      #   Mit   <  ver   <  ant    -  wort     >  lich   >  keit
      #    0    1   0    2   0     0    0      2   0     1    0
      #
      #   Ei    -  gen   =  wirt   >  schaft  =>  lich  =>  keit
      #    0    0   0    3   0     4    0      2   0     1    0

      foreach my $i (@wichtungsindices) {
        # Alle Wortteile haben einen geraden Index und sind stets am
        # Schluß von »@wichtungsindices«.
        last if not $i % 2;

        $wichtung = $wichtungen[$i];
        $g++ if $wichtung_vorher != $wichtung;
        $wichtung_vorher = $wichtung;

        # Entferne Trennung mit zu geringer Wichtung.
        $zerlegung[$i] = "" if $g > $opt_g || $wichtung <= 10;
      }

      $zeile = join '', @zerlegung;
    }
    elsif ($opt_g < 0) {
      # Schritt 6.
      $zeile =~ s/$Marker+/-/g;
    }
  }

  print "$zeile";
  print " " . $kommentar if $kommentar && $opt_x;
  print "\n";

  # Der Schlüssel im Hash ist das ungetrennte Wort, konvertiert zu
  # Kleinbuchstaben; Wert wird keiner gebraucht.
  $wortliste{lc($feld[0])} = ();

  if ($opt_U) {
    my $orig_zeile = $zeile;

    $zeile =~ tr[ÀàÁáÂâÃãÇçÈèÉéÊêËëÌìÍíÎîÏïÑñÒòÓóÔôÕõŠšÙùÚúÛûÝýŸÿŽž]
                [AaAaAaAaCcEeEeEeEeIiIiIiIiNnOoOoOoOoSsUuUuUuYyYyZz];

    $zeile =~ s/Ä/Ae/g;
    $zeile =~ s/ä/ae/g;
    $zeile =~ s/Å/Aa/g;
    $zeile =~ s/å/aa/g;
    $zeile =~ s/Æ/Ae/g;
    $zeile =~ s/æ/Ae/g;

    $zeile =~ s/Ö/Oe/g;
    $zeile =~ s/ö/oe/g;
    $zeile =~ s/Ø/Oe/g;
    $zeile =~ s/ø/oe/g;
    $zeile =~ s/Œ/Oe/g;
    $zeile =~ s/œ/oe/g;

    $zeile =~ s/Ü/Ue/g;
    $zeile =~ s/ü/ue/g;

    $wortliste_umschrift{lc($feld[0])} = $zeile if $orig_zeile ne $zeile;
  }
}

if ($opt_U) {
  # Wir geben nur Wörter aus, die nicht bereits in der originalen
  # Wortliste existieren.
  foreach my $wort (sort(keys %wortliste_umschrift)) {
    my $umschrift = $wortliste_umschrift{$wort};
    my $test = lc(entferne_marker($umschrift));

    print "$umschrift\n" if not exists ($wortliste{$test});
  }
}

# eof
