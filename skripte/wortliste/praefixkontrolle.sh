#!/bin/bash
wortlistenpraefixe() {
  ./skripte/lib/py_wortliste/wortzerlegung.py -p alle \
  | grep '^[^#]' | sed 's/ .*//'
}
zusatzlistenpraefixe() {
  { grep -v 'nicht ausgezeichnet' zusatzlisten/assimilierte_praefixe; \
  cat zusatzlisten/echte_praefixe zusatzlisten/initialkonfixe; } \
  | sed 's/[; ].*//' | ./skripte/sort.py | uniq
}
echo "Präfixe, die in der Wortliste, aber in keiner Zusatzliste auftauchen:"
diff <(wortlistenpraefixe) <(zusatzlistenpraefixe) | grep '^<' | sed 's/< //'
echo "Präfixe, die in einer Zusatzliste, aber nicht in der Wortliste auftauchen:"
diff <(wortlistenpraefixe) <(zusatzlistenpraefixe) | grep '^>' | sed 's/> //'
