#! /usr/bin/perl -w
#
# erzeuge-wortliste.pl
#
# Dieses Perl-Skript generiert aus einer gegebenen Liste von Wörtern, die
# mit Trennstellenmarkern für die reformierte Rechtschreibung versehen sind,
# Einträge für traditionelle deutsche und schweizerdeutsche Rechtschreibung
# sowie Versalformen (traditionell und reformiert) im Format der
# »wortliste«-Datei.  Es akzeptiert zusätzlich eine Liste von Ausnahmen, die
# bereits im Format der »wortliste«-Datei sein müssen und durch das Skript
# erzeugte Einträge überschreiben, wobei das erste Feld als Schlüssel
# verwendet wird.
#
# Wenn man versucht, »wortliste« aus den eigenen reformierten Wortformen zu
# erzeugen und Ausnahmen unberücksichtigt läßt, stimmen die Resultate zu
# mehr als 99.95% überein.
#
# Aufruf (zur Lesbarkeit auf mehrere Zeilen aufgeteilt):
#
#   perl [--debug] erzeuge-wortliste.pl \
#                  [liste1 liste2 ...] \
#                  [+ ausnahme1 ausnahme2 ...] > ausgabe
#
# Das Zeichen »+« trennt die Wortlistedateien von den Ausnahmedateien.
#
# Die Eingabedateien müssen in UTF-8 kodiert sein; als Zeilenformat wird
# entweder eine ein- oder zweispaltige Liste (mit »;« als Feldtrenner)
# akzeptiert, wobei Kommentare mit »#« beginnen und unmodifiziert ausgegeben
# werden.  Beispiele:
#
#    Beispiel;Bei<spiel # Format 1
#    Bei<spiel # Format 2
#
# Dateiname »-« entspricht der Standardeingabe.  Sind keine Eingabedateien
# angegeben, verwendet das Skript ebenfalls die Standardeingabe.  Beispiel:
#
#   perl erzeuge-wortliste.pl < reformiert > wortliste.erzeugt
#
# Die Ausgabe erfolgt unsortiert und sollte mit dem Skript »sort.py«
# weiterverarbeitet werden.
#
# Im folgenden Beispiel befinden sich die reformierten Einträge in den
# Dateien »a.reformiert«, »b.reformiert« usw.; die Ausnahmen sind in den
# Dateien »a.ausnahmen«, »b.ausnahmen« usw.  Eine sortierte Wortliste kann
# mit
#
#   perl erzeuge-wortliste.pl *.reformiert + *.ausnahmen \
#   | python sort.py -d \
#   > wortliste.komplett
#
# erzeugt werden.
#
# Option »--debug« gibt zusätzlich die vom Skript bestimmten Wortformen aus.

# Wir verwenden »<<>>« statt »<>« aus Sicherheitsgründen.
require 5.22.0;

use strict;
use warnings;
use utf8;                              # String-Literals direkt als UTF-8.
use open qw(:std :utf8);
use feature 'unicode_strings';
use Getopt::Long qw(:config no_ignore_case
                            no_auto_abbrev
                            no_getopt_compat);
use List::MoreUtils 'first_index';


my $debug = "";
GetOptions("debug" => \$debug);


# Analyse der Wortbestandteile
# ----------------------------
#
# Wir verwenden Subroutinen, um die folgenden Trennvarianten abzudecken:
#
#   reformierte Rechschreibung,
#   traditionelle Rechtschreibung,
#
#   reformierte Rechtschreibung, Versalform,
#   traditionelle Rechtschreibung, Versalform,
#
#   traditionelle schweizerdeutsche Rechtschreibung.
#
# Beachte: Die Wortliste hat keine separaten Einträge für traditionelle
# schweizerdeutsche Versalformen; diese fallen entweder mit den
# traditionellen Versalformen oder den traditionellen schweizerdeutschen
# Formen zusammen.
#
# Beachte weiters, daß sich Einträge für reformierte und traditionelle
# Rechtschreibung teilweise widersprechen und daher separat behandelt werden
# müssen; die Einträge für traditionelle deutsche und schweizerdeutsche
# Rechtschreibung dagegen nicht.  Aus diesem Grund ist die Wortliste für
# traditionelle schweizerdeutsche Rechtschreibung einfach eine Obermenge der
# Wortliste für die traditionelle deutsche Rechtschreibung; etwaige
# »Fehleinträge« der traditionellen schweizerdeutschen Rechtschreibung (z.B.
# »süsssauer« statt dem korrekten »süssauer«) werden nicht entfernt.


# Die Subroutinen modifizieren direkt das an sie übergebene Argument; die
# Subroutinen-Varianten mit angehängtem »_« sind für Wörter ohne
# Trennstellenmarker.


# »s-t« -> »-st« (ref. -> trad.)
#                (ref. -> trad. versal)
#                (ref. -> trad. schweiz.)
#
sub s_t {
  # Kno-ten=äs-te -> Kno-ten=äste
  # Äs-te         -> Äste
  $_[0] =~ s/(?: (?<= \W \w )
                 | (?<= ^ \w ) )
             s-t
            /st/xg;

  # Tes[-ter=/t=er<.]ken-nung -> Te[-ster=/st=er<.]ken-nung
  $_[0] =~ s/(?<= \w [^s] )
             s \[ \W+
                  t
                  (.*?)
               \/
                  t
                  (.*?)
               \]
            /[-st$1\/st$2]/xg;

  #  fas-te  -> fa-ste
  # (fass-te -> fass-te)
  $_[0] =~ s/(?<= \w [^s] )
             s-t
            /-st/xg;
}

sub s_t_ {
  # Nichts zu tun.
}


# »-ck« -> »{ck/k-k}« (ref. -> trad.)
#                     (ref. -> trad. versal)
#                     (ref. -> trad. schweiz.)
#
sub ck {
  # Dru[-cker=/ck=er<.]zeug>nis -> Dru[{ck/k-k}er=/ck=er<.]zeug>nis
  $_[0] =~ s/\[ \W+
                ck
                (.*?)
             \/
                ck
                (.*?)
             \]
            /[{ck\/k-k}$1\/ck$2]/xg;

  # He-cke -> He{ck/k-k}e
  $_[0] =~ s/[^\w\/\[\]\{\}]+
             ck
            /{ck\/k-k}/xg;

  #  Stau[=be-/b=e]cken -> Stau[=b/b=]e{ck/k-k}en
  $_[0] =~ s/\[ (.*?) (.)
                -
             \/
                (.*?) \g{2}
             \]
             ck
             (?= [aeiouäöüy] )
            /[$1\/$3]${2}{ck\/k-k}/xg;

  #  Ecke               -> E{ck/k-k}e
  #  Buch=ecker         -> Buch=e{ck/k-k}er
  # (Eck                -> Eck)
  # (Vier=eck           -> Vier=eck)
  # (blockt             -> blockt)
  $_[0] =~ s/(?: (?<= [^\w\/\[\]\{\}] \w )
                 | (?<= ^ \w ) )
             ck
             (?= [aeiouäöüy] )
            /{ck\/k-k}/xg;
}

sub ck_ {
  # Nichts zu tun.
}


# »-ß« -> »s-s« (ref. -> ref. versal)
#               (ref. -> trad. schweiz.)
#
sub bindestrich_ß_1 {
  # hei-ße -> heis-se
  $_[0] =~ s/\W+
             ß
            /s-s/xg;

  #  äße      -> äs-se
  #  auf<aßen -> auf<as-sen
  # (auf<aß   -> auf<aß)
  # (auf<aßt  -> auf<aßt)
  $_[0] =~ s/(?: (?<= \W \w )
                 | (?<= ^ \w ) )
             ß
             (?= [aeiouäöüy] )
            /s-s/xg;
}

sub bindestrich_ß_1_ {
  $_[0] =~ s/\W+
             ß
            /ss/xg;

  $_[0] =~ s/(?: (?<= \W \w )
                 | (?<= ^ \w ) )
             ß
             (?= [aeiouäöüy] )
            /ss/xg;
}


# »-ß« -> »-ss« (ref. -> trad. versal)
#
sub bindestrich_ß_2 {
  # hei-ße -> hei-sse
  $_[0] =~ s/-ß
            /-ss/xg;
}

sub bindestrich_ß_2_ {
  $_[0] =~ s/-ß
            /ss/xg;
}


# »ß-s« -> »{ss/ss-s}« (ref. -> trad. schweiz)
#
sub ß_s {
  #  süß=sau-er -> sü{ss/s=s}au-er
  # (Süß=stoff  -> Süß=stoff)
  $_[0] =~ s/ß
             (\W+)
             s
             (?= [aeiouäöüy] )
            /{ss\/ss$1s}/xg;
}

sub ß_s_ {
  $_[0] =~ s/ß
             (\W+)
             s
             (?= [aeiouäöüy] )
            /ss/xg;
}


# »ß« -> »ss« (ref. -> ref. versal)
#             (ref. -> trad. versal)
#             (ref. -> trad. schweiz.)
#
# Diese Regel ist nach den spezielleren ß-Regeln weiter oben anzuwenden.
#
sub ß {
  # heiß   -> heiss
  # büß-te -> büss-te
  $_[0] =~ s/ß
            /ss/xg;
}

sub ß_ {
  $_[0] =~ s/ß
            /ss/xg;
}


# »ss-s« -> »{ss/ss-s}« (ref. -> trad. schweiz.)
#
sub ss_s {
  #  Ess=sucht -> E{ss/ss=s}sucht
  # (Biss=spur -> Biss=spur)
  $_[0] =~ s/ss
             (\W+)
             s
             (?= [aeiouäöüy] )
            /{ss\/ss$1s}/xg;
}

sub ss_s_ {
  $_[0] =~ s/ss
             (\W+)
             s
             (?= [aeiouäöüy] )
            /ss/xg;
}


# »ss« -> »ß« (ref. -> trad.)
#
sub ss {
  #  biss=fest -> biß=fest
  #  esst      -> eßt
  #  Hass      -> Haß
  # (Fa-desse  -> Fa-desse)
  $_[0] =~ s/ss
             (?= (?: [b-df-hj-np-rtv-z]
                     | \W
                     | $ ) )
            /ß/xg;
}

sub ss_ {
  $_[0] =~ s/ss
             (?= (?: [b-df-hj-np-rtv-z]
                     | \W
                     | $ ) )
            /ß/xg;
}


# »xx-x« -> »{xx/xx-x}« (ref. -> trad.)
#                       (ref. -> trad. versal)
#                       (ref. -> trad. schweiz.)
#
# Buchstabe »x« ist irgendein Konsonant außer »s«.
#
sub xx_x {
  #  Bau=stoff==fir-ma -> Bau=sto{ff/ff==f}ir-ma
  # (Griff=flä-che     -> Griff=flä-che)
  $_[0] =~ s/([b-df-hj-np-rtv-z]) \g{1} (\W+) \g{1}
             (?= [aeiouäöüy] )
            /{$1$1\/$1$1$2$1}/xg;

  # Kipp=phä-no-men -> Ki{pp/pp=p}hä-no-men
  $_[0] =~ s/([prt]) \g{1} (\W+) \g{1}
             (?= h [aeiouäöüy] )
            /{$1$1\/$1$1$2$1}/xg;
}

sub xx_x_ {
  $_[0] =~ s/([b-df-hj-np-rtv-z]) \g{1} \W+ \g{1}
             (?= [aeiouäöüy] )
            /$1$1/xg;

  $_[0] =~ s/([prt]) \g{1} \W+ \g{1}
             (?= h [aeiouäöüy] )
            /$1$1/xg;
}


sub entferne_marker {
  # Es gilt
  #
  #   [»Variante A«/»Variante B«] -> »Variante A«
  #
  # Beispiel:
  #
  #   Dru[-cker=/ck=er<.]zeug>nis -> Dru-cker=zeug>nis
  #
  # Beachte, daß {.../...} bei reformierter Rechtschreibung nicht auftreten
  # kann.
  $_[0] =~ s/\[ (.*?) \/ .*? \]/$1/gx;

  $_[0] =~ s/\W//g;
}


# Gebe eine Zeile aus, unter Berücksichtigung der möglichen Zeilenformate.
#
sub zeile {
  my ($wort,
      $ref_wort, $ref_trennung,
      $ref_versal_wort, $ref_versal_trennung,
      $trad_wort, $trad_trennung,
      $trad_versal_wort, $trad_versal_trennung,
      $trad_schweiz_wort, $trad_schweiz_trennung,
      $kommentar) = @_;

  # Eliminiere Wortformen, die nicht mit »wort« übereinstimmen.
  $ref_trennung = "" if $wort ne $ref_wort;
  $ref_versal_trennung = "" if $wort ne $ref_versal_wort;
  $trad_trennung = "" if $wort ne $trad_wort;
  $trad_versal_trennung = "" if $wort ne $trad_versal_wort;
  $trad_schweiz_trennung = "" if $wort ne $trad_schweiz_wort;

  print $wort;
  if ($ref_trennung eq $trad_trennung
      && length($ref_trennung)) {
    print   ";"
          . $ref_trennung; # Feld 2
  }
  else {
    print   ";"
          . "-2-"
          . ";"
          . (length($trad_trennung) ? $trad_trennung
                                    : "-3-")
          . ";"
          . (length($ref_trennung) ? $ref_trennung
                                   : "-4-");

    if ($ref_trennung ne $ref_versal_trennung
        || $trad_trennung ne $trad_versal_trennung
        || $trad_trennung ne $trad_schweiz_trennung) {
      if ($ref_versal_trennung eq $trad_versal_trennung
          && ($ref_versal_trennung eq $trad_schweiz_trennung
              || !$trad_schweiz_trennung)) {
        if (length($ref_versal_trennung)) {
          print   ";"
                . $ref_versal_trennung; # Feld 5
        }
      }
      else {
        print   ";"
              . "-5-"
              . ";"
              . (length($trad_versal_trennung) ? $trad_versal_trennung
                                               : "-6-")
              . ";"
              . (length($ref_versal_trennung) ? $ref_versal_trennung
                                              : "-7-")
              . ";";

        if ($trad_versal_trennung eq $trad_schweiz_trennung) {
          print "-8-";
        }
        else {
          print length($trad_schweiz_trennung) ? $trad_schweiz_trennung
                                               : "-8-";
        }
      }
    }
  }

  # Kommentare werden nur einmal für Einträge in der reformierten
  # Schreibweise ausgegeben.
  print " " . $kommentar if $kommentar && $wort eq $ref_wort;

  print "\n";
}


# Hauptroutine
# ------------

# Wir geben zuerst die Wörter in den Ausnahmedateien in ein Hash.
my %ausnahmen;

# Alle Argumente nach dem »+«-Zeichen sind Ausnahmedateien.
my $index = first_index { $_ eq "+" } @ARGV;
if ($index > -1) {
  my @listendateien = splice(@ARGV, 0, $index + 1);

  while (<<>>) {
    # Leere Zeilen und Kommentarzeilen werden ignoriert.
    next if /^ \s* (?: $ | \# )/x;

    # Zerlege Zeile in erstes Feld und Rest.
    my @zeile = split(";", $_, 2);
    $ausnahmen{$zeile[0]} = $zeile[1];
  }

  # Konstruiere die Argumentliste für die nächste »while«-Schleife.
  @ARGV = @listendateien;
  pop @ARGV;
}

while (<<>>) {
  # Gebe Kommentarzeilen direkt aus.
  if (/^ \s* \#/x) {
    print;
    next;
  }

  chop;

  # Zerlege Zeile in ihre Bestandteile.  Mögliche Formate:
  #
  #   foobar;foo-bar # blabla
  #   foobar;foo-bar
  #   foo-bar        # blabla
  #   foo-bar
  #
  /^ (?: \S+ ;)?
     (\S+)
     \s*
     (\# .* )? $/x;

  my $trennung = $1;
  my $kommentar = defined $2 ? $2 : "";


  # Wir konstruieren jetzt alle möglichen Wortformen - der Benutzer muß
  # natürlich trotzdem alles kontrollieren.
  #
  # Die Ausgabe erfolgt unsortiert.

  # reformierte Rechtschreibung
  my $ref_trennung = $trennung;

  my $ref_wort = $trennung;
  entferne_marker($ref_wort);

  # reformierte Rechtschreibung, Versalform
  my $ref_versal_trennung = $trennung;
  bindestrich_ß_1($ref_versal_trennung);
  ß($ref_versal_trennung);

  my $ref_versal_wort = $trennung;
  bindestrich_ß_1_($ref_versal_wort);
  ß_($ref_versal_wort);
  entferne_marker($ref_versal_wort);

  # traditionelle Rechtschreibung
  my $trad_trennung = $trennung;
  s_t($trad_trennung);
  ck($trad_trennung);
  ss($trad_trennung);
  xx_x($trad_trennung);

  my $trad_wort = $trennung;
  s_t_($trad_wort);
  ck_($trad_wort);
  ss_($trad_wort);
  xx_x_($trad_wort);
  entferne_marker($trad_wort);

  # traditionelle Rechtschreibung, Versalform
  my $trad_versal_trennung = $trennung;
  s_t($trad_versal_trennung);
  ck($trad_versal_trennung);
  bindestrich_ß_2($trad_versal_trennung);
  ß($trad_versal_trennung);
  xx_x($trad_versal_trennung);

  my $trad_versal_wort = $trennung;
  s_t_($trad_versal_wort);
  ck_($trad_versal_wort);
  bindestrich_ß_2_($trad_versal_wort);
  ß_($trad_versal_wort);
  xx_x_($trad_versal_wort);
  entferne_marker($trad_versal_wort);

  # traditionelle Rechtschreibung, schweizerdeutch
  my $trad_schweiz_trennung = $trennung;
  s_t($trad_schweiz_trennung);
  ck($trad_schweiz_trennung);
  bindestrich_ß_1($trad_schweiz_trennung);
  ß_s($trad_schweiz_trennung);
  ß($trad_schweiz_trennung);
  ss_s($trad_schweiz_trennung);
  xx_x($trad_schweiz_trennung);

  my $trad_schweiz_wort = $trennung;
  s_t_($trad_schweiz_wort);
  ck_($trad_schweiz_wort);
  bindestrich_ß_1_($trad_schweiz_wort);
  ß_s_($trad_schweiz_wort);
  ß_($trad_schweiz_wort);
  ss_s_($trad_schweiz_wort);
  xx_x_($trad_schweiz_wort);
  entferne_marker($trad_schweiz_wort);

  if ($debug) {
    print "ref:           "
          . $ref_wort
          . " "
          . $ref_trennung
          . "\n";
    print "ref. versal:   "
          . $ref_versal_wort
          . " "
          . $ref_versal_trennung
          . "\n";
    print "trad:          "
          . $trad_wort
          . " "
          . $trad_trennung
          . "\n";
    print "trad. versal:  "
          . $trad_versal_wort
          . " "
          . $trad_versal_trennung
          . "\n";
    print "trad. schweiz: "
          . $trad_schweiz_wort
          . " "
          . $trad_schweiz_trennung
          . "\n";
    print "->\n";
  }


  # Gebe Zeilen aus
  # ---------------

  # Wir bilden einen Hash, um Doubletten der möglichen Wortformen und
  # Wortformen kürzer als vier Buchstaben zu entfernen.
  my %wortformen = map { length($_) > 3 ? ($_, 1)
                                        : ((), ()) }
                       ($ref_wort,
                        $ref_versal_wort,
                        $trad_wort,
                        $trad_versal_wort,
                        $trad_schweiz_wort);

  foreach my $wort (keys %wortformen) {
    if (defined($ausnahmen{$wort})) {
      print $wort . ";" . $ausnahmen{$wort};

      # Entferne Ausnahmeeintrag vom »ausnahmen«-Hash.
      delete $ausnahmen{$wort};
    }
    else {
      zeile($wort,
            $ref_wort, $ref_trennung,
            $ref_versal_wort, $ref_versal_trennung,
            $trad_wort, $trad_trennung,
            $trad_versal_wort, $trad_versal_trennung,
            $trad_schweiz_wort, $trad_schweiz_trennung,
            $kommentar);
    }
  }

}

# Abschließend geben wir die restlichen Ausnahmen aus.
foreach my $wort (keys %ausnahmen) {
  print $wort . ";" . $ausnahmen{$wort};
}

# EOF
