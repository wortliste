#!/usr/bin/env python3
# :Copyright: © 2014 Günter Milde.
#             Released without warranty under the terms of the
#             GNU General Public License (v. 2 or later)
# :Id: $Id:  $

# sprachauszug.py: Einträge einer Sprachvariante auswählen.
# =========================================================
#
# ::

"""Schreibe getrennte Wörter für eine Sprachvarante.

Eingabe: Ein Eintrag im Wortliste-Format pro Zeile.

Ausgabe: Nach Trennregeln der Sprachvariante getrenntes Wort.

Beispiele:

  Trennvorlagen für de-1996::

    python sprachauszug.py < wortliste > wortliste-de

  Trennvorlagen für de-1901, inklusive Versalschreibung aus Kurzformat

    python sprachauszug.py -language=de-1901,de-1901-x-versal -k < wlst

  Trennvorlagen für Notentext::

    ./sprachauszug.py --stil=notentext-syllabisch-einfach < wortliste

  Trennvorlagen für Notentext, nur Wörter mit zusätzlicher
  Trennmöglichkeit::

    ./sprachauszug.py --stil=notentext --test-filter < wortliste

Siehe auch README.wortliste und dokumente/Trennstile.txt.
"""

# Abhängigkeiten::

import sys, os, argparse, re
from os.path import dirname, abspath

# Dieses Skript ist ein "front-end" für das `Stilfilter`_ Modul::

# path for local Python modules
sys.path.insert(0, os.path.join(dirname(dirname(abspath(__file__))), 'lib'))

from py_wortliste.wortliste import WordEntry, ShortEntry, sprachauszug, run_filters
from py_wortliste import stilfilter

# .. _Stilfilter: ../python/py_wortliste/stilfilter.py.html
#
#
# Hauptfunktion
# -------------
#
# ::

if __name__ == '__main__':


# Optionen
# """"""""
#
# ::

    parser = argparse.ArgumentParser(description = __doc__,
                        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('INFILES', nargs='*', default=['-'],
                        help='Eingabedatei(en), Vorgabe: - (Standardeingabe).')
    parser.add_argument('--stilliste', action="store_true",
                        help='Zeige unterstützte Trennstile.')
    parser.add_argument('--sprachtags', action="store_true",
                        help='Zeige unterstützte Sprachbezeichner.')

    parser.add_argument('-l', '--language', metavar='SPRACHE,[SPRACHE...]',
                        help='Sprachvariante(n) (Vorgabe: "de-1996").',
                        default="de-1996")
    parser.add_argument('-k', '--kurzformat', action="store_true",
                        help='Verarbeite Wortliste im Kurzformat '
                        '(Vorgabe: Langformat).', default=False)
    parser.add_argument('-s', '--stil', metavar='STIL,[STIL...]',
                        help='Trennstil (Vorgabe: "morphemisch,standard,einfach")',
                        default="morphemisch,standard,einfach")
    parser.add_argument('-v', '--verbose', action="store_true",
                        help='Kommentare behalten.',
                        default=False)
    parser.add_argument('--extra-verbose', action="store_true",
                        help='Kommentarzeilen behalten, '
                        'Einträge für andere Sprachvarianten als Kommentar.',
                        default=False)
    parser.add_argument('--test-filter', action='store_true',
                        help='Schreibe nur geänderte Wörter.')
    parser.add_argument('--output-filter', metavar='EXP',
                        help='Schreibe nur Wörter, die auf den regulären '
                        'Ausdruck EXP passen.')
    parser.add_argument('-1', '--test-1er', action='store_true',
                        help='Schreibe nur Wörter mit „Flattertrennung“. '
                        '(überschreibt --output-filter)')

    args = parser.parse_args()

    if args.stilliste:
        help(stilfilter)
        sys.exit()

    if args.sprachtags:
        print('Langformat: ' + ', '.join(WordEntry.feldnamen[1:]))
        print('Kurzformat: ' + ', '.join(ShortEntry.feldnamen))
        print('Details: siehe "skripte/python/py_wortliste/wortliste.py."')
        sys.exit()

    if args.kurzformat:
        entry_class = ShortEntry
    else:
        entry_class = WordEntry

# Keine Ausgabe, wenn alle "Flatterbuchstaben" entfernt
# (überschreibt "--output-filter")::

    if args.test_1er:
        output_regexp = '[-<>=.][^-<>=./][-<>=.]'
    else:
        output_regexp = args.output_filter


# Iteration über Eingabe
# """"""""""""""""""""""
#
# ::

    for word in sprachauszug(args.INFILES or '-', args.language,
                             entry_class, args.extra_verbose):

# reine Kommentare (nur mit `extra_verbose`)::

        if word.startswith('#'):
            print(word)

# Trennstil (Filter anwenden)::

        processed = run_filters(args.stil.split(','), word)

# für Testzwecke: nur Wörter ausgeben, die durch Filter geändert wurden::

        if args.test_filter and processed == word:
            continue

# Nur Wörter ausgeben, die auf den Ausgabefilter-Ausdruck passen::

        if output_regexp and not re.search(output_regexp, processed):
            continue

# Auf Standardausgabe ausgeben::

        if args.verbose and entry.comment:
            processed += ' # ' + entry.comment.lstrip()
        print(processed)
