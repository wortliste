-- Argumente von der Kommandozeile lesen
beugen = true
i = 1
while arg[i] do
  if arg[i] == "-h" then
    hilfeAnzeigen = true
  elseif arg[i] == "-G" then
    beugen = false
  elseif string.len(arg[i]) == 6 and string.find(arg[i], "--VA=%d") then
    beugen = false
    vorschlaege = true
    adjektivvorschlaege = true
    formanzahl = tonumber(string.sub(arg[i], -1))
  elseif string.len(arg[i]) == 6 and string.find(arg[i], "--VS=%d") then
    beugen = false
    vorschlaege = true
    substantivvorschlaege = true
    formanzahl = tonumber(string.sub(arg[i], -1))
  elseif string.len(arg[i]) == 6 and string.find(arg[i], "--VV=%d") then
    beugen = false
    vorschlaege = true
    verbvorschlaege = true
    formanzahl = tonumber(string.sub(arg[i], -1))
  else
    io.stderr:write('Ungültige Option "'..arg[i]..'".\n')
    os.exit(1)
  end
  i = i+1
end

-- Ausgeben der Hilfe
if hilfeAnzeigen then
  print([[
Aufruf: texlua beugung.lua [Option] [< Eingabedatei] [> Ausgabedatei]
oder:   lua5.3 beugung.lua [Option] [< Eingabedatei] [> Ausgabedatei]

Wandlung zwischen Kurzformat und grammatischem Kurzformat

Bei Aufruf ohne Option wird die Eingabe vom grammatischen Kurzformat ins
Kurzformat umgewandelt.

Optionen:
  -h            diese Hilfe anzeigen
  -G            Eingabe vom Kurzformat ins grammatische Kurzformat umwandeln
  --VA=<Anzahl> eine Vorschlagsliste für neu zu markierende Adjektive, von denen
                bereits <Anzahl> gebeugte Formen in der Eingabe vorhanden sind,
                im grammatischen Kurzformat ausgeben; die Eingabe erfolgt im
                Kurzformat; sinnvoll sind Anzahlen zwischen 1 und 4;
                großgeschriebene Wörter werden nur berücksichtigt wenn sie auf
                "sch" oder "sche" enden (z. B. "Goethesche")
  --VS=<Anzahl> eine Vorschlagsliste für neu zu markierende Substantive, von
                denen bereits <Anzahl> gebeugte Formen in der Eingabe vorhanden
                sind, im grammatischen Kurzformat ausgeben; die Eingabe erfolgt
                im Kurzformat; sinnvoll sind Anzahlen zwischen 1 und 4; es
                werden nur großgeschriebene Wörter berücksichtigt
  --VV=<Anzahl> eine Vorschlagsliste für neu zu markierende Verben, von denen
                bereits <Anzahl> gebeugte Formen in der Eingabe vorhanden sind,
                im grammatischen Kurzformat ausgeben; die Eingabe erfolgt im
                Kurzformat; sinnvoll sind Anzahlen zwischen 1 und 6; es werden
                nur kleingeschriebene Wörter berücksichtigt

Wenn keine Eingabedatei angegeben ist, wird von der Standardeingabe gelesen.
Der Abschluss der Eingabe erfolgt dann mit Strg+D am Zeilenanfang.

Wenn keine Ausgabedatei angegeben ist, wird auf die Standardausgabe geschrieben.

Die Ausgabe erfolgt unsortiert. Bitte bei Bedarf das Skript sort.py verwenden.]])
  os.exit()
end

function trim(s)
  return string.gsub(s, "^%s*(.-)%s*$", "%1")
end

eingabeliste = {} -- eingelesene Wortliste
ausgabeliste = {} -- auszugebende Wortliste

-- Einlesen der Eingabe
for zeile in io.lines() do
  local feld = {} -- Felder der aktuell gelesenen Zeile
  local i = string.find(zeile,"#")
  if i and i < string.len(zeile) then
    kommentar = trim(string.sub(zeile,i+1))
    zeile = trim(string.sub(zeile,1,i-1))
    if beugen then
      local j = string.find(kommentar,"%-%-")
      if j and string.len(kommentar) > 2 then
        if j > 1 then
          beugungsklasse = trim(string.sub(kommentar,1,j-1))
        else
          beugungsklasse = nil
        end
        kommentar = trim(string.sub(kommentar,j+2))
      else
        beugungsklasse = kommentar
        kommentar = nil
      end
    end
  else
    beugungsklasse = nil
    kommentar = nil
  end
  for k = 1, 5 do
    if zeile then
      i = string.find(zeile,";")
      if i then
        feld[k] = string.sub(zeile,1,i-1)
        zeile = string.sub(zeile,i+1)
      else
        feld[k] = zeile
        zeile = nil
      end
    end
  end
  if feld[1] ~= "-1-" then
    schluessel = feld[1]
  elseif feld[2] ~= "-2-" then
    schluessel = feld[2]
  else
    schluessel = feld[3]
  end
  eingabeliste[schluessel] = {}
  for k = 1, 5 do
    if feld[k] then
      eingabeliste[schluessel][k] = feld[k]
    end
  end
  if beugungsklasse then
    eingabeliste[schluessel]["beugungsklasse"] = beugungsklasse
  end
  if kommentar then
    eingabeliste[schluessel]["kommentar"] = kommentar
  end
end

function istKleingeschrieben(s)
  if string.len(s) >= 1 and string.find(string.sub(s, 1, 1), "%l") then
    return true
  elseif string.len(s) >= 2 then
    local a = string.sub(s, 1, 2)
    if a == "ä" or a == "ö" or a == "ü" then
      return true
    else
      return false
    end
  else
    return false
  end
end

-- Apfel -> Äpfel; Wolf -> Wölf; Saal -> Säl; Baum -> Bäum
function lauteUm(s)
  local u
  for i = string.len(s), 1, -1 do
    local a = string.sub(s, i, i)
    if a == "A" then
      u = "Ä"
    elseif a == "a" then
      u = "ä"
    elseif a == "O" then
      u = "Ö"
    elseif a == "o" then
      u = "ö"
    elseif a == "U" then
      u = "Ü"
    elseif a == "u" then
      u = "ü"
    end
    if u then
      if u == "ä" and i > 1 and string.sub(s, i-1, i-1) == "a" then
        if i == 2 then
          return u..string.sub(s, i+1)
        elseif i < string.len(s) then
          return string.sub(s, 1, i-2)..u..string.sub(s, i+1)
        else
          return string.sub(s, 1, i-2)..u
        end
      elseif u == "ü" and i > 1 and string.sub(s, i-1, i-1) == "a" then
        if i == 2 then
          return "ä"..string.sub(s, i)
        elseif i < string.len(s) then
          return string.sub(s, 1, i-2).."ä"..string.sub(s, i)
        else
          return string.sub(s, 1, i-2).."äu"
        end
      elseif u == "ü" and i > 1 and string.sub(s, i-1, i-1) == "A" then
        if i == 2 then
          return "Ä"..string.sub(s, i)
        elseif i < string.len(s) then
          return string.sub(s, 1, i-2).."Ä"..string.sub(s, i)
        else
          return string.sub(s, 1, i-2).."Äu"
        end
      elseif i == 1 then
        return u..string.sub(s, i+1)
      elseif i < string.len(s) then
        return string.sub(s, 1, i-1)..u..string.sub(s, i+1)
      else
        return string.sub(s, 1, i-1)..u
      end
    end
  end
  return false -- kein umlautbarer Vokal gefunden
end

-- Suche in der Wortliste nach den angegebenen Formen mit gleicher
-- Feldbelegung wie der Eintrag mit der angegebenen Grundform und markiere
-- diese als gebeugt, falls alle vorhanden.
function sucheFormen(grundform, formen)
  local i, j
  for _, form in ipairs(formen) do
    if eingabeliste[form] then
      if eingabeliste[form]["kommentar"] then
        return false
      end
      for k = 1, 5 do
        if eingabeliste[grundform][k] and not eingabeliste[form][k] then
          return false
        elseif not eingabeliste[grundform][k] and eingabeliste[form][k] then
          return false
        -- Formen mit abweichenden eckigen Klammern ausschließen
        elseif eingabeliste[grundform][k] and eingabeliste[form][k] then
          i = string.find(eingabeliste[grundform][k], "%]")
          j = string.find(eingabeliste[form][k], "%]")
          if i and not j or j and not i or i and j and i ~= j then
            return false -- Bettuche etc.
          end
        end
      end
    else
      return false
    end
  end
  for _, form in ipairs(formen) do
    eingabeliste[form]["gebeugteForm"] = true
  end
  return true
end

-- Suche in der Wortliste nach den angegebenen Formen mit gleicher
-- Feldbelegung wie der Eintrag mit der angegebenen Grundform, gib die Anzahl
-- zurück.
function zaehleFormen(grundform, formen)
  local treffer = 0
  for _, form in ipairs(formen) do
    if eingabeliste[form] then
      if not eingabeliste[form]["kommentar"] then
        for k = 1, 5 do
          if eingabeliste[grundform][k] and not eingabeliste[form][k] then
            break
          elseif not eingabeliste[grundform][k] and eingabeliste[form][k] then
            break
          end
          if k == 5 then
            treffer = treffer + 1
          end
        end
      end
    end
  end
  return treffer
end

adjektivendungen = {"e", "em", "en", "er", "es"}
adjektivendungen_e = {"m", "n", "r", "s"} -- für Adjektive, die auf e enden
substantivendungen_rn = {"r", "n"} -- der Beamte, ein Beamter, des Beamten
substantivendungen_se = {"s", "es", "e", "en"} -- der Tag, des Tag(e)s, die Tage, den Tagen
substantivendungen_se_Kurzgenitiv = {"s", "e", "en"} -- der Tresor, des Tresors, die Tresore, den Tresoren
substantivendungen_se_Langgenitiv = {"es", "e", "en"} -- der Sitz, des Sitzes, die Sitze, den Sitzen
substantivendungenSingular_se = {"s", "es", "e"} -- der Wolf, des Wolf(e)s, dem Wolfe
substantivendungenSingular_se_Langgenitiv = {"es", "e"} -- das Floß, des Floßes, dem Floße
substantivendungenPlural_e = {"e", "en"} -- die Wölfe, den Wölfen; die Hände, den Händen
substantivendungen_sr = {"s", "es", "e", "er", "ern"} -- das Kind, des Kind(e)s, dem Kinde, die Kinder, den Kindern
substantivendungenSingular_sr = {"s", "es", "e"} -- das Amt, des Amt(e)s, dem Amte
substantivendungenSingular_sr_Langgenitiv = {"es", "e"} -- das Gras, des Grases, dem Grase
substantivendungenPlural_r = {"er", "ern"} -- die Ämter, den Ämtern
-- ich kaufe, du kaufst, er kauft, ich kaufte, du kauftest, wir kauften, ihr kauftet
verbendungen = {"e", "st", "t", "te", "test", "ten", "tet"}
verbendungenPraesens = {"e", "st", "t"} -- ich gehe, du gehst, er geht
verbendungenPraeteritum = {"st", "en", "t"} -- ich ging, du gingst, wir gingen, ihr gingt
verbendungenPraeteritumGemischt = {"st", "n", "t"} -- ich brachte, du brachtest, wir brachten, ihr brachtet
-- nach d u. a.: ich rede, du redest, er redet, ich redete, du redetest, wir redeten, ihr redetet
verbendungen_d = {"e", "est", "et", "e-te", "e-test", "e-ten", "e-tet"}
verbendungenPraesens_d = {"e", "est", "et"}
verbendungenPraeteritum_d = {"st", "en", "et"} -- ich schied, du schiedst, wir schieden, ihr schiedet
-- nach s-Laut: ich reise, du reist, ich reiste, du reistest, wir reisten, ihr reistet
verbendungen_s = {"e", "t", "te", "test", "ten", "tet"}
verbendungenPraesens_s = {"e", "t"} -- ich reiße, du reißt
verbendungenPraeteritum_s = {"est", "en", "t"} -- ich las, du lasest, wir lasen, ihr last
-- ich sammle, du sammelst, er sammelt, ich sammelte, du sammeltest, wir sammelten, ihr sammeltet
verbendungen_eln = {"le", "elst", "elt", "el-te", "el-test", "el-ten", "el-tet"}
verbendungenPraesens_eln = {"le", "elst", "elt"}

-- Funktion zur Mengendefinition
function Set(list)
  local set = {}
  for _, l in ipairs(list) do
    set[l] = true
  end
  return set
end

schlusskonsonanten = Set{"b", "d", "f", "g", "h", "k", "l", "m", "n", "p", "r",
  "s", "t", "v", "x", "z", "sch", "ch", "ph", "ck", "ß"}
vokale = Set{"a", "ä", "e", "i", "o", "ö", "u", "ü", "y"}
diphthonge = Set{"Au", "au", "äu", "Ei", "ei", "eu", "ay", "ey", "oy", "ee", "ie"}

-- Wörter mit den folgenden Endungen bilden den Genitiv auf -es, aber nicht auf -s
nurLanggenitivEndungen_se_sr = Set{"s",    -- Preis, Haus
                                   "ß",    -- Maß
                                   "ngst", -- Hengst
                                   "lst",  -- Wulst (und Schwulst?)
                                   "anst", -- Wanst
                                   "rnst", -- Ernst
                                   "x",    -- Reflex
                                   "z"}    -- Netz, Pelz

-- Wörter mit den folgenden Endungen bilden den Genitiv auf -s, aber nicht auf -es
nurKurzgenitivEndungen_se = Set{"end", -- Abend, Dutzend, Jahrtausend
                                "ig",  -- König
                                "ing", -- Ankömmling, Hering
                                "ak",  -- Anorak
                                "ik",  -- Mosaik
                                "al",  -- Portal, Potential, Schicksal
                                "pel", -- Archipel
                                "il",  -- Exil, Mobil, Ventil
                                "ll",  -- Appell, Metall, Protokoll
                                "ul",  -- Modul
                                "ül",  -- Molekül
                                "em",  -- Problem, System
                                "mm",  -- Diagramm, Programm
                                "om",  -- Karzinom, Polynom
                                "erm", -- Term
                                "ym",  -- Antonym, Enzym
                                "an",  -- Enzian, Organ, Roman, Sopran, Vulkan
                                "en",  -- Gen, Phänomen
                                "in",  -- Dioxin, Pinguin
                                "on",  -- Balkon
                                "op",  -- Teleskop
                                "ar",  -- Kommissar
                                "är",  -- Funktionär
                                "ier", -- Musketier, Papier, Pionier
                                "or",  -- Meteor
                                "ör",  -- Frisör
                                "ur",  -- Abitur, Akteur, Troubadour
                                "it",  -- Austenit, Defizit, Fazit
                                "iv"}  -- Archiv

-- Ausnahmen der Ausnahmen: Bei folgenden Endungen gibt es den Genitiv auf -es doch.
auchLanggenitivEndungen_se = Set{"eig",   -- Fingerzeig, Steig, Teig, Zweig
                                 "Ding",
                                 "=ding",
                                 "Ring",
                                 "=ring",
                                 "Aal",
                                 "=aal",
                                 "Mal",
                                 "=mal",
                                 "Wal",
                                 "=wal",
                                 "eil",   -- Beil, Heil, Pfeil, Seil, Teil
                                 "Stil",
                                 "=stil",
                                 "Fell",
                                 "=fell",
                                 "stell", -- Gestell
                                 "roll",  -- Troll
                                 "Dom",
                                 "=dom",
                                 "plom",  -- Diplom
                                 "a-san", -- Fasan
                                 "bi-an", -- Grobian
                                 "r-kan", -- Orkan
                                 "Kran",
                                 "=kran",
                                 "Tran",
                                 "=tran",
                                 "ain",   -- Hain, Rain
                                 "ein",   -- Stein, Wein
                                 "hron",  -- Thron
                                 "aar",   -- Haar, Paar
                                 "Bier",
                                 "=bier",
                                 "Tier",
                                 "=tier",
                                 "<tier", -- Untier
                                 "Stier",
                                 "=stier",
                                 "oor",   -- Moor
                                 "Tor",
                                 "=tor",
                                 "hör",   -- Gehör, Verhör, Zubehör
                                 "tör",   -- Stör
                                 "eit",   -- Geleit, Streit
                                 "e-dit", -- Kredit
                                 "o-fit"} -- Profit

function hatLanggenitiv(eintrag)
  local n = string.len(eintrag)
  if n > 2 and auchLanggenitivEndungen_se[string.sub(eintrag, -3)] then
    return true
  elseif n > 3 and auchLanggenitivEndungen_se[string.sub(eintrag, -4)] then
    return true
  elseif n > 4 and auchLanggenitivEndungen_se[string.sub(eintrag, -5)] then
    return true
  end
  return false
end

function hatNurKurzgenitiv(eintrag)
  local n = string.len(eintrag)
  if n > 2 and nurKurzgenitivEndungen_se[string.sub(eintrag, -2)] and not hatLanggenitiv(eintrag) then
    return true
  elseif n > 3 and nurKurzgenitivEndungen_se[string.sub(eintrag, -3)] and not hatLanggenitiv(eintrag) then
    return true
  end
  return false
end

function hatNurLanggenitiv(eintrag)
  local n = string.len(eintrag)
  if n > 1 and nurLanggenitivEndungen_se_sr[string.sub(eintrag, -1)] then
    return true
  elseif n > 2 and nurLanggenitivEndungen_se_sr[string.sub(eintrag, -2)] then
    return true
  elseif n > 3 and nurLanggenitivEndungen_se_sr[string.sub(eintrag, -3)] then
    return true
  elseif n > 4 and nurLanggenitivEndungen_se_sr[string.sub(eintrag, -4)] then
    return true
  end
  return false
end

-- hängt eine Endung an einen Wortstamm an, wobei die richtige Trennstelle gesetzt wird
-- stamm: Wortstamm ohne Schlussvokal/-konsonant
-- schluss: Schlussvokal/-konsonant des Stamms
-- endung: anzuhängende Endung
-- AR: boolescher Wert für alte Rechtschreibung
function haengeEndungAn(stamm, schluss, endung, AR)
  if endung == "" then
    return stamm..schluss
  elseif schlusskonsonanten[endung] or endung == "st" then
    if string.len(stamm) > 2 and string.sub(stamm, -2) == "·" then -- Ra-di·o
      return string.sub(stamm, 1, -3).."-"..schluss..endung -- Ra-di-os
    else -- geht, gehst
      return stamm..schluss..endung
    end
  elseif schluss == "ck" and AR then
    return stamm.."{ck/k-k}"..endung
  elseif diphthonge[schluss] or vokale[schluss] then
    if string.len(endung) == 1 then
      return stamm..schluss.."·"..endung
    elseif schluss == "ee" and endung == "en" then
      return stamm.."e-.en"
    elseif schluss == "ie" and endung == "en" then
      return stamm.."i-.en"
    else
      return stamm..schluss.."-"..endung
    end
  elseif schlusskonsonanten[schluss] and schlusskonsonanten[string.sub(endung, 1, 1)] then
    return stamm..schluss.."-"..endung
  elseif string.len(endung) > 0 then
    if utf8.len(stamm) > 1 then
      local i = utf8.offset(stamm, -2)
      local c = string.sub(stamm, i, i)
      if c == "=" or c == "<" then -- Par-kett=öl
        return stamm.."·"..schluss..endung -- Par-kett=ö·le
      else
        return stamm.."-"..schluss..endung
      end
    else -- Öl
      return stamm.."·"..schluss..endung -- Ö·le
    end
  else
    return stamm..schluss
  end
end

-- Überprüfe, ob eine schwierige Konsonantenkombination vorliegt, nach der in
-- der 2. Person ein "e" eingeschoben wird (atmen, du atmest)
function schwierigeKonsonantenkombination(stamm, schluss)
  if string.len(stamm) < 2 then
    return false
  end
  local a = string.sub(stamm, -1)
  local aa = string.sub(stamm, -2)
  if schluss == "m" then
    if a == "d" or a == "t" then -- widmen/atmen
      return true
    else
      return false
    end
  elseif schluss == "n" then
    if aa == "ch" or aa == "ck" then -- zeichnen/trocknen
      return true
    elseif a == "b" or a == "d" or a == "g" or a == "p" then -- ebnen/ordnen/leugnen/wappnen
      return true
    else
      return false
    end
  else
    return false
  end
end

-- zerlege einen Wortstamm in den Konsonanten/Vokal/Diphthong am Schluss
-- und den übrigen Teil
function zerlegeWortstamm(stamm, trennstrich)
  local schluss
  if string.len(stamm) > 3 and string.sub(stamm, -3) == "sch" then
    schluss = "sch"
    if not trennstrich then
      stamm = string.sub(stamm, 1, -4)
    elseif utf8.len(stamm) > 4 and (string.sub(stamm, -4, -4) == "-" or string.sub(stamm, -5, -4) == "·") then
      stamm = string.sub(stamm, 1, utf8.offset(stamm,-4)-1)
    end
  elseif string.len(stamm) > 2 and (string.sub(stamm, -2) == "ch" or string.sub(stamm, -2) == "ph") then
    schluss = string.sub(stamm, -2)
    if not trennstrich then
      stamm = string.sub(stamm, 1, -3)
    elseif utf8.len(stamm) > 3 and (string.sub(stamm, -3, -3) == "-" or string.sub(stamm, -4, -3) == "·") then
      stamm = string.sub(stamm, 1, utf8.offset(stamm,-3)-1)
    end
  elseif string.len(stamm) > 2 and string.sub(stamm, -2) == "ck" then
    schluss = "ck"
    if not trennstrich then
      stamm = string.sub(stamm, 1, -3)
    elseif utf8.len(stamm) > 3 and (string.sub(stamm, -3, -3) == "-" or string.sub(stamm, -4, -3) == "·") then
      stamm = string.sub(stamm, 1, utf8.offset(stamm,-3)-1)
    end
  elseif string.len(stamm) > 2 and string.sub(stamm, -2) == "ß" then
    schluss = "ß"
    if not trennstrich then
      stamm = string.sub(stamm, 1, -3)
    elseif utf8.len(stamm) > 2 and (string.sub(stamm, -3, -3) == "-" or string.sub(stamm, -4, -3) == "·") then
      stamm = string.sub(stamm, 1, utf8.offset(stamm,-2)-1)
    end
  elseif string.len(stamm) > 1 and schlusskonsonanten[string.sub(stamm,-1)] then
    schluss = string.sub(stamm, -1)
    if not trennstrich then
      stamm = string.sub(stamm, 1, -2)
    elseif utf8.len(stamm) > 2 and (string.sub(stamm, -2, -2) == "-" or string.sub(stamm, -3, -2) == "·") then
      stamm = string.sub(stamm, 1, utf8.offset(stamm,-2)-1)
    end
  elseif trennstrich then
    if utf8.len(stamm) > 3 and diphthonge[string.sub(stamm, utf8.offset(stamm,-3), -2)]
    and string.sub(stamm, -1) == "-" then
      schluss = string.sub(stamm, utf8.offset(stamm,-3), -2)
      stamm = string.sub(stamm, 1, utf8.offset(stamm,-3)-1)
    elseif utf8.len(stamm) > 2 and vokale[string.sub(stamm, utf8.offset(stamm,-2), -2)]
    and (string.sub(stamm, -1) == "-" or string.sub(stamm, -2) == "·") then
      schluss = string.sub(stamm, utf8.offset(stamm,-2), -2)
      stamm = string.sub(stamm, 1, utf8.offset(stamm,-2)-1)
    end
  else
    if utf8.len(stamm) > 2 and diphthonge[string.sub(stamm, utf8.offset(stamm,-2))] then
      schluss = string.sub(stamm, utf8.offset(stamm,-2))
      stamm = string.sub(stamm, 1, utf8.offset(stamm,-2)-1)
    elseif utf8.len(stamm) > 1 and vokale[string.sub(stamm, utf8.offset(stamm,-1))] then
      schluss = string.sub(stamm, utf8.offset(stamm,-1))
      stamm = string.sub(stamm, 1, utf8.offset(stamm,-1)-1)
    end
  end
  return stamm, schluss
end

-- melde erfolglose Suche zurück
function sucheErfolglos(zaehlen)
  if zaehlen then
    return 0
  else
    return false
  end
end

function sucheAdjektivformen(schluessel, zaehlen)
  local stamm, schluss
  stamm, schluss = zerlegeWortstamm(schluessel)
  if schlusskonsonanten[schluss] or diphthonge[schluss] then
    local formen = {}
    for _, adjektivendung in ipairs(adjektivendungen) do
      table.insert(formen, haengeEndungAn(stamm,schluss,adjektivendung))
    end
    if zaehlen then
      if istKleingeschrieben(schluessel) or schluss == "sch" then
        return zaehleFormen(schluessel, formen)
      else
        return 0
      end
    else
      return sucheFormen(schluessel, formen)
    end
  else
    return sucheErfolglos(zaehlen)
  end
end

function sucheAdjektivformen_e(schluessel, zaehlen)
  local stamm, schluss
  stamm, schluss = zerlegeWortstamm(schluessel)
  if schluss == "e" then
    local formen = {}
    for _, adjektivendung in ipairs(adjektivendungen_e) do
      table.insert(formen, haengeEndungAn(stamm,"e",adjektivendung))
    end
    if zaehlen then
      if (istKleingeschrieben(schluessel) or string.len(stamm) > 3 and string.sub(stamm, -3) == "sch") then
        return zaehleFormen(schluessel, formen)
      else
        return 0
      end
    else
      return sucheFormen(schluessel, formen)
    end
  else
    return sucheErfolglos(zaehlen)
  end
end

function sucheSubstantivformen(schluessel, beugungsklasse, zaehlen)
  local stamm, schluss, endungen
  stamm, schluss = zerlegeWortstamm(schluessel)
  if not istKleingeschrieben(schluessel) and schluss and schluss ~= "e" then
    local formen = {}
    local suffix_nis
    local suffix_in
    local doppelschluss = string.sub(schluessel, -2)
    if beugungsklasse == "se" then
      if hatNurLanggenitiv(schluessel) then
        endungen = substantivendungen_se_Langgenitiv
        if string.len(schluessel) > 3 and string.sub(schluessel, -3) == "nis" then
          suffix_nis = true
        end
      elseif hatNurKurzgenitiv(schluessel) then
        endungen = substantivendungen_se_Kurzgenitiv
      else
        endungen = substantivendungen_se
      end
    elseif beugungsklasse == "sn" then
      if doppelschluss == "el" or doppelschluss == "er" -- Stiefel/Teller
      or doppelschluss == "ul" then -- Modul
        endungen = {"s", "n"}
      else -- Autor, Elektron, See, Typ, Zeh
        endungen = {"s", "en"}
      end
    elseif beugungsklasse == "sr" then
      endungen = substantivendungen_sr
    elseif beugungsklasse == "e" then
      endungen = substantivendungenPlural_e
      if string.len(schluessel) > 3 and string.sub(schluessel, -3) == "nis" then
        suffix_nis = true
      end
    elseif beugungsklasse == "n" then
      if doppelschluss == "el" or doppelschluss == "er" then -- Angel/Faser
        endungen = {"n"}
      else
        endungen = {"en"}
        if doppelschluss == "in" then
          suffix_in = true
        end
      end
    elseif beugungsklasse == "s" then
      endungen = {"s"}
    end
    for _, substantivendung in ipairs(endungen) do
      if schlusskonsonanten[substantivendung] then
        table.insert(formen, schluessel..substantivendung)
      else
        if suffix_nis then
          table.insert(formen, haengeEndungAn(stamm.."s",schluss,substantivendung))
        elseif suffix_in then
          table.insert(formen, haengeEndungAn(stamm.."n",schluss,substantivendung))
        else
          table.insert(formen, haengeEndungAn(stamm,schluss,substantivendung))
        end
      end
    end
    if zaehlen then
      return zaehleFormen(schluessel, formen)
    else
      return sucheFormen(schluessel, formen)
    end
  else
    return sucheErfolglos(zaehlen)
  end
end

function sucheSubstantivformen_e(schluessel, endungsklasse, zaehlen)
  local stamm, schluss, endungen
  stamm, schluss = zerlegeWortstamm(schluessel)
  if schluss == "e" and not istKleingeschrieben(schluessel) then
    local formen = {}
    if endungsklasse == "rn" then
      endungen = substantivendungen_rn
    elseif endungsklasse == "sn" then
      endungen = {"s", "n"}
    elseif endungsklasse == "n" then
      endungen = {"n"}
    elseif endungsklasse == "s" then
      endungen = {"s"}
    end
    for _, substantivendung in ipairs(endungen) do
      table.insert(formen, haengeEndungAn(stamm,"e",substantivendung))
    end
    if zaehlen then
      return zaehleFormen(schluessel, formen)
    else
      return sucheFormen(schluessel, formen)
    end
  else
    return sucheErfolglos(zaehlen)
  end
end

function sucheSubstantivformenUmlaut(schluessel, endungsklasse, zaehlen)
  local stamm, schluss
  stamm, schluss = zerlegeWortstamm(schluessel)
  if schluss and schluss ~= "e" and not istKleingeschrieben(schluessel) then
    local umlautstamm = lauteUm(stamm)
    local singlarendungen
    local pluralendungen
    local formen = {}
    if endungsklasse == "se" then
      if hatNurLanggenitiv(schluessel) then
        singularendungen = substantivendungenSingular_se_Langgenitiv
      else
        singularendungen = substantivendungenSingular_se
      end
      pluralendungen = substantivendungenPlural_e
    elseif endungsklasse == "s" then
      singularendungen = {"s"}
      if string.len(schluessel) > 2 and string.sub(schluessel, -2) == "en" then -- Laden
        pluralendungen = {""}
      else -- Apfel, Hammer
        pluralendungen = {"", "n"}
      end
    elseif endungsklasse == "sr" then
      if hatNurLanggenitiv(schluessel) then
        singularendungen = substantivendungenSingular_sr_Langgenitiv
      else
        singularendungen = substantivendungenSingular_sr
      end
      pluralendungen = substantivendungenPlural_r
    elseif endungsklasse == "e" then
      singularendungen = {}
      pluralendungen = substantivendungenPlural_e
    elseif endungsklasse == "" then
      singularendungen = {}
      pluralendungen = {"", "n"}
    end
    for _, substantivendung in ipairs(singularendungen) do
      table.insert(formen, haengeEndungAn(stamm,schluss,substantivendung))
    end
    if umlautstamm then
      for _, substantivendung in ipairs(pluralendungen) do
        table.insert(formen, haengeEndungAn(umlautstamm,schluss,substantivendung))
      end
    else
      return sucheErfolglos(zaehlen)
    end
    if zaehlen then
      return zaehleFormen(schluessel, formen)
    else
      return sucheFormen(schluessel, formen)
    end
  else
    return sucheErfolglos(zaehlen)
  end
end

function sucheVerbformen(schluessel, beugungsklasse, zaehlen)
  local stamm, schluss, endungen
  if istKleingeschrieben(schluessel) then
    -- 1: Verben auf "en"
    if string.len(schluessel) > 3 and string.sub(schluessel, -2) == "en" and beugungsklasse ~= "VV" then
      stamm = string.sub(schluessel, 1, -3)
      stamm, schluss = zerlegeWortstamm(stamm, true)
      if schluss == "s" or schluss == "ß" or schluss == "x" or schluss == "z" then
        if beugungsklasse == "V" then
          endungen = verbendungen_s
        elseif beugungsklasse == "VG" then
          endungen = verbendungenPraesens_s
        end
      elseif schluss == "d" or schluss == "t" -- reden/beten
      or schwierigeKonsonantenkombination(stamm, schluss) then -- atmen
        if beugungsklasse == "V" then
          endungen = verbendungen_d
        elseif beugungsklasse == "VG" then
          endungen = verbendungenPraesens_d
        end
      elseif diphthonge[schluss] or schluss == "ä" then -- bauen/säen
        if beugungsklasse == "V" then
          endungen = verbendungen
        elseif beugungsklasse == "VG" then
          endungen = verbendungenPraesens
        end
      -- Es muss ausgeschlossen werden, dass „ärgeren“, „klammeren“ etc. als Verben
      -- klassifiziert werden.
      -- Ungelöste Problemfälle sind „beschweren“, „queren“, „scheren“.
      elseif schlusskonsonanten[schluss] and (schluss ~= "r" or (string.len(stamm) > 1
      and (string.sub(stamm, -1) ~= "e" or string.sub(stamm, -2) == "ee"
      or string.sub(stamm, -2) == "ie"))) then
        if beugungsklasse == "V" then
          endungen = verbendungen
        elseif beugungsklasse == "VG" then
          endungen = verbendungenPraesens
        end
      else
        return sucheErfolglos(zaehlen)
      end
    -- 2: Verben auf "eln"
    elseif string.len(schluessel) > 4 and string.sub(schluessel, -3) == "eln" and beugungsklasse ~= "VV" then
      stamm = string.sub(schluessel, 1, -4)
      stamm, schluss = zerlegeWortstamm(stamm, true)
      if schlusskonsonanten[schluss] then
        if beugungsklasse == "V" then
          endungen = verbendungen_eln
        elseif beugungsklasse == "VG" then
          endungen = verbendungenPraesens_eln
        end
      else
        return sucheErfolglos(zaehlen)
      end
    -- 3: Verben auf "ern"
    elseif string.len(schluessel) > 4 and string.sub(schluessel, -3) == "ern" and beugungsklasse ~= "VV" then
      schluss = "r"
      stamm = string.sub(schluessel, 1, -3)
      if beugungsklasse == "V" then
        endungen = verbendungen
      elseif beugungsklasse == "VG" then
        endungen = verbendungenPraesens
      end
    -- 4: "tun"
    elseif string.len(schluessel) > 2 and string.sub(schluessel, -3) == "tun" and beugungsklasse == "VG" then
      schluss = "u"
      stamm = string.sub(schluessel, 1, -3)
      endungen = verbendungenPraesens
    -- 5: Vergangenheitsformen gemischter Verben: ich brachte, ich sandte
    elseif beugungsklasse == "VV" and string.sub(schluessel, -2) == "te" then
      stamm, schluss = zerlegeWortstamm(schluessel)
      local siii = string.sub(stamm, -5, -3)
      local sii = string.sub(stamm, -4, -3)
      -- ausgeschlossen werden müssen: ich fechte, ...
      if siii == "ech" then
        return sucheErfolglos(zaehlen)
      -- ... ich schreite, ich biete, ich halte, ich rate, ich bitte, ich berste
      elseif sii == "ei" or sii == "ie" or sii == "al" or sii == "ra" or sii == "it" or sii == "rs" then
        return sucheErfolglos(zaehlen)
      else
        endungen = verbendungenPraeteritumGemischt
      end
    -- 6: Vergangenheitsformen starker Verben: ich ging, ich schlief etc.
    elseif beugungsklasse == "VV" then
      stamm, schluss = zerlegeWortstamm(schluessel)
      if schluss == "s" or schluss == "ß" then
        endungen = verbendungenPraeteritum_s
      elseif schluss == "d" or schluss == "t" then
        endungen = verbendungenPraeteritum_d
      elseif schlusskonsonanten[schluss] or schluss == "ie" then
        endungen = verbendungenPraeteritum
      else
        return sucheErfolglos(zaehlen)
      end
    else
      return sucheErfolglos(zaehlen)
    end
  else
    return sucheErfolglos(zaehlen)
  end
  local formen = {}
  for _, endung in ipairs(endungen) do
    table.insert(formen, haengeEndungAn(stamm,schluss,endung))
  end
  if zaehlen then
    return zaehleFormen(schluessel, formen)
  else
    return sucheFormen(schluessel, formen)
  end
end


-- Ergänze die Wortliste um die Beugungsformen des im angegebenen
-- Wortlisteneintrag enthaltenen Adjektivs
function beugeAdjektiv(eintrag)
  local n, stamm, schluss, endungen
  if eintrag[1] ~= "-1-" then
    n = 1
  elseif eintrag[2] ~= "-2-" then
    n = 2
  else
    n = 3
  end
  _, schluss = zerlegeWortstamm(eintrag[n])
  if schlusskonsonanten[schluss] or diphthonge[schluss] then
    endungen = adjektivendungen
  elseif schluss == "e" then
    endungen = adjektivendungen_e
  else
    io.stderr:write("Ungültiges Adjektiv: "..eintrag[n].."\n")
    os.exit(1)
  end
  for _, endung in ipairs(endungen) do
    local neueintrag = {}
    for k = 1, 5 do
      if eintrag[k] then
        if eintrag[k] == "-"..k.."-" then
          neueintrag[k] = eintrag[k]
        else
          stamm, schluss = zerlegeWortstamm(eintrag[k])
          if k == 1 or k == 3 then
            neueintrag[k] = haengeEndungAn(stamm, schluss, endung)
          else
            neueintrag[k] = haengeEndungAn(stamm, schluss, endung, true)
          end
        end
      end
    end
    if neueintrag[1] ~= "-1-" then
      schluessel = neueintrag[1]
    elseif neueintrag[2] ~= "-2-" then
      schluessel = neueintrag[2]
    else
      schluessel = neueintrag[3]
    end
    ausgabeliste[schluessel] = neueintrag
  end
end

-- Ergänze die Wortliste um die Beugungsformen des im angegebenen Wortlisteneintrag
-- enthaltenen Substantivs ohne Umlautbildung im Plural
function beugeSubstantiv(eintrag, endungsklasse)
  local n, stamm, schluss, endungen
  if eintrag[1] ~= "-1-" then
    n = 1
  elseif eintrag[2] ~= "-2-" then
    n = 2
  else
    n = 3
  end
  _, schluss = zerlegeWortstamm(eintrag[n])
  local suffix_nis
  local doppelschluss = string.sub(eintrag[n], -2)
  if diphthonge[schluss] then
    if endungsklasse == "se" then -- der Schrei, des Schreis, die Schreie
      endungen = substantivendungen_se
    elseif endungsklasse == "sn" then -- der See, des Sees, die Seen
      endungen = {"s", "en"}
    elseif endungsklasse == "sr" then -- das Ei, des Ei(e)s, die Eier
      endungen = substantivendungen_sr
    elseif endungsklasse == "n" then -- die Frau, die Frauen
      endungen = {"en"}
    elseif endungsklasse == "s" then -- das Plateau, des Plateaus
      endungen = {"s"}
    else
      io.stderr:write('Substantiv "'..eintrag[n]..'" kann nicht gebeugt werden.\n')
      os.exit(1)
    end
  elseif schluss == "e" then
    if endungsklasse == "rn" then -- der Beamte, ein Beamter, des Beamten
      endungen = substantivendungen_rn
    elseif endungsklasse == "sn" then -- das Gebirge, des Gebirges, den Gebirgen
      endungen = {"s", "n"}
    elseif endungsklasse == "n" then -- die Karte, die Karten
      endungen = {"n"}
    elseif endungsklasse == "s" then -- der Käse, des Käses
      endungen = {"s"}
    else
      io.stderr:write('Substantiv "'..eintrag[n]..'" kann nicht gebeugt werden.\n')
      os.exit(1)
    end
  else
    if endungsklasse == "se" then
      if hatNurLanggenitiv(eintrag[n]) then
        endungen = substantivendungen_se_Langgenitiv
        if string.len(eintrag[n]) > 3 and string.sub(eintrag[n], -3) == "nis" then -- Zeugnis
          suffix_nis = true
        end
      elseif hatNurKurzgenitiv(eintrag[n]) then
        endungen = substantivendungen_se_Kurzgenitiv
      else
        endungen = substantivendungen_se
      end
    elseif endungsklasse == "sn" then
      if doppelschluss == "el" or doppelschluss == "er" -- Stiefel/Teller
      or doppelschluss == "ul" then -- Modul
        endungen = {"s", "n"}
      else -- Autor, Elektron, See, Typ, Zeh
        endungen = {"s", "en"}
      end
    elseif endungsklasse == "sr" then
      endungen = substantivendungen_sr
    elseif endungsklasse == "e" then -- Erlaubnis
      endungen = substantivendungenPlural_e
      if string.len(eintrag[n]) > 3 and string.sub(eintrag[n], -3) == "nis" then
        suffix_nis = true
      end
    elseif endungsklasse == "n" then
      if doppelschluss == "in" then -- Lehrerin
        endungen = {"nen"}
      elseif doppelschluss == "el" or doppelschluss == "er" then -- Angel/Faser
        endungen = {"n"}
      else
        endungen = {"en"}
      end
    elseif endungsklasse == "s" then
      endungen = {"s"}
    else
      io.stderr:write('Substantiv "'..eintrag[n]..'" kann nicht gebeugt werden.\n')
      os.exit(1)
    end
  end
  for _, endung in ipairs(endungen) do
    local neueintrag = {}
    for k = 1, 5 do
      if eintrag[k] then
        if eintrag[k] == "-"..k.."-" then
          neueintrag[k] = eintrag[k]
        else
          stamm, schluss = zerlegeWortstamm(eintrag[k])
          if schluss == "s" and suffix_nis then -- Zeugnis
            neueintrag[k] = haengeEndungAn(eintrag[k], schluss, endung) -- Zeugnisses etc.
          else
            if k == 1 or k == 3 then
              neueintrag[k] = haengeEndungAn(stamm, schluss, endung)
            elseif schluss == "ß" then -- Abszeß
              neueintrag[k] = haengeEndungAn(stamm.."s", "s", endung, true) -- Abszesses etc.
            else
              neueintrag[k] = haengeEndungAn(stamm, schluss, endung, true)
            end
          end
        end
      end
    end
    if neueintrag[1] ~= "-1-" then
      schluessel = neueintrag[1]
    elseif neueintrag[2] ~= "-2-" then
      schluessel = neueintrag[2]
    else
      schluessel = neueintrag[3]
    end
    ausgabeliste[schluessel] = neueintrag
  end
end

-- Ergänze die Wortliste um die Beugungsformen des im angegebenen Wortlisteneintrag
-- enthaltenen Substantivs mit Umlautbildung im Plural
function beugeSubstantivUmlaut(eintrag, endungsklasse)
  local n, stamm, schluss
  if eintrag[1] ~= "-1-" then
    n = 1
  elseif eintrag[2] ~= "-2-" then
    n = 2
  else
    n = 3
  end
  _, schluss = zerlegeWortstamm(eintrag[n])
  local singularendungen
  local pluralendungen
  if endungsklasse == "se" then
    if hatNurLanggenitiv(eintrag[n]) then
      singularendungen = substantivendungenSingular_se_Langgenitiv
    else
      singularendungen = substantivendungenSingular_se
    end
    pluralendungen = substantivendungenPlural_e
  elseif endungsklasse == "s" then
    singularendungen = {"s"}
    if string.len(eintrag[n]) > 2 and string.sub(eintrag[n], -2) == "en" then
      pluralendungen = {""} -- der Garten, die Gärten
    else
      pluralendungen = {"", "n"} -- der Bruder, die Brüder, den Brüdern
    end
  elseif endungsklasse == "sr" then
    if hatNurLanggenitiv(eintrag[n]) then
      singularendungen = substantivendungenSingular_sr_Langgenitiv
    else
      singularendungen = substantivendungenSingular_sr
    end
    pluralendungen = substantivendungenPlural_r
  elseif endungsklasse == "e" then
    singularendungen = {}
    pluralendungen = substantivendungenPlural_e
  elseif endungsklasse == "" then
    singularendungen = {}
    pluralendungen = {"", "n"}
  else
    io.stderr:write('Substantiv "'..eintrag[n]..'" kann nicht gebeugt werden.\n')
    os.exit(1)
  end
  for _, endung in ipairs(singularendungen) do
    local neueintrag = {}
    for k = 1, 5 do
      if eintrag[k] then
        if eintrag[k] == "-"..k.."-" then
          neueintrag[k] = eintrag[k]
        else
          stamm, schluss = zerlegeWortstamm(eintrag[k])
          neueintrag[k] = haengeEndungAn(stamm, schluss, endung)
        end
      end
    end
    if neueintrag[1] ~= "-1-" then
      schluessel = neueintrag[1]
    elseif neueintrag[2] ~= "-2-" then
      schluessel = neueintrag[2]
    else
      schluessel = neueintrag[3]
    end
    ausgabeliste[schluessel] = neueintrag
  end
  for _, endung in ipairs(pluralendungen) do
    local neueintrag = {}
    local umlautstamm = nil
    for k = 1, 5 do
      if eintrag[k] then
        if eintrag[k] == "-"..k.."-" then
          neueintrag[k] = eintrag[k]
        else
          stamm, schluss = zerlegeWortstamm(eintrag[k])
          umlautstamm = lauteUm(stamm)
          if umlautstamm then
            if k == 1 or k == 3 then
              neueintrag[k] = haengeEndungAn(umlautstamm, schluss, endung)
            elseif schluss == "ß" then -- Bußerlaß
              neueintrag[k] = haengeEndungAn(umlautstamm.."s", "s", endung, AR) -- Bußerlässe etc.
            else
              neueintrag[k] = haengeEndungAn(umlautstamm, schluss, endung, AR)
            end
          else
            io.stderr:write('Substantiv "'..eintrag[k]..'" kann nicht gebeugt werden.\n')
            os.exit(1)
          end
        end
      end
    end
    if neueintrag[1] ~= "-1-" then
      schluessel = neueintrag[1]
    elseif neueintrag[2] ~= "-2-" then
      schluessel = neueintrag[2]
    else
      schluessel = neueintrag[3]
    end
    ausgabeliste[schluessel] = neueintrag
  end
end

-- Ergänze die Wortliste um die Beugungsformen des im angegebenen
-- Wortlisteneintrag enthaltenen Verbs
function beugeVerb(eintrag, beugungsklasse)
  local n
  if eintrag[1] ~= "-1-" then
    n = 1
  elseif eintrag[2] ~= "-2-" then
    n = 2
  else
    n = 3
  end
  local stamm, schluss, endungen
  -- 1: Verben auf "en"
  if string.len(eintrag[n]) > 3 and string.sub(eintrag[n], -2) == "en" and beugungsklasse ~= "VV" then
    stamm = string.sub(eintrag[n], 1, -3)
    stamm, schluss = zerlegeWortstamm(stamm, true)
    if schluss == "s" or schluss == "ß" or schluss == "x" or schluss == "z" then -- reisen/reißen/feixen/heizen
      if beugungsklasse == "V" then
        endungen = verbendungen_s
      elseif beugungsklasse == "VG" then
        endungen = verbendungenPraesens_s
      end
    elseif schluss == "d" or schluss == "t" -- reden/beten
    or schwierigeKonsonantenkombination(stamm, schluss) then -- atmen
      if beugungsklasse == "V" then
        endungen = verbendungen_d
      elseif beugungsklasse == "VG" then
        endungen = verbendungenPraesens_d
      end
    elseif schlusskonsonanten[schluss] or diphthonge[schluss] or schluss == "ä" then -- säen
      if beugungsklasse == "V" then
        endungen = verbendungen
      elseif beugungsklasse == "VG" then
        endungen = verbendungenPraesens
      end
    else
      io.stderr:write("Ungültiges Verb: "..eintrag[n].."\n")
      os.exit(1)
    end
  -- 2: Verben auf "eln"
  elseif string.len(eintrag[n]) > 4 and string.sub(eintrag[n], -3) == "eln" and beugungsklasse ~= "VV" then
    stamm = string.sub(eintrag[n], 1, -4)
    stamm, schluss = zerlegeWortstamm(stamm, true)
    if schlusskonsonanten[schluss] then
      if beugungsklasse == "V" then
        endungen = verbendungen_eln
      elseif beugungsklasse == "VG" then
        endungen = verbendungenPraesens_eln
      end
    else
      io.stderr:write("Ungültiges Verb: "..eintrag[n].."\n")
      os.exit(1)
    end
  -- 3: Verben auf "ern"
  elseif string.len(eintrag[n]) > 4 and string.sub(eintrag[n], -3) == "ern" and beugungsklasse ~= "VV" then
    schluss = "r"
    stamm = string.sub(eintrag[n], 1, -3)
    if beugungsklasse == "V" then
      endungen = verbendungen
    elseif beugungsklasse == "VG" then
      endungen = verbendungenPraesens
    end
  -- 4: "tun"
  elseif string.len(eintrag[n]) > 2 and string.sub(eintrag[n], -3) == "tun" and beugungsklasse == "VG" then
    schluss = "u"
    stamm = string.sub(eintrag[n], 1, -3)
    endungen = verbendungenPraesens
  -- 5: Vergangenheitsformen gemischter Verben: ich brachte, ich sandte
  elseif beugungsklasse == "VV" and string.sub(eintrag[n], -2) == "te" then
    stamm, schluss = zerlegeWortstamm(eintrag[n])
    endungen = verbendungenPraeteritumGemischt
  -- 6: Vergangenheitsformen starker Verben: ich ging, ich schlief etc.
  elseif beugungsklasse == "VV" then
    stamm, schluss = zerlegeWortstamm(eintrag[n])
    if schluss == "s" or schluss == "ß" then
      endungen = verbendungenPraeteritum_s
    elseif schluss == "d" or schluss == "t" then
      endungen = verbendungenPraeteritum_d
    else
      endungen = verbendungenPraeteritum
    end
  else
    io.stderr:write("Ungültiges Verb: "..eintrag[n].."\n")
    os.exit(1)
  end
  for _, endung in ipairs(endungen) do
    local neueintrag = {}
    for k = 1, 5 do
      if eintrag[k] then
        if eintrag[k] == "-"..k.."-" then
          neueintrag[k] = eintrag[k]
        else
          neueintrag[k] = haengeEndungAn(stamm, schluss, endung)
        end
      end
    end
    local schluessel
    if neueintrag[1] ~= "-1-" then
      schluessel = neueintrag[1]
    elseif neueintrag[2] ~= "-2-" then
      schluessel = neueintrag[2]
    else
      schluessel = neueintrag[3]
    end
    ausgabeliste[schluessel] = neueintrag
  end
end


if beugen then
  -- Erzeugen der gebeugten Formen
  for schluessel, eintrag in pairs(eingabeliste) do
    ausgabeliste[schluessel] = eintrag
    if eintrag.beugungsklasse == "A" then
      beugeAdjektiv(eintrag)
    elseif eintrag.beugungsklasse == "Srn" then
      beugeSubstantiv(eintrag, "rn")
    elseif eintrag.beugungsklasse == "Sse" then
      beugeSubstantiv(eintrag, "se")
    elseif eintrag.beugungsklasse == "SseU" then
      beugeSubstantivUmlaut(eintrag, "se")
    elseif eintrag.beugungsklasse == "Ssn" then
      beugeSubstantiv(eintrag, "sn")
    elseif eintrag.beugungsklasse == "SsU" then
      beugeSubstantivUmlaut(eintrag, "s")
    elseif eintrag.beugungsklasse == "Ssr" then
      beugeSubstantiv(eintrag, "sr")
    elseif eintrag.beugungsklasse == "SsrU" then
      beugeSubstantivUmlaut(eintrag, "sr")
    elseif eintrag.beugungsklasse == "Se" then
      beugeSubstantiv(eintrag, "e")
    elseif eintrag.beugungsklasse == "SeU" then
      beugeSubstantivUmlaut(eintrag, "e")
    elseif eintrag.beugungsklasse == "SU" then
      beugeSubstantivUmlaut(eintrag, "")
    elseif eintrag.beugungsklasse == "Sn" then
      beugeSubstantiv(eintrag, "n")
    elseif eintrag.beugungsklasse == "Ss" then
      beugeSubstantiv(eintrag, "s")
    elseif eintrag.beugungsklasse == "V" then
      beugeVerb(eintrag, "V")
    elseif eintrag.beugungsklasse == "VG" then
      beugeVerb(eintrag, "VG")
    elseif eintrag.beugungsklasse == "VV" then
      beugeVerb(eintrag, "VV")
    elseif eintrag.beugungsklasse then
      io.stderr:write('Ungültige Beugungsklasse "'..eintrag.beugungsklasse..'".\n')
      os.exit(1)
    end
  end
else
  -- Ermitteln der Beugungsklasseninformationen für die einzelnen Einträge
  -- der Wortliste
  -- 1. Durchlauf: Adjektive und Substantive, die nicht auf "e" enden,
  -- sowie Verben mit regelmäßigen Gegenwarts- und Vergangenheitsformen
  for schluessel, eintrag in pairs(eingabeliste) do
    if sucheVerbformen(schluessel, "V") then
      eintrag.beugungsklasse = "V"
    elseif sucheAdjektivformen(schluessel) then
      eintrag.beugungsklasse = "A"
    elseif sucheSubstantivformen(schluessel, "se") then
      eintrag.beugungsklasse = "Sse"
    elseif sucheSubstantivformenUmlaut(schluessel, "se") then
      eintrag.beugungsklasse = "SseU"
    elseif sucheSubstantivformen(schluessel, "sr") then
      eintrag.beugungsklasse = "Ssr"
    elseif sucheSubstantivformenUmlaut(schluessel, "sr") then
      eintrag.beugungsklasse = "SsrU"
    elseif sucheSubstantivformenUmlaut(schluessel, "e") then
      eintrag.beugungsklasse = "SeU"
    elseif sucheSubstantivformen(schluessel, "sn") then
      eintrag.beugungsklasse = "Ssn"
    elseif string.len(schluessel) < 2 or string.sub(schluessel, -2) ~= "er"
    and sucheSubstantivformen(schluessel, "n") then
      eintrag.beugungsklasse = "Sn"
    elseif sucheSubstantivformenUmlaut(schluessel, "s") then
      eintrag.beugungsklasse = "SsU"
    elseif string.len(schluessel) > 1 and string.sub(schluessel, -1) ~= "s"
    and sucheSubstantivformen(schluessel, "s") then
      eintrag.beugungsklasse = "Ss"
    elseif sucheSubstantivformen(schluessel, "e") then
      eintrag.beugungsklasse = "Se"
    elseif sucheSubstantivformenUmlaut(schluessel, "") then
      eintrag.beugungsklasse = "SU"
    end
    ausgabeliste[schluessel] = eintrag
  end
  -- 2. Durchlauf: Adjektive und Substantive, die auf "e" enden sowie
  -- unregelmäßige Vergangenheitsformen von Verben
  -- Ignoriert werden solche Einträge, die bereits im 1. Durchlauf als gebeugte
  -- Form markiert wurden oder bereits eine Beugungsklasse haben.
  for schluessel, eintrag in pairs(eingabeliste) do
    if not eintrag.gebeugteForm and not eintrag.beugungsklasse then
      if sucheAdjektivformen_e(schluessel) then
        eintrag.beugungsklasse = "A"
      elseif sucheSubstantivformen_e(schluessel, "sn") then
        eintrag.beugungsklasse = "Ssn"
      elseif sucheSubstantivformen_e(schluessel, "rn") then
        eintrag.beugungsklasse = "Srn"
      elseif sucheSubstantivformen_e(schluessel, "n") then
        eintrag.beugungsklasse = "Sn"
      elseif sucheSubstantivformen_e(schluessel, "s") then
        eintrag.beugungsklasse = "Ss"
      elseif sucheVerbformen(schluessel, "VV") then
        eintrag.beugungsklasse = "VV"
      end
    end
  end
  -- 3. Durchlauf: Substantive, die auf "er" enden (Beugungsklasse "Sn")
  -- sowie Verben in der Gegenwart
  -- Ignoriert werden solche Einträge, die bereits vorher als gebeugte
  -- Form markiert wurden oder bereits eine Beugungsklasse haben.
  for schluessel, eintrag in pairs(eingabeliste) do
    if not eintrag.gebeugteForm and not eintrag.beugungsklasse then
      if string.len(schluessel) > 2 and string.sub(schluessel, -2) == "er"
      and sucheSubstantivformen(schluessel, "n") then
        eintrag.beugungsklasse = "Sn"
      elseif sucheVerbformen(schluessel, "VG") then
        eintrag.beugungsklasse = "VG"
      end
    end
  end
end

if adjektivvorschlaege then
  -- Ermitteln der Beugungsklassenvorschläge für die einzelnen Einträge
  -- der Wortliste
  -- 1. Durchlauf: Adjektive, die nicht auf "e" enden
  for schluessel, eintrag in pairs(eingabeliste) do
    if not eintrag.beugungsklasse and sucheAdjektivformen(schluessel, true) == formanzahl then
      eintrag.beugungsklasse = "A"
      eintrag.vorschlag = true
    end
  end
  -- 2. Durchlauf: Adjektive, die auf "e" enden
  -- Ignoriert werden solche Einträge, die bereits im 1. Durchlauf als gebeugte
  -- Form markiert wurden.
  for schluessel, eintrag in pairs(eingabeliste) do
    if not eintrag.beugungsklasse and not eintrag.gebeugteForm
    and sucheAdjektivformen_e(schluessel, true) == formanzahl then
      eintrag.beugungsklasse = "A"
      eintrag.vorschlag = true
    end
  end
end

if substantivvorschlaege then
  -- Ermitteln der Beugungsklassenvorschläge für die einzelnen Einträge
  -- der Wortliste
  -- 1. Durchlauf: Substantive, die nicht auf "e" enden
  for schluessel, eintrag in pairs(eingabeliste) do
    if not eintrag.beugungsklasse and not istKleingeschrieben(schluessel) then
      if sucheSubstantivformen(schluessel, "se", true) == formanzahl then
        eintrag.beugungsklasse = "Sse"
        eintrag.vorschlag = true
      elseif sucheSubstantivformenUmlaut(schluessel, "se", true) == formanzahl then
        eintrag.beugungsklasse = "SseU"
        eintrag.vorschlag = true
      elseif sucheSubstantivformen(schluessel, "sr", true) == formanzahl then
        eintrag.beugungsklasse = "Ssr"
        eintrag.vorschlag = true
      elseif sucheSubstantivformenUmlaut(schluessel, "sr", true) == formanzahl then
        eintrag.beugungsklasse = "SsrU"
        eintrag.vorschlag = true
      elseif sucheSubstantivformenUmlaut(schluessel, "e", true) == formanzahl then
        eintrag.beugungsklasse = "SeU"
        eintrag.vorschlag = true
      elseif sucheSubstantivformen(schluessel, "sn", true) == formanzahl then
        eintrag.beugungsklasse = "Ssn"
        eintrag.vorschlag = true
      elseif sucheSubstantivformenUmlaut(schluessel, "s", true) == formanzahl then
        eintrag.beugungsklasse = "SsU"
        eintrag.vorschlag = true
      elseif sucheSubstantivformenUmlaut(schluessel, "", true) == formanzahl then
        eintrag.beugungsklasse = "SU"
        eintrag.vorschlag = true
      end
    end
  end
  -- 2. Durchlauf: Substantive, die auf "e" enden
  -- Ignoriert werden solche Einträge, die bereits im 1. Durchlauf als gebeugte
  -- Form markiert wurden.
  for schluessel, eintrag in pairs(eingabeliste) do
    if not eintrag.beugungsklasse and not eintrag.gebeugteForm then
      if sucheSubstantivformen_e(schluessel, "sn", true) == formanzahl then
        eintrag.beugungsklasse = "Ssn"
        eintrag.vorschlag = true
      elseif sucheSubstantivformen_e(schluessel, "rn", true) == formanzahl then
        eintrag.beugungsklasse = "Srn"
        eintrag.vorschlag = true
      end
    end
  end
end

if verbvorschlaege then
  -- Ermitteln der Beugungsklassenvorschläge für die einzelnen Einträge
  -- der Wortliste
  -- 1. Durchlauf: Verben mit regelmäßigen Gegenwarts- und Vergangenheitsformen
  for schluessel, eintrag in pairs(eingabeliste) do
    if not eintrag.beugungsklasse and sucheVerbformen(schluessel, "V", true) == formanzahl then
      eintrag.beugungsklasse = "V"
      eintrag.vorschlag = true
    end
  end
  -- 2. Durchlauf: unregelmäßige Vergangenheitsformen
  -- Ignoriert werden solche Einträge, die bereits im 1. Durchlauf als gebeugte
  -- Form markiert wurden.
  for schluessel, eintrag in pairs(eingabeliste) do
    if not eintrag.beugungsklasse and not eintrag.gebeugteForm
    and sucheVerbformen(schluessel, "VV", true) == formanzahl then
      eintrag.beugungsklasse = "VV"
      eintrag.vorschlag = true
    end
  end
end

-- Ausgeben der Wortliste
for _, eintrag in pairs(ausgabeliste) do
  if ((vorschlaege and eintrag.vorschlag)
  or (not vorschlaege and (beugen or eintrag.beugungsklasse or not eintrag.gebeugteForm))) then
    for k = 1, 5 do
      if eintrag[k] then
        if k > 1 then
          io.write(";");
        end
        io.write(eintrag[k]);
      end
    end
    if (not beugen and eintrag.beugungsklasse) or eintrag.kommentar then
      io.write(" #")
      if not beugen and eintrag.beugungsklasse then
        io.write(" "..eintrag.beugungsklasse)
      end
      if eintrag.kommentar then
        if not beugen
        and (string.len(eintrag.kommentar) < 3 or string.sub(eintrag.kommentar, 1, 3) ~= "-- ") then
          io.write(" --")
        end
        io.write(" "..eintrag.kommentar)
      end
    end
    io.write("\n");
  end
end
