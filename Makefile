# -*- coding: utf-8 -*-
#
# (See below for a German introduction.)
#
# This Makefile creates German hyphenation patterns in subdirectories
# `$(TRAD)` and `$(REFO)` for traditional and new orthography, respectively.
# Hyphenation patterns for traditional Swiss German are generated in
# directory `$(SWISS)`.
#
# The input data is expected to be in `$(SRCDIR)`, which by default is set to
# the directory containing the Makefile.  Output goes to directory
# `$(OUTDIR)`, which by default is set to './muster'.
#
# The possible targets are `pattern-trad`, `pattern-refo`, and
# `pattern-swiss`.  If no target (or target `all`) is given, all patterns for
# all three targets are built.
#
# Example call:
#
# ```
# mkdir build-patterns
# cd build-patterns
# make --makefile=~/git/wortliste/Makefile OUTDIR=. pattern-trad
# ```
#
# If you add one of the (phony) targets `major`, `fugen`, `suffix`, `primary`,
# or `secondary`, patterns that only use major hyphenation points
# ('Haupttrennstellen') are created.  If you add the (phony) target `gesang`,
# patterns usable for German lyrics are created.  Example:
#
# ```
# make major pattern-refo
# ```
#
# The output directories have `-major` (etc.) appended to their names.  Note
# that the `major`, `fugen`, and `suffix` targets reflect the markup in the
# `wortliste` file; they are not intended to produce patterns for 'good'
# hyphenation in text but rather to test the consistency of the markup, and
# to assist in adding new words.
#
# To control the used weights in the major hyphenation patterns, add variable
# `W=N`, where `N` gives the quality: value 1 specifies the best hyphenation
# points only, value 2 both the best and second-best points, etc.  The
# default is value 0, using all major hyphenation points.
# Primary and secondary hyphenation patterns are major hyphenation patterns
# with `W=1` and `W=2`, respectively.
#
# The targets `schluss-s`, `schluss-s-1901`, and `schluss-s-1996` create
# (experimental) patterns and word lists for converting words in traditional
# and new orthography that make a distinction between long and round S as
# needed for typesetting with Fraktur fonts.  An example for applying these
# patterns is the script `skripte/lib/py_patuse/long_s_conversion.py`.
#
# The targets `de_ligaturaufbruch`, `de-1901_ligaturaufbruch`, and
# `de-1996_ligaturaufbruch` create (experimental) patterns and word lists for
# 'breaking up' ligatures: The "hyphenation" points indicate positions where
# ligatures like 'ﬂ' must not occur.


# Dieses Makefile erzeugt deutsche Trennmuster in den Unterverzeichnissen
# `$(TRAD)` und `$(REFO)` für die traditionelle bzw. reformierte
# Rechtschreibung.  Trennmuster für tradionelles deutschschweizerisches
# Deutsch werden Verzeichnis `$(SWISS)` erzeugt.
#
# Die Eingabedaten werden im Verzeichnis `$(SRCDIR)` erwartet, welches
# standardmäßig identisch zum dem Verzeichnis ist, welches die
# `Makefile`-Datei enthält.  Die Ausgabe wird in Verzeichnis `$(OUTDIR)`
# erzeugt, welches standardmäßig den Namen './muster' hat.
#
# Die möglichen Make-Ziele sind `pattern-trad`, `pattern-refo` und
# `pattern-swiss`.  Wenn kein Ziel angegeben ist (oder man das Ziel `all`
# verwendet), werden alle drei Trennmuster erzeugt.
#
# Beispiel:
#
# ```
# mkdir build-patterns
# cd build-patterns
# make --makefile=~/git/wortliste/Makefile OUTDIR=. pattern-trad
# ```
#
# Wird eines der zusätzlichen (künstlichen) Ziele `major`, `fugen`, `suffix`,
# `primary` oder `secondary` angegeben, werden Haupttrennstellmuster erzeugt.
# Wird das (künstliche) Ziel `gesang` angegeben, werden Muster mit
# Gesangstrennstellen erzeugt.
#
# Beispiel:
#
# ```
# make major pattern-refo
# ```
#
# Die verwendeten Verzeichnisnamen sind die gleichen wie oben, allerdings mit
# einem angehängten `-major`, `-fugen`, `-suffix`, `-primary`, `-secondar` bzw.
# `-gesang`.
#
# Die Haupttrennstellmuster spiegeln die Auszeichnung in der Liste direkt
# wider.  Sie haben nicht das Ziel, "gute" Trennungen in Texten zu erzeugen,
# sondern sind zum Testen der Konsistenz der Auszeichnung sowie zum
# "kategorisierten" Markieren der Trennstellen neuer Wörter gedacht.
#
# Bei `major` kann die Menge der verwendeten Haupttrennstellen mittels der
# Variable `W=N` (Wichtungs-Schwellwert) kontrolliert werden, wo `N` die
# Qualität angibt: Wert 1 selektiert nur die besten Haupttrennstellen, Wert 2
# die besten und zweitbesten Haupttrennstellen usw.  Der Standardwert für `W`
# ist 0; er gibt an, dass alle Haupttrennstellen verwendet werden sollen.
# `primary` entspricht Haupttrennstellen mit `W=1`, `secondary` solchen mit
# `W=2`.
#
# Die Ziele `schluss-s`, `schluss-s-1901` und `schluss-s-1996` erzeugen
# (experimentelle) Wortlisten und Muster für die Wandlung von
# Wörtern in die Orthographievariante mit Unterscheidung von langem und
# runden S (Binnen-S vs. Schluß-S), wie sie im Satz mit gebrochenen Schriften
# benötigt wird (de-Latf).  Ein Beispiel für die Anwendung dieser Muster
# ist das Skript `skripte/lib/py_patuse/long_s_conversion.py`.
#
# Die Ziele `de_ligaturaufbruch`, `de-1901_ligaturaufbruch`, and
# `de-1996_ligaturaufbruch` erzeugen (experimentelle) Wortlisten und
# Trennmuster, um Ligaturen 'aufzubrechen': Die Trennstellen zeigen an,
# wo Ligaturen wie 'ﬂ' nicht auftreten dürfen.


#
# setup
#

W = 0

SRCDIR = $(dir $(realpath $(lastword $(MAKEFILE_LIST))))
DATADIR = $(SRCDIR)/daten
SCRIPTDIR = $(SRCDIR)/skripte
WORDLIST = $(SRCDIR)/wortliste

OUTDIR = ./muster

TRAD_TITLE_EN = German Hyphenation Patterns (Traditional Orthography)
REFO_TITLE_EN = German Hyphenation Patterns (Reformed Orthography, 2006)
SWISS_TITLE_EN = Swiss-German Hyphenation Patterns (Traditional Orthography)
TRAD_AND_REFO_TITLE_EN = German Hyphenation Patterns
TRAD_TITLE_DE = TeX-Trennmuster für die traditionelle deutsche Rechtschreibung
REFO_TITLE_DE = TeX-Trennmuster für die reformierte (2006) deutsche Rechtschreibung
SWISS_TITLE_DE = TeX-Trennmuster für die traditionelle deutsch-schweizerische Rechtschreibung
TRAD_AND_REFO_TITLE_DE = TeX-Trennmuster für die deutsche Sprache
TRAD_LANG_NAME = German, traditional spelling
REFO_LANG_NAME = German, reformed spelling
SWISS_LANG_NAME = German, traditional Swiss spelling
TRAD_AND_REFO_LANG_NAME = German
TRAD_LANG_TAG = de-1901
REFO_LANG_TAG = de-1996
SWISS_LANG_TAG = de-CH-1901
TRAD_AND_REFO_LANG_TAG = de
TEX_WRAPPER_SUFFIX = normal

.PHONY: major fugen suffix gesang


ifneq ($(findstring major,$(MAKECMDGOALS)),)
  PATTYPE = -major
  # A single `-` gets removed; all other combinations of `-`, `<`, `>`, and
  # `=` are converted to a hyphen.
  SED_PATTYPE = $(SED) -e '/[=<>-]/!n' \
                       -e 's/---*/=/g' \
                       -e 's/-//g' \
                       -e 's/[=<>][=<>]*/-/g'
  PERL_PATTYPE = -g $(W) -1 -U
  TRAD_TITLE_EN := $(subst German,German Major,$(TRAD_TITLE_EN))
  REFO_TITLE_EN := $(subst German,German Major,$(REFO_TITLE_EN))
  SWISS_TITLE_EN := $(subst German,German Major,$(SWISS_TITLE_EN))
  TRAD_TITLE_DE := $(subst Trennmuster,Trennmuster (nur Haupttrennstellen),$(TRAD_TITLE_DE))
  REFO_TITLE_DE := $(subst Trennmuster,Trennmuster (nur Haupttrennstellen),$(REFO_TITLE_DE))
  SWISS_TITLE_DE := $(subst Trennmuster,Trennmuster (nur Haupttrennstellen),$(SWISS_TITLE_DE))
  TRAD_LANG_NAME := $(TRAD_LANG_NAME), major
  REFO_LANG_NAME := $(REFO_LANG_NAME), major
  SWISS_LANG_NAME := $(SWISS_LANG_NAME), major
  TRAD_LANG_TAG := $(TRAD_LANG_TAG)-x-major
  REFO_LANG_TAG := $(REFO_LANG_TAG)-x-major
  SWISS_LANG_TAG := $(SWISS_LANG_TAG)-x-major
  TEX_WRAPPER_SUFFIX := spezial

  ifeq ($(words $(MAKECMDGOALS)),1)
    major: all
  else
    # This is to suppress the 'nothing to be done' warning.
    major:
	    @:
  endif
else ifneq ($(findstring fugen,$(MAKECMDGOALS)),)
  PATTYPE = -fugen
  # All combinations of `-`, `<`, `>`, `<=`, `=>` get removed, runs of `=`
  # are converted to a hyphen.
  SED_PATTYPE = $(SED) -e '/[=<>-]/!n' \
                       -e 's/--*//g' \
                       -e 's/<=*//g' \
                       -e 's/=*>//g' \
                       -e 's/[<>][<>]*//g' \
                       -e 's/[=][=]*/-/g'
  PERL_PATTYPE = -g $(W) -1 -U
  TRAD_TITLE_EN := $(subst German,German Compound,$(TRAD_TITLE_EN))
  REFO_TITLE_EN := $(subst German,German Compound,$(REFO_TITLE_EN))
  SWISS_TITLE_EN := $(subst German,German Compound,$(SWISS_TITLE_EN))
  TRAD_TITLE_DE := $(subst Trennmuster,Trennmuster (nur Wortfugen),$(TRAD_TITLE_DE))
  REFO_TITLE_DE := $(subst Trennmuster,Trennmuster (nur Wortfugen),$(REFO_TITLE_DE))
  SWISS_TITLE_DE := $(subst Trennmuster,Trennmuster (nur Wortfugen),$(SWISS_TITLE_DE))
  TRAD_LANG_NAME := $(TRAD_LANG_NAME), compound
  REFO_LANG_NAME := $(REFO_LANG_NAME), compound
  SWISS_LANG_NAME := $(SWISS_LANG_NAME), compound
  TRAD_LANG_TAG := $(TRAD_LANG_TAG)-x-compound
  REFO_LANG_TAG := $(REFO_LANG_TAG)-x-compound
  SWISS_LANG_TAG := $(SWISS_LANG_TAG)-x-compound
  TEX_WRAPPER_SUFFIX := spezial

  ifeq ($(words $(MAKECMDGOALS)),1)
    fugen: all
  else
    # This is to suppress the 'nothing to be done' warning.
    fugen:
	    @:
  endif
else ifneq ($(findstring suffix,$(MAKECMDGOALS)),)
  PATTYPE = -suffix
  # All combinations of `-`, `<`, `=` get removed, runs of `>` are converted
  # to a hyphen.
  SED_PATTYPE = $(SED) -e '/[=<>-]/!n' \
                       -e 's/-//g' \
                       -e 's/[<=][<=]*//g' \
                       -e 's/[>][>]*/-/g'
  PERL_PATTYPE = -g $(W) -1 -U
  TRAD_TITLE_EN := $(subst German,German Suffix,$(TRAD_TITLE_EN))
  REFO_TITLE_EN := $(subst German,German Suffix,$(REFO_TITLE_EN))
  SWISS_TITLE_EN := $(subst German,German Suffix,$(SWISS_TITLE_EN))
  TRAD_TITLE_DE := $(subst Trennmuster,Trennmuster (nur Suffixtrennstellen),$(TRAD_TITLE_DE))
  REFO_TITLE_DE := $(subst Trennmuster,Trennmuster (nur Suffixtrennstellen),$(REFO_TITLE_DE))
  SWISS_TITLE_DE := $(subst Trennmuster,Trennmuster (nur Suffixtrennstellen),$(SWISS_TITLE_DE))
  TRAD_LANG_NAME := $(TRAD_LANG_NAME), suffix
  REFO_LANG_NAME := $(REFO_LANG_NAME), suffix
  SWISS_LANG_NAME := $(SWISS_LANG_NAME), suffix
  TRAD_LANG_TAG := $(TRAD_LANG_TAG)-x-suffix
  REFO_LANG_TAG := $(REFO_LANG_TAG)-x-suffix
  SWISS_LANG_TAG := $(SWISS_LANG_TAG)-x-suffix
  TEX_WRAPPER_SUFFIX := spezial

  ifeq ($(words $(MAKECMDGOALS)),1)
    suffix: all
  else
    # This is to suppress the 'nothing to be done' warning.
    suffix:
	    @:
  endif
else ifneq ($(findstring primary,$(MAKECMDGOALS)),)
  PATTYPE = -primary
  # A single `-` gets removed; all other combinations of `-`, `<`, `>`, and
  # `=` are converted to a hyphen.
  SED_PATTYPE = $(SED) -e '/[=<>-]/!n' \
                       -e 's/---*/=/g' \
                       -e 's/-//g' \
                       -e 's/[=<>][=<>]*/-/g'
  PERL_PATTYPE = -g 1 -1 -U
  TRAD_TITLE_EN := $(subst German,German Primary,$(TRAD_TITLE_EN))
  REFO_TITLE_EN := $(subst German,German Primary,$(REFO_TITLE_EN))
  SWISS_TITLE_EN := $(subst German,German Primary,$(SWISS_TITLE_EN))
  TRAD_TITLE_DE := $(subst Trennmuster,Trennmuster (nur Primärtrennstellen),$(TRAD_TITLE_DE))
  REFO_TITLE_DE := $(subst Trennmuster,Trennmuster (nur Primärtrennstellen),$(REFO_TITLE_DE))
  SWISS_TITLE_DE := $(subst Trennmuster,Trennmuster (nur Primärtrennstellen),$(SWISS_TITLE_DE))
  TRAD_LANG_NAME := $(TRAD_LANG_NAME), primary
  REFO_LANG_NAME := $(REFO_LANG_NAME), primary
  SWISS_LANG_NAME := $(SWISS_LANG_NAME), primary
  TRAD_LANG_TAG := $(TRAD_LANG_TAG)-x-primary
  REFO_LANG_TAG := $(REFO_LANG_TAG)-x-primary
  SWISS_LANG_TAG := $(SWISS_LANG_TAG)-x-primary
  TEX_WRAPPER_SUFFIX := spezial
  PATGEN_PARAMS = -p

  ifeq ($(words $(MAKECMDGOALS)),1)
    primary: all
  else
    # This is to suppress the 'nothing to be done' warning.
    primary:
	    @:
  endif
else ifneq ($(findstring secondary,$(MAKECMDGOALS)),)
  PATTYPE = -secondar
  # A single `-` gets removed; all other combinations of `-`, `<`, `>`, and
  # `=` are converted to a hyphen.
  SED_PATTYPE = $(SED) -e '/[=<>-]/!n' \
                       -e 's/---*/=/g' \
                       -e 's/-//g' \
                       -e 's/[=<>][=<>]*/-/g'
  PERL_PATTYPE = -g 2 -1 -U
  TRAD_TITLE_EN := $(subst German,German Secondary,$(TRAD_TITLE_EN))
  REFO_TITLE_EN := $(subst German,German Secondary,$(REFO_TITLE_EN))
  SWISS_TITLE_EN := $(subst German,German Secondary,$(SWISS_TITLE_EN))
  TRAD_TITLE_DE := $(subst Trennmuster,Trennmuster (nur Sekundärtrennstellen),$(TRAD_TITLE_DE))
  REFO_TITLE_DE := $(subst Trennmuster,Trennmuster (nur Sekundärtrennstellen),$(REFO_TITLE_DE))
  SWISS_TITLE_DE := $(subst Trennmuster,Trennmuster (nur Sekundärtrennstellen),$(SWISS_TITLE_DE))
  TRAD_LANG_NAME := $(TRAD_LANG_NAME), secondary
  REFO_LANG_NAME := $(REFO_LANG_NAME), secondary
  SWISS_LANG_NAME := $(SWISS_LANG_NAME), secondary
  TRAD_LANG_TAG := $(TRAD_LANG_TAG)-x-secondar
  REFO_LANG_TAG := $(REFO_LANG_TAG)-x-secondar
  SWISS_LANG_TAG := $(SWISS_LANG_TAG)-x-secondar
  TEX_WRAPPER_SUFFIX := spezial
  PATGEN_PARAMS = -p

  ifeq ($(words $(MAKECMDGOALS)),1)
    secondary: all
  else
    # This is to suppress the 'nothing to be done' warning.
    secondary:
	    @:
  endif
else ifneq ($(findstring gesang,$(MAKECMDGOALS)),)
  PATTYPE = -gesang
  SED_PATTYPE = cat
  PERL_PATTYPE = -G -S
  TRAD_TITLE_EN := $(subst German,German Lyrics,$(TRAD_TITLE_EN))
  REFO_TITLE_EN := $(subst German,German Lyrics,$(REFO_TITLE_EN))
  SWISS_TITLE_EN := $(subst German,German Lyrics,$(SWISS_TITLE_EN))
  TRAD_TITLE_DE := $(subst Trennmuster,Trennmuster (Gesangstexte),$(TRAD_TITLE_DE))
  REFO_TITLE_DE := $(subst Trennmuster,Trennmuster (Gesangstexte),$(REFO_TITLE_DE))
  SWISS_TITLE_DE := $(subst Trennmuster,Trennmuster (Gesangstexte),$(SWISS_TITLE_DE))
  TRAD_LANG_NAME := $(TRAD_LANG_NAME), lyrics
  REFO_LANG_NAME := $(REFO_LANG_NAME), lyrics
  SWISS_LANG_NAME := $(SWISS_LANG_NAME), lyrics
  TRAD_LANG_TAG := $(TRAD_LANG_TAG)-x-lyrics
  REFO_LANG_TAG := $(REFO_LANG_TAG)-x-lyrics
  SWISS_LANG_TAG := $(SWISS_LANG_TAG)-x-lyrics
  TEX_WRAPPER_SUFFIX := spezial

  ifeq ($(words $(MAKECMDGOALS)),1)
    gesang: all
  else
    # This is to suppress the 'nothing to be done' warning.
    gesang:
	    @:
  endif
else ifneq ($(findstring ligaturaufbruch,$(MAKECMDGOALS)),)
  PATTYPE = -ligaturaufbruch
  TRAD_TITLE_EN := $(subst Hyphenation,Ligature Suppression,$(TRAD_TITLE_EN))
  REFO_TITLE_EN := $(subst Hyphenation,Ligature Suppression,$(REFO_TITLE_EN))
  TRAD_AND_REFO_TITLE_EN := $(subst Hyphenation,Ligature Suppression,$(TRAD_AND_REFO_TITLE_EN))
  TRAD_TITLE_DE := $(subst Trennmuster,Pseudotrennmuster (Ligaturaufbruch),$(TRAD_TITLE_DE))
  REFO_TITLE_DE := $(subst Trennmuster,Pseudotrennmuster (Ligaturaufbruch),$(REFO_TITLE_DE))
  TRAD_AND_REFO_TITLE_DE := $(subst Trennmuster,Pseudotrennmuster (Ligaturaufbruch),$(TRAD_AND_REFO_TITLE_DE))
  TRAD_LANG_NAME := $(TRAD_LANG_NAME), ligature suppression
  REFO_LANG_NAME := $(REFO_LANG_NAME), ligature suppression
  TRAD_AND_REFO_LANG_NAME := $(TRAD_AND_REFO_LANG_NAME), ligature suppression
  TRAD_LANG_TAG := $(TRAD_LANG_TAG)-x-liga
  REFO_LANG_TAG := $(REFO_LANG_TAG)-x-liga
  TRAD_AND_REFO_LANG_TAG := $(TRAD_AND_REFO_LANG_TAG)-x-liga
else ifneq ($(findstring schluss-s,$(MAKECMDGOALS)),)
  PATTYPE = -schluss-s
  TRAD_TITLE_EN := $(subst Hyphenation,Round s,$(TRAD_TITLE_EN))
  REFO_TITLE_EN := $(subst Hyphenation,Round s,$(REFO_TITLE_EN))
  TRAD_AND_REFO_TITLE_EN := $(subst Hyphenation,Round s,$(TRAD_AND_REFO_TITLE_EN))
  TRAD_TITLE_DE := $(subst Trennmuster,Pseudotrennmuster (Rund-s),$(TRAD_TITLE_DE))
  REFO_TITLE_DE := $(subst Trennmuster,Pseudotrennmuster (Rund-s),$(REFO_TITLE_DE))
  TRAD_AND_REFO_TITLE_DE := $(subst Trennmuster,Pseudotrennmuster (Rund-s),$(TRAD_AND_REFO_TITLE_DE))
  TRAD_LANG_NAME := $(TRAD_LANG_NAME), round s
  REFO_LANG_NAME := $(REFO_LANG_NAME), round s
  TRAD_AND_REFO_LANG_NAME := $(TRAD_AND_REFO_LANG_NAME), round s
  TRAD_LANG_TAG := $(TRAD_LANG_TAG)-x-round-s
  REFO_LANG_TAG := $(REFO_LANG_TAG)-x-round-s
  TRAD_AND_REFO_LANG_TAG := $(TRAD_AND_REFO_LANG_TAG)-x-round-s
else
  PATTYPE =
  SED_PATTYPE = cat
  PERL_PATTYPE = -1 -U
endif

TRAD = dehypht-x$(PATTYPE)
REFO = dehyphn-x$(PATTYPE)
SWISS = dehyphts-x$(PATTYPE)
# The following variable is needed for special patterns such as ligature suppression.
TRAD_AND_REFO = dehyph-x$(PATTYPE)

LC_ENVVARS = LC_COLLATE=de_DE.UTF-8 \
             LC_CTYPE=de_DE.UTF-8

CAT = cat
CHDIR = cd
COPY = cp
DATE = $(shell date '+%Y-%m-%d')
ECHO = echo
GIT = git
MKDIR = mkdir -p
PERL = perl
PWD = pwd
EXTRACT_TEX = $(SCRIPTDIR)/wortliste/extract-tex.pl
SPRACHAUSZUG = $(SCRIPTDIR)/wortliste/sprachauszug.py
S2LONG_S = $(SCRIPTDIR)/spezialmuster/lang_s/s2long-s.py
SED = sed
SH = bash
SORT = $(LC_ENVVARS) sort -d $(bsnl)\
       | $(LC_ENVVARS) uniq -i
TR = tr

ifneq ($(findstring gesang,$(MAKECMDGOALS)),)
  GERMAN_TR = $(DATADIR)/german-gesang.tr
  LEFTHYPHENMIN = 1
  RIGHTHYPHENMIN = 1
else ifneq ($(findstring schluss-s,$(MAKECMDGOALS)),)
  GERMAN_TR = $(DATADIR)/german-gesang.tr
  LEFTHYPHENMIN = 1
  RIGHTHYPHENMIN = 1
else
  GERMAN_TR = $(DATADIR)/german.tr
  LEFTHYPHENMIN = \
    $(strip $(shell $(SED) 's/^\(..\).*/\1/;q' < $(GERMAN_TR)))
  RIGHTHYPHENMIN = \
    $(strip $(shell $(SED) 's/^..\(..\).*/\1/;q' < $(GERMAN_TR)))
endif

GIT_VERSION := `$(CHDIR) $(SRCDIR) \
                && $(GIT) log --format=%H -1 HEAD --`

TRADDIR = $(OUTDIR)/$(TRAD)
REFODIR = $(OUTDIR)/$(REFO)
SWISSDIR = $(OUTDIR)/$(SWISS)
TRAD_AND_REFO_DIR = $(OUTDIR)/$(TRAD_AND_REFO)

ifeq ("$(PATTYPE)", "-ligaturaufbruch")
  TRADFILES = $(TRADDIR)/$(TRAD)-$(DATE).pat
  REFOFILES = $(REFODIR)/$(REFO)-$(DATE).pat
  TRAD_AND_REFO_FILES = $(TRAD_AND_REFO_DIR)/$(TRAD_AND_REFO)-$(DATE).pat
else ifeq ("$(PATTYPE)", "-schluss-s")
  TRADFILES = $(TRADDIR)/$(TRAD)-$(DATE).pat
  REFOFILES = $(REFODIR)/$(REFO)-$(DATE).pat
  TRAD_AND_REFO_FILES = $(TRAD_AND_REFO_DIR)/$(TRAD_AND_REFO)-$(DATE).pat
else
  TRADFILES = $(TRADDIR)/$(TRAD)-$(DATE).pat $(TRADDIR)/$(TRAD)-$(DATE).tex
  REFOFILES = $(REFODIR)/$(REFO)-$(DATE).pat $(REFODIR)/$(REFO)-$(DATE).tex
  SWISSFILES = $(SWISSDIR)/$(SWISS)-$(DATE).pat $(SWISSDIR)/$(SWISS)-$(DATE).tex
endif

# This macro defines a backslash followed by a newline.  We use it to
# beautify canned recipes, avoiding overlong lines in the make output.
define bsnl
\$(strip)

endef


override SRCDIR := $(shell cd $(SRCDIR) && $(PWD))


#
# main targets
#

all: pattern-trad pattern-refo pattern-swiss

.PHONY: pattern-trad pattern-refo pattern-swiss
pattern-trad: $(TRADFILES)
pattern-refo: $(REFOFILES)
pattern-swiss: $(SWISSFILES)


# auxiliary targets

.PHONY: words-trad words-refo words-swiss
words-trad: $(TRADDIR)/words.hyphenated.trad
words-refo: $(REFODIR)/words.hyphenated.refo
words-swiss: $(SWISSDIR)/words.hyphenated.swiss


.PHONY: pre-trad pre-refo pre-swiss pre-trad-and-refo
pre-trad:
	$(MKDIR) $(TRADDIR)
pre-refo:
	$(MKDIR) $(REFODIR)
pre-swiss:
	$(MKDIR) $(SWISSDIR)
pre-trad-and-refo:
	$(MKDIR) $(TRAD_AND_REFO_DIR)

$(TRADFILES) $(TRADDIR)/words.hyphenated.trad: | pre-trad
$(REFOFILES) $(REFODIR)/words.hyphenated.refo: | pre-refo
$(SWISSFILES) $(SWISSDIR)/words.hyphenated.swiss: | pre-swiss
$(TRAD_AND_REFO_FILES) $(TRAD_AND_REFO_DIR)/words.hyphenated.de: | pre-trad-and-refo

# GNU make supports creation of multiple targets by a single invocation of a
# recipe only for pattern rules, thus we have to use a 'sentinel file' (using
# 'echo' for the time stamp).

$(TRADDIR)/pattern.8 \
$(TRADDIR)/pattern.rules: \
  $(TRADDIR)/make-full-pattern-trad
$(REFODIR)/pattern.8 \
$(REFODIR)/pattern.rules: \
  $(REFODIR)/make-full-pattern-refo
$(SWISSDIR)/pattern.8 \
$(SWISSDIR)/pattern.rules: \
  $(SWISSDIR)/make-full-pattern-swiss
$(TRAD_AND_REFO_DIR)/pattern.8 \
$(TRAD_AND_REFO_DIR)/pattern.rules: \
  $(TRAD_AND_REFO_DIR)/make-full-pattern-de


$(DATADIR)/german-gesang.tr: $(DATADIR)/german.tr
	$(SED) -e '1c\
 1 1' < $< > $@


# $(1): pattern directory
define make-full-pattern
$(CHDIR) $(strip $(1)) $(bsnl)\
&& $(SH) $(SCRIPTDIR)/trennmuster/make-full-pattern.sh $(bsnl)\
  $(PATGEN_PARAMS) $(<F) $(GERMAN_TR)
$(ECHO) done > $@
endef

# Both `make-full-pattern.sh` and `german.tr` control hyphenation parameters;
# it is thus a good idea to make them prerequisites.
$(TRADDIR)/make-full-pattern-trad \
$(REFODIR)/make-full-pattern-refo \
$(SWISSDIR)/make-full-pattern-swiss \
$(TRAD_AND_REFO_DIR)/make-full-pattern-de: \
  $(SCRIPTDIR)/trennmuster/make-full-pattern.sh \
  $(GERMAN_TR)

$(TRADDIR)/make-full-pattern-trad: $(TRADDIR)/words.hyphenated.trad
	$(call make-full-pattern, $(TRADDIR))
$(REFODIR)/make-full-pattern-refo: $(REFODIR)/words.hyphenated.refo
	$(call make-full-pattern, $(REFODIR))
$(SWISSDIR)/make-full-pattern-swiss: $(SWISSDIR)/words.hyphenated.swiss
	$(call make-full-pattern, $(SWISSDIR))
$(TRAD_AND_REFO_DIR)/make-full-pattern-de: $(TRAD_AND_REFO_DIR)/words.hyphenated.de
	$(call make-full-pattern, $(TRAD_AND_REFO_DIR))

# $(1): pattern file name
# $(2): pattern directory
# $(3): English pattern title
# $(4): German pattern title
# $(5): language name
# $(6): language tag
define make-pat-file
$(CAT) $(DATADIR)/kopf $(bsnl)\
| $(SED) -e "s/@ENGLISH_TITLE@/$(strip $(3))/" $(bsnl)\
         -e "s/@GERMAN_TITLE@/$(strip $(4))/" $(bsnl)\
         -e "s/@DATE@/$(DATE)/" $(bsnl)\
         -e "s/@GIT_VERSION@/$(GIT_VERSION)/" $(bsnl)\
         -e "s/@LANG_NAME@/$(strip $(5))/" $(bsnl)\
         -e "s/@LANG_TAG@/$(strip $(6))/" $(bsnl)\
         -e "s/@LEFTHYPHENMIN@/$(LEFTHYPHENMIN)/" $(bsnl)\
         -e "s/@RIGHTHYPHENMIN@/$(RIGHTHYPHENMIN)/" $(bsnl)\
         -e "s/@FILE_NAME@/$(strip $(1))/" > $@ $(bsnl)\
&& $(CAT) $(strip $(2))/pattern.rules >> $@ $(bsnl)\
&& $(CAT) $(DATADIR)/patterns-anfang >> $@ $(bsnl)\
&& $(CAT) $(strip $(2))/pattern.8 >> $@ $(bsnl)\
&& $(CAT) $(DATADIR)/patterns-ende >> $@
endef

$(TRADDIR)/$(TRAD)-$(DATE).pat: \
  $(TRADDIR)/pattern.8 \
  $(TRADDIR)/pattern.rules
	$(call make-pat-file, \
                 $(TRAD), $(TRADDIR), \
                 $(TRAD_TITLE_EN), $(TRAD_TITLE_DE), \
                 $(TRAD_LANG_NAME), $(TRAD_LANG_TAG))
$(REFODIR)/$(REFO)-$(DATE).pat: \
  $(REFODIR)/pattern.8 \
  $(REFODIR)/pattern.rules
	$(call make-pat-file, \
	         $(REFO), $(REFODIR), \
	         $(REFO_TITLE_EN), $(REFO_TITLE_DE), \
	         $(REFO_LANG_NAME), $(REFO_LANG_TAG))
$(SWISSDIR)/$(SWISS)-$(DATE).pat: \
  $(SWISSDIR)/pattern.8 \
  $(SWISSDIR)/pattern.rules
	$(call make-pat-file, \
	         $(SWISS), $(SWISSDIR), \
	         $(SWISS_TITLE_EN), $(SWISS_TITLE_DE), \
	         $(SWISS_LANG_NAME), $(SWISS_LANG_TAG))
$(TRAD_AND_REFO_DIR)/$(TRAD_AND_REFO)-$(DATE).pat: \
  $(TRAD_AND_REFO_DIR)/pattern.8 \
  $(TRAD_AND_REFO_DIR)/pattern.rules
	$(call make-pat-file, \
	         $(TRAD_AND_REFO), $(TRAD_AND_REFO_DIR), \
	         $(TRAD_AND_REFO_TITLE_EN), $(TRAD_AND_REFO_TITLE_DE), \
	         $(TRAD_AND_REFO_LANG_NAME), $(TRAD_AND_REFO_LANG_TAG))

# $(1): arguments for `extract-tex.pl`
define extract-tex
$(CAT) $< $(bsnl)\
| $(PERL) $(EXTRACT_TEX) \
            $(strip $(1)) $(PERL_PATTYPE) $(bsnl)\
| $(SED_PATTYPE) $(bsnl)\
| $(SORT) $(bsnl)\
> $@
endef

# $(1): arguments for `-l` parameter of `sprachauszug.py`
define ligaturaufbruch-eingabe
$(SPRACHAUSZUG) -l $(strip $(1)) -s "morphemgrenzen,einfach" $(bsnl)\
  $< > $@
endef

# words with hyphens following final "s" inside a word
# (Aus-sage, …, Zynis-mus)
define schluss-s-eingabe
$(SCRIPTDIR)/spezialmuster/lang_s/final_s_quasihyph.py < $< > $@
endef

ifeq ("$(PATTYPE)", "-ligaturaufbruch")
  $(TRADDIR)/words.hyphenated.trad: $(WORDLIST) $(SPRACHAUSZUG)
	  $(call ligaturaufbruch-eingabe, \
                 "de-1901:de-CH-1901")
  $(REFODIR)/words.hyphenated.refo: $(WORDLIST) $(SPRACHAUSZUG)
	  $(call ligaturaufbruch-eingabe, \
                 "de-1996:de-CH-1996")
  $(TRAD_AND_REFO_DIR)/words.hyphenated.de: $(WORDLIST) $(SPRACHAUSZUG)
	  $(call ligaturaufbruch-eingabe, \
                 "de-1901:de-CH-1901:de-1996:de-CH-1996")
else ifeq ("$(PATTYPE)", "-schluss-s")
  $(TRADDIR)/words.hyphenated.trad: exzerpte/de-Latf-1901
	  $(call schluss-s-eingabe)
  $(REFODIR)/words.hyphenated.refo: exzerpte/de-Latf-1996
	  $(call schluss-s-eingabe)
  $(TRAD_AND_REFO_DIR)/words.hyphenated.de: exzerpte/de-Latf
	  $(call schluss-s-eingabe)
else
  $(TRADDIR)/words.hyphenated.trad: $(WORDLIST) $(EXTRACT_TEX)
	  $(call extract-tex, -t)
  $(REFODIR)/words.hyphenated.refo: $(WORDLIST) $(EXTRACT_TEX)
	  $(call extract-tex)
  $(SWISSDIR)/words.hyphenated.swiss: $(WORDLIST) $(EXTRACT_TEX)
	  $(call extract-tex, -s)
endif

# $(1): pattern file name
# $(2): language tag (will be converted to lower case, important for
#       "de-CH-1901")
define make-tex-file
$(CAT) $(DATADIR)/mantel.1 $(bsnl)\
  | $(SED) -e "s/@DATE@/$(DATE)/" $(bsnl)\
           -e "s/@FILE_NAME@/$(strip $(1))/" > $@ $(bsnl)\
&& LANG_TAG="$(shell $(ECHO) $(strip $(2)) \
                     | $(TR) '[:upper:]' '[:lower:]')" \
&& $(CAT) $(DATADIR)/mantel.2-$(TEX_WRAPPER_SUFFIX) $(bsnl)\
     | $(SED) "s/@LANG_TAG@/$$LANG_TAG/" >> $@ $(bsnl)\
&& $(CAT) $(DATADIR)/mantel.3 >> $@
endef

$(TRADDIR)/$(TRAD)-$(DATE).tex: \
  $(DATADIR)/mantel.1 \
  $(DATADIR)/mantel.2-$(TEX_WRAPPER_SUFFIX) \
  $(DATADIR)/mantel.3
	$(call make-tex-file, $(TRAD), $(TRAD_LANG_TAG))
$(REFODIR)/$(REFO)-$(DATE).tex: \
  $(DATADIR)/mantel.1 \
  $(DATADIR)/mantel.2-$(TEX_WRAPPER_SUFFIX) \
  $(DATADIR)/mantel.3
	$(call make-tex-file, $(REFO), $(REFO_LANG_TAG))
$(SWISSDIR)/$(SWISS)-$(DATE).tex: \
  $(DATADIR)/mantel.1 \
  $(DATADIR)/mantel.2-$(TEX_WRAPPER_SUFFIX) \
  $(DATADIR)/mantel.3
	$(call make-tex-file, $(SWISS), $(SWISS_LANG_TAG))


#
# patterns for handling round 's' vs. long 'ſ'
#

# Word lists and patterns for converting words with only round 's' to
# words with distinction of round and long 's' according to the German
# orthograpy variant "de-Latf" (for texts typeset using *Fraktur*) .
#
# The patterns contain only round 's'. Hyphenation points after 's' indicate
# final 's' inside a word ('Aus-schuss' == 'Ausſchuſs').

.PHONY: schluss-s schluss-s-1901 schluss-s-1996
schluss-s: $(TRAD_AND_REFO_FILES)
schluss-s-1901: $(TRADFILES)
schluss-s-1996: $(REFOFILES)

# unhyphenated words with distinction of long-s and round-s
# (Auſsage, …, Zynismus)
exzerpte/de-Latf: $(WORDLIST) $(S2LONG_S)
	$(S2LONG_S) --drop-homonyms -l 'de-1901,de-1996' < $< > $@
exzerpte/de-Latf-1901: $(WORDLIST) $(S2LONG_S)
	$(S2LONG_S) --drop-homonyms -l de-1901 < $< > $@
exzerpte/de-Latf-1996: $(WORDLIST) $(S2LONG_S)
	$(S2LONG_S) --drop-homonyms -l de-1996 < $< > $@

#
# patterns for 'breaking up' typographic ligatures
#

# Word lists and patterns 'de_ligaturaufbruch': 'de-1901' (old orthography),
# 'de-1996' (reformed orthgraphy), and mixed.  The final patterns contain
# hyphenation points at positions where ligatures like 'fl' must not
# occur ('Dorfladen' => 'Dorf-laden').

.PHONY: de_ligaturaufbruch de-1901_ligaturaufbruch de-1996_ligaturaufbruch
de_ligaturaufbruch: $(TRAD_AND_REFO_FILES)
de-1901_ligaturaufbruch: $(TRADFILES)
de-1996_ligaturaufbruch: $(REFOFILES)


#
# experimental stuff
#


# Exzerpte mit `sprachauszug.py`

STILFILTER = $(SCRIPTDIR)/lib/py_wortliste/stilfilter.py

exzerpte/de-1996_morphemgrenzen: $(WORDLIST) $(STILFILTER)
	$(SPRACHAUSZUG) -l de-1996 \
          -s "morphemgrenzen,einfach" < $< > $@
exzerpte/de-1901_morphemgrenzen: $(WORDLIST) $(STILFILTER)
	$(SPRACHAUSZUG) -l de-1901 \
          -s "morphemgrenzen,einfach" < $< > $@
exzerpte/de-1996_hyphenmin3: $(WORDLIST) $(STILFILTER)
	$(SPRACHAUSZUG) -l "de-1996,de-1996-x-versal" \
          -s "standard,morphemisch,hyphenmin3,einfach" < $< > $@
exzerpte/de-1996_gesangstext-syllabisch: $(WORDLIST) $(STILFILTER)
	$(SPRACHAUSZUG) -l "de-1996,de-1996-x-versal" \
          -s "syllabisch,keine_schwankungsfaelle,einfach" < $< > $@
exzerpte/de-1996_gesangstext-morphemisch: $(WORDLIST) $(STILFILTER)
	$(SPRACHAUSZUG) -l "de-1996,de-1996-x-versal" \
          -s "morphemisch,keine_schwankungsfaelle,einfach" < $< > $@

# Wortliste mit Orthographie für Fraktursatz (s/ſ-Unterscheidung),
exzerpte/wortliste-Latf: $(WORDLIST) $(S2LONG_S)
	$(S2LONG_S) -w < $< > $@

# EOF
