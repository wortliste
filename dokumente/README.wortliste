Wortliste deutscher Wörter mit Trennungen
*****************************************

Beschreibung der in der Datei "wortliste" verwendeten Syntax.


Format
======

Jede Zeile enthält einen Eintrag mit durch Semikolon „;“ getrennten Feldern.

Das Zeichen „#“ leitet eine Kommentar ein.
(Zur Syntax in Kommentaren siehe letzter Abschnitt ↗Kommentare.)


Bedeutung der Felder
--------------------

Neben dem hier beschriebenem „Langformat“ gibt es das 2017 entwickelte
„Kurzformat“, das in „../skripte/lib/py_wortliste/wortliste.py“ beschrieben
ist.

  Feldnummer  Bedeutung
  ----------  -------------------------------------------------------------
        1     Wort ungetrennt.
        2     Wort mit Trennungen, falls für alle Varianten identisch,
              anderenfalls leer.
        3     Falls Feld 2 leer, Trennung nach traditioneller
              Rechtschreibung.
        4     Falls Feld 2 leer, Trennung nach reformierter Rechtschreibung
              (2006).
        5     Falls Feld 2 leer, Trennung für Wortform, die in der Schweiz
              oder mit Großbuchstaben oder Kapitälchen benutzt wird und für
              traditionelle und reformierte Rechtschreibung identisch ist.
        6     Falls Feld 5 leer, Trennung für Wortform, die in der Schweiz
              oder mit Großbuchstaben oder Kapitälchen benutzt wird,
              traditionelle Rechtschreibung.
        7     Falls Feld 5 leer, Trennung für Wortform, die in der Schweiz
              oder mit Großbuchstaben oder Kapitälchen benutzt wird,
              reformierte Rechtschreibung (2006).
        8     Falls Feld 5 leer und nicht identisch mit Feld 6, Trennung
              nach (deutsch)schweizerischer, traditioneller Rechtschreibung
              mit Trennung von „ss“, auch wenn es für „ß“ steht.

Leere Felder sind mit „-x-“ markiert („x“ ist die Feldnummer).  Das
bedeutet, die Schreibweise existiert in der entsprechenden Rechtschreibung
nicht.

Ausnahme:
  Zur Vereinfachung wird bei Wörtern mit „ß“ auf die „Auskreuzung“ von
  Feld 5 verzichtet.  Ein Eintrag mit weniger als 5 Feldern bedeutet also
  nicht automatisch, dass das entsprechende Wort in der Schweiz oder in
  Versalschreibung existiert.

Felder 3 und 4 als auch Felder 6 und 7 treten immer simultan auf, um die
Lesbarkeit zu erhöhen.

Feld 8 wird nur angegeben, wenn sich die Schreibweise nach
(deutsch)schweizerischer, traditioneller Rechtschreibung von der
traditionellen Versalschreibung nach Duden unterscheidet.

Zur Erstellung von Trennmustern für die traditionelle Rechtschreibung
benötigt man Felder 2, 3, 5 und 6; für die reformierte Rechtschreibung die
Felder 2, 4, 5 und 7, und für die traditionelle (deutsch)schweizerische die
Felder 2, 3, 5, 6 und 8.  Felder mit höherer Feldnummer haben Priorität.


Sortierung
==========

Die Sortierung erfolgt nach Dudensortierung.  Die genauen Regeln sind in der
Datei „skripte/sort.py“ beschrieben, welche auch zur maschinellen Sortierung
der Liste verwendet werden kann.


Kennzeichnung der Trennstellen
==============================

Zur Wahl der Markierungen in neuen Einträgen, siehe auch
Trennzeichen-HOWTO.txt.


Unvollständigkeit
-----------------

Es sind nicht alle regulär zulässigen Trennstellen markiert. Dies betrifft
einige Trennungen zwischen Vokalen mit uneinheitlicher Aussprache sowie eine
größere Zahl der mit der Rechtschreibreform 1996 eingeführten Wahltrennungen.

Motivation:

* Ein Ziel der Alternativ-Trennregeln ist es, dass der Schreibende
  auch ohne Bezug auf ein explizites Wörterverzeichnis konforme
  Trennungen setzen kann (gut für den Schreiber).

  Die Wortliste ist ein explizites Wörterverzeichnis mit dem Ziel, Schreib-
  oder Anzeigeprogrammen „gute“ Trennungen beizubringen (gut für den Leser).
  Ein Test, ob eine gegebene Trennung nach amtlichen Regeln zulässig ist,
  gehört nicht zu den Anwendungsfällen.

  Die unterschiedliche Zielsetzung erklärt auch Abweichungen von anderen
  Wörterverzeichnissen bei der Auswahl gekennzeichneter Trennstellen.

* Im Falle der Alternativtrennungen in Fremdwörtern nach §112
  (no-b-le, Zy-k-lus, Fe-b-ru-ar, Hy-d-rant, Ar-th-ri-tis, ...) sind
  nur die traditionellen Trennstellen markiert. Trennungen nach
  deutschen Regelsilben können automatisiert erstellt werden (vgl.
  „Trennstile.txt“). Die Aufnahme von nach §112 formal zulässigen,
  aber in traditioneller Rechtschreibung unzulässigen Trennungen
  (Te-chnik, Zen-tner, Inte-rnat) erscheint kontraproduktiv.

* Bei Trennung nach „Empfinden“ des Schreibenden bei „verblasster
  Herkunft“, §113 gibt es keine allgemeingültige Festlegung über die
  Trennmöglichkeiten.  Daher kann eine Vollständigkeit grundsätzlich nicht
  erreicht werden. In der Wortliste sind Alternativtrennungen nur
  gekennzeichnet, wenn sie für hilfreich (oder zumindest unschädlich) für
  das Textverständnis und günstig für die Silbentrennung in Gesangstexten
  angesehen werden.

* Auch bei Trennung nach Aussprache („Vokalbuchstaben, die zu verschiedenen
  Silben gehören“, §109) existiert eine Grauzone. In der Wortliste werden die
  traditionellen Trennstellen gekennzeichnet, wenn sie mit den aktuellen
  Regeln konform sind.


Kategorisierung
---------------

Trennstellen an der Grenze funktionstragender Einheiten (Morpheme) können
speziell gekennzeichnet sein:

  Marker  Bedeutung und Position
  ------  ----------------------------------------------------------------
      =   an der Fuge in Zusammensetzungen: Wort=fu-ge

      <   nach Präfix, Konfix oder Verbalpartikel: Vor<sil-be, auf<zäh-len

      >   vor Suffix: Gleich>heit, Freund>schaf-ten

      -   innerhalb eines Morphems: ge-hen

Kombinationen dieser Zeichen kennzeichnen die „Wichtung“ einer Trennstelle
oder eine Kombination von Eigenschaften (siehe unten):

    A<·scor-bin, Be<=gut=ach-tungs==frist

Motivation:

* Die Trennung an sinnbildenden Einheiten fördert den Lesefluss und ist
  daher zu bevorzugen.

* Die Markierung von Morphemgrenzen hilft bei der regelbasierten
  Entscheidung über den Aufbruch typographischer Ligaturen und die
  Schreibung des S-Lautes in Frakturschrift (Rund-S „s“ vs. Lang-S „ſ“).

* Die Markierung erlaubt die Zuordnung von Wahltrennungen nach §113 des
  amtlichen Regelwerks (vgl. „Trennstile.txt“):

    hi-n<auf, He-li-ko<p-ter, Os-t=al-gie, Mon-t=re-al, Pä-d<a-go-gik

Anmerkungen:

* Morphemgrenzen, an denen keine Trennung erfolgt, werden nicht
  gekennzeichnet.

* Schwierig ist die Markierung von assimilierten und „verblassten” Affixen,
  die im heutigen Sprachverständnis nicht als eigenständig wahrgenommen
  werden.

  Kriterien sind die „Güte“ der Trennstelle (geringe Störung des Leseflusses
  bei Zeilenumbruch an dieser Stelle), Abweichungen von den Regeln zum
  Trennen einfacher Wörter, die Lang-S-Schreibung im Fraktursatz und der
  Aufbruch von Ligaturen an dieser Stelle.

    En<er-gie     # Vokal am Silbenanfang
    Af-fekt       # ff-Ligatur trotz < lat. afficere < ad- + facere
    Pe-ri<o·de    # Trennung nach Präfix nur im Gesangstext erlaubt

  Wenn keine besonderen Gründe vorliegen, wird die einfache Auszeichnung
  bevorzugt:

    Ant-wort, Di-rek-tor, In-te-gral (trotz in<te-ger),
    Va-ri-a-ble

* Trennstellen nach §112 der amtlichen Regeln (Muta cum Liquida) sind keine
  Morphemgrenzen, können aber mit diesen zusammenfallen

    Ma-gnet, Zy-klus, Re<flek-tor, Di·a<gno-se

* Die Unterscheidung, ob ein Wortbestandteil ein Affix oder ein
  lexikalisches Morphem ist, ist oft nicht eindeutig:

    em-por<ar-bei-ten    oder    em-por=ar-bei-ten
    wel-len>för-mig      oder    wel-len=för-mig

  Grundidee ist, dass „=“ Teile trennt, die als eigenständige Teile
  wahrgenommen werden.

  Morpheme, die eine lexikalische Bedeutung besitzen, aber nicht frei
  auftreten können (Konfixe bzw. gebundene lexikalische Morpheme), werden
  aus pragmatischen Gründen (meist) wie Affixe ausgezeichnet:

    Alt=pleis-to<zän  statt  Alt==pleis-to=zän

  Zu einigen dieser Konfixe existiert ein freies Homonym mit eigener
  Bedeutung:

    Au-to<kor<re<la-ti.on  aber  Au-to=ab<ga-se
    Ra-dio<ak-ti-vi-tät    aber  Ra-dio=an<sa-ge-rin

Für Details siehe `Trennzeichen-HOWTO.txt`.


Bindungsstärke und Wichtung
---------------------------

Die „Bindungsstärke“ gibt die Zusammengehörigkeit von Wortteilen an.
Für die oben eingeführten Kategorien gilt:

  Marker  Funktion      Bindung
  ------  ------------  -------
     -    Sprechsilben  stark

     <    Präfixgrenze  .
     >    Suffixgrenze  .

     =    Wortfugen     schwach

Beispiele:

  Aus<wahl=lis-te    # (Aus- + wahl) + liste
  fern=ab<ge<le-gen  # fern + (ab- + (ge- + legen))
  Abend=lüft>chen    # Abend + (lüft + -chen)

Falls eine Trennstelle weder einen Teil eines Kompositums markiert noch
ein Präfix oder ein Suffix abgrenzt, aber trotzdem bevorzugt ist, wird
„--“ verwendet:

  Lan-go--bar-de

Präfixtrenner sind „rechtsbindend“:¹

  un<voll<stän-dig  # un + (voll + ständig)
  un<aus<ge<go-ren  # un + (aus + (ge + (go-ren)))

  ¹ Ausnahme: Ein eingeschobenes „zu“ oder „ge“ übernimmt die Markierung der
    Einschubstelle auf beiden Seiten:

      nach<zu<wei-sen  # (nach + weisen) + zu
      ab<ge<fah-ren    # (ab- + fahren) + ge-

Es gibt auch eine kleine Zahl von komplexen, vorangestellten Konfixen, wo
„<“ gedoppelt ist:

  Tri<go-no<<me-trie  # (Tri + gono) + metrie

Suffixtrenner sind „linksbindend“:

  Acht>lo-sig>keit  # (Acht + lo-sig) + keit

Bei mehrteiligen Komposita kann die Bindungshierarchie durch Doppelung des
Trennzeichens genauer beschrieben werden:

  Alp=horn==trio              # (Alp + horn) + trio
  Berg===fünf=fin-ger==kraut  # Berg + ((fünf + (fin-ger)) + kraut)

Trennstellen gleicher Kategorie ohne Doppelung bezeichnen entweder
gleichwertige Wortbestandteile, Zerlegungsalternativen, eingeschobene Teile
oder sind noch nicht gewichtet:

  Abend=brot=zeit   # Abendbrot-Zeit / Abend-Brotzeit

  stand=zu=hal-ten  # stand+zu+halten (eingeschobenes zu)
  Bundes=familien=ministerium # Bundesministerium für Familie,
                                Senioren, Frauen und Jugend

Die gemischten Trennzeichen „<=“ und „=>“ kennzeichnen die
Bindungsverhältnisse, wenn sich ein Affix auf mehrere Teile eines
Kompositums bezieht:

  un<=wahr=schein-lich      # un + (wahr + scheinlich)
  Be<=gut=ach-tungs==frist  # (Be + (gut + achtungs)) + frist

  an-dert=halb=>fach        # (andert + halb) + fach
  zwei==ein=halb==>fa-che   # (zwei + (ein + halb)) + fache
  Vor<her=sag=>bar=>keit    # (((Vor + her) + sag) + bar) + keit

Die Bindungsstärke nimmt dabei in der Regel mit der Länge des
Trennzeichens ab:

  Bindungsstärke  Zeichen
  --------------  -------------
  stark           -
  .               --
  .               <
  .               >
  .               <<
  .               =
  .               <=
  .               =>
  .               ==
  .               <==
  .               ==>
  schwach         ===

Im Allgemeinen ist die Bindungsstärke umgekehrt proportional zur „Güte“
(oder dem „Gewicht“) einer Trennstelle:²

  Bindungsstärke  Güte/Gewicht       Beispiel
  --------------  -----------------  -----------
  schwach         gut                Auswahl-
                                     liste

  mittel          mittel             Aus-
                                     wahlliste

  stark           ungünstig          Auswahllis-
                                     te

² In eine genauere „Gütebestimmung“ geht auch der Abstand zu anderen
  Trennstellen und die Position im Wort ein.

ungünstige Trennungen
---------------------

Ungünstige Trennstellen sind mit einem Punkt markiert. In der Regel folgt
der Punkt dem Trennzeichen:

  An<den-.ken, Re<im<.port  # ↗irreführende Trennungen
  Se-.en                    # ↗Nottrennung

es sei denn, die Trennung soll im Gesangstext unterdrückt werden:

  Thy-mi.an                 # ↗Schwankungsfall

Achtung:
  Die Markierung ungünstiger Trennstellen ist weder eindeutig noch
  vollständig.

Bei der Generation der Trennmustern für TeX werden diese Trennstellen in der
Regel aussortiert (bis auf ↗Schwankungsfälle wenn kein Flattervokal
vorliegt: ge-ni.al, Ra-ti.on).
In einigen Trennstilen (Gesangstext oder enge Spalten) werden auch
ungünstige Trennstellen berücksichtigt (↗Trennstile.txt).

Treten Flattervokale bei einer ungünstigen Trennung auf, sind beide
Trennstellen als ungünstig markiert:

  Staats=e·x<.a-.men

irreführende Trennungen
~~~~~~~~~~~~~~~~~~~~~~~
Bei irreführenden oder sinnentstellenden Trennungen folgt der Punkt auf das
Trennzeichen. Besonders ungünstige Trennstellen können mit mehreren Punkten
gekennzeichnet sein.

  Punktzahl  Qualität           Beispiele
  ---------  -----------------  ------------------------------------
      1      ungünstig          An<den-.ken, Ost=en-.de, Re<im<.port
      2      sehr ungünstig     Ge<winn=er<..war-tung
      3      äußerst ungünstig  An<=al-...pha=bet

Nottrennungen
~~~~~~~~~~~~~

Im Regelteil der Leipziger Dudenausgabe von 1971 wird die Trennung

* bei Ableitungen slawischer Namen auf -ow (Teltow-.er;Telto-.wer),
* in Vokalverbindungen im ↗Schwankungsfall (Mil-li.on, Mil-li.o-när)
* und bei entfallenem „e“ (Arme-.en, Se-.en, Demokrati-.en)

als zulässig, aber nicht empfohlen beschrieben.

Der „Einheitsduden“ (1991) und Wahrig (1980) nutzen den Begriff der
Nottrennung nicht. Im Wörterverzeichnis werden Nottrenstellen entweder als
reguläre Trennstellen markiert, oder (wenn eine Ein-Vokal-Silbe folgt und
bei entfallenem „e“) nicht angegeben.

Die „amtlichen Regeln“ (1996) geben keine Trennempfehlungen,
Wörterverzeichnisse in neuer Rechtschreibung listen die Nottrennstellen
als zulässige Trennmöglichkeiten auf. Die Empfehlungen des
Online-Duden schließen zum Teil frühere Nottrennungen ein.

Schwankungsfälle
~~~~~~~~~~~~~~~~
In einigen Wörtern ist unklar, ob benachbarte Vokale zur gleichen Silbe oder
zu verschiedenen Silben gehören:

  Ion/I-on, ge-nial/ge-ni-al, Ri-vie-ra/Ri-vi-e-ra, Mil-lio-nen/Mil-li-o-nen

Häufig weichen die im aktuellen Duden angegebenen Trennmöglichkeiten
von der Textverteilung in Liedern ab:

  Duden(2006): na-ti-o-nal
  Liedtext:    „Ju- gend al- ler Na- tio- nen…“

Diese Trennungen werden in der Wortliste mit einem einfachen Punkt (ohne
vorangestelltes Trennzeichen) gekennzeichnet

  Na-ti.on, Mil-li.on, To.i-let-te, ge-ni.al, ge-ni.a-le

und, im Gegensatz zu anderen ungünstigen Trennungen, bei der Ausgabe von
Gesangstext nicht berücksichtigt.


Gesangstrennungen
-----------------

Im Gesangstext unter Noten muss jede getrennt gesprochene Silbe abgeteilt
werden (↗Trennstile.txt, ↗Gesangstext.txt). Die im „normalen“ Text nicht
zulässigen Trennungen an Wortanfang oder -ende sowie zwischen einzeln
gesprochenen Buchstaben von Abkürzungen werden mit dem Mittenpunkt „·“
markiert:

  A·i-da        # getrennt gesprochen
  schau·e       # ""
  Back=o·fen    # auch neben einer Wortfuge wird nur im Gesangstext getrennt
  Zo·o<lo-gie   # ""
  Bo-rus-si.·a  # Schwankungsfall am Wortrand (meist dreisilbig gesungen)
  A<·s-pekt     # Wahltrennung nach §113: Präfix „a-“ oder Sprechsilbe
  A·pri-ko-se   # Trennung nach §112 (Muta cum Liquida)
  A·R·D         # Abk.: allg. Rundfunk Deutschlands
  I·SO 		# Abk.: International Standard Organization
  P·K·Ws        # Abk.: Personenkraftwagen (Mz.)

Hinweis:
  Fällt eine Randtrennung mit einer Wahltrennung nach §113 zusammen, wird
  sie in NR mit „-“ oder „.“ markiert.

    Abitur;-2-;Ab<i·tur;A·b<i-tur
    Santiago;-2-;Sant=i.·a-go;San-t=i.a-go

  (Nach den amtlichen Regeln sind „Ab-itur“ und „Abi-tur“ zulässig.)


spezielle Trennungen
--------------------

Folgende Notation wird verwendet, um spezielle Trennungen (für die
traditionelle Rechtschreibung) zu kennzeichnen:

  {„ungetrennt“/„getrennt“}

Dreikonsonantenregel:

  Ab<fa{ll/ll=l}a-ger

Trennung von „ck“:

  Dru{ck/k-k}er

Um z.B. eine Wortliste für TeX zu konstruieren (traditionelle
Rechtschreibung), das solche speziellen Trennungen nicht benutzen kann, ist
folgender regulärer Ausdruck in „perl“ anzuwenden, um sie zu entfernen:

  s|\{ (.*?) / .*? \}|$1|gx

doppeldeutige Trennungen
------------------------

Abweichende Trennstellen in mehrdeutigen Wörtern werden folgendermaßen
deklariert:

  [„Variante A“/„Variante B“]

Beispiel:

  er[<b/b=]recht         # Erb=recht    / er<brecht
  Kin[-der=/d=er<.]satz  # Kin-der=satz / Kind=er<.satz

Für TeX-Trennmuster ist es vielleicht am besten, solche Einträge zu
entfernen und die häufigeren Fälle in einem \hyphenation-Block zu behandeln.
Hier der entsprechende Code für „perl“:

  sub entferne_marker {
    my $arg = shift;
    $arg =~ s/[.·<>=-]//g;
    return $arg;
  }

  s|\[ (.*?) / .*? \]|entferne_marker($1)|egx;

Sowohl in „[…/…]“ als auch in „{…/…}“ können alle Arten von Trennstellen
auftreten.

Kommentare
==========

Optionale Kommentare geben Zusatzinformationen zu Bedeutung, Herkunft oder
Anwendungsbereich. Sie sind für die Editoren der Wortliste und für Skripte
zur automatischen Bearbeitung bestimmt.

Kommentare werden im Allg. nur für die Stammform eines Wortes gesetzt.


Zeichen mit besonderer Bedeutung
--------------------------------

  <   Herkunft
  /   Alternativen
  ,   Trennzeichen in Kommentaren (bitte kein „;” verwenden)
  =   Synonym/Bedeutung
  []  Aussprache

Schlüsselwörter
---------------

Schlüsselwörter dienen (neben der allg. Information für Bearbeiter) der
Markierung von Wörtern, die von den allgemeinen Regeln der Rechtschreibung
und Silbentrennung abweichen können.

  Name      Eigenname / Markenname
  Abk.      Abkürzung
  Kurzwort  Neubildung durch Zusammensetzung von Teilen einer Bezeichnung

Herkunftssprachen
-----------------

Für die Angabe der Herkunftssprache werden die dudenüblichen Abkürzungen
verwendet. Bei Lehnwörtern wird das Herkunftszeichen „<“ vorangestellt.

  Account;Ac-count # engl.
  Bergasse;Ber-gas-se # franz. Name
  abhorreszieren;ab<hor-res-zie-ren # < lat.
  Bosporus;Bos=po-rus # < griech. Rinder=furt

Abkürzungen
-----------

  Abk.    Abkürzung
  bio.    Biologie
  bot.    Botanik (Pflanzenname)
  chem.   Chemisch
  obs.    obsolet
  techn.  Technisch
  ugs.    umgangssprachlich
  vulg.   vulgär
  zool.   Zoologie (Tiernamen)
